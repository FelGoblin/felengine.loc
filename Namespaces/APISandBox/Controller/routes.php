<?php
Route::both('/apisandbox/', [
    'controller' =>'ApiSandBoxController', //триггер класс
    'action' => 'main', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутреа)
]);