<?php

namespace Namespaces\ApiSandBox\Controller;

use Engine\Controller;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SiteComponentException;
use Engine\IController;
use Engine\Modules\BaseSiteModule;
use Modules\ApiSandBox\ApiSandBox;
use ReflectionException;

/**
 * Router триггер, который определяется в фалйе Controller/routes.php
 * Class ApiSandBoxController
 * @package Namespaces\ApiSandBox\Controller
 */
final class ApiSandBoxController extends Controller
{
    /**
     * @param BaseSiteModule $mod
     * @param $data
     * @return Controller|null
     * @throws ReflectionException
     * @throws ConfigException
     * @throws SiteComponentException
     */
    public function main(BaseSiteModule $mod = null, $data = null): ?IController
    {
        return parent::main($mod ?? new ApiSandBox(), $data);
    }
}