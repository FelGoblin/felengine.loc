<?php

namespace Namespaces\Service;

use Engine\Core\Objects\INamespace;
use Engine\Core\View\ComponentManager;

class NmspcConfig implements INamespace
{

    static function getMode(): int
    {
        return self::MODE_INCLUDE;
    }

    /**
     * Получить список исключённых системных модулей для текущего неймспейса
     * @return array
     */
    static function getExcludedSystemComponents(): array
    {
        return [

        ];
    }

    /**
     * Получить список подключённых неймспейсов, их layouts и из ссылок на шаблоны
     * ВНимание! Каждый последующий немспейс переопределяет предыдущий. Однако у текущего самый большой приоритет.
     * @return array
     */
    static function getLinkedNamespaces(): array
    {
        return [
            'Default' //подсасываемся к Default неймспейсу
        ];
    }

    /**
     * Список только включённых компонентов
     * @return array
     */
    static function getIncludedSystemComponents(): array
    {
        return [
            'debuginfo',
            'leftMenu',
            'notify-panel',
            'outdata'
        ];
    }
}