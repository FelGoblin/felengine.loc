<?php
/**
 * Служебные руты
 */
Route::both('/service', [
    'controller' => 'ServiceController', //триггер класс
    'action' => 'index', //метод класса
    'permission' => [
        'required' => [
            'authorized'
        ],
        'access' => [
            'moderator'
        ]
    ]
]);
Route::both('/service/config', [
    'controller' => 'ServiceController', //триггер класс
    'action' => 'config', //метод класса
    'permission' => [
        'required' => [
            'authorized'
        ],
        'access' => [
            'moderator'
        ]
    ]
]);

Route::both('/service/config/(target:any)', [
    'controller' => 'ServiceController', //триггер класс
    'action' => 'config', //метод класса
    'permission' => [
        'required' => [
            'authorized'
        ],
        'access' => [
            'moderator'
        ]
    ]
]);