<?php

namespace Namespaces\Service\Controller;


use Engine\Controller;
use Engine\IController;
use Modules\_Services\_MainServicePage;
use Modules\_Services\_ServiceConfigs;

/**
 * Перенаправляем всё в модуль _ServiceConfigs
 * Class ServiceController
 * @package Namespaces\Service\Controller
 */
final class ServiceController extends Controller
{
    /**
     * @param null $target
     * @return IController|null
     * @throws \Engine\Exceptions\ConfigException
     * @throws \Engine\Exceptions\SiteComponentException
     * @throws \ReflectionException
     */
    public function index($target = null): ?IController
    {
        return self::main(new _MainServicePage(['target' => $target]));
    }

    /**
     * @param null $target
     * @return IController|null
     * @throws \Engine\Exceptions\ConfigException
     * @throws \Engine\Exceptions\SiteComponentException
     * @throws \ReflectionException
     */
    public function config($target = null): ?IController
    {
        $data = [
            'target' => $target,
            'paths' => [
                [
                    'title' => lang('@services'),
                    'path' => 'Service'
                ],
                [
                    'title' => lang('@services-config'),
                    'path' => 'Config',
                    'active' => $target == null
                ],
                [
                    'path' => $target,
                    'active' => $target != null
                ]
            ]
        ];
        return self::main(new _ServiceConfigs($data), $data);
    }
}