<?php


namespace Namespaces\Widget;


use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseSiteModule;
use Engine\Widget\BaseWidget;
use Engine\Widget\IWidgetData;
use Exception;
use ReflectionException;

class Widget extends BaseSiteModule
{
    /**
     * @var string
     */
    protected string $activeTemplateLabel = 'error';

    /**
     * @var IWidgetData
     */
    protected $widgetData;

    /**
     * @var SiteComponent|string
     */
    private $widgetComponent = '';
    /**
     * @var string
     */
    private $out = '';

    /**
     * @var array
     */
    private $data = [];
    /**
     * @var BaseWidget
     */
    private $widget;

    /**
     * Проверяем лоступа пользователя к текущей команде.
     * @param array $accessList
     * @return bool
     */
    public static function checkAccess(array $accessList, string $widget): bool
    {
        $access = array_map('trim', $accessList);

        return (
            in_array('widget', $access, true) //смотрим общую группу
            OR in_array('widget.' . $widget, $access, true) //смотрим конкретный триггер
        );
    }

    /**
     * @return SiteComponent|string
     */
    public function getWidgetComponent()
    {
        return $this->widgetComponent;
    }

    /**
     * Виджет - обычный модуль сайта. Отключим ему админ панель
     * @param bool $autoBuild
     * @return IComponent|null
     */
    public function makeAdminToolbar(bool $autoBuild = true): ?IComponent
    {
        return null;
    }

    /**
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function build(): void
    {
        $this->adminToolbarLabel = null;
        $this->widget->build($this->activeTemplateLabel);
        parent::build();
    }

    /**
     * @param null $options
     */
    protected function controller($options = null): void
    {
        //dump($options);
        try {
            $mod = ucfirst($options['widget']);
            $widgetClass = 'Modules\\' . $options['widget'] . '\\Widget';
            if (!class_exists($widgetClass)) {
                //ищем в папке с виджетами
                //$widgetClass = 'Widgets\\' . $options['widget'] . '\\' . $options['widget'];
                $widgetClass = sprintf('Widgets\\%s\\%s', $options['widget'], $options['widget']);
                if (!class_exists($widgetClass)) {
                    throw new Exception(sprintf('Widget <b>%s</b> not found', $mod));
                }
            }

            /** @var BaseWidget $widgetClass */
            $this->widget = new $widgetClass($options);
            $this->activeTemplateLabel = $this->widget->getTemplateLabel();
            parent::controller($options);

        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }
}
