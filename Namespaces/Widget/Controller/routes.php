<?php
Route::get('/widget', [ //get - обрабатывать только GET запросы, post - только POST запросы, both - оба
    'controller' =>'WidgetController', //триггер класс
    'action' => 'widget' //метод класса
]);

Route::get('/widget/(widget:any)', [ //get - обрабатывать только GET запросы, post - только POST запросы, both - оба
    'controller' =>'WidgetController', //триггер класс
    'action' => 'widget' //метод класса
]);

Route::get('/widget/(widget:any)/(token:any)', [ //get - обрабатывать только GET запросы, post - только POST запросы, both - оба
    'controller' =>'WidgetController', //триггер класс
    'action' => 'widget' //метод класса
]);