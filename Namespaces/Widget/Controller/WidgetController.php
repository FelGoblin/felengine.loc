<?php

namespace Namespaces\Widget\Controller;


use Engine\Controller;
use Engine\IController;
use Namespaces\Widget\Widget;
use Exception;

class WidgetController extends Controller
{
    /**
     * @param int $token
     * @param string $widget
     * @return Controller|null
     * @throws Exception
     */
    public function widget(string $widget = null, string $token = null): ?IController
    {
        return parent::main(new Widget(['widget' => $widget, 'token' => $token]));
    }
}