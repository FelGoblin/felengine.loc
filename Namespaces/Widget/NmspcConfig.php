<?php

namespace Namespaces\Widget;

use Engine\Core\Objects\INamespace;
use Engine\Core\View\ComponentManager;

class NmspcConfig implements INamespace
{

    /**
     * Получить список исключённых системных модулей
     * @return array
     */
    static function getExcludedSystemComponents(): array
    {
        return [ComponentManager::EXCLUDE_ALL]; //отключаем ВСЕ системные компоненты
    }

    /**
     * Текущий режим
     * @return int
     */
    static function getMode(): int
    {
        return self::MODE_INCLUDE;
    }

    /**
     * Список только включённых компонентов
     * @return array
     */
    static function getIncludedSystemComponents(): array
    {
        return ['main'];
    }

    /**
     * Получить список подключённых неймспейсов, их layouts и из ссылок на шаблоны
     * @return array
     */
    static function getLinkedNamespaces(): array
    {
        return [];
    }
}