<?php


namespace Modules\SiteWidgets;


interface IActiveWidget
{
    /**
     * Получить путь текущего виджета
     * @return string
     */
    public function getWidgetPath(): string;

    /**
     * Тип
     * @return string
     */
    public function getWidgetType(): string;

    /**
     * Получить имя активного виджета
     * @param bool $toLower
     * @return string
     */
    public function getActiveWidgetName(bool $toLower = false): string;
}