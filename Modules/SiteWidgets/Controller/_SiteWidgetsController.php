<?php


namespace Modules\SiteWidgets\Controller;


use Engine\Controller;
use Engine\IController;
use Engine\Modules\BaseSiteModule;
use Modules\SiteWidgets\SiteWidgets;

class _SiteWidgetsController extends Controller
{
    public function main(BaseSiteModule $mod = null, $data = null): ?IController
    {
        return parent::main($mod ?? new SiteWidgets(), $data);
    }
}