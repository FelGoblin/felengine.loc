<?php
Route::both('/widgets', [
    'controller' => '_SiteWidgetsController',
    'action' => 'main',
    'permission' => [
        'authorized',
        'streamer'
    ]
]);