<?php


namespace Modules\SiteWidgets;


use Engine\Core\View\ComponentManager;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseSiteCardModule;
use ReflectionException;

class SiteWidgets extends BaseSiteCardModule
{
    private $widgets = [];

    /**
     * @inheritDoc
     */
    protected function controller($options = null): void
    {
        parent::controller([
            'uri' => 'Widgets'
        ]);

        $this->doInit();
    }

    /**
     *
     */
    private function doInit(): void
    {
        $activeWidgetsPath = pathSeparator(__DIR__ . '/Widget/');

        if (is_dir($activeWidgetsPath)) {
            $find = array_diff(scandir($activeWidgetsPath, SCANDIR_SORT_DESCENDING), Array(".", ".."));
            foreach ($find as $d) {
                $widgetFile = $activeWidgetsPath . $d;
                if (file_exists($widgetFile)) {
                    $activeWidgetNmspc = str_replace([ROOT_DIR, DIRECTORY_SEPARATOR, '.php'], ['', '\\', ''], $widgetFile);
                    if (class_exists($activeWidgetNmspc) AND $activeWidgetNmspc . '::class' instanceof IActiveWidget) {
                        /** @var IActiveWidget $activeWidgetNmspc */
                        $this->widgets[] = new $activeWidgetNmspc();
                    }
                }
            }
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    protected function getDisplayData(array $data = []): array
    {
        $result = [];
        /** @var IActiveWidget $widget */
        foreach ($this->widgets as $widget) {
            $c = ComponentManager::createSiteComponent($this->getModuleName(true, true) . '.' . $widget->getActiveWidgetName(true),
                [],
                InjectComponentConfig::make($this->getActiveLabel(), 'widget', IComponent::INJECT_AS_ARRAYDATA)
            );

            ComponentManager::createSiteComponent($this->getModuleName(true, true) . '.js',
                [
                    'widgetService' => $widget->getWidgetPath(),
                    'widgetType' => $widget->getWidgetType(),
                    'name' => $widget->getActiveWidgetName(),
                    'label' => lang('@widget_' . strtolower($widget->getActiveWidgetName())),
                    'streamer' => $this->user->getName(),
                    'token' => $this->user->getUserData()->getApiToken(),
                    'onConnect' => 'widgetOn' . $widget->getActiveWidgetName() . 'Connect',
                    'onConnectClose' => 'widgetOn' . $widget->getActiveWidgetName() . 'ConnectClose',
                    'onData' => 'widgetOn' . $widget->getActiveWidgetName() . 'Data',
                    'onShow' => 'widgetOn' . $widget->getActiveWidgetName() . 'Show',
                    'onEnd' => 'widgetOn' . $widget->getActiveWidgetName() . 'End'
                ],
                InjectComponentConfig::make($c->getId(), 'jsBuilder')
            );
            $result['active'][] = [
                'label' => lang('@widget_' . $widget->getActiveWidgetName(true)),
            ];
        }
        return $result;
    }
}