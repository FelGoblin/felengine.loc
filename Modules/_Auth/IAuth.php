<?php

namespace Modules\_Auth;


interface IAuth
{
    public function GetLink();

    public function GetAccessToken($code = null);

    public function DoAuth($accessToken);
}