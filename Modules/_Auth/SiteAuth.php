<?php

namespace Modules\_Auth;

use Engine\Core\Core;
use Engine\Core\Http\Input;
use Engine\Core\Objects\IUser;
use Engine\Core\Session\Session;
use Engine\Core\View\Render;
use Engine\Object\User\_User;
use Exception;
use Object\User\User;

/**
 * Class Auth Авторизация через внешние сервисы пройдена. Авторизуемся на сайте
 * @package Modules\_Auth
 */
class SiteAuth extends BaseAuth
{
    /**
     * @throws Exception
     */
    public static function authController(): string
    {
        $auth_user = Session::get('auth_user');
        $auth_method = Session::get('auth_method');
        /** @var IUser $userClass */
        $userClass = Core::getUser();
        //dump($auth_user, $auth_method);
        //_Auth::destroy();
        //exit();

        //по каким-то причинам у нас исчезла авторизация из сервисов.
        if (empty($auth_user) or empty($auth_method)) {
            Render::setError('Ошибка обработки данных. Необходимо начать процедуру заново', true);
            Auth::destroy();
            //self::Redirect();
            return self::_reload_page_;
        }

        $user = $userClass::getUser($auth_user);

        if ($user == null) {
            $userData = User::createNewUser(
                [
                    'userName' => $auth_user,
                    'joinMethod' => (string)Session::get('auth_method'),
                    'blocked' => false
                ]
            );
            $user = new $userClass($userData);
        }

        //dump($user);

        if ($user == null) {
            Render::setError('Произошла ошибка в обработке пользователя. Повторите еще раз. Если ошибка не проходит, обратитесь к администратору.', true);
            //self::Redirect();
            return self::_reload_page_;
        }

        if ($user->isBlocked()) //смотрим блокировку пользователя.
        {
            Render::setError('Учётная запись заблокирована', true);
            //self::Redirect();
            return self::_reload_page_;
        }

        $result = -1;
        if (self::checkSitePassword($user, $result)) {

            self::doAuth($user);

            Session::delete([
                'auth_user',
                'auth_method'
            ]);
            //self::Redirect();
            //return self::_reload_page_;
        }
        return $result;
    }

    /**
     * @param _User $user
     * @param int $result
     * @return bool
     * @throws Exception
     */
    private static function checkSitePassword(_User $user, int &$result): bool
    {
        /*
         * теперь смотрим пароль.
         *      1. У пользователя есть пароль, значит нужно вывести форму для пароля.
         *      2. Формы была выведена и пользователь вводил пароль. Если пароль не совпал, выводим снова форму
         *
         *      3. У пользователя нет пароля. Вывести форму создания пароля
         *      4. Пользователь создал пароль и если нет ошибок, то впустить на сайт или снова вывести форму пароля.
         */
        //dump($userData,$userData->getPassword());
        if (!empty($user->getUserData()->getPassword())) {
            if (is_null(Input::getPost('doPassword'))) { //нет пароля, дадим форму и остановим скрипт
                $result = self::_show_password_dialog_;
                return false;
            } else {
                $pass = (string)Input::getPost('password');
                if (self::MakePasswordHash($pass, $user->getUserData()->getSalt()) != $user->getUserData()->getPassword()) {
                    Render::setError('Ой, кажется это не та комбинация.');
                    $result = self::_wrong_password_dialog_;
                    return false;
                }
            }
        } else {
            //dump(Input::get(), Input::getPost('setPassword', 's'));
            if (is_null(Input::getPost('setPassword', 's'))) { //
                //dump('create');
                $result = self::_create_password_dialog_;
                return false;
            } else { //введён пароль.
                //dump('check create');
                if (self::setPassword($user) == false) {
                    $result = self::_wrong_create_password_dialog_;
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param IUser $user
     * @return bool
     * @throws Exception
     */
    private static function setPassword(IUser $user): bool
    {

        $pass = (string)Input::getPost('password');
        $repass = (string)Input::getPost('repassword');

        if (empty($pass) or empty($repass)) {
            Render::setError('Пароль не может быть пустым.', true);
            return false;
        }

        if ($pass != $repass) {
            Render::setError('Пароли не совпадают.', true);
            return false;
        }

        self::saveSitePasswordHash($user, $pass);

        return true;
    }

    /**
     * Проводим авторизацию пользователя, пвошедшего из внешнего сервиса на сайте
     *
     * @param IUser $user
     * @param bool $inSession
     * @return void
     */
    public static function doAuth(IUser $user, bool $inSession = true)
    {
        //создаём новый хеш авторизации
        $hash = self::GenerationHash();
        //записываем его
        $user->getUserData()->setHash($hash);
        if ($inSession) {
            //сохраняем метод входа
            $user->getUserData()->setJoinMethod(Session::get('auth_method'));
        }
        //сохраняем в бд
        $user->getUserData()->saveData();
        //сохраняем авторизацию в сессии и куках
        if ($inSession) {
            self::saveAuth($user->getUserData()->getName(), $hash);
        }
        //dump($user);
    }
}
