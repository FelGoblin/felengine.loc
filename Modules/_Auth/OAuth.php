<?php

namespace Modules\_Auth;

use ErrorException;
use Exception;
use Modules\_Auth\OAuth\GoodGame;
use Modules\_Auth\OAuth\OAuth2;
use Modules\_Auth\OAuth\Twitch;
use Modules\_Auth\OAuth\YouTube;
use Modules\_Auth\OpenId\BaseOpenID;
use Modules\_Auth\OpenId\Steam;
use Engine\Core\Http\Input;
use Engine\Core\View\Render;
use Engine\Core\Session\Session;

/**
 * Class OAuth Идентификация через внешние сервисы.
 *
 * @package Modules\_Auth
 */
class OAuth extends BaseAuth
{
    /**
     * @param string $method
     * @return int
     * @throws ErrorException
     * @throws Exception
     */
    public static function authorize(?string $method)
    {
        if ($method == null) {
            Render::setError('_Auth method error', true);
            return self::_reload_page_;
            //throw new Exception('_Auth method error');
        }

        $oauth2 = self::getOAuth2($method);

        Session::put('auth_method', $method);

        if ($oauth2 instanceof BaseOpenID) {
            //dump($oauth2);
            if (!$oauth2->getOpenId()->mode) {
                self::Redirect($oauth2->GetLink());
                exit();
            } else if (!$oauth2->getOpenId()->mode == 'cancel') {
                Render::setError('Отменено пользоваетелем.');
            } else if (!$oauth2->getOpenId()->mode == 'error') {
                Render::setError($oauth2->getOpenId()->error);
            } else {
                //try get steamid
                $oauth2->GetAccessToken(null);
                //check steam
                if (Session::get('steamid') != null) {
                    $userName = $oauth2->DoAuth(Session::get('steamid'));
                    //dump($userName);
                    if ($userName != null) {
                        Session::put('auth_user', $userName);
                        SiteAuth::authController();
                        return self::_undefined_;
                    } else {
                        Render::setError('Не удалось получить пользователя из сервиса. Попробуйте еще раз или выберите другой метод авторизации. Если ошибка не исчезает, обратитесь к администрации.', true);
                    }
                }
            }
        } else {
            self::Redirect($oauth2->GetLink());
            exit();
        }
        //exit();
        return self::_reload_page_;
    }

    /**
     * @param string|null $method
     * @return OAuth2
     * @throws \ErrorException
     */
    private static function getOAuth2(string $method = null): IAuth
    {
        switch ($method) {
            case 'twitch':
                return new Twitch();
                break;

            case 'goodgame':
                return new GoodGame();
                break;

            case 'youtube':
                return new YouTube();
                break;

            case 'steam':
                return new Steam();
                break;

            default:
                Render::setError('Ошибка обработки метода входа.', true);
                self::Redirect();
        }
        return null;
    }

    /**
     * STEAM API
     * @param string $id
     * @param BaseOpenID $openID
     * @return string|null
     */
    public static function doIdentAuth(string $id, BaseOpenID $openID): ?string
    {
        $url = file_get_contents("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" . $openID->client_id . "&steamids=" . $_SESSION['steamid']);
        $content = json_decode($url, true);
    }

    /**
     * @return null|string - Имя пользователя
     * @throws \ErrorException
     */
    public static function doOAath2(): ?string
    {
        $oauth2 = self::getOAuth2(Session::get('auth_method'));

        //dump(Session::get('auth_method'), Input::get());

        $code = Input::getGet('code');

        $userName = null;

        if ($code != null) {//к нам пришёл код

            $access_token = $oauth2->GetAccessToken($code);

            //dump($access_token);

            $userName = $oauth2->DoAuth($access_token);

        } else {//предположительно этот участок кода никогде не заработает
            Render::setError('Ответ сервиса не соответствует данным для продолжения авторизации.', true);
        }

        return $userName;
    }

}