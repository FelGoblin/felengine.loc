<?php


namespace Modules\_Auth;

use Engine\Core\Http\Input;
use Engine\Core\View\Render;
use Engine\Core\Session\Session;
use Exception;

class Auth extends BaseAuth
{
    /**
     * @var string
     */
    public static $error = '';

    /**
     * @param string|null $method
     * @return string
     * @throws Exception
     */
    public static function controller(string $method = null): string
    {
        if (is_null($method)) {
            $method = Session::get('auth_method');
        } else {
            Session::put('auth_method', $method);
        }
        //Steam: https://stream-console.ru/oauth2?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=id_res&openid.op_endpoint=https%3A%2F%2Fsteamcommunity.com%2Fopenid%2Flogin&openid.claimed_id=https%3A%2F%2Fsteamcommunity.com%2Fopenid%2Fid%2F76561198073738330&openid.identity=https%3A%2F%2Fsteamcommunity.com%2Fopenid%2Fid%2F76561198073738330&openid.return_to=https%3A%2F%2Fstream-console.ru%2Foauth2&openid.response_nonce=2018-12-27T20%3A24%3A19ZRbknG%2BWBEjkMZzlhnYUk0qAJrf4%3D&openid.assoc_handle=1234567890&openid.signed=signed%2Cop_endpoint%2Cclaimed_id%2Cidentity%2Creturn_to%2Cresponse_nonce%2Cassoc_handle&openid.sig=RN5acS3D63CUJhBlFFfx%2BjkPiOI%3D

        //вычисляем этап авторизации
        if (Session::get('auth_user')) { //прошли авторизацию в внешних сервисах
            //dump(1);
            return SiteAuth::authController();
        } elseif (Input::getGet('error')) { //пришла ошибка
            //dump(2);
            Render::setError(Input::getGet('error_description'), true);
            return self::_reload_page_;
        } elseif (Input::get('code')) { //пришёл код из внешних сервисов
            //dump(3);

            $userName = OAuth::doOAath2();

            if (empty($userName)) {
                Render::setError('Не удалось получить пользователя из сервиса. Попробуйте еще раз или выберите другой метод авторизации. Если ошибка не исчезает, обратитесь к администрации.', true);
                return self::_reload_page_;
            } else {
                //dump('Put User Name');
                Session::put('auth_user', $userName);
                return SiteAuth::authController();
            }
        } else { //начинаем процесс авторизации через внешние сервисы
            //dump('begine?');
            //Дла пропуска авторизации в режиме разработки, использовать ссылку типа OAuth?dev=true&user=%userName%
            if (!is_null(Input::get('dev')) and self::$dev) {
                Session::put('auth_user', Input::get('user', '?s') ?? 'FelGoblin');
                Session::put('auth_method', Input::get('method', '?s') ?? 'goodgame');
                return SiteAuth::authController();
            } else {
                return OAuth::authorize($method);
            }
        }
    }
}