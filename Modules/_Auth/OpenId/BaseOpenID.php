<?php

namespace Modules\_Auth\OpenId;


use Modules\_Auth\IAuth;

abstract class BaseOpenID implements IAuth
{
    protected $openId;

    public $base_url;
    public $client_id;

    /**
     * @return mixed
     */
    public function getOpenId(): OpenID
    {
        return $this->openId;
    }
}