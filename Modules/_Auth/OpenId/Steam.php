<?php

namespace Modules\_Auth\OpenId;

use Engine\Core\Session\Session;

class Steam extends BaseOpenID
{
    /**
     * Steam constructor.
     * @throws \ErrorException
     */
    public function __construct()
    {
        $this->base_url = 'https://steamcommunity.com/openid/';
        $this->client_id = 'C505140FA3460008D54DA0C2D6DD716D';
        $this->openId = new OpenID($this->base_url, 'http://felgoblin.ru/oauth2');
    }

    /**
     * @return String
     * @throws \ErrorException
     */
    public function GetLink()
    {
        return $this->openId->authUrl();
    }

    /**
     * @param null $code
     * @throws \ErrorException
     */
    public function GetAccessToken($code = null)
    {
        if (!$this->getOpenId()->validate()) {
            $id = $this->getOpenId()->identity;
            $ptn = "/^https?:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
            preg_match($ptn, $id, $matches);
            Session::put('steamid', $matches[1]);
        }
    }

    public function DoAuth($accessToken)
    {
        $url = file_get_contents("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={$this->client_id}&steamids=$accessToken");
        $content = json_decode($url, true);
        //dump($content);
        return $content['response']['players'][0]['Pawnname'] ?? null;
    }
}