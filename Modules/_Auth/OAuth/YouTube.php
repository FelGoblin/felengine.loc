<?php

namespace Modules\_Auth\OAuth;

class YouTube extends OAuth2
{
    public function __construct()
    {
        self::$base_url = 'https://accounts.google.com/o/oauth2/';
        self::$client_id = '754046016764-emu8cjo3hk9ro6a6h54s1gpsvt7733hu.apps.googleusercontent.com';
        self::$client_secret = '7kyYKjAycKY_FX1aenEE6H7B';
        self::$scope_array = array('https://www.googleapis.com/auth/userinfo.email ', 'https://www.googleapis.com/auth/userinfo.profile');
    }

    public function GetLink()
    {
        self::$params = 'auth?';
        self::$responce = 'code';
        return $this->Authenticate();
    }

    public function GetAccessToken($code = null)
    {
        self::$params = 'token?';
        return $this->Get_access_token($code);
    }

    public function DoAuth($accessToken)
    {
        self::$params = 'info?';
        return $this->GetGoogleInfo($accessToken);
    }
}

?>