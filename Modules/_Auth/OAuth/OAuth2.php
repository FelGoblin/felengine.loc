<?php

namespace Modules\_Auth\OAuth;

use Modules\_Auth\IAuth;

abstract class OAuth2 implements IAuth
{
    protected static $base_url;
    protected static $client_id;
    protected static $redirect_url = 'http://felgoblin.ru/oauth2';
    protected static $scope_array = array();
    protected static $client_secret;
    protected static $responce;
    protected static $params = '';
    //twitch: https://api.twitch.tv/kraken/oauth2/authorize?client_id=g87jqcj06pld3otu74cac7zrr9frq4&redirect_uri=http://felgoblin.ru/auth.php&response_type=token&scope=openid&state=c3ab8aa609ea11e793ae92361f002671
    //https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=g87jqcj06pld3otu74cac7zrr9frq4&redirect_uri=https://auth.felgoblin.ru&scope=user_read+chat_login
    //GG: https://api2.goodgame.ru/oauth/authorize?response_type=token&client_id=FelGoblin.ru&redirect_uri=http://felgoblin.ru/auth.php&state=0123456789&scope=chat.token

    protected function Authenticate()
    {
        //twitch $params = 'oauth2/authorize?';
        //GG $params = 'oauth/authorize?';
        if (!empty(self::$scope_array)) {
            $scope = '&scope=' . implode('+', self::$scope_array);
        }
        $authenticate_url =
            self::$base_url . self::$params .
            'response_type=' . self::$responce .
            '&client_id=' . self::$client_id .
            '&redirect_uri=' . self::$redirect_url .
            $scope;
        return $authenticate_url;
    }

    protected function Get_access_token($code)
    {
        if ($code == null)
            return null;
        $ch = curl_init(self::$base_url . self::$params);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        $fields = array(
            'client_id' => self::$client_id,
            'client_secret' => self::$client_secret,
            'grant_type' => 'authorization_code',
            'redirect_uri' => self::$redirect_url,
            'code' => $code
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
        $data = curl_exec($ch);
        $response = json_decode($data, true);
        //dump($data, $response);
        if (isset($response["access_token"]))
            return $response["access_token"];
        return null;
    }

    protected function GetGoogleInfo($accessToken)
    {
        if ($accessToken == null)
            return null;
        $params['access_token'] = $accessToken;
        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['id'])) {
            return $userInfo['name'];
        }
        return null;
    }

    protected function GetGGInfo($access_token)
    {
        $ch = curl_init(self::$base_url . self::$params);
        //dump(self::$base_url . self::$params);
        curl_setopt($ch, CURLOPT_URL, self::$base_url . self::$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token
        ));
        $output = curl_exec($ch);
        $response = json_decode($output, true);

        //dump($output, $response);

        curl_close($ch);

        if (!isset($response['token']))
            return null;

        if (isset($response['token']['error'])) {
            return null;
        }
        if (!isset($response['user']))
            return null;
        $username = $response['user']['username'];
        return $username;
    }

    /**
     * Twitch
     * @param $access_token
     * @return |null
     */
    protected function Authenticated_user($access_token)
    {
        $ch = curl_init();
        $url = self::$base_url . self::$params;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: OAuth ' . $access_token
        ));
        $output = curl_exec($ch);
        $response = json_decode($output, true);
        curl_close($ch);
        return $response['login'] ?? null;
    }
}