<?php

namespace Modules\_Auth\OAuth;

class GoodGame extends OAuth2
{
    public function __construct()
    {
        self::$base_url = 'https://api2.goodgame.ru/';
        self::$client_id = 'Site FelGoblin.ru';
        self::$client_secret = 'ahu8jld-pmh78hdhjjds';
        self::$scope_array = array(/*'chat.token', */'channel.subscribers');
    }

    public function GetLink()
    {
        self::$params = 'oauth/authorize?';
        self::$responce = 'code';
        return $this->Authenticate() . '&state=felgoblin';
    }

    public function GetAccessToken($code = null)
    {
        self::$params = 'oauth';
        return $this->Get_access_token($code);
    }

    public function DoAuth($accessToken)
    {
        self::$params = 'info';
        return $this->GetGGInfo($accessToken);
    }
}

?>