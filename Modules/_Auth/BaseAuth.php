<?php

namespace Modules\_Auth;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Http\Redirect;
use Engine\Core\Objects\IUser;
use Engine\Core\Session\Session;
use Engine\Helper\Cookie;
use Engine\Helper\UriHelper;

class BaseAuth
{

    const  _undefined_ = -1;
    const  _reload_page_ = 0;
    const  _show_password_dialog_ = 1;
    const  _wrong_password_dialog_ = 2;
    const  _create_password_dialog_ = 3;
    const  _wrong_create_password_dialog_ = 4;
    const _join_form_ = 5;

    /**
     * Перенаправлять
     * @var bool
     */
    protected static $redirect = true;
    /**
     * Разработка! TODO: Обязательно отключить в дальнейшем
     * @var bool
     */
    protected static $dev = true;

    /**
     * Уничтожаем сессию
     */
    public static function destroy(): void
    {
        Session::delete(['userID', 'userHASH', 'auth_user', 'auth_Method', 'method']);
        Cookie::delete(['PHPSESSID', 'userID', 'userHASH', 'auth_user', 'auth_Method', 'method'], self::getUri());
    }

    /**
     * Получить домен текущего URI
     * @return mixed
     */
    public static function getUri()
    {
        $uri = parse_url('http://' . $_SERVER['HTTP_HOST']);
        $host = $uri['host'];
        $host_names = explode(".", $host);
        $bottom_host_name = $host_names[count($host_names) - 2] . "." . $host_names[count($host_names) - 1];

        return $bottom_host_name ?? SiteConfig::get('uri');
    }

    /**
     * перенаправляем
     * @param string|null $url
     * @throws \Exception
     */
    public static function Redirect(string $url = null)
    {
        if (self::$redirect) {
            if ($url == null) {
                Redirect::go(UriHelper::getProtocol() . '://' . self::getUri());
                //dump('reload page');
            } else
                Redirect::go($url);
            exit();
        }
    }


    /**
     * Сохраняем хеш пароля сайта от учётной записи
     * @param IUser $user
     * @param string $password
     * @param string|null $salt
     * @return string
     */
    protected static function saveSitePasswordHash(IUser &$user, string $password, string $salt = null): string
    {
        if ($salt == null) {
            $salt = self::GenerationHash();
        }

        $password = self::MakePasswordHash($password, $salt);

        $user->getUserData()->setPassword($password);
        $user->getUserData()->setSalt($salt);

        /*
        DataBase::query()
            ->update([
                'password' => $password,
                'salt' => $salt
            ])
            ->into('user')
            ->where('userName', $userName)
            ->run();
        */
        return $password;
    }

    /**
     * Генерируем хеш
     * @return string
     */
    public static function GenerationHash() //генерируем случайные хеши
    {
        return md5(uniqid(rand()));
    }

    /**
     * Создаём хеш пароля
     * @param string $pass
     * @param string $salt
     * @return string
     */
    public static function MakePasswordHash(string $pass, string $salt)
    {
        return md5($pass . $salt);
    }

    /**
     * Сохраняем авторизацию
     */
    protected static function saveAuth($id, $hash)
    {
        Session::put('userID', $id);
        Session::put('userHASH', $hash);
        /*
        Cookie::set('userID', $id); //сохраняем на 31 день
        Cookie::set('userHASH', $hash); //сохраняем на 31 день
        */
        setcookie("userID", $id, time() + 31 * 24 * 3600, '/', self::getUri());
        setcookie("userHASH", $hash, time() + 31 * 24 * 3600, '/', self::getUri());
        //dump_g();
    }
}
