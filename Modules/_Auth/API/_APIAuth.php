<?php


namespace Modules\_Auth\API;

use Engine\API\ModuleAPI;
use Engine\Core\Core;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Engine\Exceptions\ArgumentNullException;
use Exception;
use Modules\_Auth\BaseAuth;
use Modules\_Auth\SiteAuth;
use Modules\Online\OnlineStreamers;
use Object\User\User;

/**
 * Class APIAuth
 * @package Modules\_SiteModule\API
 */
class _APIAuth extends ModuleAPI
{
    /**
     * @var string
     */
    public static $mode = self::MODE_PROTECT;

    /**
     * Авторизация через апи https://site.com/api/null/service/auth
     * ------------------------------------------------
     *  {
     *      "isSalt": true - флаг того что отправляем чистый пароль или его хеш (для автоматической авторизации)
     *      "resetHash" : false - флаг того, что создавать новую, или брать текущую. При первом варианте, пользователя выкинет с сайта, если он там находился.
     *  }
     * ------------------------------------------------
     * @throws ArgumentNullException
     * @throws APIHeaderException
     * @throws APIException
     * @throws AccessException
     * @throws Exception
     */
    public function doAuth(): void
    {
        //dump($this->getAPIData());
        /** @var ModuleAPI $this */
        $fields = $this->getStrictFields([
            'userName',
            'password'
        ]);
        /** @var ModuleAPI $this */
        $isSalt = $this->getAPIData()->getCastParam('isSalt', 'b');

        /** @var User $user */
        $user = Core::getUser();
        if ($user === null) {
            throw new APIException('System error. Class User::class no implement');
        }
        /** @var User $user */
        $user = $user::getUser($fields['userName'], ['empty']);

        if ($user->getUserData()->isBlocked()) //смотрим блокировку пользователя.
        {
            throw new AccessException('Пользователь заблокирован. Свяжитесь с администрацией.');
        }

        $userPassword = $user->getUserData()->getPassword();
        $password = $fields['password'];
        if (!$isSalt) {
            $salt = $user->getUserData()->getSalt();
            $password = BaseAuth::MakePasswordHash($password, $salt);
        }

        if ($userPassword !== $password) {
            throw new APIException('Не верный логин или пароль.');
        }
        /** @var ModuleAPI $this */
        if ($this->getAPIData()->getCastParam('resetHash', 'b') OR empty($user->getUserData()->getHash())) {
            SiteAuth::doAuth($user, false);
        }

        $this->response = [
            'hash' => $user->getUserData()->getHash(), //отошлём хеш авторизации.
            'saltPass' => $password, //отошлём хеш пароя
            'apiToken' => $user->getUserData()->getAPIToken(), //отправим ApiТокен
            'MOTD' => 'SEND MOTD' /*AdminControl::getMOTD()*/ //отошлём Сообщение дня
        ];
    }

    /**
     * Проверяем авторизацию стримера  https://site.com/api/%userToken%/service/checkAuth
     * ------------------------------------------------
     *  "hashToken": "23e5db4865cf8dd4dbd66d878a221d0c", - хэш токен авторизации пользователя
     *  "password":"cde692bf6253983271c0ec82ac082bce, - хеш пароля пользователя. Если хеш пароля сменился, то авторизаия провалена.
     * ------------------------------------------------
     * @throws APIException
     * @throws APIHeaderException
     * @throws ArgumentNullException
     * @throws Exception
     */
    public function checkAuth(): void
    {
        //получаем данные хеша
        /** @var ModuleAPI $this */
        $fields = $this->getStrictFields([
            //'userLib',
            'passwordHash',
            'hashToken'
        ]);
        /** @var User $user */
        /** @var ModuleAPI $this */
        $user = $this->getAPIData()->getUser();

        if ($user == null) {
            throw new APIException('User not found');
        }
        //dump($fields);
        //получаем передаваемую информацию о текущей сессии
        /*
        $userData = $fields['authData'] ?? null;

        if (empty($userData)) { //данных нет
            throw new ArgumentNullException('Параметры передаваемой авторизации не корректны или пусты.');
        }
        */
        // Сверяем хеш парооли. Планировалось, что в случае кражи пароля пользователя,
        // пользователь сможет поменять пароль и вора выкинет из системы.
        // Однако если юзер сменит пароь и войдёт под новым на сайт, вора всё равно выкинет.
        // Пока не знаю, оставлять или нет.
        if ($user->getUserData()->getPassword() !== ($fields['passwordHash'] ?? null)) {
            throw new AccessException('Хеш пароля перестал быть актуальным.');
        }

        /** @var ModuleAPI $this */
        if (!User::checkHash($user->getUserData(), $fields['hashToken'])) {
            throw new AccessException('Хеш авторизации не совпадает. Кто-то уже сидит пож этой учётной записью??.');
        }
        //if is streamer?
        OnlineStreamers::addOnline($user->getName());
        /*
        // Сверяем библиотеки, если есть различия, пользователь теряет авторизацию.
        if (!array_compare($user->getUserData()->isStreamer(), $userLib ?? [])) {
            throw new APIException("Произошли изменения в библиотеке игр. Необходимо переавторизироваться заново.");
        }
        */
        $this->response = ['auth' => 'valid']; //аутенфикация валидная
    }
}