<?php

namespace Modules\_Services;


use Engine\AdmControl\LangController;
use Engine\AdmControl\MenuController;
use Engine\AdmControl\SiteConfigsController;
use Engine\Core\Development\DebugInfo;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseSiteCardModule;
use Engine\Modules\BaseSiteModule;

class _ServiceConfigs extends BaseSiteModule
{
    private $configControllers = [];

    private $target;
    /**
     * @var BaseSiteCardModule
     */
    private $cardMod;

    private $config = [
        'site',
        'lang',
        'menu'
    ];

    /**
     * @throws ConfigException
     * @throws SiteComponentException
     * @throws \ReflectionException
     */
    public function build(): void
    {
        parent::build();
        $parent = BaseSiteCardModule::inherit($this);
        $parent->build();

    }

    /**
     * @param array $data
     * @return array
     * @throws \ReflectionException
     */
    public function getDisplayData(array $data = []): array
    {
        switch ($this->target) {
            case 'site':
            case 'lang':
            case 'menu':
                return [
                    'paramValue' => $this->configControllers
                ];
            default:
                return [
                    'link' => self::getUri() . 'Config/',
                    'links' => ViewModel::getConfigLinks($this->configControllers)
                ];
        }
    }

    /**
     * @param null $options
     * @throws AccessException
     * @throws \ReflectionException
     */
    protected function controller($options = null): void
    {
        parent::controller([
            'uri' => 'Service',
            'options' => $options
        ]);

        $this->target = $options['target'] ?? null;
        $this->loadConfigs($this->target);
        //$this->make();
        //dump($this->target, $this->configControllers);
    }

    /**
     * Загружаем конфиги
     * @param $target
     * @param bool $load
     * @throws \Exception
     */
    private function loadConfigs($target, bool $load = true): void
    {
        switch ($target) {
            case 'site':
                //Загружаем конфиги сайта
                $c = SiteConfigsController::init($this->configControllers, $load);
                DebugInfo::debugAddF('Loaded [%s] > %d configs', $c->getId(), $c->getCountAllLoadedConfigs());
                break;
            case 'lang':
                //загружаем все конфиги по языкам
                $c = LangController::init($this->configControllers, $load);
                DebugInfo::debugAddF('Loaded [%s] > %d configs', $c->getId(), $c->getCountAllLoadedConfigs());
                break;
            //TODO: скорей всего редактор менюшек надо пилить отдельный
            case 'menu':
                //загружаем конфиг левых менюшек
                $c = MenuController::init($this->configControllers, $load);
                DebugInfo::debugAddF('Loaded [%s] > %d configs', $c->getId(), $c->getCountAllLoadedConfigs());
                break;
            default:
                foreach ($this->config as $config) {
                    $this->loadConfigs($config, false);
                }
        }
    }
}