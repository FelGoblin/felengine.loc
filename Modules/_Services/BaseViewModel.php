<?php


namespace Modules\_Services;

class BaseViewModel
{

    /**
     * @param array $array
     * @param $field
     */
    protected static function extractBool(array &$array, $field): void
    {
        if (($array[$field] ?? null) != null) {
            $array[$field] = Cast($array[$field], 'b');
        }
    }
}