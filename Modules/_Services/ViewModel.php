<?php


namespace Modules\_Services;


use Engine\AdmControl\Ext\ConfigInspected;

class ViewModel extends BaseViewModel
{
    public static function getCurrentConfigs(ConfigInspected $controller): array
    {

    }

    /**
     * @param array $configControllers
     * @return array
     * @throws \Exception
     */
    public static function getConfigLinks(array $configControllers): array
    {
        $result = [];
        /** @var ConfigInspected $controller */
        foreach ($configControllers as $controller) {
            //dump($controller);
            $result[] = $controller->toArray();
        }
        return $result;
    }
}