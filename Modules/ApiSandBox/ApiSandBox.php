<?php


namespace Modules\ApiSandBox;


use Engine\Modules\BaseSiteCardModule;
use Exception;
use Object\User\User;
use Object\User\UserData;

final class ApiSandBox extends BaseSiteCardModule
{
    protected function controller($options = null): void
    {
        parent::controller([
            'uri' => 'APISandbox',
            'options' => $options
        ]);
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    protected function getDisplayData(array $data = []): array
    {
        /** @var UserData $userData */
        $userData = $this->user->getUserData();
        return parent::getDisplayData([
            'authHash' => $userData->getHash(),
            'apiToken' => $userData->getAPIToken()
        ]);
    }
}