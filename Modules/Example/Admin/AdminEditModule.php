<?php

namespace Modules\Example\Admin;


use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Modules\BaseSiteModule;

/**
 * Модуль редактирования
 * Class SiteNewsAdminEditModule
 * @package Modules\Example\Admin
 */
final class AdminEditModule extends BaseSiteModule
{
    protected string $adminToolbarLabel = 'example.admin-edit-toolbar';


    protected function controller($options = null): void
    {
        //Указываем админ контроллер для данного модуля (админ команды)
        $this->adminNameSpace = 'Modules\Example\Admin'; //мы так же указываем тот же админ класс что и для основного модуля Example
        //
        parent::controller([
            'uri' => 'Example' //этот под модуль как бы относится к модулю Example и ссылку ридерикта передаём ему
        ]);
    }

    /**
     * Замороченная ситуация, так как динамически все метки вычисляются согласно неймспейса
     * @param bool $asClass
     * @return string
     */
    protected function getActiveLabel(bool $asClass = true): string
    {
        //но так как наш модуль лежит внутри дургого модуля (даже внутри вообще левой папки)
        if (!$asClass) { //то переназначим всё вручную
            return 'example.admin-edit-module'; //имя шаблона
        } else {//а тут в любом случае динамически некоторые элемнты принимают последний пусть неймспейса (возможно в будущем поменяю), например в тулбаре
            return 'admin.example.admin-edit-module'; //поэтому укажем что это часть admin
        }
        //единственный минус такого подхода (хотя может быть и плюс), в том что layouts для такого нам надо создавать ОБЩИЙ для всех админ подмодулей, так как имя файла - первый токен в имени шаблона, а в данном случае это admin
    }

    public function build(): void
    {
        parent::build();

        $this->createSiteComponent($this->getActiveLabel(false), $this->displayData, InjectComponentConfig::make('outdata', 'data'));
    }
}
