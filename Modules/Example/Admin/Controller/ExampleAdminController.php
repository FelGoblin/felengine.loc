<?php

namespace Modules\Example\Admin\Controller;


use Engine\Exceptions\AccessException;
use Exception;
use Namespaces\Admin\BaseAdminController;

/**
 * КОнтроллер админ команд на уровне контроллераа модуля.
 * До обработки триггера пользователя.
 * Автоматически подключаются все контроллеры и первый вернувший true означает, что админ контроллер отработал.
 *
 * Class ExampleAdminController
 * @package Modules\Example\Admin\Controller
 */
class ExampleAdminController extends BaseAdminController
{
    /**
     * Обязательный метод контроллера кхм.. контроллер?
     * @param array $data
     * @return bool
     * @throws AccessException
     * @throws Exception
     */
    function controller(array $data = []): bool
    {
        if (false /* something*/) {
            return true;
        }
        return false;
    }
}