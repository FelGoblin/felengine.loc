<?php

namespace Modules\Example\Admin;

use Engine\Modules\ICard;
use Exception;
use Engine\Core\Http\Input;
use Engine\Core\Http\Redirect;
use Engine\Core\View\Render;
use Engine\IController;
use Engine\Controller;
use Modules\Example\Example;
use Modules\Example\Controller\ExampleController;
use Namespaces\Admin\SiteAdmin;

/**
 * Это клас для контроллеров администратора
 * Class Admin
 * @package Modules\Example\Admin
 */
final class Admin extends SiteAdmin
{
    /**
     * Это низкоуровневый контроллер который обрабатывается в роутере.
     * Т.е. этот контроллер работает от пути в адресной строке /Example/Admin/Add или /Example/Edit/1 и ссылка на него идёт непосредственно в роут-контроллере
     * @param IController $controller - контроллер с которого идёт запрос
     * @param array $params
     * @return Controller
     * @throws Exception
     */
    public static function preController(IController $controller, array $params): IController
    {
        $controller = parent::preController($controller, $params);

        //dump(self::$trigger);

        switch (self::$trigger) {

            case 'delete'://Например если у нас команда удаления
                $id = $params['id'] ?? 0;
                if ($id <= 0) { // но в параметрах не передали ID
                    Render::setWarning('ID is null'); //уведомление - ошибка
                    return $controller->main(null); //вернём main метод текущего тригер-контроллера
                } else {
                    /*
                     * Производим удаление чего-либо (что будет передано в параметрах, к примеру)
                     *  $result = $this->>doDeleteSomething($id);
                     */
                    Render::setNotice('DELETE success', true);//сохраняем в сессию, так как будет перезгрузка сайта
                    Redirect::go('/Example'); //перезагрузим страницу
                }
                break;

            //Редактирование ВНУТРИ модуля. Мы можем как управлять меткой шаблона, так и, например, флагом параметров для шаблона.
            case 'edit':
                $data = [
                    'editMode' => true, //например для переключения уже внутри самого шаблона.
                    'templateLabel' => 'main-edit', //для смены всего шаблона карточки. В данном случае, мы продублируем main шаблон, в котором введём отображение по флагу.
                    'editText' => Input::getPost('edit', CAST_NULLSTR) ?? 'Данные для редактирования'
                ];
                /** @var ExampleController $controller */
                return $controller->main(null, $data);//просто перехватываем данные вывода модуля у текущего триггера;
                break;

            //Редактирование с помощью ВНЕШНЕГО МОДУЛЯ
            case 'edit2':
                $data = [
                    'templateLabel' => 'admin.edit',
                    'editText' => 'Текст для textarea которую мы отдельно вывели в переменную $editText'
                ];
                /** @var ExampleController $controller */
                return $controller->main(new AdminEditModule(), $data);

            case 'addNew':
                return $controller->main(null, [ //просто перехватываем данные вывода модуля у текущего триггера
                    'templateLabel' => 'admin.new'
                ]);
                break;

            case 'make': //например было что-то сделано и мы просто перезагрузим всё
                Render::setNotice('Admin MAKE CONTROLLER success', true);//сохраняем в сессию, так как будет перезгрузка сайта
                Redirect::go('/Example');
                break;

            default: //неизвестныая команда
                Render::setWarning('Unknown admin trigger... abort all');
                break;
        }
        return $controller->main(null);
    }


    /**
     * Конотроллер, который обрабатывается внутри модуля, который отправляет запрос в данные админ контроллера
     * Он реагирует уже на переданные POST параметры и обрабатывает нажатие submit`ов (например кнопка "Создать")
     * <button name="admControl" value="addNew">Создать</button>
     * @param null $data
     * @throws Exception
     */
    public static function controller($data = null): void
    {
        if (!self::isAdmin()) {
            return; //Не админ, контроллер не обрабатывается
        }
        //dump(self::getTrigger());
        //Самый простой способ - перенаправлять запросы, согласно входящим параметрам на низкоуровневый контроллер, который обрабатыет уже URI пути
        switch (self::getTrigger()) {

            //Просто тестовая команда
            case 'test':
                Redirect::go(Example::getUri('Admin/Test'));
                break;

            //Что-то добавляем
            case 'addNew':
                Redirect::go(Example::getUri('Admin/New'));
                break;

            //Что-то удаляем
            case 'doDel':
                $id = Input::getPost('cardID', CAST_INT);
                Redirect::go(Example::getUri('Admin/Delete/' . $id));
                break;


            case 'doCreateNew': //по сути добавдение нового - это тоже самое сохранение.
                Render::setNotice('CREATE success', true);//сохраняем в сессию, так как будет перезгрузка сайта
                Redirect::go(Example::getUri()); //перезагрузим страницу
                break;

            //Сохраняем
            case 'doSave':
                Render::setNotice('ADMIN SAVE Success', true);
                Redirect::go(Example::getUri()); //перезагрузим страницу
                break;

            case 'doSaveError':
                //Иммитируем ошибку сохранения.
                self::$edit = true; //остаётся режим редактирования
                Render::setWarning('Какая-то ошибка сохранения');
                break;


            //Что-то правим
            case 'doQEdit': //быстрая правка с использоованием карточки модуля
                $id = Input::getPost('cardID', CAST_INT);
                Redirect::go(Example::getUri('Admin/Edit/' . $id));
                break;

            //Что-то правим
            case 'doEdit'://правка, при которой шаблон редактируемого элемента перезаписывается
                $id = Input::getPost('cardID', CAST_INT);
                Redirect::go(Example::getUri('Admin/Edit2/' . $id));
                break;
        }
    }

    /**
     * Пример добавления в карточку моделя в шапку дополнительные контролы
     * @param ICard $card
     * @param array $postData
     * @throws Exception
     */
    public static function setAdminCardControls(ICard &$card, array $postData): void
    {
        if (self::isAdmin()) {
            if (!self::$edit) { //например не в режиме редактирования
                $card->addControls('hidden', ['name' => 'cardID', 'value' => $postData['id']]);
                $card->addControls('button', ['icon' => 'edit', 'label' => 'Edit Inside', 'tooltip' => 'Редакиторование внутри модуля.', 'name' => 'admControl', 'action' => 'doQEdit']);
                $card->addControls('button', ['icon' => 'edit', 'label' => 'Edit OutSide', 'tooltip' => 'Редакиторование отдельным модулем.', 'name' => 'admControl', 'action' => 'doEdit']);
                $card->addControls('button', ['icon' => 'close', 'label' => 'Удалить', 'name' => 'admControl', 'action' => 'doDel', 'tooltip' => 'Иммитация удаления', 'confirm' => 'Чо прям удалить????']);
            }
        }
    }
}