<?php

namespace Modules\Example\SiteComponents;


use Engine\Core\View\SiteComponent\SiteComponent;

class ExampleSiteComponent extends SiteComponent
{
    public static string $componentRegisteredName = 'example-component';

    public string $path = 'interface/example/component';
}
