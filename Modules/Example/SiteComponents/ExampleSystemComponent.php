<?php

namespace Modules\Example\SiteComponents;

use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\SystemComponent;
use Engine\Exceptions\SiteComponentException;

/**
 * Class ExampleSystemComponent
 * Этот компонент автоподключается в системе  и автоиницируется
 * @package Modules\Example\SiteLibraryElements
 */
class ExampleSystemComponent extends SystemComponent
{
    public static bool $enable = false;
    /**
     * Данный компонент добавляется в контент
     * @var int
     */
    protected int $injectMode = self::INJECT_AS_CONTENT;
    /**
     * Имя компнента
     * @var string
     */
    public static string $componentRegisteredName = 'examplesystem';
    /**
     * Путь до шаблона
     * @var string
     */
    public string $path = 'interface/example/components/system-component';

    /**
     * Инициализация системного компонента
     * @param string|null $path
     * @param array $injectControllers
     * @return AbstractComponent
     * @throws SiteComponentException
     */
    public static function init(string $path = null, array $injectControllers = []): AbstractComponent
    {
        //dump('Инициируем системный компонент модуля Example');
        return new ExampleSystemComponent(static::$componentRegisteredName); //имя и путь до шаблона будет использован из класса
    }
}
