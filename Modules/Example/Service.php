<?php

namespace Modules\Example;

use Engine\Core\Core;
use Engine\Modules\AbstractService;
use Engine\Modules\HomePage;

/**
 * Class Service
 * Инициализация модуля на уровне инициализации системы
 * @package Modules\Example
 */
class Service extends AbstractService
{
    /**
     * Метод загрузки сервиса.
     *      SYSTEM_LOAD - загрузка перед инициализацие перед инициализацией основных системных компонентов, а так же перед проверкой на закрытие сайта.
     *          * т.е. такой режим загрузит данные В ЛЮБОМ случае, вне зависимости от того, что там с системными компонентами и доступностью сайта
     *      POST_LOAD - загрузка после инициализации системных компонентов
     *      ALWAYS_LOAD - загружать всегда. ПО факту инициализация будет вызвана дважды. При SYSTEM_LOAD и при POST_LOAD. Остальное регулирование остаётся за модом
     * @var int
     */
    public static $mode = POST_LOAD;

    /**
     * Создание псевдонимов
     */
    public function classAlias(): void
    {
        class_alias('\\Modules\Example\\Example', 'Example'); //пример установки псевдонима
        //dump('Установка псевдонима для модуля  Example. Теперь в любом месте сайте, а так же в шаблонах можно обращаться к данному модулю как \Examlpe, минуя весь Неймспейс: 'Modules\Example\Example);
    }

    /**
     * Инициализация.
     *  Чтобы выполнить инициализацию, необходимо  в файле /Src/Config/Services.php в секции 'service' добавить в массив Modules\Example\Service::class или аналогично в файле services.php в корне модуля.
     * @param string|null $config
     * @param int $mode
     */
     public function init(?string $config, int $mode): void
    {
        //dump('Инициализируем данный модуль Example::Service(). Однако это не имеет никакого отношения непосредственно к самому модулю Example, однако даёт точку входа для запуска любого его статичного метода. Например создадим данные для внедрение их в системный модуль \'heder\'. Эти данные можно будет перезаписать в любой момент, но они будут автоматически внедрены, если такого не случится.');
        Example::testInjectOnInitModule();
        //dump('Внедряем в HomePage модуль Example (TODO: Отключен модуль)');
        //Homepage::inject('Modules\Example\Example'); //Пример перехвата домашней страницы при отсутствующих роутах.
        /*
         * По-умолчанию роут http://site.ru активирует Namespaces\Site\Controller\HomeTrigger::index
         * Этот роут выводит данные Homepage псевдомодуля. Необходимо внедрить свой модуль вывода в Homepage
         *
         * Homepage::inject(new News()); //пример пример иньекции в домашнюю страницу
         * Вот пример если требуетося авторизация пользователя
         * Events::subscribe(Core\EventsHandler\Event::onUserAuthorize, function($args) {
         *      //авторизация пользователя внедрит модуль новости в HomePage
         *      Homepage::inject(new News());
         * });
         */
    }
}