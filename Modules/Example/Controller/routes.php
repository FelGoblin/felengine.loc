<?php
/*
 * EXAMPLE routes
 */
Route::both('/example/', [ //get - обрабатывать только GET запросы, post - только POST запросы, both - оба
    'controller' =>'ExampleController', //триггер класс
    'action' => 'main', //метод класса
    'permission' => ['authorized'] //разрешения для доступа к этому  триггеру (на уровне роутреа)
]);
/*
 * Пример роутов с параметрами
 * ВАЖНО. Приоритет роутов: более поздние имеют меньший приоритет.
 */
//приоритет 1
Route::both('/example/(param:any)', [ //пример обработки GET типа http://site/example/любой_символ
    'controller' =>'ExampleController', //триггер класс
    'action' => 'exampleAny', //метод класса
    'permission' => ['authorized'] //разрешения для доступа к этому  триггеру (на уровне роутреа)
]);
//Приритет 2. Т.е. сперва обработаются цифровые пути, только потом строчные
Route::both('/example/(param:numeric)', [ //пример обработки GET типа http://site/example/1
    'controller' =>'ExampleController', //триггер класс
    'action' => 'exampleNumeric', //метод класса
    'permission' => ['authorized'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);

/* #############
 * ### ADMIN ###
 * #############
 * Обработка админских команды внутри текущего шаблона текущего NameSpace`a
 */
Route::both('/example/admin/new/', [ //test
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'addNew', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
//DELETE
Route::both('/example/admin/delete/', [ //something delete (Однако это роут выдаст ошибку в дальнейшем, так как не передаётся ID)
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'delete', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
Route::both('/example/admin/delete/(param:numeric)', [ //something delete
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'delete', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
//#### EDIT ####
//Быстрое редактирование внутри текущего шаблона
Route::both('/example/admin/edit/', [ //something edit (Однако это роут выдаст ошибку в дальнейшем, так как не передаётся ID)
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'edit', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
Route::both('/example/admin/edit/(param:numeric)', [ //something edit
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'edit', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
//Редактирование с выводом отдельного шаблона
Route::both('/example/admin/edit2', [ //something edit
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'edit2', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);
Route::both('/example/admin/edit2/(param:numeric)', [ //something edit
    'controller' =>'ExampleController', //админский триггер класс
    'action' => 'edit2', //метод класса
    'permission' => ['authorized', 'moderator'] //разрешения для доступа к этому  триггеру (на уровне роутера)
]);