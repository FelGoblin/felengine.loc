<?php

namespace Modules\Example\Controller;
//namespace Modules\Example\Controller;

use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SiteComponentException;
use Engine\IController;
use Engine\Controller;
use Engine\Modules\BaseSiteModule;
use Exception;
use Modules\Example\Admin\Admin;
use Modules\Example\Example;
use ReflectionException;

/**
 * Class ExampleController
 * Router триггер, который определяется в фалйе Controller/routes.php
 * @package Modules\Example\Controller
 */
final class ExampleController extends Controller
{
    /**
     * @param BaseSiteModule $mod
     * @param $data
     * @return Controller|null
     * @throws ReflectionException
     * @throws ConfigException
     * @throws SiteComponentException
     */
    public function main(BaseSiteModule $mod = null, $data = null): ?IController
    {
        return parent::main($mod ?? new Example(), $data);
    }

    /**
     * @param int $param
     * @return IController|null
     * @throws ConfigException
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function exampleNumeric(int $param): ?IController
    {
        return parent::main(new Example(['numericParam' => $param, 'from' => sprintf('called: %s', __METHOD__)]));
    }

    /**
     * Modules\Example\Controller\ExampleController::exampleAny()
     * @param $param int|string
     * @return IController|null
     * @throws ConfigException
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function exampleAny($param): ?IController
    {
        return parent::main(new Example(['anyParam' => $param, 'from' => sprintf('called: %s', __METHOD__)]));
    }

    //##################################################################################################################################

    /**
     * ADMINS TRIGGERS
     * Используется отсюда, так как нам нужен текущий шаблон, если использовать из NameSpace Admiт, то там будетзагрузка своего шаблона.
     *  Мы прост внедряем админские команды в интерфейс
     * @throws Exception
     */
    public function addNew(): Controller
    {
        return Admin::preController($this, ['action' => 'addNew']);
    }

    /**
     * @param int $id
     * @return Controller
     * @throws \Exception
     */
    public function delete(int $id = 0): Controller
    {
        return Admin::preController($this, ['action' => 'delete', 'id' => $id]);
    }

    /**
     * @param int $id
     * @return Controller
     * @throws Exception
     */
    public function edit(int $id = 0): Controller
    {
        return Admin::preController($this, ['action' => 'edit', 'id' => $id]);
    }

    /**
     * @param int $id
     * @return Controller
     * @throws Exception
     */
    public function edit2(int $id = 0): Controller
    {
        return Admin::preController($this, ['action' => 'edit2', 'id' => $id]);
    }

    /**
     * @param array $options
     * @return IController|null
     * @throws ConfigException
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function adminController(array $options): ?IController
    {
        return parent::main(new Example($options));
    }
}