<?php

namespace Modules\Example;

use Engine\API\APIData;
use Engine\API\ModuleAPI;
use Engine\Core\Objects\INamespace;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseSiteCardModule;
use Exception;
use Modules\Example\Admin\Admin;
use ReflectionException;

/**
 * Class Example
 *  Основной исполняюший файл модуля.
 * @package Modules\Example
 */
class Example extends BaseSiteCardModule //МОДУЛЬ ИСПОЛЬЗУЕТ ШАБЛОН-КАРТОЧКУ
{
    /**
     * Примеер подключения АПИ для текущего модуля. Это необходимо при сборке модулей. Прооверка на существование такого метода в данном классе тригернёт АПИ для модуля.
     * @param APIData $data
     * @return ModuleAPI
     */
    public static function API(APIData $data): ?ModuleAPI
    {
        if (self::$API == null) {
            self::$API = new API($data); //это ссылка на API.php, который лежит в текущей папке модуля
        }
        return self::$API;
    }

    /**
     * Чтобы выполнить инициализацию постконтроллера, необходимо  в файле services.php в секции 'postInit' добавить в массив Modules\Example\Example::class
     * @throws ReflectionException
     */
    public static function postControllerRun(int $mode = INamespace::MODE_NORMAL): void
    {

        parent::postControllerRun();
        //dump('Обработка пост контроллера модуля Example. Это метод будет автозагружен, так как становится сервисным после того как прописали его в services.php');

        //static::injectSystemComponent('header');
    }

    /**
     * Внедряем данные TODO: старый вариант и уже больше не используется. Будет удалён.
     */
    public static function testInjectOnInitModule(): void
    {
        /*
        static::$injectData = [
            'exampleTest' => 'Эти данные внедрились при инициализации модуля Example в методе testInjectOnInitModule, который вызван из Init.php::init. При обращении к контроллеру модуля http://site/example эти данные можно перезаписать в контроллере.'
        ];
        */
        //что-то делаем. Наверное.
    }

    /**
     * Контроллер модуля.
     * @param null $options
     * @throws AccessException
     * @throws Exception
     */
    public function controller($options = null): void
    {
        //dump('Запуск контроллера модуля Example');
        /*
         * //пришли данные из роут-контроллера
         * if (!empty($options)) {
         *   dump(['footer' => 'Нам пришли доп. параметры с контроллера. (Обработка роутов и передача парметров)'], $options);
         * }
         */
        //Указываем админ контроллер для данного модуля (админ команды)
        $this->adminNameSpace = 'Modules\Example\Admin';

        //родительский контроллер
        parent::controller([
            'uri' => 'Example', //необходимо для использования self::getUrl(); //ссылка на текущий модуль в виде http://server/Example, можно добавлять линки: self::getUrl('ExampleLink') = http://site.com/Example/ExampleLink
            'getDirect' => true, //нам нужен POST/GET параметр direct (например для сортировки), хотя это можно получить самостоятельно из Input::getPost('direct');
            'getTrigger' => true, //нам нужен POST/GET параметр trigger. Если указать как bool, то тригером будет do, если указать string, то будет искать этот параметр
            //* 'getTrigger'=> 'myCustomTrigger', //например, хотя это можно получить самостоятельно из $trigger = Input::getPost('do');
            'getSearch' => true, //нам нужен POST/GET параметр search. Ищет в переменной s, хотя это можно получить самостоятельно из Input::getPost('s');
            'permission' => null, //передаём требавания по доступу к модулю. null - не требуется, например доступ только разработчикам: 'permission'=>'dev'
            'self' => true //в модуль запишется данные о текущем пользователе User::this() //TODO: скорей всего устарело и текущий пользователь по-умолчанию добавляется в модуль.
        ]);
    }

    /**
     * Переопределение для метки шаблона
     *     При том, что метку шаблона на рендер можно переключить в любой момент $this->activeTemplateLabel = 'tpl';
     * @return string
     */
    public function getActiveTemplateLabel(): string
    {
        if ($this->trigger == 'customTemplate') { //метка на якобы кастомную страницу
            return 'custom';
        } else {
            return parent::getActiveTemplateLabel(); //по-умолчанию вернётся текущая метка
        }
    }

    //################$$$####
    //##### CARD TITLE ######
    //###################$$$#

    /**
     * Заголовок основной карточки шаблона (если шаблон использует всего одну карточку). Необходимо, если не надо автоподключать файл. Напрмиер если данные в хаголовке не динамические.
     * TODO: в дальнейшем добавить метки длЯ шаблонов заголовок карточки
     * @param string|null $data
     * @return string
     */
    public function getCardTitle(string $data = null): string
    {
        if ($this->trigger == 'customTitle') {
            return 'Устанавливаем заголовок карточки самостоятельно.';
        } else {
            return parent::getCardTitle($data); //отдадим в автокомпиляцию
        }
    }

    /**
     * Подвал основной карточки (если шаблон использует всего одну карточку). Необходимо, если не надо автоподключать файл. Напрмиер если данные в хаголовке жинамические.
     * По принципу работы аналогично Title
     * @param string|null $data
     * @return string
     */
    public function getFooter(string $data = null): string
    {
        if ($this->trigger == 'customFooter') {
            return 'Устанавливаем заголовок футера самостоятельно.';
        } else {
            return parent::getFooter($data);
        }
    }
    //###################
    //##### FOOTER ######
    //###################

    /**
     * Тулбар для рендера.
     * @param bool $autoBuild
     * @return void
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function makeToolbar(bool $autoBuild = true): void
    {
        //отправляем в автосборку родительского класса
        parent::makeToolbar(true);
        /*
         * Пример создания кастомного тулбара.
         * return $this->createSiteComponent($this->toolbarLabel,
         *    $this->getToolbarData(),
         *    InjectComponentConfig::make($this->getActiveLabel(), 'toolbar')
         * );
         */
    }

    /**
     * Это тулбар для пользователей с доступом admin и выше.
     * @param bool $autoBuild
     * @return IComponent|null
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function makeAdminToolbar(bool $autoBuild = true): ?IComponent
    {
        //В main шаблоне модуля админ тулбар будет находиться в переменной $adminToolbar
        if (!$this->getAccess()) {
            return null;
        }


        if (Admin::isNew()) {
            //return SiteComponent::quick('interface/example/toolbar/admintoolbar');
            $this->adminToolbarLabel = 'edit-new-toolbar';
        } elseif (Admin::isEdit()) {
            $this->adminToolbarLabel = 'edit-toolbar';
        } else {
            //return SiteComponent::quick('interface/example/toolbar/admintoolbar3');
            $this->adminToolbarLabel = 'admin-toolbar';
        }
        //dump($this->adminToolbarLabel);
        $c = parent::makeAdminToolbar(true);

        //пример добавления стороннего элемента в тулбар
        $this->createSiteComponent(
            'extra-toolbar-element', //метка шаблона
            //данные, которые мы передаём в шаблон
            [
                'test1' => 'value_test1',
                'test2' => 'value_test2'
            ],
            //конфиг подключения
            InjectComponentConfig::make(
                $this->getAdminToolbarLabel(),//Куда втыкаем, ВНИМАНИЕ есть особенность того, что меткам прикрепляется имя модуля (или же часть неймспейса) в начале имени, поэтому использовать метод, а не метку
                //но можно и указать вручную как 'example.' . $this->adminToolbarLabel
                'extraElement'// под какой меткой (переменной)
            )
        );
        /*
         * Для простого тулбара действует аналогичное правило, за исключением того, что в конфиге подключения, мы указываем цель $this->getToolbarLabel()
         */

        //dump($this->adminToolbarLabel);
        return $c;
    }

    //####################
    //##### TOOLBAR ######
    //####################

    /**
     * Рендер шаблона для вывода
     * @param string|null $mainPath
     * @throws ReflectionException
     * @throws SiteComponentException
     * @throws Exception
     */
    public function build(string $mainPath = null): void
    {
        //Добавим к заголовку карточки дополнительные контролы (кнопки управления)
        Admin::setAdminCardControls(
        /** @param Card $this ->outComponent */
            $this->outComponent, //это текущая карточка, наследуется
            //данные которые мы прередадим в этот конструктор
            [
                'id' => 1 //предположим, что id мы берём из POST; GET; приходит из контроллера, для примера ID будет статично
            ]
        );

        //для примера подключаем только в мейн шаблон
        if ($this->getActiveLabel() == 'example.main') { //это подключение можно также организовать в doTrigger() ну или в любой функции класса;
            //пример подключения внешнего шаблона
            $extraComponent = $this->createSiteComponent(
                'extra-component', //метка шаблона
                //данные, которые мы передаём в шаблон
                [
                    'test1' => 'value_test1',
                    'test2' => 'value_test2'
                ],
                //конфиг подключения
                InjectComponentConfig::make($this->getActiveLabel(), 'extraComponent'));
            //пример подключения в добавленный компонент другого компонента
            $this->createSiteComponent(
                'sub-extra-component', //метка шаблона
                //данные, которые мы передаём в шаблон
                [
                    'sub_test1' => 'sub_value_test1',
                    'sub_test2' => 'sub_value_test2'
                ],
                //конфиг подключения
                InjectComponentConfig::make($extraComponent->getId(), 'subInject'));
        }

        //отправляем в родительский класс для окончательной сборки.
        parent::build(); //сборка не обрабатывает информацию о модификации и она получает данные как есть.
    }

    /**
     * Обработка триггера
     */
    protected function doTrigger(): void
    {
        switch ($this->trigger) {
            //тулбар кнопка
            case 'doUserTrigger':
                $this->extraData = 'Была нажата кнопка тулбара.'; //данное сообщение уёдёт в шаблон в переменную $extraData
                break;
        }
    }

    //##########################
    //##### ADMIN TOOLBAR ######
    //##########################

    /**
     * Функция возвращающая данные для шаблонов
     *  ВНИМАНИЕ. Эта функция никак не может получать информацию о модификации данных из вне. Поэтому она возвращает данные как есть.
     * @param array $data
     * @return array
     */
    protected function getDisplayData(array $data = []): array
    {
        //Данные для вывода шаблона (в основном используется при автогенерации карточки  модуля)
        return parent::getDisplayData([
            'exampleParam' => 'Этот пример значения переменной  $exmpleParam в main шаблоне модуля Example.' //теперь в шаблонах модуля доступна переменная $exampleParam,  которая имеет значение "value"
        ]);
    }

    /**
     * Данные, которые мы отправим в заголовок текущей карточки. Работает только при автокомпиляции.
     * @return array
     */
    protected function getTitleData(): array
    {
        return [ //эти данные мы передаём в титтл карточки
            'param' => 'EXAMPLE TITLE PARAM'
        ];
    }

    /**
     * Данные, которые мы отправим в футер карточки. Работает только при автокомпиляции.
     * @return array
     */
    protected function getFooterData(): array
    {
        return [ //эти данные мы передаём в титтл карточки
            'param' => 'EXAMPLE FOOTER PARAM'
        ];
    }

    /**
     * Данные для шаблона тулбара
     * @return array
     */
    protected function getToolbarData(): array
    {
        return [
            'param' => 'Какие-то данные для тулбара в переменной $param'
        ];
    }

    /**
     * СОбираем данные для админ тулбара
     * @return array
     */
    protected function getAdminToolbarData(): array
    {
        return [
            'adminParam' => 'is Admin param toolbar value'
        ];
    }

    /**
     * Заголовок таблицы
     * @return string
     */
    protected function getPageTitle(): string
    {
        return 'Заголовок Example страницы';
    }
}
