<?php


namespace Modules\Example\API;


use Engine\API\ModuleAPI;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Exception;

/**
 * Class APIPublicExample
 * Данный модуль имеем внешнее (не обязательно) API. Обращение к нему через псевдоним, указанный в self::$alias. При отсутсвии, по имени класса (apiexample)
 * Данный моуль имеет public режим и его ответ может быть перехвачен и переопределён в любом можуле апи, который подписался в инициализации на событие
 * @package Modules\Example\API
 */
final class APIPublicExample extends ModuleAPI
{
    /**
     * Псевдоним модуля, который будет идти в параметре api запроса http://site.ru/api/%user_token%/example/%trigger%/
     * @var string
     */
    public static $alias = 'example';

    /**
     * Режим работы данного модуля и возможность его переопределения или расширения
     *      self::MODE_PRIVATE  - Модуль не расширяется и не дополняется.
     *      self::MODE_PROTECT - Модуль может быть расширен только в пределах работы данного модуля
     *      self::MODE_PUBLIC -  Модуль может быть расширен из любого апи метода любого апи модуля
     * @var string
     */
    public static $mode = self::MODE_PUBLIC;

    /**
     * Переопределение и расширение.
     *  в $args будут переданы:
     *      [
     *          'mod' - имя сработавшего модуля API
     *          'trigger' - имя сработавшего триггера апи модуля
     *          'apiData' - Переданные данные для апи (можно переопределить)
     *          'response' - ответ отработанного апи модуля (можно переопределить)
     *      ]
     * @throws Exception
     */
    public static function modEventsInit(): void
    {
        //вызываем. Можно сразу передать =имя требуемого модуля и тригера
        self::doModuleEvent(function ($args) {
            $ext = ['extend' => 'Расширенные данные ответа апи'];
            if (is_array($args['response'])) {
                $args['response'] = array_merge($args['response'], $ext);
            } else {
                $resp = $args['response'];
                $args['response'] = array_merge(['original' => $resp], $ext);
            }
        }, null, 'exampleTrigger');

        //Для примера попробуем хукнуть модуль example2 (APIProtectedExample.php). Однако это никогда не сработает, так как модуль example2 имеет защищённый режим и не даёт другим модулям доступ к событию.
        self::doModuleEvent(function ($args) {
            dump('Хукнули модуль example2. А нет не хукнули');
            $args['response'] = 'Пролностью поменяли ответ (не самом деле нет)';
        }, 'example2');
    }

    /**
     * @throws APIException
     * @throws APIHeaderException
     * Триггер апи (контроллер)
     *      http://site.ru/api.php?...&module=example&trigger=exampleTrigger
     * (
     *  Для обработки возникающих ошибок (ЭТО ПРИМЕР!)
     *      http://site.ru/api.php?...&module=example&trigger=exampleTrigger&error=true
     * )
     */
    public function exampleTrigger(): void
    {
        //... какой-то глубокий и осмысленный код
        //....
        //А тут мы ищем GET параметр в API запросе под именем error с значением true (error=true)
        $error = $this->getAPIData()->getCastParam('error', CAST_BOOL);
        if ($error == true) {
            //если надо прекратить выполнение API
            throw new APIException('Произошла какая-то (абсолютно любая) ошибка в работе API');
            /*
             * Ответом будет
             *      { "access":false, "data":{ "error": "Произошла какая-то (абсолютно любая) ошибка в работе API" }}
             */
        }
        //а теперь заполняем данные, которые вернутся на запрос
        $this->response = 'Hallo World!';
        /*
         * Ответом будет
         *  { "access": true", "data": { 'Hallo World' }}
         */
    }
}