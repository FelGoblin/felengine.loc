<?php


namespace Modules\Example\API;


use Engine\API\ModuleAPI;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Exception;

/**
 * Class APIPublicExample
 * Данный модуль имеем внешнее (не обязательно) API. Обращение к нему через псевдоним, указанный в self::$alias. При отсутсвии, по имени класса (apiexample)
 * Данный моуль имеет public режим и его ответ может быть перехвачен и переопределён в любом можуле апи, который подписался в инициализации на событие
 * @package Modules\Example\API
 */
final class APIProtectedExample extends ModuleAPI
{
    /**
     * Псевдоним модуля, который будет идти в параметре api запроса http://site.ru/api/%user_token%/example/%trigger%/
     * @var string
     */
    public static $alias = 'example2';

    /**
     * Режим работы данного модуля и возможность его переопределения или расширения
     *      self::MODE_PRIVATE  - Модуль не расширяется и не дополняется.
     *      self::MODE_PROTECT - Модуль может быть расширен только в пределах работы данного модуля
     *      self::MODE_PUBLIC -  Модуль может быть расширен из любого апи метода любого апи модуля
     * @var string
     */
    public static $mode = self::MODE_PROTECT;

    /**
     * Переопределение и расширение.
     *  в $args будут переданы:
     *      [
     *          'mod' - имя сработавшего модуля API
     *          'trigger' - имя сработавшего триггера апи модуля
     *          'apiData' - Переданные данные для апи (можно переопределить)
     *          'response' - ответ отработанного апи модуля (можно переопределить)
     *      ]
     * @throws Exception
     */
    public static function modEventsInit(): void
    {
        //хукаем ЛЮБОЙ отработавший модуль с любым тригером. Это сработает в случае, если модуль был публичный, или текущий
        self::doModuleEvent(function ($args) {
            dump('Хукаем любой публичный API модуль или собственный приватный. Хукнули: ' . $args['mod']);
            $hooked = ['hooked' => 'Это хук данные из вообще левого модуля APIProtectedExample, который получил доступ к public методу или перехватил собственный метод'];
            if (is_array($args['response'])) {
                $args['response'] = array_merge($args['response'], $hooked);
            } else {
                $resp = $args['response'];
                $args['response'] = array_merge(['original' => $resp], $hooked);
            }
        });
    }
    /*
 * Описание функций апи
 */
    /**
     * @throws APIException
     * @throws APIHeaderException
     * тупо скопировали из APIPublicExample::exampleTrigger();
     */
    public function exampleTrigger2(): void
    {
        //... какой-то глубокий и осмысленный код
        //....
        //А тут мы ищем GET параметр в API запросе под именем error с значением true (error=true)
        $error = $this->getAPIData()->getCastParam('error', CAST_BOOL);
        if ($error == true) {
            //если надо прекратить выполнение API
            throw new APIException('Произошла какая-то (абсолютно любая) ошибка в работе API');
            /*
             * Ответом будет
             *      { "access":false, "data":{ "error": "Произошла какая-то (абсолютно любая) ошибка в работе API" }}
             */
        }
        //а теперь заполняем данные, которые вернутся на запрос
        $this->response = 'Hallo World!';
        /*
         * Ответом будет
         *  { "access": true", "data": { 'Hallo World' }}
         */
    }
}