<?php
/**
 * Подключение сервисов модуля.
 */
return [
    'site' => [ //работа внутри сайта
        'service' => [ //Иницииализация до корневого контроллера
            //Example
            Modules\Example\Service::class,
        ],
        'postInit' => [ //пост инициализация
            //Example
            Modules\Example\Example::class,
        ]
    ],
    'cron'=>[
        'service'=>[
            Modules\Example\Service::class
        ]
    ]
];