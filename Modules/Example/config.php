<?php
/**
 * Конфиг автоподключится в конструкторе модуля и сохранится в общем конфиге сайта
 * SiteConfig::get('exampleConfigParam','example');
 */
return [
    'exampleConfigParam' => 'exampleConfigValue'
];