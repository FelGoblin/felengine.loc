<?php

namespace Modules\Example;

use Engine\Modules\CronModule;

/**
 * Задание из планировщика
 * Class Cron
 * @package Modules\Example
 */
class Cron extends CronModule
{
    /**
     * По сути тут только одна функция - выполнить задачу. Окружение Cron получает весь функционал сайта,
     * однако модули доступные для него описываются в Config\Services.php в секцию Cron -> services или в аналогичную ветку в файле service.php  в корне папки модуля.
     *  Пример активации Cron планировщика в OpenServer
     *      %progdir%\modules\php\%phpdriver%\php-win.exe -f %sitedir%\felgoblin.loc\cron.php 010BCF785EBD71A2FAD13095802C161E Example
     *      (010BCF785EBD71A2FAD13095802C161E - это токен, который хранится в конфиге)
     *  Пример активации Cron задания из командной строки cmd windowns
     *      cd /d F:\WebServer\OSPanel\modules\php\PHP_7.3-x64 - это переход в директорию с PHP Интерпритаором
     *      php.exe -q -f F:\WebServer\OSPanel\domains\felengine.loc\public_html\cron.php BCF785EBD71A2FAD13095802C161E Example
     *      (010BCF785EBD71A2FAD13095802C161E - это токен, который хранится в конфиге)
     * @param null $data
     * @return void
     */
    public function task($data = null): void
    {
        $b = 1 + 1; //произведём сверх сложное вычисление
        //добавим в собющение вывода
        $this->addLog('Task complete: ' . $b); //оставит в лог файле запись Task complete: 2
    }
}