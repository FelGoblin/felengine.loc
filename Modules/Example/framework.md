#Запросы
##POST, GET, FILE, REQUEST
Все GET/POST запросы чистятся **htmlspecialchars**
* Параметр **@key** не обязателен, при отсутствии ввернётся ВЕСЬ массив. По сути анаологичен запросу **get()[@key]**
* Параметр **@cast** не обязателен, он указывает в каком типе данных вернуть значение _(варианты обозначений, использовать можно любой):_  
    * **string**
        * s
        * str
        * string    
    * **string OR null**
        * nullstr
        * ?s
        * ?str
        * ?string    
    * **float**
        * float
        * f
        * d    
    * **integer**
        * num
        * numeric
        * i
        * int
        * integer  
    * **integer OR null**
        * ?num
        * ?numeric
        * ?i
        * ?int
        * ?integer   
    * **boolean**
        * b
        * bool
        * boolean      
    * **array**
        * a
        * array
 
###GET
**Input::getGet(@key, @cast)**
>Input::getGet('test','?str');
##POST
**Input::getPost(@key, @cast)**  
**Input::getRaw(@key)**
> Возвращает "чистые" данные, не обработанные htmlspecialchars
###REQUEST
**Input::getRequest(@key, @cast)**  
**Input::get(@key, @cast)**
>в данном случае собираются ВСЕ запросы одновременно: GET, POST, REQUEST
###JSON
JSON данные автоматически загрузятся, преобразуются ив массив и будут доступны только из метода   
**Input::getJson(@key, @cast)**  

Если известно, что будет загружен файл фoрмата JSON, то можно получить сразу данные:  
**Input::getJSONFromFile(@asArray, @throw)**  
  
**bool @asArray** [default true] - Преобразовать данные в ассоциативный массив  
**bool @throw** [default false] - При ошибке выбросить исключение  
###Files
$file =**Input::getFileControl()->getUploadFile()**
>Получить загруженный файл, вернёт **null** при отсутствии файла

**$uploaded = $file->getUploadedFile();**
> Ссылка на загруженный фаил

#Редирект, нотисы, куки, сессии
##Редирект
**Redirect::go(@extraPath);** - перенаправит на http://site.com. При наличии @extraPath, перенаправит на http://site.com/extraPath  
 В модулях стоит использовать **Redirect::go(self::getUri(@extraPath));**
 ##Нотисы
 **Render::setError(@message, @session)** - Вывести сообщение ошибки
 **Render::setNotice(@message, @session)** - Вывести сообщение уведомления
 **Render::setWarning(@message, @session)** - Вывести сообщение опасности
 
 * @param **bool** _$session_ [default: false] - вывести немедлено в текущей сборке, или занести в сессю для вывода в следующей сборке. Это необходимо, если работа скрипта прерывается и необходимо перезагрузить страницу.
##Куки
**Cookie::get(@key)** - получить куки  
**Cookie::delete(@key, @domain)** - удалить куки  
**Cookie::set(@key, @value, @damain, @days, @http)** - установить куки
 * @param **string** _$key_
 * @param **string** _$value_
 * @param **string** | **null** _$domain_
 * @param **int** _$days_
 * @param **bool** _$http_

##Сессии
**Session::add(@key, @value)** - добавить данные в сессию. Данные будут добавляться как элемент массива  
**Session::put(@key, @value)** - добавить данные в сессию. Новые данные перезапишут старые.  
**Session::has(@key)** - такой ключ есть в сессии  
**Session::delete(@key | @keys)** - удалить данные в сесиии по ключу. Можно передать массив ключей   
**Session::reset()** - очистить данные сессии

**Random::randomBool()** - true или false. С вероятностью 50/50
#Функции
##Random
**Random::$rand** - текущее сгенерированное число  
**Random::random(@min, @maxValue)** - Случайное целое число от @min [default 0] до @maxValue [default 100]    
**Random::randomFloat(@min, @maxValue** - Случайное дробно число @min [default 0] до @maxValue [default 100]  
**Random::randarr(@arrayLength, @min, @maxValue)** - Создать массив длиной @arrayLength, заполненный случайными числами от @min [default 0] до @maxValue [default 100]  
**Random::chance(@percent, @included)** 
* @param **int** $proc - Шанс выпадения.
* @param **bool** $include - Шанс включительно.
* @return **bool** - true - выпало, false - нет  

**Random::chanceF(@percent, @included, @genration)**  
* @param **float** $proc - Шанс выпадения.
* @param **bool** $include - Шанс включительно.
* @param **bool** $geneation - Генерировать новое число или нет
* @return **bool** - true - выпало, false - нет

**Random::fromArray(@array, @pointCount)**
* @param **array** $array - Массив из которого извлекаем
* @param **int** $count - количество извлекаемых элементов
##Пути

**pathSeparator(@path)** - представление пути до файлов, согласно текущей ОС 
**pathSeparatorF(@path, @param,...,@paramN)** - представление пути до файлов, согласно текущей ОС с форматированием внутри пути 
##Массивы
**trimArray(@array)** - вернёт массив, у которого удалены все пробелы в начале и в конце его элементов.  
**array_compare(@array1, @array2)** - Сравнение массивов. Вернёт bool результата сравнения.  
**array_merge_recursive_distinct(@array1, @array2)** - Рекурсивное слияние массивов  
**array_do_column(@array, @key)** - Преобразует индекс массива по значениям указанных колонок внутренних массивов
>     a = [
>             0 => (a=>A,b=>B,c=>C),
>             1 => (a=>D,b=>E,c=>F),
>             2 => (a=>G,b=>imageHeigth,c=>I) 
>         ]
>
>     array_do_column (a,'a')  
>        return [     
>            A => (a=>A,b=>B,c=>C),  
>            D => (a=>D,b=>E,c=>F),  
>            G => (a=>G,b=>imageHeigth,c=>I)  
>     ]
**array_column_ext(@array, @key)** - Собрать массив из значений колонок
>     a = [
>        a1 => [id=1, test=2],
>        a2 => [id=2, test=2],
>        a3 => [id=3, test=2],
>     ]
>     array_column_ext(a, 'id') => [1, 2, 3]
**array_deleteValue(@value, @array)** - Удалить из массива элемент по значению  
**array_getsum(@array, @format)** - Получить сумму в массиве аналог array_sum, но корректно суммирует отформатированные number_format пробелы в цифре  
**array_normalize(@array)** - Собрать массивы внутри массива в нормальный массив
>     Предобразуем массив из
>     [
>          key=>[f1,f2..fN],
>          key2=>[a1,a2..aN]
>     ]
>     в
>     [
>          0=>[key=>f1, key2=>a1],
>          1=>[key=>f2, key2=>a2],
>          ...,
>          N=>[key=>fN, key2=>aN]
>     ]

Еще куча функции, лень расписывать.

#Компоненты
Компоненты - это php шаблон, в который передаются параметры и они внутри шаблона доступны как имена переменных
##Создание компонента вне модуля
**ComponentManager::createSiteComponent(@name, @data, @injectConfig)**
Где:
* @param **string** _$name_ - Имя компонента (в данном случае она же и метка)
* @param **array** _$data_ - Массив данных, который будет передан в шаблон
* @param **InjectComponentConfig** | **null** _$inject_ - Конфигурация внедрения компонента в другие компоненты.

##Создание компонента внутри модуля
**$this->createSiteComponent(@label, @data,  @injectConfig)**
* @param **string** _$label_ - Метка компонента (НЕ ИМЯ. В имя автоматически добавится имя модуля)
* @param **array** _$data_ - Массив данных, который будет передан в шаблон
* @param **InjectComponentConfig** | **null** _$inject_ - Конфигурация внедрения компонента в другие компоненты. 
###InjectComponentConfig
Конфигурация подключения компонента
**InjectComponentConfig::make(@target, @asLabel, @mode, @pos)**
Где:
* @param **int** | **string** _$target_ - Цель. Можно передать Id компоннета, или его имя (не путать с меткой)
* @param **string** _$asLabel_ - под именем какой переменной компонент будет доступен внутри другого компонента.
* @param **int** _$mode_ [default **IComponent::INJECT_AS_DATA**] - режим подключения:
    * **IComponent::INJECT_AS_DATA** - Компонент внедрён как данные. Т.е. доступен внутри согласно имени переменной $asLabel
    * **IComponent::INJECT_AS_CONTENT** - Компонент внедрён как дополнительный контент и содержимое будет автоматически добавлено в контент компонента цели как готовый (скомпилированный HTML текст), получается что $asLabel не нужен.
    * **IComponent::INJECT_AS_ARRAYDATA** - Компонент внедрён как данные, но при этом если есть совпадение по ключу, то ключ превращается в массив, который доступен в переменной c именем $asLabel
        > На примере инвентаря:  
        **$bag = $this->createSiteComponent('bag', []);** //иньекции пропишем в конфиг файле layouts.php  
        **for ($i=0; $i<=10; $i++){  
        $slot = $this->createSiteComponent('bagSlot', [..some slot array data..], InjectComponentConfig::make($bag->getId(), 'slots', IComponent::INJECT_AS_ARRAYDATA);  
        }**  
        В шаблоне bag теперь доступна переменная **$slots**, содержащая в себе массив из слотов.
* @param **int** _$pos_ (не обязательно) - позиция сборки. При сборке автоматически сортирует внутрилежащие компоненты согласно позициям и рендерит с учётом положения в списке.
