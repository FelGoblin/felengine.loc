<?php


namespace Widgets;


use Engine\API\Client\APIClient;
use Engine\Modules\AbstractService;
use Exception;

abstract class _SiteWidget extends AbstractService
{
    /**
     * @param array $build
     * @param string $path
     * @param bool $throw
     * @return string|null
     * @throws Exception
     */
    protected static function send(array $build, string $path = '', bool $throw = true): ?string
    {
        try {
            $res = APIClient::execs(sprintf(
                'http://%s:8099/Widget' . $path,
                //SiteConfig::get('broadcast-service', 'api', 'localhost')),
                'localhost'),
                'JSON',
                $build,
                false);
            //dump($build, $res);
            return $res;
        } catch (Exception $ex) {
            if ($throw) {
                throw $ex;
            } else {
                return 'false';
            }
        }
    }
}