<?php
return [
    [
        'title' => 'Элемент меню', //TODO: translate
        'description' => 'Элемент бокового меню', //TODO: translate
        'struct' => [
            [
                'title' => 'Тип',
                'description' => 'Тип элемента меню [menu / br]',
                'param' => 'type',
                'value' => 'menu'
            ],
            [
                'title' => 'Иконка',//TODO: translate
                'description' => 'Описание класса иконки &lt;i class=&quot;fel-...&quot;&gt;',//TODO: translate
                'param' => 'icon',
                'value' => '%string%'
            ],
            [
                'title' => '',//TODO: translate
                'description' => '',//TODO: translate
                'param' => 'title',
                'value' => '%string%'
            ],
            [
                'title' => '',//TODO: translate
                'description' => '',//TODO: translate
                'param' => 'url',
                'value' => '/%string%'
            ],
            [
                'title' => '',//TODO: translate
                'description' => '',//TODO: translate
                'param' => 'module',
                'value' => '%string%'
            ],
            [
                'title' => '',//TODO: translate
                'description' => '',//TODO: translate
                'param' => 'permission',
                'value' => '%array%|%string%',
                'note' => ['modmaster, moderator, development']
            ]
        ]
    ],
     [
        'title' => '',//TODO: translate
        'description' => '',//TODO: translate
        'struct' => [
            [
                'title' => '',//TODO: translate
                'description' => '',//TODO: translate
                'param' => 'type',
                'value' => 'menu'
            ]
        ]
    ]
];
