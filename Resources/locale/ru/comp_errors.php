<?php
return [
    '@error_template_undefined' => 'У компонента <b>%s</b> не определён шаблон вывода.',
    '@error_template_file_not_exists'=>'Файл шаблона [<b>%s</b>] не найден в [<b>%s</b>]',
    '@error_template_exception'=>'<warning>Возникло <b>%s</b> исключение в шаблоне <b>%s</b>:<br/><b>%s</b> Линия: <b>%d</b></warning>',
    '@error_template_data_inject'=>'Данный компонент <b>%s</b> уже скомпилирован. Внедрение новых данных невозможно.',
    '@error_template_data_empty'=>'<warning>Попытка присвоение пустых или не определённых данных в компонет <b>%s</b></warning>',
    '@error_component_file_not_found'=>'"Файл <b>%s</b> не найден."',
    '@error_injection_root'=>'<warning>Не найдена точка иньекции для <b>%s</b>. Компонент удалён.</warning>',
    '@error_comp_fail_save'=>'Filed save component: [%s]',
    '@error_comp_components_directory_not_exists'=>'Components directory <b>%s</b> not exists.'
];