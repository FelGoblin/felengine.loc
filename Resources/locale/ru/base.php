<?php
return [
    '@delete' => 'удалить',
    '@save' => 'сохранить',
    '@cancel' => 'отмена',
    '@update' => 'обновить',
    '@component' => 'компонент',
    '@config_not_found' => 'файл конфигурации [%s] не найден',
    '@config_unknown_file_config_extensions' => 'неизвестное расширение [%s] конфиг файла [%s] в узле [%s]',
    '@empty_data_out' => 'Вывод данных для метки [%s] пуст',
    '@services' => 'Конфигурация движка',
    '@services-config' => 'Редактирование конфигураций',
    '@site_configs_controller_title' => 'Все конфигурации сайта',
    '@lang_configs_controller_title' => 'Баблиотеки языков',
    '@menu_controller_title' => 'Конфигурация меню'
];