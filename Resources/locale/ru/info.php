<?php
return [
    '@route_add_count_routes' => '<success>Добавлено %d маршрутов</success>',
    '@route_added' => '<muted>Добавлен [%s] маршрут: [%s] как [<b>%s::%s</b>]</muted>',
    '@route_duplicate_warning' => '<warning>Дублирование маршрутов: [%s] как [%s]. Конфигурация маршрута игнорируется..</warning>',
    '@route_rewrite_rule' => '<info>Перезаписано %d правил</info>',
    '@render_compiled_count_components' => '<success>[Render] скомпилировано компонентов:  <b>%d</b></success>',
    '@random_number' => 'Выпавшее число %f',
    '@events_register_with_count_count' => '(#<span class="digits">%d</span>) Регистрация события <b class="event">%s</b>; [<b>%s</b>] Файл: <b>%s</b> линия: <b>%s</b><br/>',
    '@event_has_no_implementation' => '<warning>У вызываемого события <b>%s</b> нет реализаций</warning>',
    '@line_file_info' => '-> [<b>%s</b>] Файл: <b>%s</b> линия: <b>%s</b><br/>',
    '@event_line_file_info' => 'Вызов события ( <b class="event">%s</b>) [<b>%s</b>] Файл: <b>%s</b> линия: <b>%s</b><br/>'
];