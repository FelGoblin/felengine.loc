<?php
return [
    '@dev_controller_access_deny' => 'Нет доступа к dev контроллеру',
    '@dev_key_dev_is_undefined' => 'Нет ключа [dev] или он не действительный',
    '@admin_unknown_controller' => '<warning>[admin] Unknown or empty ADMIN controller</warning>',
    '@unknown_inject_type_controller' => 'Неизвестный тип контроллера для внедрения данных',
];