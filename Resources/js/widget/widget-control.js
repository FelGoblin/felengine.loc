'use strict';
(() => {
    /**
     *
     * @param options {object}
     * @constructor
     */
    function WidgetOptions(options) {
        this.debug = options['doDebug'] || false;
        //метод получения данных виджета
        this.transport = options['protocol'] || 'ws://';
        this.host = options['host'];
        this.port = options['port'] || 6699;
        this.servise = '/' + options['service'];
        this.streamer = options['streamer'];
        this.token = options['token'];
        /**
         * events
         * @type {*|(function(...[*]=))}
         */
        this.onShow = options['onShow'] || function (data) {
        };
        this.onConnect = options['onConnect'] || function () {
        };
        this.onCloseConnect = options['onCloseConnect'] || function () {
        };
        this.onData = options['onData'] || function (data) {
        };
        this.onEnd = options['onEnd'] || function (data) {
        };
    }

    /**
     *
     * @param options {object}
     * @constructor
     */
    function WidgetControl(options) {
        /**
         *
         * @type {null|WidgetOptions}
         */
        this.options = new WidgetOptions(options);
        /**
         *
         * @type {null}
         */
        this.builder = null;
        /**
         * Очередь оповещений
         * @type {Array}
         */
        this.list = [];
        WidgetControl.instance = this;
        _startSocket.call(this);
    }

    /**
     *
     * @private
     */
    function _startSocket() {
        let
            self = this,
            opt = this.options,
            ws,
            /**
             * Режим работы: 0 - представляемся, 1 - работаем с данными
             * @type {number}
             */
            mode = 0,
            reason = 'unknown';
        if (opt.host === "" || opt.host === undefined || opt.host === "absolute") {
            opt.host = "localhost";
        }
        try {
            ws = self.ws = new WebSocket(opt.transport + opt.host + ':' + opt.port + opt.servise);
            //on open socket
            ws.onopen = () => {
                _errorMsg('', true);
                //представляемся
                ws.send(`{ "action": "handshake", "token":"${opt.token}"}`);
                //socket message
                ws.onmessage = resp => {
                    try {
                        let data = JSON.parse(resp.data);
                        if (mode === 0) {
                            if (data['action'] === 'handshake') {
                                if (data['access']) {
                                    mode = 1;
                                    if (typeof opt.onConnect === 'function') {
                                        //обработчик события
                                        opt.onConnect.call(this, ws, opt);
                                    }
                                }
                            } else {
                                ws.close('Мне тут не рады :(');
                            }
                        } else {
                            _controller.call(self, data);
                        }
                    } catch (ex) {
                        console.warn('Widget: WARNING data -', ex);
                    }
                };
                console.log('Widget: CONNECT to' + opt.servise);
            };
            //close socket, reconnect
            ws.onclose = function (ev) {
                // _broadCast(self, 'widgetOnDisconnect');
                _errorMsg('webSocket close [' + (ev.reason || reason) + ']');
                if (typeof opt.onCloseConnect === 'function') {
                    //обработчик события
                    opt.onCloseConnect.call(this, (ev.reason || reason));
                }
                setTimeout(function () {
                    _startSocket.call(self);
                }, 2000);
            };
            //window close... close socket too
            window.onbeforeunload = () => {
                if (ws != null) {
                    ws.onclose = function () {
                    };
                    ws.close();
                }
            };
            //ws error... just create error reason
            ws.onerror = (event) => {
                reason = 'socket error';
            };
            //out back
            this.options.socket = ws;
        } catch (ex) {
            WidgetBuilder.errorMsg(ex.message);
        }
    }

    /**
     * Сообщение ошибки
     * @param msg
     * @param clear
     * @private
     */
    function _errorMsg(msg, clear) {
        let main = El('#log');
        if (!main.isNull()) {
            if (!clear) {
                msg = 'Widget Error: ' + msg;
            }
            main.text(msg);
        }
    }

    /**
     *
     * @param data {object}
     * @private
     */
    function _controller(data) {
        //console.log(data);
        let service = (data['service'] || 'widget');
        switch (service) {
            case 'widget':
                if (typeof this.options.onData === 'function') {
                    //обработчик события
                    this.options.onData.call(this, data);
                }
                break;

            case 'broadcast': //события на виджете
                switch (data.data.event) {
                    case "widgetOnStart"://виджет начал показ
                        this.options.onShow.call(this, data.data.data); //ехал data через data, видит data в data data. Сунул data data в data.
                        break;
                    case "widgetOnEnd"://виджет закончил показ
                        this.options.onEnd.call(this, data.data.data); //омайво мо! Шиндеру. НАНИ!!!
                        break;
                }

                break;

            case 'system:':
                console.log(data);
                break;
        }
    }

    window.WidgetControl = WidgetControl;
})();