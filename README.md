# FelEngine

![Картинка][image1]

[image1]:https://felgoblin.ru/Resources/images/FelEngine-logo.png
Установка
------------
- Для установки требуется PHP версии 7.1 и выше.
- Чтобы начать работу с движком, необходимо распаковать структуру в корень сайта.
- Необходимо сконфигурировать подключение к MySql, для этого надо создать файл database.php с содержимым
```php
return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'username' => '%username%',
    'password' => '%password%',
    'db_name' => '%database_name%',
    'charset' => 'utf8'
];
```
- Необходимо создать реализацию интерфейса ``IUser``, наследуемый от ``_User``

Документация
-------------
- В процессе...
