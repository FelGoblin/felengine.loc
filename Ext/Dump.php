<?php
/*
 * DUMP:
 *  Функция для вывода информации по PHP переменным, массивам, классам.
 *  FelGoblin 2018-2019
 */

/**
 * Class DumpLang
 *  TODO: Добавить мультиязычность
 */
class DumpLang
{

}

/**
 * Стили
 * Class DumpStyle
 * @package Engine
 */
class DumpStyle
{
    public const DEFAULTSTYLE = 'default';
    public const BRACECOLOR = 'cornflowerblue';
    public const FNCOLOR = '#607D8B';
    public const STRINGCOLOR = '#3F51B5';
    public const INTCOLOR = 'Red';
    public const DOUBLECOLOR = '#ff5722';
    public const BOOLTRUECOLOR = '#006600';
    public const BOOLFALSECOLOR = '#e14141';
    public const NULLCOLOR = '#69008c';
    public const TYPECOLOR = '#a2a2a2';
    public const VERTICALLINECOLOR = '#ccc';
    public const OBJECTCOLOR = '#a78600';
    public const PRIVATEMETHODCOLOR = '#a70000';
    public const PROTECTEDMETHODCOLOR = '#298c68';
    public const VARIABLECOLOR = '#9e5700';
    public const EXPANDERDCOLOR = '#31818f';
    public static $inject = false;
    /**
     * Пресет стилей
     * @var array
     */
    private $stylesPreset = [
        'default' => [
            'color' =>
                [
                    'head' => '#000',
                    'body' => '#000',
                    'footer' => '#000'
                ],
            'bg' =>
                [
                    'head' => 'rgba(204,204,204,0.4)',
                    'body' => '#fff',
                    'footer' => 'rgba(204,204,204,0.4)'
                ]
        ],
        'red' => [
            'color' =>
                [
                    'head' => '#000',
                    'body' => '#000',
                    'footer' => '#100c0c'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => '#e6a8a8',
                    'footer' => '#ec9a9a8c'
                ]
        ],
        'blue' => [
            'color' =>
                [
                    'head' => '#fff',
                    'body' => '#000',
                    'footer' => '#100c0c'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => '#a0d6ef',
                    'footer' => 'rgba(138, 195, 222, 0.4)'
                ]
        ],
        'yellow' => [
            'color' =>
                [
                    'body' => '#000',
                    'head' => '#000',
                    'footer' => '#000'],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => '#ffd700',
                    'footer' => '#f2f2b1'
                ]
        ],
        'green' => [
            'color' =>
                [
                    'body' => '#000',
                    'head' => '#fff',
                    'footer' => '#fff'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => '#4CAF50',
                    'footer' => '#4CAF50'
                ]
        ],
        'dark' => [
            'color' =>
                [
                    'body' => '#000',
                    'head' => '#fff',
                    'footer' => '#fff'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => '#636161',
                    'footer' => 'rgba(99, 97, 97, 0.67)'
                ]
        ],
        'orange' => [
            'color' =>
                [
                    'body' => '#000',
                    'head' => '#fff',
                    'footer' => '#000'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => 'orange',
                    'footer' => '#efc890'
                ]
        ],
        'pink' => [
            'color' =>
                [
                    'body' => '#000',
                    'head' => '#fff',
                    'footer' => '#000'
                ],
            'bg' =>
                [
                    'body' => '#fff',
                    'head' => 'hotpink',
                    'footer' => '#ffadd6'
                ]
        ]
    ];

    private $currentStyle = [];

    /**
     * DumpStyle constructor.
     * @param null $style
     */
    public function __construct($style = null)
    {
        $this->currentStyle = $this->getStylesPreset(self::DEFAULTSTYLE);
        if (is_array($style)) {
            foreach ($style as $k => $v) {
                if (isset($this->currentStyle[$k])) {
                    if (is_array($v)) {
                        $this->currentStyle[$k] = array_merge($this->currentStyle[$k], $v);
                    } else {
                        $this->currentStyle[$k] = $v;
                    }
                }
            }

        } elseif ($style != null) {
            $this->currentStyle = $this->getStylesPreset($style);
        }
    }

    /**
     * @return array
     */
    private function getStylesPreset(string $preset): array
    {
        return $this->stylesPreset[$preset] ?? $this->stylesPreset[self::DEFAULTSTYLE];
    }

    /**
     * @param string $btnExp
     * @param string $btnCol
     * @param string|null $curButton
     * @param bool $float
     * @return string
     */
    public static function getToggleButton(string $btnExp = '[+]', string $btnCol = '[-]', string $curButton = null, bool $float = false): string
    {
        if ($curButton == null) {
            $curButton = $btnCol;
        }
        //🞂 🞃
        $js = "(function(el){let next=el.parentNode.nextElementSibling;if(next.style['display']=='none'){next.style['display']='';el.innerHTML='$btnCol';}else{next.style['display']='none';el.innerHTML='$btnExp';}})(this);";
        if ($float) {
            return '<div style="width=20px;float:right;cursor:pointer;" onClick="' . $js . '">' . $curButton . '</div>';
        } else {
            return '<div style="color:' . DumpStyle::EXPANDERDCOLOR . ' ;width=20px;display: inline-block;cursor:pointer;" onClick="' . $js . '">' . $curButton . '</div>';
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->currentStyle[$name] ?? '';
    }
}

/**
 * Конфиг
 * Class DumpConfig
 * @package Engine
 */
class DumpConfig
{
    /**
     * Предстовление объектов как они есть - объектами. Видимы будут только public свойства
     */
    const AS_OBJECT = 0;
    /**
     * Представление объектов как ассоциативные массивы. Видимы все свойства с любым доступом (Выбрано ПО-УМОЛЧАНИЮ)
     */
    const ASSOC_OBJECT = 1;
    /**
     * Функция возвращает строку, а не выводит.
     */
    const RETURN_STRING = 13002;
    /**
     * Максимальное количество рекурсий
     */
    const MAX_RECURSION_LIMIT = 1024 * 1024;
    public static $globalExpand = true;
    /**
     * Конфиг был загружен
     * @var bool
     */
    public $loaded = false;
    /**
     * Настройки
     * @var array
     */
    private $options =
        [
            'style' => '',
            'expand' => false,
            'cast' => self::ASSOC_OBJECT,
            'private' => false,
            'protected' => true,
            'trace' => false,
            'exit' => false,
            'footer' => '',
            'die' => false,
            'return' => false
        ];

    /**
     * DumpConfig constructor.
     * @param null $data
     */
    public function __construct($data = null)
    {
        $this->options['expand'] = self::$globalExpand;

        if ($data instanceof DCLoader) {
            $data = $data->options;
        }

        foreach ($data ?? [] as $opt => $value) {
            if (isset($this->options[$opt])) {
                $this->loaded = true;//был загружен какой-то параметр
            }
            $this->$opt = $value;
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->options[$name] ?? '';
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    public function __set($name, $value): void
    {
        if (isset($this->options[$name])) {
            if (is_array($this->options[$name]) AND is_array($value)) {
                $this->options[$name] = array_merge_recursive($this->options[$name], $value);
            } else {
                $this->options[$name] = $value;
            }
        }
    }
}

/**
 * Загрузчик конфига
 * Class DumpConfigLoader
 * @package Engine
 */
class DCLoader
{
    public $options = [];

    /**
     * DumpConfigLoader constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public static function load(array $options): self
    {
        return new DCLoader($options);
    }
}

/**
 * Вывод данных
 * Class PrintDump
 * @package Engine
 */
class PrintDump
{
    /**
     * @var  DumpConfig
     */
    private $config;
    /**
     * @var DumpStyle
     */
    private $style;

    private $margin_type = '<span style="color:' . DumpStyle::VERTICALLINECOLOR . '">|&nbsp;&nbsp;&nbsp;</span>';

    /**
     * VarDump constructor.
     * @param DumpConfig $config
     * @param $style
     */
    public function __construct(DumpConfig $config, DumpStyle $style)
    {
        $this->config = $config;
        $this->style = $style;
    }

    /**
     * @param $title
     * @param $body
     * @param $footer
     * @return string
     */
    public function printDump($title, $body, $footer)
    {
        $print = $this->getPart('head', $title) . $this->getPart('body', $body) . (!empty($footer) ? $this->getPart('footer', $footer) : '');
        return $this->getPart('main', $print);
    }

    public function getPart(): string
    {
        $in = func_get_args();

        $part = array_shift($in);

        switch ($part) {
            case 'main':
                $tpl = '<div style="text-align: left; margin:0 auto;margin-bottom:10px;w-max-width: 900px;min-width:350px;display:block;background:white;color:black;font-family:Verdana,sans-serif;font-size:11px;border:1px solid #cccccc;line-height:12px;word-wrap:break-word;text-shadow:none;box-shadow:0 8px 5px -7px black;">{data}</div>';
                break;

            case 'head':
                $btn = !$this->config->expand ? '[+]' : '[-]';
                $tpl = '<div style="padding:5px;color:' . $this->style->color['head'] . ';background-color:' . $this->style->bg['head'] . ';">' . DumpStyle::getToggleButton('[+]', '[-]', $btn, true) . '<div>{data}</div></div>';
                break;

            case 'body':
                $hide = !$this->config->expand ? 'display:none;' : '';
                $tpl = '<div style="color:' . $this->style->color['body'] . ';background-color:' . $this->style->bg['body'] . ';' . $hide . '">{data}</div>';
                break;

            case 'body_counter':
                $tpl = '<div style="border-top:1px solid #ccc;padding:5px 10px; color: #9e9e9e;font-size: 10px;line-height: 8px;">Вызов: {data} из {data1}</div>';
                break;

            case 'body_data':
                $tpl = '<div style="padding:5px 10px;">{data}</div>';
                break;

            case 'footer':
                $tpl = '<div style="border-top:1px solid #ccc;padding:2px 5px;color:' . $this->style->color['footer'] . ';background-color:' . $this->style->bg['footer'] . ';">{data}</div>';
                break;
        }
        $i = 0;
        foreach ($in as $data) {
            $tpl = str_replace('{data' . ($i == 0 ? '' : $i) . '}', $data, $tpl);
            $i++;
        }
        return $tpl;
    }

    /**
     * @param $var
     * @param string $var_name
     * @param string $margin
     * @param int $depth
     * @return string
     * @throws ReflectionException
     */
    private function arrayPrint(&$var, $var_name = '$', $margin = '', int &$depth = 0): string
    {
        $count = count($var);
        $out = sprintf(
            '<div>%s%s %s => <span style="color:%s">Array(%d)</span></div><div>%s<span style="color: %s">(</span><br />',
            $margin,
            DumpStyle::getToggleButton('🞂', '🞃', null, false),
            $var_name,
            DumpStyle::TYPECOLOR,
            $count,
            $margin,
            DumpStyle::BRACECOLOR
        );
        $keys = array_keys($var);
        foreach ($keys as $name) {
            $value = &$var[$name];
            $out .= $this->dump($value, "['$name']", $margin . $this->margin_type, $depth++);
        }
        //$out .= "$margin<span style='color:cornflowerblue'>)</span><br>";
        $out .= sprintf(
            '%s<span style="color:%s">)</span></div>',
            $margin,
            DumpStyle::BRACECOLOR
        );
        return $out;
    }

    /**
     * Выводим массив
     * @param $var
     * @param string $var_name
     * @param string $margin
     * @param int $depth
     * @return string
     * @throws ReflectionException
     */
    public function dump(&$var, $var_name = '$', $margin = '', int $depth = 0): string
    {
        //слишком ушли в реккурсию
        if ($depth >= DumpConfig::MAX_RECURSION_LIMIT) {
            return '..';
        }
        $out = '';

        //ARRAY
        if (is_array($var)) {
            $out .= $this->arrayPrint($var, $var_name, $margin, $depth); //Attention can recursion!!
        } elseif (is_string($var)) { //STRING
            $out .= $margin . self::PrintVar($var, $var_name);
        } //OBJECT}
        elseif (is_callable($var)) {
            $out .= $this->callablePrint($var, $var_name, $margin, $depth);
        } //OBJECT
        elseif (is_object($var)) {
            $out .= $this->objectPrint($var, $var_name, $margin, $depth); //Attention can recursion!!
        } //OTHER VARIABLES (num, bool..)
        else {
            $out .= $margin . self::PrintVar($var, $var_name);
        }
        return $out;
    }

    /**
     * Выводим данные о переменных
     * @param $var
     * @param string $name
     * @return string
     */
    public function printVar($var, $name = '$'): string
    {
        $color = '';
        $type = strtolower(gettype($var));

        if ($type == "string") {
            $color = DumpStyle::STRINGCOLOR;
            $type = 'String (' . strlen($var) . ')';
            $var = '"' . htmlspecialchars($var) . '"';
        } elseif ($type == "integer") {
            $color = DumpStyle::INTCOLOR;
            $type = "Integer";
        } elseif ($type == "double") {
            $color = DumpStyle::DOUBLECOLOR;
            $type = "Float";
        } elseif ($type == "boolean") {
            $type = "Bool";
            if ($var == 1) {
                $var = "TRUE";
                $color = DumpStyle::BOOLTRUECOLOR;
            } else {
                $var = "FALSE";
                $color = DumpStyle::BOOLFALSECOLOR;
            }
        } elseif ($type == "null") {
            $color = DumpStyle::NULLCOLOR;
            $var = 'NULL';
            $type = "NULL";
        }

        return sprintf(
            '%s = <span style="color: %s">%s</span>: <span style="color:%s">%s</span><br>',
            $name,
            DumpStyle::TYPECOLOR,
            $type,
            $color,
            $var
        );
    }

    /**
     * @param $var
     * @param string $var_name
     * @param string $margin
     * @param int $depth
     * @return string
     * @throws ReflectionException
     */
    private function callablePrint(&$var, $var_name = 'fn', $margin = '', int &$depth = 0): string
    {
        $out = sprintf(
            '<div>%s%s %s => <span style="color:%s;"><b>Closure function()</b></span></div><div>%s<span style="color: %s">{</span><br/>',
            $margin,
            DumpStyle::getToggleButton('🞂', '🞃', null, false),
            $var_name,
            DumpStyle::FNCOLOR,
            $margin,
            DumpStyle::BRACECOLOR
        );

        $reflFunc = new ReflectionFunction($var);

        $ig = [
            'fileName' => $reflFunc->getFileName(),
            'startLine' => $reflFunc->getStartLine(),
            'endLine' => $reflFunc->getEndLine()
        ];
        $_margin = $margin . $this->margin_type;
        foreach ($ig as $k => $v) {
            $out .= sprintf(
                '%s<span style="color:%s">%s</span>: <span style="color:%s">%s</span><br />',
                $_margin,
                DumpStyle::TYPECOLOR,
                $k,
                DumpStyle::FNCOLOR,
                $v
            );
        }


        $out .= sprintf(
            '%s<span style="color:%s">}</span></div>',
            $margin,
            DumpStyle::BRACECOLOR
        );
        return $out;
    }

    /**
     * @param $var
     * @param string $var_name
     * @param string $margin
     * @param int $depth
     * @return string
     * @throws ReflectionException
     */
    private function objectPrint(&$var, $var_name = '$', $margin = '', int &$depth = 0): string
    {
        $name = get_class($var);
        $out = sprintf(
            '<div>%s%s %s -> <span style="color:%s">Object:  <span style="color: %s">\%s</span></span></div><div>%s<span style="color: %s">(</span><br />',
            $margin,
            DumpStyle::getToggleButton('🞂', '🞃', null, false),
            $var_name,
            DumpStyle::TYPECOLOR,
            DumpStyle::OBJECTCOLOR,
            $name,
            $margin,
            DumpStyle::BRACECOLOR
        );
        if ($this->config->cast) {
            $keys = (array)$var;
        } else {
            $keys = get_object_vars($var);
        }
        $i = 0;
        foreach ($keys as $name => $value) {
            $_name = '';
            $type = $this->getTypeMethod($name, $_name);
            if ($type == 1) {
                if (!$this->config->protected) {
                    $out .= sprintf(
                        '%s%s[\'<span style="color:%s">Protected</span> %s\'] <span style="color:%s">Protected</span><br>',
                        $margin,
                        $this->margin_type,
                        DumpStyle::PROTECTEDMETHODCOLOR,
                        $_name,
                        DumpStyle::PROTECTEDMETHODCOLOR
                    );
                    continue;
                } else {
                    $name = sprintf('<span style="color:%s">Protected</span>  %s',
                        DumpStyle::PROTECTEDMETHODCOLOR,
                        $_name
                    );
                }
            } elseif ($type == 2) {
                if (!$this->config->private) {
                    $out .= sprintf(
                        '%s%s[\'<span style="color:%s">Private</span> @%s\'] <span style="color:%s">(endDisplay)</span><br>',
                        $margin,
                        $this->margin_type,
                        DumpStyle::PRIVATEMETHODCOLOR,
                        $_name,
                        DumpStyle::TYPECOLOR
                    );
                    continue;
                } else {
                    $name = sprintf('<span style="color:%s">Private </span>%s',
                        DumpStyle::PRIVATEMETHODCOLOR,
                        $_name
                    );
                }
            }
            $out .= $this->dump($value, "['$name']", $margin . $this->margin_type, $depth++);
            $i++;
        }
        $out .= sprintf(
            '%s<span style="color: %s">)</span></div>'
            , $margin,
            DumpStyle::BRACECOLOR);
        return $out;
    }

    /**
     * @param string $methodName
     * @param string $name
     * @return int
     *      0 - public
     *      1 - protected
     *      2 - private
     */
    private function getTypeMethod(string $methodName, &$name = ''): int
    {
        $name = trim($methodName);
        $aux = explode("\0", $name);
        if ($aux[0] == '*') {
            return 1;
        } elseif (count($aux) > 1) {
            $name = $aux[count($aux) - 1];
            return 2;
        } else return 0;
    }
}

/**
 * Дамп функция
 * @return string
 */
function dump()
{
    $debug = debug_backtrace();
    $args = func_get_args();

    if (count($args) == 0) {
        echo '<b>Dump arguments is empty or null;</b>';
        return;
    }

    $opt = $args[0];
    if ($opt instanceof DCLoader) {
        $config = new DumpConfig($opt);
    } elseif (is_array($opt)) {
        $config = new DumpConfig(DCLoader::load($opt));
    } else {
        $config = new DumpConfig();
    }
    if ($config->loaded) { //еонфиг был загружен
        array_shift($args); //убираем первый элемено массива
    }
    $style = new DumpStyle($config->style);
    $dump = new PrintDump($config, $style);
    $count = count($args);
    $title = $body = $footer = '';

    if ($config->trace) {
        //array_shift($debug); //последняя точка TODO: если однако трасировка была в конфиге, то это не правильно.
        foreach ($debug as $d) {
            $title .= sprintf('[<b>%s</b>] Файл: <b>%s</b> линия: <b>%s</b><br/>', $d['function'] ?? '_unknown_funct', $d['file'] ?? '_unknown_file', $d['line'] ?? '_unknown_line');
        }
    } else {
        $title = 'Файл: <b>' . $debug[0]['file'] . '</b> линия: <b>' . $debug[0]['line'] . '</b>';
    }

    $i = 1;
    foreach ($args as $arg) {
        if ($count > 1)
            $body .= $dump->getPart('body_counter', $i, $count);
        $body .= $dump->getPart('body_data', $dump->dump($arg));
        $i++;
    }
    $footer = $config->footer;

    $result = $dump->printDump($title, $body, $footer);


    if ($config->return) {
        return $result;
    } else {
        echo $result;
        if ($config->exit) {
            exit();
        }
        return null;
    }
}

function dump_exit()
{
    dump(new DCLoader(['exit' => true]), func_get_args());
}

function dump_trace()
{
    dump(new DCLoader(['trace' => true]), func_get_args());
}

function dumpF()
{
    $format = call_user_func_array('sprintf', func_get_args());
    call_user_func('dump', $format);
}

/**
 * Отображение всех глобальных переменных
 * @throws ReflectionException
 */
function dump_g()
{
    dump(new DCLoader(['footer' => 'dump object']), func_get_args());
    dump(new DCLoader(['style' => 'pink', 'footer' => '$_COOKIE']), $_COOKIE ?? '$_COOKIE не обнаружено');
    dump(new DCLoader(['style' => 'blue', 'footer' => '$_GET']), $_GET ?? '$_GET не обнаружено');
    dump(new DCLoader(['style' => 'blue', 'footer' => '$_POST']), $_POST ?? '$_POST не обнаружено');
    dump(new DCLoader(['style' => 'red', 'footer' => '$_REQUEST']), $_REQUEST ?? '$_REQUEST не обнаружено');
    dump(new DCLoader(['style' => 'green', 'footer' => '$_ENV']), $_ENV);
    dump(new DCLoader(['style' => 'yellow', 'footer' => 'json']), file_get_contents('php://input'));
    dump(new DCLoader(['style' => 'orange', 'footer' => '$_SESSION']), $_SESSION ?? '$_SESSION не обнаружено');
    dump(new DCLoader(['style' => 'dark', 'footer' => '$_SERVER']), $_SERVER);
}

function dump_help()
{
    /*
     *     public const DEFAULTSTYLE = 'default';
    public const BRACECOLOR = 'cornflowerblue';
    public const STRINGCOLOR = '#3F51B5';
    public const INTCOLOR = 'Red';
    public const DOUBLECOLOR = '#ff5722';
    public const BOOLTRUECOLOR = '#006600';
    public const BOOLFALSECOLOR = '#e14141';
    public const NULLCOLOR = '#69008c';
    public const TYPECOLOR = '#a2a2a2';
    public const VERTICALLINECOLOR = '#ccc';
    public const OBJECTCOLOR = '#a78600';
    public const PRIVATEMETHODCOLOR = '#a70000';
    public const PROTECTEDMETHODCOLOR = '#298c68';
    public const VARIABLECOLOR = '#9e5700';
     */
    $brace_c = DumpStyle::BRACECOLOR;
    $str_c = DumpStyle::STRINGCOLOR;
    $int_c = DumpStyle::INTCOLOR;
    $d_c = DumpStyle::DOUBLECOLOR;
    $true_c = DumpStyle::BOOLTRUECOLOR;
    $false_c = DumpStyle::BOOLFALSECOLOR;
    $null_c = DumpStyle::NULLCOLOR;
    $var_c = DumpStyle::VARIABLECOLOR;

    class TestClass
    {
        private $a = 1;
        protected $b = 2;
        public $c = false;
    }

    $var = new TestClass();
    $preset1 = dump(['return' => true], $var);
    $preset2 = dump(["style" => "red", "expand" => false, "cast" => 1, "private" => true, "protected" => true, "trace" => true, "footer" => "Тестовое сообщение в футер вывода", "return" => true], $var);
    $preset3 = dump(['return' => true, "style" => ["color" => ["head" => "red", "body" => "red", "footer" => "red"], "bg" => ["head" => "#000", "body" => "#222", "footer" => "#000"]], "footer" => "Тестовое сообщение в футер вывода"], $var);
    echo <<<"EOD"
<div style="overflow: hidden;text-align: left; margin:0 auto;margin-bottom:10px;w-max-width: 900px;min-width:350px;display:block;background:white;color:black;font-family:Verdana,sans-serif;border:1px solid #cccccc;font-size:11px;line-height:13px;word-wrap:break-word;text-shadow:none;box-shadow:0 8px 5px -7px black;">
    <div style="padding:5px;color:#000;background-color:rgba(204,204,204,0.4);">
        <div style="width=20px;float:right;cursor:pointer;" onClick="(function(el){var next = el.parentNode.nextElementSibling;if (next.style['display'] == 'none'){next.style['display'] = '';el.innerHTML = '[-]';}else{next.style['display']='none';el.innerHTML='[+]';}})(this);">[-]</div>
        <div>Помощь по использованию <b>dump()</b> функции.</div>
    </div>
    <div style=";color:#000;background-color:#fff">
        <div style="padding: 5px 10px;">
            <b>dump</b><span style="color:{$brace_c}">(</span>[..],<span style="color:{$var_c}">\$var</span><span style="color:{$brace_c}">)</span> - отобразить дамп переменной, используя первый входящий параметр как конфигурацию (опционально). Конфигурация загружается, если <b>хоть один параметр</b> совпал с существующим параметром конфигурации. Если вероятность совпадения дамп данных с конфигурациий, то можно напрямую скастить конфиг - 
            <b>dump</b><span style="color:{$brace_c}">(</span>new \<b>DCLoader</b><span style="color:{$brace_c}">(</span>[...]<span style="color:{$brace_c}">)</span>, <span style="color:{$var_c}">\$var</span><span style="color:{$brace_c}">)</span>
            <br /><br />Параметры конфига:
        </div>
        <ul>
            <li>
                <b>style</b>: стиль текущего отображения. <span style="color:{$str_c}">STRING</span> - имя пресета (red, blue, green, dark, yellow, pink, orange), ARRAY = свои параметры стилей.
                <div style="padding: 5px 10px;">
                    <b>style</b> в виде массива применяет к выводу свои стили. Не обязательно указывать все. За основу берётся стиль-по-умолчанию, указанные стили просто перезаписывают стандартный.
                    <div>
                        <ul style="margin: 10px;list-style: none;padding: 5px 35px;background-color: #e9e9e9;color: #605252;display: inline-block;">
                            <li><b>"color"</b> => ['</li>
                            <li>&nbsp;&nbsp;<b>"head"</b> => "#000"</li>
                            <li>&nbsp;&nbsp;<b>"body"</b> => "#000"</li>
                            <li>&nbsp;&nbsp;<b>"footer"</b> => "#000"</li>
                            <li>]</li>
                            <li><b>bg</b> => [</li>
                            <li>&nbsp;&nbsp;<b>"head"</b> => "#FFF"</li>
                            <li>&nbsp;&nbsp;<b>"body"</b> => "#FFF"</li>
                            <li>&nbsp;&nbsp;<b>"footer"</b> => "#FFF"</li>
                            ]  
                        </ul>
                    </div>
                </div>
            </li>
            <li>
                <b>expand</b>(bool): (default <span style="color:{$true_c}">TRUE</span>) - раскрывать окно или нет
            </li>
            <li>
                <b>cast</b>(int): (default <span style="color:{$int_c}">1</span>) - объект будет представлен как массив. если 0, то объект будет представлен как есть.
            </li>
            <li>
                <b>private</b>(bool): (default <span style="color:{$false_c}">false</span>) - выводить приватные методы классов (cast = 1).
            </li>       
             <li>
                <b>protected </b>(bool): (default <span style="color:{$true_c}">true</span>) - выводить защищённые методы классов (cast = 1)
            </li>
             <li>
                <b>trace</b>(bool): (default <span style="color:{$false_c}">false</span>) - в заголовок будет выведена трассировка
            </li>
             <li>
                <b>footer</b>(string): (default <span style="color:{$null_c}">empty</span>) - вывести доп информацию в Футер вывода
            </li>
             <li>
                <b>return</b>(bool): (default <span style="color:{$false_c}">false</span>) - <span style="color:{$true_c}">TRUE</span> - функция вернёт результат, <span style="color:{$false_c}">FALSE</span> - функция выведет результат.
            </li>
        </ul>
        <div style="padding: 5px 10px;">
            Пример использования <b>dump()</b> функции со всеми существующими параметрами конфигурации:
            <div><span style="color:{$var_c}">\$var</span> = new <b>TestClass</b><span style="color:$brace_c">()</span>;</div>
            <div>  
                <ul style="margin: 10px 0px; width: 100%; list-style: none;padding: 5px 10px;background-color: #e9e9e9;color: #605252;display: inline-block;">
                    <li style="margin-top: 5px; border-bottom: 1px solid #c7c7c7; padding: 5px">  
                        Вариант без конфигурации вывода (самый простой и явный вариант)<br />
                        <div style="padding-left: 10px;">
                            <b>dump</b><span style="color:{$brace_c}">(</span><span style="color:{$var_c}">\$var</span><span style="color:{$brace_c}">)</span>;<br />
                        </div>
                        <br />
                        $preset1
                    </li>
                    <li style="margin-top: 5px; border-bottom: 1px solid #c7c7c7;padding: 5px">  
                        Вариант с стилем из пресетов (и полным набором параметров конфигурации)<br />
                        <div style="padding-left: 10px;">
                            <b>dump</b><span style="color:{$brace_c}">(</span>["style"=>"red", "expand"=>false, "cast"=>1, "private"=>true, "protected"=>true, "trace"=>true, "footer"=>"Тестовое сообщение в футер вывода", "return"=>false], <span style="color:{$var_c}">\$var</span><span style="color:{$brace_c}">)</span>;
                        </div>
                        <br />
                        $preset2
                    </li>
                    <li style="margin-top: 5px;padding: 5px">  
                        Вариант с стилем из пресетов (без набора конфигурации)<br />
                        <div style="padding-left: 10px;">
                            <b>dump</b><span style="color:{$brace_c}">(</span>["style"=>["color"=>["head"=>"red","body"=>"red","footer"=>"red"], "bg"=>["head"=>"#000","body"=>"#222","footer"=>"#000"]], "footer"=>"Тестовое сообщение в футер вывода"], <span style="color:{$var_c}">\$var</span><span style="color:{$brace_c}">)</span>;
                        </div>
                        <br />
                        $preset3
                    </li>                
                </ul>
            </div>
        </div>
    </div>
</div>
EOD;
}
