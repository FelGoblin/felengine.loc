<?php

namespace Ext\ParamValue;

use Exception;
use Ext\ParamValue\_Interface\IInspectedClass;

final class ParamValue
{

    private static $instance;

    private $inspected;

    /**
     * ParamValue constructor.
     * @param $inspected
     * @throws Exception
     */
    public function __construct($inspected)
    {
        try {
            if (is_array($inspected)) {
                $arr = $inspected;
            } elseif (is_object($inspected)) {
                $arr = (array)$inspected;
            } elseif (file_exists($inspected)) {
                $arr = require $inspected;
            } else {
                throw new Exception('The data being checked cannot be inspected');
            }
            $this->makeInspect($arr);
        } catch (Exception $ex) {
            echo "<b>Inspected Error</b>: {$ex->getMessage()}";
        }
    }

    /**
     * @param array $inspected
     */
    private function makeInspect(array $inspected)
    {
        dump($inspected);
    }

    /**
     * @param $object
     * @throws \Exception
     */
    public static function create()
    {
        new ParamValue();
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function display(array $data = []): string
    {
        /*
        $data = self::extract(self::$required, $data);
        $data['inspect'] = self::$instance->inspected;
        $data['id'] = static::$id++;
        */
        //dump($data);
        return '';//parent::customTemplate('library.paramvalue', $data);
    }

    /**
     * @param IInspectedClass $object
     */
    public static function loadObject(IInspectedClass $object): void
    {
        self::$instance->inspected[] = $object;
        /*
        if (!is_object($object)) {
            throw new InvalidArgumentException(lang('@error_income_params_is_not_object'));
        }

        dump(['private' => true], $object);

        $inspected = get_object_vars($object);

        foreach ($inspected as $k => $v) {
            //dump($k, $v);
            if ($inspectedField !== null AND $inspectedField == $k) {
                self::$instance->inspected[self::$id++] = $v;
            } else {
                self::$instance->inspected[self::$id++][$k] = $v;
            }
        }
        */
    }

    private function getParamValue($param, $value): array
    {
        return [
            'type' => '',
            'param' => $param,
            'value' => $value
        ];
    }
}
