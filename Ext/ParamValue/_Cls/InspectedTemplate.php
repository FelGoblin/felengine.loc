<?php

namespace  Ext\ParamValue\_Cls;


final class InspectedTemplate
{
    public /* string */
        $title = '';
    public /* string */
        $description = '';
    public /*array of IInspectedNewParam*/
        $struct = [];

    public function __construct(array $template)
    {
        $this->title = $template['title'] ?? '@default_title';
        $this->description = $template['description'] ?? '@default_description';
        foreach ($template['struct'] ?? [] as $struct_element) {
            $this->struct[] = new InspectedNewParam($struct_element);
        }
    }

    /**
     *
     */
    public function build(): void
    {

    }
}