<?php


namespace Ext\ParamValue\_Cls;

use Engine\TemplatesLibrary\SiteLibraryElements\IInspectedNewParam;

final class InspectedNewParam implements IInspectedNewParam
{
    public /* string */
        $title;
    public /* string */
        $description;
    public /*string*/
        $param;
    public /*int*/
        $value;

    /**
     * InspectedNewParam constructor.
     * @param array $template
     */
    public function __construct(array $template)
    {
        $this->title = $template['title'] ?? '@default_title';
        $this->description = $template['description'] ?? '@default_description';
        $this->param = self::int;
        $this->value = $this->converter($template['value'] ?? '');
    }

    /**
     * Конвертируем
     * @param string $param
     * @return int
     */
    public function converter(string $param): int
    {
        switch ($param) {
            case '%bool%':
                return self::bool;
            case '%int%':
                return self::int;
            case '%string%':
                return self::string;
            default:
                return self::undefined;
        }
    }

    /**
     * @inheritDoc
     */
    public function build(): void
    {
        // TODO: Implement build() method.
    }
}