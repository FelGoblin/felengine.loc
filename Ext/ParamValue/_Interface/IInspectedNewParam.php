<?php


namespace Ext\ParamValue\_Interface;


interface IInspectedNewParam
{
    /**
     *
     */
    public const undefined = -1;
    public const bool = 0;
    public const int = 1;
    public const string = 2;

    /**
     * IInspectedNewParam constructor.
     * @param array $template
     */
    public function __construct(array $template);

    /**
     * @param string $param
     * @return int
     */
    public function converter(string $param): int;

    /**
     *
     */
    public function build(): void;
}