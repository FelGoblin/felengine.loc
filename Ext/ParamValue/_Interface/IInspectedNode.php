<?php


namespace Ext\ParamValue\_Interface;

/**
 * Исследуемый узел (файл конфигурации)
 * Class IInspectedNode
 * @package Engine\TemplatesLibrary\SiteLibraryElements
 */
interface IInspectedNode
{
    /**
     * Текущий ID узла (файла) - уникальное имя
     * @return string
     */
    public function getId(): string;

    /**
     * Имя узла в текущем классе инспекции.
     * @return string
     */
    public function getNodeName(): string;

    /**
     * Текущий файл конфигурации.
     * @return string
     */
    public function getFile(): string;

    /**
     * Расширение файла (он же тип сохраняемой конфигурации)
     * @return string
     */
    public function getExt(): string;

    /**
     * Массив иследуемых параметров.
     * @return array
     */
    public function getParam(): array;
}