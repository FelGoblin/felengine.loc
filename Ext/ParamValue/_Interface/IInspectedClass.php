<?php

namespace Ext\ParamValue\_Interface;

interface IInspectedClass
{
    /**
     * Ид
     * @return string
     */
    public function getId(): string;

    /**
     * Заголовок
     * @return string
     */
    public function getTitle(): string;

    /**
     * Описание класса
     * @return string
     */
    public function getDescription(): string;

    /**
     * Можно ли добавлять новые узлы в параметры (узел = новый файл)
     * @return bool
     */
    public function isCanNodeInsert(): bool;

    /**
     * @return bool
     */
    public function isCanNewParamValue(): bool;

    /**
     * Массив с исследуемыми параметрами
     * @return array
     */
    public function getInspected(): array;

    /**
     * Сохранить текущий класс
     * @param IInspectedNode|null $node
     * @return bool
     */
    function save($node = null): bool;
}