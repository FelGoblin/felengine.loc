<?php

namespace Engine\Exceptions;

use Exception;

class ConfigException extends Exception
{
    public function __construct(string $message = "", int $code = 0)
    {
        parent::__construct($message, $code);
    }
}