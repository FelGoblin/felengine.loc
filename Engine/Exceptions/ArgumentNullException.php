<?php

namespace Engine\Exceptions;

use Exception;

class ArgumentNullException extends Exception
{
    protected $emptyParams = [];

    /**
     * @return array
     */
    public function getEmptyParams(): array
    {
        return $this->emptyParams;
    }

    public function __construct(string $message = "", array $param = [], int $code = 0)
    {
        $this->emptyParams = $param;
        parent::__construct($message, $code);
    }
}