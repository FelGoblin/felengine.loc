<?php

namespace Engine\Exceptions;


use Exception;
use Engine\Core\View\SiteComponent\IComponent;

class SiteComponentException extends Exception
{
    /**
     * @var IComponent
     */
    private $component;

    /**
     * SiteComponentException constructor.
     * @param string $message
     * @param IComponent $component
     */
    public function __construct(IComponent $component, string $message = "")
    {
        $this->component = $component;
        parent::__construct($message);
    }

    /**
     * @return IComponent
     */
    public function getComponent(): IComponent
    {
        return $this->component;
    }
}