<?php

namespace Engine\Exceptions;

use Exception;

class ComponentException extends Exception
{
    public function __construct(string $message = "", int $code = 0)
    {
        parent::__construct($message, $code);
    }
}