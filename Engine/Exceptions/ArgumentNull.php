<?php

namespace Engine\Exceptions;


class ArgumentNull
{
    /**
     * ArgumentNull constructor.
     * @param string $message
     * @throws ArgumentNullException
     */
    public function __construct(string $message)
    {
        throw new ArgumentNullException($message);
    }
}