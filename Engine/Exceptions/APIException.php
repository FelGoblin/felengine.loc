<?php

namespace Engine\Exceptions;

use Exception;

class APIException extends Exception
{
    /**
     * APIException constructor.
     * @param string $message
     * @param int $code
     * @param bool $header
     * @throws APIHeaderException
     */
    public function __construct(string $message = "", int $code = 0, bool $header = false)
    {
        if ($header) {
            throw new APIHeaderException($message, $code);
        }
        parent::__construct($message);
    }
}