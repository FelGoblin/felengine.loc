<?php

namespace Engine;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Core;
use Engine\Core\SystemModule\HomePage;
use Engine\Core\View\Render;
use Engine\Modules\BaseSiteModule;
use Exception;
use Namespaces\Site\Controller\HomeController;
use ReflectionException;

abstract class Controller implements IController
{
    /**
     * @var string
     */
    protected static string $homeController = '\\Engine\\Namespaces\\Site\\Controller\\HomeController';

    /**
     * @param BaseSiteModule|null $mod
     * @param null $data
     * @return IController|null
     * @throws Exceptions\SiteComponentException
     * @throws ReflectionException
     */
    public function home(BaseSiteModule $mod = null, $data = null): ?IController
    {
        return $this->main(new HomePage(), $data);
    }

    /**
     * Слишком много переделывать
     * @param BaseSiteModule|null $mod
     * @param null $data
     * @return IController|null
     * @throws Exceptions\SiteComponentException
     * @throws ReflectionException
     */
    public function main(BaseSiteModule $mod = null, $data = null): ?IController
    {
        if (is_array($data)) {
            $mod->setDisplayData($data, false);
        }
        $mod->build();
        return $this;
    }

    /**
     * @param string $module
     * @param bool $doRender
     * @return IController|null
     * @throws Exception
     */
    protected function checkModule(string $module, bool $doRender = true): ?IController
    {
        $check = (bool)SiteConfig::isModule($module);//, $group, false);
        if (!$check) {
            Render::setError("Данный модуль сайта [$module] не доступен.");
            if ($doRender) {
                return (new HomeController())->main();
            }
        }
        return null;
    }

    /**
     * @param string $permission
     * @param bool $doRender
     * @return bool
     * @throws Exception
     */
    protected function checkAccess(string $permission, bool $doRender = true): bool
    {
        $user = Core::getThisUser();
        if ($user == null) {
            return false;
        }

        $check = $user->userAccessCheck($permission);
        if (!$check AND $doRender) {
            $this->accessDeny($this);
        }
        return $check;
    }

    /**
     * @param IController $controller
     * @param string|null $message
     */
    protected function accessDeny(IController $controller, string $message = null): void
    {
        Render::setError($message ?? 'Данная функция пользователю недоступна.');
        $controller->main(); //вызовем майн контроллер
    }

    /**
     * @param IController $controller
     */
    protected function moduleDisable(IController $controller): void
    {
        Render::setError('Данный модуль сайта не доступен.');
        $controller->main(); //вызовем майн контроллер
    }

    /**
     * @param null $out
     *
     * public function doRender($out = null)
     * {
     * if ($out != null)
     * $this->out = $out;
     *
     * Render::setOutData($this->out);
     * }
     */
}
