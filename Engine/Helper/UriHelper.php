<?php

namespace Engine\Helper;


use Engine\Core\Http\Request;

class UriHelper
{
    static function getProtocol(): string
    {
        return 'http' . (Request::https() ? 's' : '');
    }

    /**
     * @return bool
     */
    static function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    static function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return bool|string
     */
    static function getPathUrl($pathUrl = null)
    {
        if ($pathUrl == null) {
            $pathUrl = $_SERVER['REQUEST_URI'];
        }
        if ($position = strpos($pathUrl, '?')) {

            $pathUrl = substr($pathUrl, 0, $position);
        }
        if (substr($pathUrl, -1) !== '/')
            $pathUrl .= '/';

        return $pathUrl;
    }
}