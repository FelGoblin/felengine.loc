<?php

namespace Engine\Helper;

class FileSystem
{

    public static function createDir($whole_path, &$result = null): bool
    {
        $dirs = explode('/', trim($whole_path, '/'));
        $current = '';
        $i = 0;
        foreach ($dirs as $dir) {
            if (strlen($dir) == 0)
                continue;
            $current .= $dir;
            if (!is_dir($current)) {
                try {
                    @mkdir('./' . $current);
                    $i++;
                } catch (\Exception $ex) {
                    $result = $ex->getMessage();
                    return false;
                }
            }
            $current .= '/';
        }
        $result = $i; //количество созданных деректорий
        return true;
    }

    public static function deleteDir($path)
    {
        if (!@is_dir($path))
            return;
        if ($handle = @opendir($path)) {

            while (false !== ($file = @readdir($handle))) {
                if ($file == '.' || $file == '..')
                    continue;
                $file = self::ptSep($path) . $file;
                if (@is_dir($file)) {
                    self::deleteDir($file);
                    @rmdir($file);
                } else {
                    @unlink($file);
                }
            }

            @closedir($handle);
            @rmdir($path);
        }
    }

    private static function fileList($path, $with_dir, &$file_list)
    {
        if ($handle = opendir($path)) {
            $ok = true;
            while (false !== ($file = readdir($handle))) {
                if ($file == '.' || $file == '..')
                    continue;

                $file = self::ptSep($path) . $file;

                if (is_dir($file)) {
                    if ($with_dir)
                        $file_list[] = $file; // iconv('windows-1251', 'UTF-8',$file);
                    if (!self::fileList($file, $with_dir, $file_list)) {
                        $ok = false;
                        break;
                    }
                } else {
                    $file_list[] = $file; // iconv('windows-1251', 'UTF-8',$file);
                }
            }
            closedir($handle);
            return $ok;
        } else
            return false;
    }

    public static function getFiles($path, $with_dir = false)
    {
        if (!is_dir($path))
            return NULL;
        $file_list = array();
        if (!self::fileList($path, $with_dir, $file_list))
            return null;
        return $file_list;
    }

}
