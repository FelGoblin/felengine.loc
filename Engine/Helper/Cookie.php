<?php
namespace Engine\Helper;

/**
 * Class Cookie
 * @package Flexi\Helper
 */
class Cookie {

    /**
     * @param string $key
     * @param string $value
     * @param string|null $domain
     * @param int $days
     * @param bool $http
     */
    public static function set(string $key, string $value, string $domain = null, $days = 31, bool $http = true): void
    {
        $expire =  $days >0 ? (int)time() + $days * 24 * 3600: time() - 1;
        setcookie($key, $value, $expire, '/', $domain ?? $_SERVER['HTTP_HOST'], false, $http) ;
        if ($days >= 0)
            $_COOKIE[$key] = $value;
    }

    /**
     * Get cookies by key
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
        return null;
    }

    /**
     * Delete cookies by key or keys
     * @param mixed $key
     * @param string|null $domain
     */
    public static function delete($key,string $domain = null)
    {
        if (is_array($key)){
            foreach ($key as $value)
            {
                self::delete($value, $domain);
            }
        }
        elseif (is_string($key)) {
            if (isset($_COOKIE[$key])) {
                self::set($key, '', $domain ?? $_SERVER['HTTP_HOST'], -1);
                unset($_COOKIE[$key]);
            }
        }
    }
}
