<?php

use Engine\Localization\Localization;
use Engine\Modules\IService;

const INSTANCE_SYSTEM = 'system';

const INSTANCE_EXTERNAL = 'external';

const INSTANCE_MOD = 'mod-component';

const INSTANCE_MOD_DEPENDENCY = 'mod-dependence';
/**
 * @param $num
 * @param int $dec
 * @return float|int
 */
function floor_f($num, $dec = 0)
{
    return floor($num * pow(10, $dec)) / pow(10, $dec);
}

/**
 * @param $condition
 * @param bool|string|int|callable $trueResult
 * @param bool|string|int|callable $falseResult
 * @param bool $dump
 * @return string|null
 */
function _assert($condition, $trueResult = true, $falseResult = false, bool $dump = false)
{
    if (is_array($condition)) {
        list($data, $field) = $condition;
        if (count($condition) == 2) {
            $eq = '_undefined_value_equal_';
        } elseif (count($condition) == 3) {
            list($data, $field, $eq) = $condition;
        } else {
            $eq = '_undefined_value_equal_';
            $field = 0;
        }
        if (isset($data[$field])) {
            if ($eq != '_undefined_value_equal_') {
                $condition = $data[$field] === $eq;
            } else {
                //return _assert($data[$field], $trueResult, $falseResult, $dump);
                $condition = $data[$field];
            }
            $result = is_bool($condition) ? $condition : true;
        } else {
            $result = false;
        }
    } elseif (is_numeric($condition)) {
        $result = true;
    } elseif (is_bool($condition)) {
        $result = $condition === true;
    } elseif (is_null($condition)) {
        $result = false;
    } else {
        $result = true;
    }

    if ($result === true) {
        if (fixIsCallable($trueResult)) {
            return $trueResult($condition);
        } elseif (is_string($trueResult)) {
            return str_replace('$1', $condition, $trueResult);
        } else {
            return $trueResult;
        }
    } else {
        if (fixIsCallable($falseResult)) {
            return $falseResult($condition);
        } else {
            return $falseResult;
        }
    }
    //(fixIsCallable($trueResult) ? $trueResult() : str_replace('$1', $condition, $trueResult)) : $falseResult;
}

/**
 * --
 * PRETTY var_export with php 5.4 array type
 *      array() => []
 * --
 * @param $var
 * @param string $indent
 * @return mixed|string
 */
function pretty_var_export($var, $indent = "")
{
    switch (gettype($var)) {
        case "string":
            return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
        case "array":
            $indexed = array_keys($var) === range(0, count($var) - 1);
            $r = [];
            foreach ($var as $key => $value) {
                $r[] = "$indent    "
                    . ($indexed ? "" : pretty_var_export($key) . " => ")
                    . pretty_var_export($value, "$indent    ");
            }
            return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
        case "boolean":
            return $var ? "TRUE" : "FALSE";
        default:
            return var_export($var, TRUE);
    }
}

/**
 * Локализация
 * @param string $key
 * @param bool $upperFirst
 * @return string
 */
function lang(string $key, bool $upperFirst = true): string
{
    return Localization::getInstance()->translate($key, $upperFirst);
}

/**
 * @return string
 */
function langF(): string
{
    $args = func_get_args();
    $key = array_shift($args);
    return vsprintf(Localization::getInstance()->translate($key, true), $args);
}

function mb_ucfirst($string, $encoding = 'utf8')
{
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

/**
 * Преобразовать наклонные черты в черты путей текущей ОС
 * @param string|null $str
 * @param bool $ifNull
 * @return string
 */
function pathSeparator(string $str = null, bool $ifNull = false): string
{
    if ($str == null) {
        if ($ifNull) {
            return DIRECTORY_SEPARATOR;
        }
        return '';
    } else {
        return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $str);
    }
}

/**
 * Аналогично pathSeparator, но можно передавать пути с форматированием передавать
 * @return string
 */
function pathSeparatorF(): string
{
    $data = func_get_args();
    $str = array_shift($data);
    return pathSeparator(vsprintf($str, $data));
}

/**
 * Удалитьтпробелы в значениях внутри массива
 * @param $Input
 * @return array|string
 */
function trimArray($Input)
{

    if (!is_array($Input))
        return trim($Input);

    return array_map('trimArray', $Input);
}

function getProvider(string $service): IService
{
    return new $service();
}

/**
 * @param array|null $arr1
 * @param array|null $arr2
 * @return bool
 */
function array_compare(?array $arr1, ?array $arr2): bool
{
    if (($arr1 == null AND $arr2 == null) OR (empty($arr1) AND empty($arr2))) //пустые массивы. Они  равны
        return true;

    if ($arr1 == null OR $arr2 == null or (empty($arr1) AND !empty($arr2)) or (!empty($arr1) AND empty($arr2))) {
        return false;
    }
    $len = max(count($arr1), count($arr2));

    return (count(array_intersect($arr1, $arr2)) == $len);
}

/**
 * Рекурсивное слияние массивов
 * @param array $array1
 * @param array $array2
 * @return array
 */
function array_merge_recursive_distinct(array &$array1, array &$array2)
{
    $merged = $array1;

    foreach ($array2 as $key => &$value) {
        if (is_array($value) && isset ($merged [$key]) && is_array($merged [$key])) {
            $merged [$key] = array_merge_recursive_distinct($merged [$key], $value);
        } else {
            $merged [$key] = $value;
        }
    }

    return $merged;
}

/**
 * Преобразует индекс массива по значениям указанных колонок внутренних массивов.
 *    a = array(
 *           0 => (a=>A,b=>B,c=>C),
 *           1 => (a=>D,b=>E,c=>F),
 *           2 => (a=>G,b=>imageHeigth,c=>I)
 *  )
 *  array_do_column (a,'a');
 *      return array (
 *          A => (a=>A,b=>B,c=>C),
 *          D => (a=>D,b=>E,c=>F),
 *          G => (a=>G,b=>imageHeigth,c=>I)
 *  )
 * @param array $array
 * @param string $key
 * @return array
 */
function array_do_column(array $array, string $key)
{
    $result = array();
    foreach ($array as $val) {
        if (is_array($val)) {
            foreach ($val as $k => $v) {
                if ($key == $k) {
                    $result[$v][] = $val;
                    break;
                }
            }
        } else continue;
    }
    return $result;
}

/**
 * Собрать массив из значений колонок
 *  a = [
 *      a1 => [id=1, test=2],
 *      a2 => [id=2, test=2],
 *      a3 => [id=3, test=2],
 * ]
 * array_column_ext(a, 'id') => [1, 2, 3]
 * @param array $array
 * @param string $key
 * @return array
 */
function array_column_ext(array $array, string $key)
{
    $result = [];
    foreach ($array as $val) {
        if (is_array($val)) {
            foreach ($val as $k => $v) {
                if ($key == $k) {
                    $result[] = $val;
                    break;
                }
            }
        } elseif (is_object($val) AND property_exists($val, $key)) {
            $result[] = $val->$key;
        } else continue;
    }
    return $result;
}

/**
 * Удалить из массива элемент по значению.
 *
 * @param $value
 * @param $array
 */
function array_deleteValue($value, &$array)
{
    if (($key = array_search($value, $array)) !== false) {
        unset($array[$key]);
    }
}

/**
 * Получить сумму в массиве (аналог array_sum, но корректно суммирует отформатированные number_format пробелы в цифре)
 * @param $array
 * @param bool $format
 * @return int
 */
function array_getsum($array, bool $format = false)
{
    $a = 0;

    array_map(function ($key, $b) use (&$a) {
        $a += str_replace(' ', '', $b);
    }, array_keys($array), $array);

    if ($format)
        return number_format((float)$a, 0, '.', ' ');
    else
        return $a;
}

/**
 * Предобразуем массив из
 *  [
 *      key=>[f1,f2..fN],
 *      key2=>[a1,a2..aN]
 * ]
 *  в
 * [
 *      0=>[key=>f1, key2=>a1],
 *      1=>[key=>f2, key2=>a2],
 *      ...,
 *      N=>[key=>fN, key2=>aN]
 * ]
 * @param $vector
 * @return array
 */
function array_normalize($vector)
{
    $result = array();
    foreach ($vector as $key1 => $value1)
        foreach ($value1 ?? [] as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

/**
 * Поиск первого вхождения в массиве
 * @param array $array
 * @param string $field
 * @param $search
 * @return mixed|null
 */
function array_find_first(array $array, string $field, $search)
{
    return array_values(array_filter($array, function ($el) use ($field, $search) {
            return $el[$field] == $search;
        }))[0] ?? null;
}

/**
 * Поучить новый массив из текущего, кно используя только указанные ключи
 * @param array $array Исходный массив
 * @param array $keys Набор ключей, ко которым извлекаются данные
 * @param array $append Массив будет добавлен в новый
 * @param bool $delete Удалить эти данные их исходного массива
 * @return array
 */
function array_extract_by_keys(array &$array, array $keys, array $append = [], bool $delete = false): array
{
    $result = [];
    foreach ($keys as $key) {
        if (isset($array[$key])) {
            $result[$key] = $array[$key];
            if ($delete) {
                unset($array[$key]);
            }
        }
    }
    if (!empty($append)) {
        return array_merge_recursive($result, $append);
    }

    return $result;
}

/**
 * СОртировка массива
 * @param $array
 * @param $on
 * @param int $order
 * @return array
 */
function array_sort($array, $on, $order = SORT_ASC)
{

    $new_array = array();
    $sortable_array = array();

    if (count($array) == 0)
        return [];


    foreach ($array as $k => $v) {

        if (is_array($v)) {
            foreach ($v as $k2 => $v2) {
                if ($k2 == $on) {
                    $sortable_array[$k] = $v2;
                }
            }
        } elseif (is_object($v)) {
            $sortable_array[$k] = [
                $on => $v->getParam($on),
                $v
            ];
        } else {
            $sortable_array[$k] = $v;
        }
    }
    switch ($order) {
        case SORT_ASC:
            asort($sortable_array);
            break;
        case SORT_DESC:
            arsort($sortable_array);
            break;
    }

    //dump($sortable_array);

    foreach ($sortable_array as $k => $v) {
        $new_array[$k] = $array[$k];
    }

    return $new_array;
}

/**
 * Преобразовать в строку. обхекты приводятся к пустой строке
 * @param $el
 * @param bool $notNull
 * @return string
 */
function toString($el, bool $notNull = true): ?string
{
    if ($el === 0) {
        return "0";
    } elseif (is_string($el))
        return $el;
    elseif (is_numeric($el))
        return (string)$el;
    elseif (is_array($el)) {
        if (empty($el)) {
            if ($notNull)
                return '';
            return null;
        }
        return '';
    } /*
    elseif ($el == '')
        return '';
    */
    else {
        if ($notNull)
            return '';
        return null;
    }
}

/**
 * Преобразовать в булевое
 * @param $el
 * @return bool
 */
function toBool($el): bool
{
    if (is_bool($el)) {
        return $el;
    } elseif (is_numeric($el)) {
        if ($el <= 0) {
            return false;
        } else {
            return true;
        }
    } elseif (is_string($el)) {
        switch (strtolower($el)) {
            case 'true':
            case 'on':
                return true;
                break;
            case 'false':
            case 'off':
            default:
                return false;
                break;
        }
    } elseif (is_array($el)) {
        return !empty($el);
    } else {
        return false;
    }
}

//############# random function ###############
/**
 * Получить случяайное число в диапазоне.
 *
 * @param int $min
 * @param int $max
 * @return int
 * @throws Exception
 */
function random(int $min = 0, int $max = 100)
{
    return random_int($min, $max);
}

/**
 * Получить вероятность процента от 100
 *
 * @param $proc
 * @return bool
 * @throws Exception
 */
function procRand(int $proc): bool
{
    return (random() < $proc);
}

/**
 * Получить шанс с плавающей точкой процента от 100F
 *
 * @param float $proc
 * @return bool
 * @throws Exception
 */
function procFRand(float $proc): bool
{
    $rnd = random(0, 1000000) / 10000;
    return ($rnd < $proc);
}


/**
 *  Разбить массив (аналог explode), но если указать массив ключей, то вернется массив с переданными ключами и получившимися значениями
 *
 * @param string $delimiter
 * @param string $string
 * @param array $keys
 * @return array
 *     $srtring = 'string, string2, string3';
 *      extExplode(',', $string, array('key1','key2','key3')) = array
 *      (
 *      'key1'= 'string1',
 *      'key3'= 'string2',
 *      'key3'= 'string3',
 *      )
 */
function extExplode(string $delimiter, string $string, array $keys = []): array
{
    if (!empty($string))
        $string = trim($string);

    if (empty($keys))
        return explode($delimiter, $string);

    else {
        $expl = explode($delimiter, $string);
        if (count($keys) > count($expl))
            $keys = array_slice($keys, 0, count($expl));
        elseif (count($keys) < count($expl))
            return $expl;//$keys = array_pad($keys,count($expl),'unknown');
        //dump($expl,$keys);
        return array_combine($keys, $expl);
    }
}

function clearString(string $str): string
{
    return strtolower(trim($str));
}

/**
 * Получить булевое сравнение данных.
 *
 * @param $mainBool
 * @param $incomeBool
 * @param string $operator
 * @return bool
 */
function andOr($mainBool, $incomeBool, $operator = 'and'): bool
{
    //dump($mainBool, $incomeBool, $operator);
    if (is_string($operator)) {
        $operator = strtolower($operator) == 'or';
    }

    if (is_null($mainBool) AND !is_null($incomeBool)) {
        return $incomeBool;
    } elseif (is_null($mainBool) AND is_null($incomeBool)) {
        return false;
    } elseif ($operator) {
        return ($incomeBool OR $mainBool);
    } else {
        return ($incomeBool AND $mainBool);
    }
}

/**
 * Распаковать строку в массив
 *
 * @param $string
 * @return array
 */
function deCompress(string $string): array
{
    return unserialize(@gzuncompress(base64_decode($string))); //return array
}

/**
 * Упаковать массив в строку
 *
 * @param array $array
 * @return string
 */
function compress(array $array): string
{
    return base64_encode(gzcompress(serialize($array))); //return string
}

/**
 * Поиск файла по имени (TODO: добавить маску)
 * @param $dir
 * @param $toSearch
 * @return string
 */
function findFile($dir, $toSearch): string
{
    $files = array_diff(scandir($dir), Array(".", ".."));
    foreach ($files as $d) {
        if (!is_dir($dir . "/" . $d)) {
            if ($d == $toSearch)
                return $dir . "/" . $d;
        } else {
            $res = findFile($dir . "/" . $d, $toSearch);
            if ($res)
                return $res;
        }
    }
    return false;
}

function filesizeFormat($file): string
{
    if (!file_exists($file)) {
        return '-не найден -';
    } else {
        return convertsize(filesize($file));
    }
}

function convertsize($size)
{
    if ($size == 0) {
        return '0 Б';
    }

    $times = 0;
    $comma = '.';
    while ($size > 1024) {
        $times++;
        $size = $size / 1024;
    }
    $size2 = floor($size);
    $rest = $size - $size2;
    $rest = $rest * 100;
    $decimal = floor($rest);

    $addsize = $decimal;
    if ($decimal < 10) {
        $addsize .= '0';
    };
    if ($times == 0) {
        $addsize = $size2;
    } else {
        $addsize = $size2 . $comma . substr($addsize, 0, 2);
    }
    switch ($times) {
        case 0 :
            $mega = ' Б';
            break;
        case 1 :
            $mega = ' КБ';
            break;
        case 2 :
            $mega = ' МБ';
            break;
        case 3 :
            $mega = ' ГБ';
            break;
        case 4 :
            $mega = ' ТБ';
            break;
    }
    $addsize .= $mega;
    return $addsize;
}

function cast($param, $format)
{

    switch ($format) {
        case 's':
        case 'str':
        case 'string':
            return toString($param);

        case 'nullstr':
        case '?s':
        case '?str':
        case '?string':
            return toString($param, false);

        case 'float':
        case 'f':
        case 'd':
            return (float)$param;

        case 'i':
        case 'numeric':
        case 'num':
        case 'int':
        case 'integer':
            return (int)$param;

        case '?i':
        case '?numeric':
        case '?num':
        case '?int':
        case '?integer':
            if (is_null($param))
                return null;
            return (int)$param;

        case 'b':
        case 'bool':
        case 'boolean':
            return toBool($param);

        case 'a':
        case 'array':
            if (is_array($param))
                return $param;
            else
                return [$param];

        default:
            return $param;
    }
}

/**
 * @param string $btnExp
 * @param string $btnCol
 * @param string|null $curButton
 * @param bool $float
 * @return string
 */
function getToggleButton(string $btnExp = '[+]', string $btnCol = '[-]', string $curButton = null, bool $float = false): string
{
    if ($curButton == null) {
        $curButton = $btnCol;
    }
    $js = "(function(el){let next=el.parentNode.nextElementSibling;if(next.style['display']=='none'){next.style['display']='';el.innerHTML='$btnCol';}else{next.style['display']='none';el.innerHTML='$btnExp';}})(this);";
    if ($float) {
        return '<div style="width=20px;float:right;cursor:pointer;" onClick="' . $js . '">' . $curButton . '</div>';
    } else {
        return '<div style="color:' . DumpStyle::EXPANDERDCOLOR . ' ;width=20px;display: inline-block;cursor:pointer;" onClick="' . $js . '">' . $curButton . '</div>';
    }
}

/**
 * Конвертер длины в метрах в сантиметры и ммм
 * @param float $length
 * @return string
 */
function mConvert(float $length): string
{
    $length = abs($length);
    if ($length < 1) {
        if ($length < 0.01) { //mm
            return sprintf('%d мм', ($length * 1000));
        } else { //cm
            return sprintf('%.2f см', ($length * 100));
        }
    } else {
        return sprintf('%.2f м', $length);
    }
}

/**
 * КОнвертер веса в кг или гр.
 * @param float $weight
 * @return string
 */
function wConvert(float $weight): string
{
    if ($weight < 1) {
        return sprintf('%d гр', ($weight * 1000));
    } else {
        return sprintf('%.2f кг', $weight);
    }
}

/**
 * Транслитерация кириллического текста в латиницу
 * @param $str
 * @param bool $onlyLower
 * @param bool $trim
 * @return mixed
 */
function translit($str, bool $onlyLower = false, bool $trim = false)
{
    $rus = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
    $lat = ['a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya'];
    if ($onlyLower) {
        $str = mb_strtolower($str);
    } else {
        $rus = array_merge($rus, ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я']);
        $lat = array_merge($lat, ['A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'topRightPadding', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'imageHeigth', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya']);
    }
    if ($trim) {
        $rus = array_merge($rus, [' ']);
        $lat = array_merge($lat, ['_']);
    }
    return str_replace($rus, $lat, $str);
}

/**
 * УБрать некоторые моменты с проверкой на вызов is_callable
 * @param $var
 * @return bool
 */
function fixIsCallable($var): bool
{
    $blacklist = [
        'system',
        'user'
    ];
    foreach ($blacklist as $deny) {
        if ($var === $deny) {
            $var = str_replace($deny, '#empty', $var);
        }
    }
    return is_callable($var);
}

/**
 * Генератор ID
 * @return string
 */
function guidv4()
{
    if (function_exists('com_create_guid') === true)
        return trim(com_create_guid(), '{}');

    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}