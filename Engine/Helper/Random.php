<?php

namespace Engine\Helper;


use Engine\Core\Development\DebugInfo;
use Exception;

class Random
{
    /**
     * Выпавшее число
     *
     * @var
     */
    public static $rand;

    /**
     * Создать массив заполненный случайными числами
     * @param $N
     * @param int $min
     * @param int $max
     * @return array
     */
    public static function randarr($arrayLength, $min = 0, $max = 100)
    {
        return array_map(
            function () use ($min, $max) {
                return rand($min, $max);
            },
            array_pad([], $arrayLength, 0)
        );
    }

    /**
     * Получить вероятность для шанса с плавающей запятой (если нужна точность)
     *
     * @param float $proc
     * @param bool $include
     * @param bool $geneation
     * @return bool
     * @throws \Exception
     */
    public static function chanceF(float $proc, bool $include = true, bool $geneation = true): bool
    {
        if ($proc <= 0)
            return false;
        elseif ($proc >= 100) {
            return true;
        } else {
            if ($geneation) {
                self::$rand = self::randomFloat();
            }
            return $include ? (self::$rand <= $proc) : (self::$rand < $proc);
        }
    }

    /**
     * @param float $min
     * @param float $max
     * @param int $precision
     * @return float
     */
    public static function randomFloat(float $min = 0, float $max = 100, int $precision = 0): float
    {
        if ($min == 0 AND $max == 0) {
            return self::setRand(self::_randomF($precision));
        }
        $num = ($min + self::_randomF() * (abs($max - $min)));
        if ($precision > 0) {
            $num = floor_f($num, $precision);
        }
        return self::setRand($num);
    }

    /**
     * @param mixed $rand
     * @return float
     */
    public static function setRand($rand): float
    {
        if (class_exists('DebugInfo')) {
            DebugInfo::debugAddF(lang('@random_number'), $rand);
        }
        return (self::$rand = $rand);
    }

    /**
     * ## Alias lcg_value() ##
     * @param int $precision
     * @return float
     */
    private static function _randomF(int $precision = 0): float
    {
        try {
            $num = random_int(0, 1000000) / 1000000;
            if ($precision > 0) {
                return floor_f($num, $precision);
            }
            return $num;
        } catch (Exception $ex) {
            return 0;
        }
    }

    /**
     * Получить случайное число из массива
     *
     * @param $array
     * @param int $count
     * @return null
     */
    public static function fromArray($array, int $count = 0)
    {
        if (empty($array)) {
            return null;
        }
        if ($count > 1) {
            self::$rand = array_rand($array, $count);
            return array_extract_by_keys($array, self::$rand);
        } else {
            self::$rand = array_rand($array);
            return $array[self::$rand];
        }
    }

    /**
     * Случайное булевое значение
     *
     * @return bool
     * @throws \Exception
     */
    public static function randomBool(): bool
    {
        return self::chance(50);
    }

    /**
     * Получить вероятность при шансе
     *
     * @param int $proc - Вероятность от 0% до 100%
     * @param bool $include
     * @return bool
     * @throws \Exception
     */
    public static function chance(int $proc, bool $include = true): bool
    {
        if ($proc <= 0)
            return false;
        elseif ($proc >= 100) {
            return true;
        } else {
            return $include ? (self::random() <= $proc) : (self::random() < $proc);
        }
    }

    /**
     * Получить случяайное число в диапазоне.
     *
     * @param int $min
     * @param int $max
     * @return int
     * @throws Exception
     */
    public static function random(int $min = 0, int $max = 100): int
    {
        return (int)self::setRand(random_int($min, $max));
    }
}