<?php


namespace Engine\Helper;


use Exception;

final class Graph
{
    /**
     * @var resource|null
     */
    private $img = null;

    private $opt = [
        'countYLines' => 10,
        'color' => [],
        'fontWidth' => 2,
        'width' => 800,
        'height' => 600
    ];

    private $fontWidth;

    private $data = [];

    private $defaultColors = [
        'imgBg' => [255, 255, 255],
        'graphBg' => [231, 231, 231],
        'axesColor' => [212, 212, 212],
        'graphGrid' => [184, 184, 184],
        'fontColor' => [236, 136, 136],
        'fontGraphColor' => [0, 0, 0],
        'graphColor' => [65, 170, 191]
    ];

    private $imgColors = [];

    private $padding = [
        'left' => 8,
        'top' => 5,
        'right' => 15,
        'bottom' => 20
    ];
    /**
     * Graph Real Coords
     * @var array
     */
    private $grc = [
        'x0' => 0,
        'y0' => 0,
        'width' => 0,
        'height' => 0
    ];
    /**
     * @var float|int
     */
    private $textWidth = 0;

    /**
     * Graph constructor.
     * @param array $data
     * @param array $opt Настройки
     * @param array $colors
     */
    public function __construct(array $data, array $opt = [], array $colors = [])
    {
        $this->data = $data;
        $this->opt = array_merge($this->opt, $opt);
        $this->defaultColors = array_merge($this->defaultColors, $colors);
        $this->img = imagecreate($this->opt['width'], $this->opt['height']);
        // Ширина одного символа
        $this->fontWidth = imagefontwidth($this->opt['fontWidth']);
    }

    /**
     * Построить
     * @param bool $asHeader
     * @return false|string
     * @throws Exception
     */
    public function build(bool $asHeader = true)
    {

        if (empty($this->data)) {
            throw new Exception('Graph data is empty');
        }

        $this->setColors();
        $this->fixLeftPadding();
        $this->setGraphRealCoords();
        $this->buildMainBorder();

        $step = $this->grc['height'] / $this->opt['countYLines'];

        $this->buildGrid($step);
        //$this->printLabel($step);

        $this->buildGraph();
        $result = null;
        //dump($this->graphRealCoords);
        if ($asHeader) {
            header("Content-Type: image/png");
            ImagePNG($this->img);
        } else {
            ob_start();
            // Генерация изображения
            ImagePNG($this->img);
            $result = base64_encode(ob_get_contents());
            ob_end_clean();
        }
        imagedestroy($this->img);
        return $result;
    }

    /**
     * Установить цвета
     */
    private function setColors(): void
    {
        foreach ($this->defaultColors as $k => $color) {
            $this->imgColors[$k] = $this->getColor($color);
        }
    }

    /**
     * @param string $target
     * @return false|int|null
     */
    private function getColor($target)
    {
        if (is_array($target)) {
            $color = $target;
        } elseif (is_string($target)) {
            $color = $this->opt['color'][$target] ?? $this->defaultColors[$target] ?? null;
        }
        if ($color == null) {
            return null;
        }
        if (count($color) == 4) {//rgba
            list($r, $g, $b, $a) = $color;
            return $this->rgba($r, $g, $b, $a);
        } elseif (count($color) == 3) {//
            list($r, $g, $b) = $color;
            return $this->rgb($r, $g, $b);
        } else {
            return null;
        }
    }

    /**
     * @param int $r
     * @param int $g
     * @param int $b
     * @param int $a
     * @return false|int
     */
    private function rgba(int $r, int $g, int $b, int $a)
    {
        return imagecolorallocatealpha($this->img, $r, $g, $b, $a);
    }

    /**
     * @param int $r
     * @param int $g
     * @param int $b
     * @return false|int
     */
    private function rgb(int $r, int $g, int $b)
    {
        return imagecolorallocate($this->img, $r, $g, $b);
    }

    /**
     * меняем левый отступ
     */
    private function fixLeftPadding(): void
    {
        $text_width = 0;
        $maxValue = $this->maxValue();
        // Вывод подписей по оси Y
        for ($i = 1; $i <= $this->opt['countYLines']; $i++) {
            $val = floor_f(($maxValue / $this->opt['countYLines']) * $i, 2);
            $strl = strlen($val) * $this->fontWidth;
            if ($strl > $text_width) $text_width = $strl;
        }
        $this->textWidth = $text_width;
        $this->padding['left'] += $text_width;
    }

    /**
     * @param bool $inc Увеличение максимального значения на 10%
     * @return float
     */
    private function maxValue(bool $inc = true): float
    {
        $max = 0;
        foreach ($this->data as $graphElement) {
            list('point' => $point, 'value' => $value) = $graphElement;
            $max = max($max, $value/*$graphElement[1]*/);
        }
        if ($inc) {
            $max += $max * 0.1;
        }
        return floor_f($max, 2);
    }

    private function setGraphRealCoords(): void
    {
        $this->grc['width'] = $this->opt['width'] - $this->padding['left'] - $this->padding['right'];
        $this->grc['height'] = $this->opt['height'] - $this->padding['bottom'] - $this->padding['top'];
        $this->grc['x0'] = $this->padding['left'];
        $this->grc['y0'] = $this->opt['height'] - $this->padding['bottom'];
    }

    /**
     * Отрисовываем фон графика
     */
    private function buildMainBorder(): void
    {
        // Вывод главной рамки графика
        imagefilledrectangle($this->img,
            $this->grc['x0'],
            $this->grc['y0'] - $this->grc['height'],
            $this->grc['x0'] + $this->grc['width'],
            $this->grc['y0'],
            $this->imgColors['graphBg']
        );

        imagerectangle($this->img,
            $this->grc['x0'],
            $this->grc['y0'],
            $this->grc['x0'] + $this->grc['width'],
            $this->grc['y0'] - $this->grc['height'],
            $this->imgColors['graphGrid']);
    }

    /**
     *  Вывод сетки
     * @param float $step
     */
    private function buildGrid(float $step)
    {
        $maxValue = $this->maxValue();
        // Вывод сетки по оси Y
        for ($i = 1; $i <= $this->opt['countYLines']; $i++) {
            $y = $this->grc['y0'] - $step * $i;
            $str = floor_f(($maxValue / $this->opt['countYLines']) * $i, 2);
            imagestring($this->img,
                $this->opt['fontWidth'],
                2,/*$this->grc['x0'] - strlen($str) * $this->opt['fontWidth'] - $this->padding['left'] / 4 - 2,*/
                $this->grc['y0'] - $step * $i - imagefontheight(2) / 2,
                $str,
                $this->imgColors['fontColor']
            );
            //горизонтальные линии
            imageline($this->img,
                $this->grc['x0'],
                $y,
                $this->grc['x0'] + $this->grc['width'],
                $y,
                $this->imgColors['graphGrid']);
            //точки занчений на оси Y
            imagefilledellipse(
                $this->img,
                $this->grc['x0'],
                $y,
                6,
                6,
                $this->imgColors['graphGrid']
            );
        }

        // Вывод сетки по оси X
        $pointCount = $this->getPointsCount();
        $step = $this->grc['width'] / $pointCount;
        //$_x0 = $this->grc['x0'] + $step;
        $dx = ($this->grc['width'] / $pointCount) / 2;
        //$px = intval($this->grc['x0'] + $dx);
        for ($i = 0; $i < $pointCount; $i++) {
            //$x = $_x0 + $i * $step;\
            $x = intval($this->grc['x0'] + $i * ($this->grc['width'] / $pointCount) + $dx);

            //вертикальныйе линии
            imageline($this->img,
                $x,
                $this->grc['y0'],
                $x,
                $this->grc['y0'] - $this->grc['height'],
                $this->imgColors['graphGrid']
            );

            //точки значений на оси X
            imagefilledellipse(
                $this->img,
                $x,
                $this->grc['y0'],
                6,
                6,
                $this->imgColors['graphGrid']
            );

            $str = $this->data[$i]['point'];

            imagestring($this->img,
                $this->opt['fontWidth'],
                $x - strlen($str) * 2,
                $this->grc['y0'] + 6,
                $str,
                $this->imgColors['fontColor']
            );
            $px = $x;
        }
    }

    /**
     * Количество точек
     * @return int
     */
    private function getPointsCount(): int
    {
        return count($this->data);
    }

    /***
     *
     */
    private function buildGraph(): void
    {
        $i = 0;
        $maxValue = $this->maxValue(true);
        $pointCount = $this->getPointsCount();
        // Вывод линий графика
        $dx = ($this->grc['width'] / $pointCount) / 2;
        //$px = intval($this->grc['x0'] + $dx);
        //$px = $this->grc['x0'];// + ($i + 1) * ($this->grc['width'] / $pointCount);//intval($this->grc['x0'] + $dx);\
        $px = intval($this->grc['x0'] + $dx);
        $py = $this->getGraphY($i, $maxValue);//$this->grc['y0'] - ($this->grc['height'] / $maxValue * $this->data[0][0]);

        $this->drawGraphValue($i, $px, $py, $maxValue);

        for ($i = 0; $i < $pointCount; $i++) {
            $x = intval($this->grc['x0'] + $i * ($this->grc['width'] / $pointCount) + $dx);
            //$x = $this->grc['x0'] + $i * ($this->grc['width'] / $pointCount);
            $y = $this->getGraphY($i, $maxValue);//$this->grc['y0'] - ($this->grc['height'] / $maxValue * $this->data[$i][1]);

            $this->drawGraphValue($i, $x, $y, $maxValue);

            $this->imagelinethick($this->img,
                $px,
                $py,
                $x,
                $y,
                $this->imgColors['graphColor'], 3);

            $py = $y;
            $px = $x;
        }
    }

    /**
     * @param int $i
     * @param $maxValue
     * @return float|int|mixed
     */
    private function getGraphY(int $i, $maxValue)
    {
        $p = $maxValue * $this->data[$i]['value'];
        if ($p != 0) {
            return $this->grc['y0'] - ($this->grc['height'] / $p);
        }
        return 0;
    }

    /**
     * @param $i
     * @param $x
     * @param $y
     * @param $maxValue
     */
    private function drawGraphValue($i, $x, $y, $maxValue): void
    {
        $str = $this->data[$i]['value'];
        imagefilledellipse(
            $this->img,
            $x,
            $y,
            6,
            6,
            $this->imgColors['fontGraphColor']
        );
        if ($i < count($this->data) - 1) {
            $ny = $this->getGraphY($i + 1, $maxValue);
            if ($ny < $y) {
                $y += 4;
            } elseif ($ny > $y) {
                $y -= 14;
            } else {
                $x -= 5;
            }
        }
        imagestring($this->img,
            $this->opt['fontWidth'],
            $x - strlen($str) * 2,
            $y,
            $str,
            $this->imgColors['fontGraphColor']
        );
    }

    /**
     * @param $image
     * @param $x1
     * @param $y1
     * @param $x2
     * @param $y2
     * @param $color
     * @param int $thick
     * @return bool
     */
    private function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
    {
        /* этот способ применим только для ортогональных прямых
        imagesetthickness($image, $thick);
        return imageline($image, $x1, $y1, $x2, $y2, $color);
        */
        if ($thick == 1) {
            return imageline($image, $x1, $y1, $x2, $y2, $color);
        }
        $t = $thick / 2 - 0.5;
        if ($x1 == $x2 || $y1 == $y2) {
            return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
        }
        $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
        $a = $t / sqrt(1 + pow($k, 2));
        $points = array(
            round($x1 - (1 + $k) * $a), round($y1 + (1 - $k) * $a),
            round($x1 - (1 - $k) * $a), round($y1 - (1 + $k) * $a),
            round($x2 + (1 + $k) * $a), round($y2 - (1 - $k) * $a),
            round($x2 + (1 - $k) * $a), round($y2 + (1 + $k) * $a),
        );
        imagefilledpolygon($image, $points, 4, $color);
        return imagepolygon($image, $points, 4, $color);
    }

    /**
     * Выводим подписи к осям
     * @param float $step
     */
    private function printLabel(float $step): void
    {
        $maxValue = $this->maxValue();
        // Вывод подписей по оси Y
        for ($i = 1; $i <= $this->opt['countYLines']; $i++) {
            $str = floor_f(($maxValue / $this->opt['countYLines']) * $i, 2);
            imagestring($this->img,
                $this->opt['fontWidth'],
                2,/*$this->grc['x0'] - strlen($str) * $this->opt['fontWidth'] - $this->padding['left'] / 4 - 2,*/
                $this->grc['y0'] - $step * $i - imagefontheight(2) / 2,
                $str,
                $this->imgColors['fontColor']
            );
        }
        // Вывод сетки по оси X
        $pointCount = $this->getPointsCount();
        for ($i = 0; $i < $pointCount; $i++) {
            $x = $this->grc['x0'] + $i * ($this->grc['width'] / $pointCount);
            $str = $this->data[$i]['point'];

            imagestring($this->img,
                $this->opt['fontWidth'],
                $x,
                $this->grc['y0'] + 6,
                $str,
                $this->imgColors['fontColor']
            );
        }
        /*
        // Вывод подписей по оси X
        $prev = 100000;
        $twidth = $this->opt['fontWidth'] * strlen($this->data[0][0]) + 6;
        $i = $this->grc['x0'] + $this->grc['width'];
        $pointCount = $this->getPointsCount();
        while ($i > $this->grc['x0']) {
            if ($prev - $twidth > $i) {
                $drawx = $i - ($this->grc['width'] / $pointCount) / 2;
                if ($drawx > $this->grc['x0']) {
                    $str = $graphData["x"][round(($i - $x0) / ($graphWidth / $pointCount)) - 1];
                    imageline($image, $drawx, $y0, $i - ($graphWidth / $pointCount) / 2, $y0 + 5, $text);
                    imagestring($image, 2, $drawx - (strlen($str) * $fontWidth) / 2, $y0 + 7, $str, $text);
                }
                $prev = $i;
            }
            $i -= $graphWidth / $pointCount;
        }
        */
    }

}
