<?php

namespace Engine\Helper;


class ObjToStr
{
    /**
     * Распаковать строку в массив
     *
     * @param $string
     * @return array
     */
    public static function deCompress(string $string) : ?object
    {
        $res = unserialize(@gzuncompress(base64_decode($string))); //return object

        if ($res === false)
            return null;
        return $res;
    }

    /**
     * Упаковать массив в строку
     *
     * @param array $array
     * @return string
     */
    public static function compress($object): string
    {
        return base64_encode(gzcompress(serialize($object))); //return string
    }
}