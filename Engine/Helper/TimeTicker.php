<?php

namespace Engine\Helper;

use DateInterval;
use DateTime;

final class TimeTicker
{
    /**
     * @param $date
     * @param $duration
     * @return bool
     * @throws \Exception
     */
    public static function DateInterval($date, $duration): bool
    {
        if ($date == null) {
            return true;
        }
        $date1 = new DateTime("now"); //текущее время
        $date2 = new DateTime($date);
        $date2->add(DateInterval::createFromDateString(self::PHPDateFormat($duration)));
        return ($date2 <= $date1); //время вышло
    }

    /**
     * @param $seconds
     * @return string
     */
    public static function PHPDateFormat($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor($seconds % 3600 / 60);
        $seconds = $seconds % 60;
        //$result = sprintf("%d hours + %d minuts + %d seconds", $hours, $minutes, $seconds); //PHP 7.1
        $result = sprintf("%d hours + %d min + %d seconds", $hours, $minutes, $seconds); //PHP 7.3
        return $result;
    }

    /**
     * @param $interval
     * @return mixed
     */
    public static function Format(DateInterval $interval): string
    {
        $result = array();

        if ($interval->d)
            $result[] = ' %d дн.';
        if ($interval->h)
            $result[] = ' %h ч.';
        if ($interval->i)
            $result[] = ' %i мин.';
        if ($interval->s)
            $result[] = ' %s сек.';

        return $interval->format(implode(' ', $result));
    }


    /**
     * @param $id
     * @param string $startTime
     * @param int $duration
     * @return DateInterval
     * @throws \Exception
     */
    public static function coolDown($id, string $startTime, $duration): DateInterval
    {
        $date1 = new DateTime("now");
        $date2 = new DateTime($startTime);
        $addDate = DateInterval::createFromDateString(self::PHPDateFormat($duration));

        //dump($date1, $duration, $addDate, $date2);

        $date2->add($addDate);

        //dump($date2);

        $d_curr = time();
        $d_from = strtotime($startTime);
        $d_to = strtotime($date2->format("Y-m-d imageHeigth:i:s"));

        $interval = $date1->diff($date2);
        $interval->d_from = $d_from;
        $interval->d_to = $d_to;
        $interval->timeLeft = $d_to - $d_curr;
        $interval->name = $id;
        $interval->type = true;
        $interval->progress = 100 - round((($d_curr - $d_from) * 100) / ($d_to - $d_from), 2);
        return $interval;
    }
}