<?php

namespace Engine\Modules;

use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;

/**
 * Class HomePage
 * Простенькая конструкция которая вызывается контроллером.
 * @package Engine\Modules
 */
class HomePage extends BaseSiteModule
{
    /**
     * Внедрённый модуль
     * @var BaseSiteModule
     */
    private static $_inject = null;

    /**
     * Внедряем модуль
     * @param $env
     */
    public static function inject($env)
    {
        //dump('inject', $env);
        self::$_inject = $env;
    }

    /**
     * Отображаем
     * @return string
     * @throws \Exception
     */
    public static function display(): void
    {
        //dump(self::$_inject,class_exists(self::$_inject));
        if (self::$_inject != null AND class_exists(self::$_inject)) {
            //dump(self::$_inject);
            $env = new self::$_inject();
            $env->build(true);
        } else {
            (new HomePage())->build();
        }
    }

    public function build(): void
    {
        //указываем метку шаблона
        $this->activeTemplateLabel = 'homepage';
        //нету админ тулбара
        $this->adminToolbarLabel = null;
        //строим тулбары
        parent::build();
        //создаём компонент стартовой страницы и внедряем его в main
        //dump($this->activeTemplateLabel);
        $this->createSiteComponent($this->activeTemplateLabel, [], InjectComponentConfig::make('outdata', 'data', IComponent::INJECT_AS_ARRAYDATA));
    }

    /**
     * Заголовк страницы
     * @return string
     */
    protected function getPageTitle(): string
    {
        return 'Добро пожаловать';
    }
}