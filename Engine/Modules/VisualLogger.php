<?php

namespace Engine\Modules;


use Engine\Core\Config\SiteConfig;
use Engine\Exceptions\SiteComponentException;
use Exception;
use Modules\SiteTotalizator\Object\TotalizatorCurrentFight;
use Modules\SiteTotalizator\Object\TotalizatorHistories;
use Object\CustomControls\CustomControlSpoiler;
use ReflectionException;

final class VisualLogger
{
    private $logData = [];

    private $head = 'Проследние %d действия';

    private $count = 0;

    /**
     * Logger constructor.
     * @param string|null $head
     * @param int $count
     */
    public function __construct(string $head = null, int $count = 0)
    {
        if ($head != null) {
            $this->head = $head;
        }

        $this->count = $count;
    }

    /**
     * @param IVisualLogElement $addLog
     * @throws Exception
     */
    public function addLog(IVisualLogElement $addLog): void
    {
        $data = $addLog->toArray();
        $data['item'] = $addLog->getItem();
        $this->logData[] = $data;
    }

    /**
     * @return bool
     */
    public function isLogData(): bool
    {
        return !empty(($this->logData));
    }

    /**
     * @param string $target
     * @param string $logLabel
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public function build(string $target, string $logLabel): void
    {
        CustomControlSpoiler::make(
            sprintf($this->head, $this->count > 0 ? $this->count : SiteConfig::get('quickLog', 'main', 10)),
            $target,
            'logs',
            $logLabel,
            [
                'logs' => $this->logData
            ],
            [
                'class' => 'dark log'
            ]
        );
    }
}