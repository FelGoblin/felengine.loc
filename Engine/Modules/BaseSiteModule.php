<?php

namespace Engine\Modules;

use Engine\API\ModuleAPI;
use Engine\Core\Core;
use Engine\Core\DataBase\DataBase;
use Engine\Core\DataBase\SQL\SqlQuery;
use Engine\Core\Http\Input;
use Engine\Core\Objects\INamespace;
use Engine\Core\Objects\IUser;
use Engine\Core\View\Notice\WarningMessage;
use Engine\Core\View\Render;
use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\DeleteException;
use Engine\Exceptions\SaveException;
use Engine\Exceptions\SiteComponentException;
use Engine\Object\Admin\SiteAdmin;
use Exception;
use PDOException;
use ReflectionException;

//## exceptions ##

class BaseSiteModule extends BaseAbstractModule implements IBaseDisplayModule
{
    /**
     * АПИ
     * @var ModuleAPI|null
     */
    protected static ?ModuleAPI $API;
    /**
     * Текущий URI модуля (в основном это имя класса)
     * @var
     */
    protected static $uri;
    /**
     * Внедрённые из вне данные
     * @var array
     */
    protected static $injectData = [];

    /**
     * @var BaseSiteModule
     */
    private static BaseSiteModule $instance;
    /**
     * @var IUser|null
     */
    protected ?IUser $user;
    /**
     * Карточка модуля
     * @var AbstractComponent
     */
    protected AbstractComponent $outComponent;
    /**
     * @var
     */
    protected $sortFilter;
    /**
     * @var
     */
    protected $search;
    /**
     * MySQL провайдер
     * @var SqlQuery|null
     */
    protected ?SqlQuery $query;

    /**
     * @var array|null
     */
    protected ?array $options = [];

    /**
     * Данные для ренедера
     * @var array
     */
    protected array $displayData = [];

    /**
     * Данные которые появляются в процессе работы модуля
     * @var array
     */
    protected array $extraData = [];

    /**
     * Метка для тулбара (если надо будет переключать тулбары, то меняя значение этой метки, мы меняем ссылки на шаблоны)
     * @var string
     */
    protected string $toolbarLabel = 'toolbar.toolbar';
    /**
     * Метка для тулбара админа(если надо будет переклюать тулбары, то меняя значение этой метки, мы меняем ссылки на шаблоны)
     * @var string
     */
    protected string $adminToolbarLabel = 'admin.toolbar';
    /**
     * Данные для тулбара
     * @var array
     */
    protected array $toolbarData = [];
    /**
     * Админ данные для тулбара
     * @var array
     */
    protected array $adminToolbarData = [];
    /**
     * Данные для тулбара специализации
     * @var array
     */
    protected array $specToolbarData = [];
    /**
     * Данные были модифицированы
     * @var bool
     */
    protected bool $displayDataModify = false;
    /**
     * Значения trigger`a из POST
     * @var string|null
     */
    protected ?string $trigger;
    /**
     * Текущий активный шаблон
     * @var string
     */
    protected string $activeTemplateLabel = 'main';
    /**
     * @var string
     */
    protected $pageTitle = SERVER_URI;
    /**
     * @var array
     */
    protected array $triggersMap = [];
    /**
     * @var null | IComponent
     */
    private ?IComponent $toolbar = null;
    /**
     * @var null | IComponent
     */
    private ?IComponent $adminToolbar = null;
    /**
     * @var callable
     */
    private $defaultTrigger;

    /**
     * Обязателен конструктор
     * IStreamConsole constructor.
     * @param null $options
     * @param callable|null $onload
     * @param callable|null $onError
     * @throws ConfigException
     * @throws ReflectionException
     */
    public function __construct($options = null, callable $onload = null, callable $onError = null)
    {
        //singleton
        self::$instance = $this;
        //parent construct
        parent::__construct();
        //self counstruct
        try {
            try {
                $error = false;
                $this->query = DataBase::query();
                $this->user = Core::getThisUser();
                //запускаем контроллер
                $this->controller($options);
            } catch (AccessException $e) { //TODO: добавить логирование
                throw new Exception('Ошибка доступа: ' . $e->getMessage());
            } catch (DeleteException $e) {
                throw new Exception('Ошибка удаления: ' . $e->getMessage());
            } catch (SaveException $e) {
                throw new Exception('Ошибка сохранения: ' . $e->getMessage());
            } catch (PDOException $e) {
                throw new Exception('Ошибка базы данных: ' . $e->getMessage() . '[' . $this->getQuery()::$sql . ']');
            }
        } catch (Exception $ex) {
            Render::setError($ex->getMessage());
            //событие
            if (is_callable($onError)) {
                $onError($ex->getMessage());
            }
            $file = Input::getFileControl()->getUploadFile();
            if ($file != null) {
                $uploaded = $file->getUploadedFile();
                if (!empty($uploaded) and file_exists($uploaded)) {
                    unlink($uploaded);
                }
            }
        } finally {
            if ($this->getQuery() !== null) {
                $this->getQuery()->flush();
            }
            //событие
            if (is_callable($onload)) {
                $onload();
            }
        }
    }

    /**
     * Обработка GET, POST
     * @param null $options
     * @throws AccessException
     * @throws ReflectionException
     * @throws Exception
     */
    protected function controller($options = null): void
    {
        //dump_trace($options);

        if (!empty($options['uri'])) {
            //текущий ативный модуль
            Core::$activeModule = $options['uri'];
            self::$uri = SERVER_URI . (substr(SERVER_URI, -1) != '/' ? '/' : '') . $options['uri'];
        }

        if (!empty($options['getId'])) {
            $this->id = Input::get('id');
        }


        if (!empty($options['name'])) {
            $this->name = $options['name'];
        }

        if (!empty($options['getTrigger'])) {
            $this->trigger = $this->trigger ?? Input::get(is_string($options['getTrigger']) ? $options['getTrigger'] : 'do', 'string');
        } else {
            $this->trigger = $this->trigger ?? Input::get('do', '?s');
        }
        //триггер по  умолчанию
        if ($this->trigger == null and !empty($options['defaultTrigger'])) {
            $this->trigger = $options['defaultTrigger'];
        }
        //dump($this->trigger, Input::get('do', '?s'));

        if (!empty($options['getEdit'])) {
            $this->edit = Input::get('request') == 'edit';
        }

        if (!empty($options['getDirect']))
            $this->direct = Input::get('direct') ?? ' desc ';

        if (!empty($options['getSearch'])) {
            $this->search = Input::get('s', 'string');
            $this->isSearch = !empty($this->search);
        }

        if (!empty($options['sort'])) {
            $this->sortFilter = Input::get('sort', CAST_NULLSTR);
        }

        if (!empty($options['options'])) {
            $this->options = $options['options']; //НУЖНО БОЛЬШЕ OPTIONS!!!! Уха-ха-ха-ха. Ой простите.
        }

        //есть указания на то, что данный триггер доолжен иметь доступ к исполнению.
        if ($this->trigger != null and !empty($options['permission'])) { //получаем имя функции доступа
            //есть какие-то команды
            $permission = self::checkPermission($options['permission']); //проверяем то, что данная функция проприсана у пользователя
            if (!$permission) {//прерываем контроллер
                throw new AccessException('Доступ для подобной операции не доступен данному пользователю.');
            }
        }

        /*
         *  = Админ контроллер =
         */
        if (!empty($this->adminNameSpace)) {
            $admin = $this->adminNameSpace . '\\Admin';
        } else {
            $this->adminNameSpace = $this->getNameSpacePath() . '\\Admin';
            $admin = $this->adminNameSpace . '\\Admin';
        }


        //запускаем админ контроллер
        /** @var SiteAdmin $admin */
        //dump(class_exists($admin), $admin);
        if (class_exists($admin)) {
            $admin::controller([
                'caller' => get_class($this),
                'admin' => $this->adminNameSpace,
                'extend' => $options['extendAdminFunction'] ?? null,
                'data' => $options['data'] ?? null,
                'user' => $options['target'] ?? $this->user
            ]);
        }

        $this->doTrigger();
    }

    /**
     * @param string $permission
     * @return bool
     * @throws Exception
     */
    protected function checkPermission(string $permission = 'Moderator'): bool
    {
        if ($this->user != null and $this->user instanceof IUser) {
            return $this->user->userAccessCheck($permission);
        }
        return false;
    }

    /**
     * Switch $trigger
     */
    protected function doTrigger(): void
    {
        $target = $this->trigger;
        //dump($target);
        $fn = $this->triggersMap[$target] ?? null;
        if ($target != null and is_callable($fn)) {
            $fn();
            return;//вызываем только первый по совпадению
        }
        //не нашли триггеров.
        $fn = $this->defaultTrigger;
        if (is_callable($fn)) {
            $fn();
        }
    }

    /**
     * @return SqlQuery
     */
    protected function getQuery(): ?SqlQuery
    {
        return $this->query;
    }

    /**
     * @param string $path
     * @return mixed
     */
    public static function getUri(string $path = ''): string
    {
        if (empty(static::$uri)) {
            $cl = get_called_class();
            $ex = explode('\\', $cl);
            static::$uri = array_pop($ex);
        }
        return static::$uri . (substr($path, -1) != '/' ? '/' : '') . $path;
    }

    /**
     * Пост контроллер. Для обработки уже завершённых операций.
     * @param int $mode
     * @throws ReflectionException
     */
    static function postControllerRun(int $mode = INamespace::MODE_NORMAL): void
    {
        //Добавляем системные компоненты.
        self::loadSystemComponents(self::getComponentList());
    }

    /**
     * @param array $array
     * @param $field
     */
    protected static function extractBool(array &$array, $field): void
    {
        if (($array[$field] ?? null) != null) {
            $array[$field] = Cast($array[$field], 'b');
        }
    }

    /**
     * @return void
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function build(): void
    {
        //создаём данные для рендера
        $this->makeOutComponentData();

        if (!empty($this->adminToolbarLabel)) {
            // собираем админ панель (если есть доступ конечно)
            $this->makeAdminToolbar();
        }
        //собираем тулбар
        $this->makeToolbar();
        //тулбары специализации
        $this->makeSpecToolbar();
        //заголовок страницы
        $this->setPageTitle($this->getPageTitle());
    }

    /**
     * Собираем данные для шаблона вывода
     */
    private function makeOutComponentData(): void
    {
        $this->displayData = $this->getDisplayData();
    }

    /**
     * Функция для переопределения
     * @param array $data
     * @return array
     */
    protected function getDisplayData(array $data = []): array
    {
        return array_merge(
            $this->displayData,
            $data,
            [
                'extraData' => $this->extraData
            ]);
    }

    /**
     * Установить/добавить данные для вывода (принудительно)
     * @param array $data
     * @param bool $merge
     */
    public function setDisplayData(array $data = [], bool $merge = false): void
    {
        if (is_null($data))
            return;

        $this->displayDataModify = true; //модифицируем из вне

        //есть указания из параметров на текущий шаблон
        if (!empty($data['templateLabel'])) {
            $this->activeTemplateLabel = $data['templateLabel'];
            unset($data['templateLabel']);
        }

        if ($merge) {
            $this->displayData = array_merge_recursive_distinct($this->displayData, $data);
        } else {
            $this->displayData = $data;
        }
    }

    //###########// ToolBars //###########//

    /**
     * @param bool $autoBuild
     * @return IComponent|null
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function makeAdminToolbar(bool $autoBuild = true/*, bool $singleClass = false*/): ?IComponent
    {
        if ($autoBuild and $this->getAccess()) {
            $this->adminToolbar = $this->_makeToolbar($this->adminToolbarLabel, 'adminToolbar', $this->getAdminToolbarData(), $singleClass = true);
        }
        return $this->adminToolbar;
    }

    /**
     * Доступ к модулю
     * @param string $permission
     * @return bool
     */
    protected function getAccess(string $permission = 'admin'): bool
    {
        return ($this->user != null and $this->user instanceof IUser and $this->user->userAccessCheck($permission));
    }

    /**
     * @param string $label
     * @param string $as
     * @param array $data
     * @param bool $singleClass
     * @return IComponent
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    protected function _makeToolbar(string $label, string $as, array $data, bool $singleClass = false): IComponent
    {
        //например мы создаём
        //dump($this->getActiveLabel(), $label);
        return $this->createSiteComponent($label/* метку можем менять динамически в контроллере*/,
            $data,
            InjectComponentConfig::make($this->getActiveLabel(), $as),
            false,
            $singleClass
        );
    }

    /**
     * @param bool $asClass
     * @return string
     * @throws ReflectionException
     */
    protected function getActiveLabel(bool $asClass = true): string
    {
        if (explode('.', $this->getActiveTemplateLabel())[0] !== 'global') {
            return sprintf('%s.%s', $this->getModuleName(true, $asClass), $this->getActiveTemplateLabel());
        } else {
            return $this->getActiveTemplateLabel();
        }
    }

    /**
     * @return string
     */
    public function getActiveTemplateLabel(): string
    {
        return $this->activeTemplateLabel;
    }

    /**
     * @return array
     */
    protected function getAdminToolbarData(): array
    {
        return $this->adminToolbarData;
    }

    /**
     * @param bool $autoBuild
     * @return string
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    protected function makeToolbar(bool $autoBuild = false): void
    {
        if ($autoBuild) {
            $this->toolbar = $this->_makeToolbar($this->getToolbarLabel(), 'toolbar', $this->getToolbarData(), true);
        }
    }

    /**
     * Получить имя тулбара
     * @return string|null
     * @throws ReflectionException
     */
    protected function getToolbarLabel(): ?string
    {
        return $this->toolbarLabel;
    }

    /**
     * @return array
     */
    protected function getToolbarData(): array
    {
        return $this->toolbarData;
    }

    /**
     * @param bool $autoBuild
     * @return string
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function makeSpecToolbar(bool $autoBuild = false): void
    {
        if ($autoBuild) {
            $this->_makeToolbar($this->toolbarLabel, 'spectoolbar', $this->getSpecToolbarData());
        }
    }

    /**
     * @return array
     */
    protected function getSpecToolbarData(): array
    {
        return $this->specToolbarData;
    }

    protected function getPageTitle(): string
    {
        return $this->pageTitle;
    }

    /**
     * @param string $title
     * @throws Exception
     */
    protected function setPageTitle(string $title): void
    {
        if ($label = Core::getSiteLabel()) {
            $label::set('pageTitle', $title);
        }
    }

    /**
     * Добавляем в компонент вывода другой компонент.
     * @param SiteComponent $component
     * @throws ReflectionException
     */
    public function injectComponent(SiteComponent $component): void
    {
        if ($this->getDisplay() != null) {
            $this->getDisplay()->addInnerComponents($component);
        }
    }

    // ===== устаревшие =====

    /**
     * @return AbstractComponent|null
     */
    public function getDisplay(): ?AbstractComponent
    {
        return $this->outComponent;
    }

    /**
     * Отходим от switch
     * @param string $target
     * @param callable $fn
     * @param bool $default
     */
    protected function addTrigger(string $target, callable $fn, bool $default = false): void
    {
        $this->triggersMap[$target] = $fn;

        if ($default) {
            if (is_callable($default)) {
                new WarningMessage('@default_trigger_is_replaced');
            }
            $this->defaultTrigger = $fn;
        }
    }

    /**
     * Получить имя админ тулбара
     * @return string|null
     */
    protected function getAdminToolbarLabel(): ?string
    {
        return $this->getToolbarName($this->adminToolbar);
    }

    private function getToolbarName(?IComponent $toolbar): ?string
    {
        if ($toolbar === null)
            return null;
        return $toolbar->getName();
    }
}
