<?php

namespace Engine\Modules;


interface IBaseDisplayModule
{
    /**
     * Вывод модуля
     * @return string
     */
    public function build(): void;
}