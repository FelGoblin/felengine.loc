<?php

namespace Engine\Modules;


abstract class AbstractService implements IService
{
    /**
     * Режим загрузки сервиса:
     *      SYSTEM_LOAD - загружать до инициализации контроллера
     *      POST_LOAD - загружать после контроллера
     *      ALWAYS_LOAD - загружать всегда.
     * @var int
     */
    public static $mode = SYSTEM_LOAD;
    /**
     * Для переопределения. Псевдоним текущего сервиса.
     */
    public function classAlias(): void
    {
        //Нет ничего. По-умолчанию пусто
    }
}