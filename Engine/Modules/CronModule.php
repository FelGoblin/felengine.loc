<?php

namespace Engine\Modules;


use Engine\Core\Development\DebugInfo;

abstract class CronModule
{
    abstract public function task($data = null): void;

    /**
     *
     */
    public function logOut(): void
    {
        foreach (DebugInfo::debugInfo() as $log) {
            printf("> %s\n", $log);
        }
    }

    /**
     * Add Log
     * @param string $log
     */
    protected function addLog(string $log): void
    {
        DebugInfo::debugAdd($log);
    }

    /**
     * @param mixed ...$args
     */
    protected function addLogF(...$args): void
    {
        call_user_func_array(array('Engine\Core\Development\DebugInfo', 'debugAddF'), func_get_args());
    }
}