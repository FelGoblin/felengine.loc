<?php


namespace Engine\Modules;


class LabelsStore
{
    protected static $list = [];

    public static function set($key, $value, bool $override = false)
    {
        if (isset(self::$list[$key])) {
            if ($override) //перезапись
                self::$list[$key] = $value;
            else //добавление
                self::$list[$key] .= $value;
        } else
            self::$list[$key] = $value;
    }

    public static function get(string $key, bool $error = false)
    {
        return self::$list[$key] ?? ($error ? "Label [$key] not found" : '');
    }
}