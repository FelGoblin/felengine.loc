<?php

namespace Engine\Modules;


use Engine\Core\Objects\INamespace;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;

interface IBaseModule
{
    /**
     * Пост контроллер. Для обработки уже завершённых операций.
     * @param int $mode
     */
    static function postControllerRun(int $mode = INamespace::MODE_NORMAL): void;

    /**
     * @param string $tplLabel
     * @param array $data
     * @param InjectComponentConfig|null $inject
     * @return IComponent
     */
    function createSiteComponent(string $tplLabel, array $data, InjectComponentConfig $inject = null): IComponent;

    /**
     * @param IComponent $component
     * @param InjectComponentConfig|null $injectComponentConfig
     */
    public function addSiteComponent(IComponent $component, InjectComponentConfig $injectComponentConfig = null): void;

    /**
     * Получить имя модуля
     * @param bool $lower
     * @param bool $asClass
     * @return string
     */
    public function getModuleName(bool $lower = false, bool $asClass = false): string;
}