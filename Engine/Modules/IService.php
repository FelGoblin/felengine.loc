<?php

namespace Engine\Modules;


use Engine\Core\Core;

interface IService
{
    /**
     * Инициализация
     * @param string|null $config
     * @param int $mode
     */
    public function init(?string $config, int $mode): void;

    /**
     * Псевдоним провайдера
     */
    public function classAlias(): void;
}