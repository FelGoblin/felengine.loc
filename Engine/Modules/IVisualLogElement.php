<?php

namespace Engine\Modules;


//use Object\Item\Objects\IItem;

interface IVisualLogElement
{
    /**
     * @param string $component
     */
    public function build(string $component): void;

    /**
     * @return array|null
     */
    public function toArray(): ?array;

    /**
     * @return IItem|null
     *
    public function getItem(): ?IItem;
     * */
}