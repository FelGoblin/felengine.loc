<?php


namespace Engine\Modules;


use Engine\API\ModuleAPI;
use Engine\Core\Objects\FillClass;

final class ModData extends FillClass
{
    /**
     * Класс
     * @var null|ModuleAPI
     */
    protected $class;

    /**
     * Неймспейс
     * @var string
     */
    protected $nmspc = '';

    /**
     * Папка с модулем
     * @var string
     */
    protected $modDir = '';

    /**
     * Алиас апи
     * @var string|array
     */
    protected $apiAlias;

    /**
     * @return string
     */
    public function getModDir(): string
    {
        return $this->modDir;
    }

    /**
     * @return string
     */
    public function getNmspc(): string
    {
        return $this->nmspc;
    }

    /**
     * @return ModuleAPI|null
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * TODO: есть вариант созданиея мультипсевдонимов, но имхо это только запутает в конце концов
     * @return array|string
     */
    public function getApiAlias(): string /* mixed */
    {
        return $this->apiAlias;
    }
}