<?php

namespace Engine\Modules;

use Engine\API\ModuleAPI;
use ReflectionClass;
use ReflectionException;

/**
 * Class ModulesDetour
 * Обходчик модулей
 * @package Engine\Modules
 */
final class ModulesDetour
{
    /**
     * Обходим моды
     * @param string $target
     * @param callable $callback
     * @param string|null $subDir
     * @return array список обойдённых модов
     * @throws ReflectionException
     */
    public static function detour(string $target, callable $callback, string $subDir = null): array
    {
        $dir = MOD_DIR . $subDir;

        if (!is_dir($dir)) {
            return [];
        }

        $find = array_diff(scandir($dir, SCANDIR_SORT_DESCENDING), Array(".", ".."));
        $result = [];
        foreach ($find as $d) {
            switch ($target) {
                case DETOUR_API:
                    //dump($d);
                    if ($subDir == null) {
                        $apiDir = pathSeparator($d . '/API/');
                        self::detour(DETOUR_API, $callback, $apiDir);
                    } else {
                        $apiFile = $dir . $d;
                        if (file_exists($apiFile)) {
                            $apiNmspc = str_replace([ROOT_DIR, DIRECTORY_SEPARATOR, '.php'], ['', '\\', ''], $apiFile);
                            /** @var ModuleAPI $apiNmspc */
                            if (class_exists($apiNmspc)) {
                                $reflector = new ReflectionClass($apiNmspc);
                                $cbData = [
                                    'class' => $reflector->getName(),
                                    'nmspc' => $reflector->getNamespaceName(),
                                    'modDir' => dirname(str_replace(ROOT_DIR, '', $reflector->getFileName())),
                                    'apiAlias' => $apiNmspc::$alias ?? strtolower($reflector->getShortName())
                                ];
                                $callback($cbData);
                            }
                        }
                    }
                    break;

                case DETOUR_COMP:
                    $modFile = pathSeparator($dir . $d . DIRECTORY_SEPARATOR . $d . '.php');
                    //dump($modFile);
                    if (file_exists($modFile)) {
                        $callback([
                            'class' => 'Modules\\' . $d . '\\' . $d,
                            'nmspc' => 'Modules\\' . $d,
                            'modDir' => $dir . $d . DIRECTORY_SEPARATOR
                        ]);
                    }
                    break;

                case DETOUR_CRON:
                    $cronFile = pathSeparator($dir . $d . DIRECTORY_SEPARATOR . 'API.php');
                    $cronNmspc = 'Modules\\' . $d . '\\API';
                    //dump($apiFile, file_exists($apiFile), class_exists($apiNmspc));
                    if (file_exists($cronFile) AND class_exists($cronNmspc)) {
                        $cbData = [
                            'class' => 'Modules\\' . $d . '\\' . $d,
                            'nmspc' => 'Modules\\' . $d,
                            'modDir' => $dir . $d . DIRECTORY_SEPARATOR
                        ];
                        $callback($cbData);
                    }
                    break;
                /*
                    case 'lang':
                        if (is_dir(MOD_DIR . $d) AND is_dir($_d = MOD_DIR . $d . DIRECTORY_SEPARATOR . 'Locale')) {
                            $cbData = [

                            ];
                            $callback($cbData);
                        }
                        break;
                */
            }

            $result[] = 'Modules/' . $d;
        }
        return $result;
    }
}