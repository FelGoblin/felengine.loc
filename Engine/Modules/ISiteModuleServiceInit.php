<?php


namespace Engine\Modules;
/**
 * Инициализация сервиса модуля сайта
 * Interface ISiteModuleServiceInit
 * @package Engine\Modules
 */
interface ISiteModuleServiceInit
{
    /**
     * Событие
     * @param array $data
     */
    public static function doSiteModuleEventInit(array $data = []): void;

    /**
     * @param array $data
     */
    public static function doSiteModuleEventPostInit(array $data = []): void;
}