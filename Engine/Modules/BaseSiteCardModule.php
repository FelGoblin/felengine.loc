<?php

namespace Engine\Modules;

use Engine\Core\Core;
use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SiteComponentException;
use Exception;
use Modules\_SystemModule\SiteComponents\Card;
use ReflectionException;

/**
 * Вывод - одна карточка
 * Class BaseSiteCardModule
 * @package Engine\Modules
 */
class BaseSiteCardModule extends BaseSiteModule
{
    /**
     * @var BaseSiteCardModule|null
     */
    protected ?BaseSiteCardModule $host = null;

    /**
     * Данные длдя заголовка
     * @var array
     */
    protected array $titleData = [];

    /**
     * Данные для футера
     * @var array
     */
    protected array $footerData = [];

    /**
     * Card
     * @var AbstractComponent
     */
    protected AbstractComponent $outComponent;

    /**
     * BaseSiteCardModule constructor.
     * @param null $options
     * @param callable|null $onload
     * @throws ConfigException
     * @throws ReflectionException
     * @throws Exception
     */
    public function __construct($options = null, ?callable $onload = null)
    {
        if ($card = Core::getSiteCard()) {
            $this->outComponent = new $card();
        } else {
            $this->outComponent = new SiteComponent();
        }
        parent::__construct($options, $onload);
    }

    /**
     * Наследуем
     * @param BaseSiteModule $mod
     * @return BaseSiteCardModule
     * @throws ConfigException
     * @throws ReflectionException
     */
    public static function inherit(BaseSiteModule $mod): BaseSiteCardModule
    {
        //dump($mod->options);
        $result = new BaseSiteCardModule([
            'options' => $mod->options
        ]);
        $result->setDisplayData($mod->getDisplayData());
        $result->activeTemplateLabel = $mod->activeTemplateLabel;
        //принудительно указываем имя потомка для корректной отработка меток
        $result->forceChildModule = $mod->getModuleName(true, true);
        //dump($result);
        return $result;
    }

    /**
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function build(): void
    {
        parent::build();

        $this->addSiteComponent($this->outComponent, InjectComponentConfig::make('outdata', 'data'));

        $title = $this->getCardTitle();
        $footer = $this->getFooter();

        if ($this->outComponent instanceof ICard) {
            if (!$this->outComponent->autoCompile) {
                $this->outComponent->setTitle($title);
                $this->outComponent->setFooter($footer);
            } else {
                if (!empty($title)) { //автоматически, но у нас есть предопределение
                    $this->outComponent->setTitle($title);
                }
                if (!empty($footer)) {//автоматически, но у нас есть предопределение
                    $this->outComponent->setFooter($footer);
                }
            }
            //футер данные не получились
            if (empty($this->getFooter())) {
                //создадим компонент текущего футера карточки и зарегистрируем его в системе. в овремя рендеринга попробуем его автоподключить
                $this->createSiteComponent('footer', $this->getFooterData(), InjectComponentConfig::make($this->outComponent->getId(), 'footer'), false, true);
            }
            //заоловок карточки
            if (empty($this->getCardTitle())) {
                //создадим компонент текущего заголовка карточки и зарегистрируем его в системе. в овремя рендеринга попробуем его автоподключить
                $this->createSiteComponent('title', $this->getTitleData(), InjectComponentConfig::make($this->outComponent->getId(), 'title'), false, true);
            }

            //создаём компонент по метке и внедряем его в тело карточки
            $this->createSiteComponent($this->getActiveTemplateLabel(), array_merge(
                [
                    'injected' => $this->extraData
                ],
                $this->displayData ?? [],
            ), InjectComponentConfig::make($this->outComponent->getId(), 'body'), false, true);
        }
    }

    /**
     * Добавить в заголовок карточки описание от контроллера
     * @param string|null $data
     * @return string
     */
    public function getCardTitle(string $data = null): string
    {
        if ($data != null) {
            $this->outComponent->setTitle($data);
        }

        if ($this->host != null and $this->host instanceof IBaseModule) { //причём преопределение от хоста
            $this->outComponent->setOnTitleCompile(function ($content) {
                return $this->host->getCardTitle($content);
            });
        }
        return '';
    }

    /**
     * Футер для майн карточки
     * @param string|null $data
     * @return string
     */
    public function getFooter(string $data = null): string
    {
        if ($data != null) {
            $this->outComponent->setFooter($data);
        }

        //причём преопределение от хоста
        if ($this->host != null and $this->host instanceof IBaseModule) {
            $this->outComponent->setOnFooterCompile(function ($content) {
                return $this->host->getFooter($content);
            });
        }
        return '';
    }

    protected function getFooterData(): array
    {
        return $this->footerData;
    }

    /**
     * @return array
     */
    protected function getTitleData(): array
    {
        //dump($this->options);
        if (!empty($this->options['paths'])) {
            $this->titleData = array_merge_recursive($this->titleData, ['paths' => $this->options['paths']]);
        }
        return $this->titleData;
    }
}
