<?php


namespace Engine\Modules;


abstract class SiteModuleServiceInit implements ISiteModuleServiceInit
{
    /**
     * @param array $data
     */
    public static function doSiteModuleEventInit(array $data = []): void
    {
        return;
    }

    /**
     * @param array $data
     */
    public static function doSiteModuleEventPostInit(array $data = []): void
    {
        return;
    }
}