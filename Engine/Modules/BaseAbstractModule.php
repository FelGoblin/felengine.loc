<?php

namespace Engine\Modules;


use Engine\Core\Config\SiteConfig;
use Engine\Core\Core;
use Engine\Core\View\ComponentManager;
use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SiteComponentException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;
use ReflectionException;
use SplFileInfo;

abstract class BaseAbstractModule implements IBaseModule
{
    /**
     * @var BaseAbstractModule
     */
    //protected static $instance;
    /**
     * Список компонентов текущего модуля
     * @var array
     */
    protected array $components = [];

    /**
     * Принудительно указать имя потомка
     * @var null
     */
    protected $forceChildModule = null;

    /**
     * Список компонентов для рендера
     * @var array
     */
    protected array $buildComponents = [];

    /**
     * @var string|null
     */
    protected ?string $adminNameSpace;

    /**
     * BaseAbstractModule constructor.
     * @throws ReflectionException
     * @throws ConfigException
     */
    public function __construct()
    {
        self::autoloadConfig();
        $this->components[] = self::getComponentList();
        Core::addModule($this);
    }

    /**
     * Автозагружаем конфиг. TODO: сделать полностью автозагружаемым без указания в инициализации сервиса.
     * @param callable|null $cb
     * @throws ConfigException
     * @throws ReflectionException
     */
    public static function autoloadConfig(callable $cb = null): void
    {
        $file = self::getModulePath() . DIRECTORY_SEPARATOR . 'config.php';
        if (file_exists($file)) {
            if ($cb == null) {
                SiteConfig::addConfigFile($file, strtolower((new ReflectionClass(get_called_class()))->getShortName()), true);
            } else {
                $cb($file);
            }
        }
        //прочекаем папку с конфигами
        $dir = self::getModulePath() . DIRECTORY_SEPARATOR . 'Config';
        if (is_dir($dir)) {
            $directory = new RecursiveDirectoryIterator(pathSeparator($dir));
            $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
            /** @var SplFileInfo $splFileInfo */
            foreach ($iterator as $splFileInfo) {
                if ($splFileInfo->getFilename() == '.' or $splFileInfo->getFilename() == '..') //скипаем папки
                    continue;
                if (!$splFileInfo->isDir()) { //это не папка
                    //$parent = str_replace($dir . DIRECTORY_SEPARATOR, '', $splFileInfo->getPath()); //заменяем корневой путь на слеш
                    //dump(['style' => 'red'], $splFileInfo->getRealPath(), strtolower((new ReflectionClass(get_called_class()))->getShortName()));
                    SiteConfig::addConfigFile($splFileInfo->getRealPath(), strtolower((new ReflectionClass(get_called_class()))->getShortName()), true);
                }
            }
        }
    }

    /**
     * @param null $class
     * @return string
     * @throws ReflectionException
     */
    public static function getModulePath($class = null): string
    {
        $class_info = new ReflectionClass($class ?? get_called_class());
        return dirname($class_info->getFileName());
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    protected static function getComponentList(): array
    {
        $result = [];
        $dir = static::getModulePath() . DIRECTORY_SEPARATOR . 'SiteComponents';
        //dump(['style' => 'yellow'], $dir, is_dir($dir));
        if (is_dir($dir)) {
            $find = array_diff(scandir($dir), array(".", ".."));
            foreach ($find as $file) {
                $file = realpath($dir . DIRECTORY_SEPARATOR . $file);
                if (is_file($file)) {
                    $f = pathinfo($file);
                    if ($f['extension'] == 'php') {
                        $comp = static::getNameSpacePath() . '\\SiteComponents\\' . $f['filename'];
                        $result[] = $comp;
                    }
                }
            }
        }
        //dump(['style' => 'yellow'], $result);
        return $result;
    }

    /**
     * @return string
     * @throws ReflectionException
     */
    protected static function getNameSpacePath(): string
    {
        $class_info = new ReflectionClass(get_called_class());
        return $class_info->getNamespaceName();
    }

    /**
     * Загрузка системных компонентов
     * @param array $components
     */
    public static function loadSystemComponents(array $components): void
    {
        //dump(['style' => 'dark', 'footer' => 'add system component'], get_called_class(), $this->components);
        foreach ($components as $component) {
            if (get_parent_class($component) == 'Engine\\Core\\View\\SiteComponent\\SystemComponent') {
                //dump($component);
                ComponentManager::addSystemComponent(get_called_class(), $component);
            }
        }
    }

    /**
     * @return array
     */
    public function getBuildComponents(): array
    {
        return $this->buildComponents;
    }

    /**
     * @param string $tplLabel
     * @param array $data
     * @param InjectComponentConfig|null $inject
     * @param bool $exception
     * @param bool $singleClass
     * @return IComponent
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function createSiteComponent(string $tplLabel, array $data, InjectComponentConfig $inject = null, bool $exception = false, bool $singleClass = false): IComponent
    {
        if ((explode('.', $tplLabel)[0] ?? null) !== 'global') {
            $name = sprintf('%s.%s', $this->getModuleName(true, $singleClass), $tplLabel);
        } else {
            $name = $tplLabel;
        }
        $component = new SiteComponent($name, null, $data, $inject);
        $component->templateException = $exception;
        $this->buildComponents[] = $component;
        return $component;
    }

    /**
     * @param IComponent $component
     * @param InjectComponentConfig|null $injectComponentConfig
     *////*string $inject = null, string $as = null*/
    /**
     * Имя текущего модуля
     * @param bool $lower
     * @param bool $asClass
     * @return string
     * @throws ReflectionException
     */
    public function getModuleName(bool $lower = false, bool $asClass = false): string
    {
        if (!$asClass) {
            $nmspcs = explode('\\', $this->getNameSpacePath());
            $nmspcs = array_pop($nmspcs);
        } else {
            if ($this->forceChildModule) {
                return $this->forceChildModule;
            }
            //$nmspcs = explode('\\', get_called_class());
            $class_info = new ReflectionClass(get_called_class());
            $nmspcs = $class_info->getShortName();
        }

        $res = $lower ? strtolower($nmspcs) : $nmspcs;

        return $res;
    }


    public function addSiteComponent(IComponent $component, InjectComponentConfig $injectComponentConfig = null): void
    {
        if ($injectComponentConfig != null) {
            /** @var AbstractComponent $component */
            $component->setInject($injectComponentConfig);
        }

        $this->buildComponents[] = $component;
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function getComponentsLabels(): array
    {
        $result = [];
        foreach ($this->buildComponents as $buildComponent) {
            $result[] = $buildComponent->getName();
        }
        return $result;
    }
}
