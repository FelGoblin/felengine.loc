<?php


namespace Engine\API;


interface IAPIModule
{
    public static function modEventsInit(): void;
}