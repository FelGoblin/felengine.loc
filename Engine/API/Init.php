<?php
/*
 * Инициализация api.php
 */

require($dirname . 'Engine/Core/Defines.php');
require ROOT_DIR . '/Ext/Dump.php';
require ENGINE_DIR. '/Autoloader.php';

use Engine\API\API;
use Engine\Autoloader;

try {
    //Автоматически загружаем все доступные API с модулей
    Autoloader::Init();
    //инициируем сервисы
    API::init();

} catch (Exception $ex) {
    echo $ex->getMessage(); //TODO: Error Page!
}
?>