<?php

namespace Engine\API;

use Engine\Core\Core;
use Engine\Core\Database\DataBase;
use Engine\Core\Objects\IUser;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Exception;

class APITokenControl
{
    public static $error = '';


    /**
     * Проверяем параметры
     * @param APIData $data
     * @return bool
     * @throws Exception
     */
    public static function check(APIData &$data): bool
    {
        //dump($data);

        $apiUser = null;

        $accessList = self::getApiAccess($data->getToken(), $apiUser );

        //dump($accessList, $apiUser);

        if (!class_exists('User')) {
            throw new APIException('System Error: Not Implemented Class User::IUser');
        }

        $user = Core::getUser(); //User::getUser($apiUser, ['access','mods']);

        if (empty($user)) {
            throw new APIException('System Error: Class User::IUser is null or undefined');
        }

        /** @var IUser $user */
        $_user = $user::getUser($apiUser, ['access', 'mods']);

        if (empty($_user)) {
            throw new APIException('User token is invalid');
        }

        if ($_user->isBlocked()) {
            throw new AccessException('Пользователь заблокирован..');
        }

        $data->setAccess($accessList);

        //dump(['private' => true], $_user, $data);

        if (!self::getAccess($data)) {
            throw new AccessException('Api User access deny for: ' . sprintf('%s%s%s', $data->getModule(), '.', $data->getTrigger()));
        }

        $data->setUser($_user);

        return true;
    }

    /**
     * Проверяем лоступа пользователя к текущей команде.
     * @param APIData $data
     * @return bool
     */
    public static function getAccess(APIData $data): bool //TODO: дублируется с WODGET
    {
        $access = array_map('trim', $data->getAccess());

        foreach ($access as $a) {
            $accessFunction = sprintf('%s%s%s', $data->getModule(true), '.', $data->getTrigger());
            $res = (
                in_array($data->getModule(true), $access, true) //смотрим общую группу
                OR in_array($accessFunction, $access, true) //смотрим конкретный триггер
            );
            if ($res) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param null|string $token
     * @param null|string $apiUser
     * @return array
     * @throws APIException
     * @throws APIHeaderException
     * @throws Exception
     */
    private static function getApiAccess(?string $token, ?string &$apiUser): array //TODO: дублируется функциона с WIDGET!!
    {
        if ($token == null) {
            throw new APIException('API token is null');
        }

        $access = DataBase::query()
            ->select('t1.*')
            ->from('api-token-access as t1')
            ->joinTable('api-token as t2')
            ->join('t1.apiUser', 't2.apiUser')
            ->where('t2.token', $token)
            ->all();

        //dump(DataBase::query()::$sql, $access);

        if (count($access) == 0) {
            throw new APIException('Invalid token.');
        }
        $apiUser = $access[0]['apiUser'];

        if (empty($apiUser)) {
            throw new APIException('API user does not exist');
        }

        return array_column($access, 'access');
    }
}