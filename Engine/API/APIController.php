<?php

namespace Engine\API;

use Engine\Core\Http\Input;
use Engine\Core\Objects\CoreServiceController;

abstract class APIController extends CoreServiceController
{
    /**
     * @return APIData
     */
    protected static function getAPIData(): APIData
    {
        //dump(Input::getGet());
        //Возвращаем
        return new APIData([
            'token' => Input::get('token'),
            'module' => Input::get('module'),
            'trigger' => Input::get('trigger'),
            'responseFormat' => Input::get('responseFormat'),
            'doDump' => Input::get('doDump') ?? false,
            'mode' => Input::get('mode') ?? 'normal'
        ]);
    }

    /**
     * @param APIData $apiData
     * @param int $mode
     */
    protected static function loadAPIData(APIData &$apiData, int $mode): void
    {
        $_apiData = [];
        switch ($mode) {
            case ModuleAPI::BOTH: //любые
                $_apiData = Input::getAll();
                break;

            case ModuleAPI::POST: //POST, по которому передавался JSON
                $_apiData = Input::getJson();
                break;

            case ModuleAPI::GET: //из гет запроса
                $_apiData = Input::getGet();
                break;
        }
        $data = [
            'user' => $apiData->getUser(),
            'token' => $apiData->getToken(),
            'module' => $apiData->getModule(),
            'trigger' => $apiData->getTrigger(),
            'mode' => $apiData->getMode(),
            'doDump'=>$apiData->isDump(),
            'responseReturn' => (bool)($_apiData['responseReturn'] ?? false),
            'responseFormat' => $_apiData['responseFormat'] ?? $_apiData['format'] ?? 'JSON'
        ];

        foreach ($data as $k => $v) {
            unset($_apiData[$k]);
        }

        $data['params'] = $_apiData;
        //Возвращаем
        $apiData = new APIData($data);
    }

    /**
     * Ответ API контроллера
     * @param $raw
     * @param bool $json
     * @param bool $return
     * @param bool $dump
     * @return string
     */
    protected function SendResponse($raw, bool $json = false, bool $return = false, bool $dump = false): string
    {
        if ($json) {
            $raw = Trim(json_encode($raw, JSON_UNESCAPED_UNICODE));
        }

        if ($return) {
            return $raw;
        } else {
            if ($dump) {
                dump($raw);
            } else {
                if (is_array($raw)) {
                    $raw = implode('; ', $raw);
                }
                echo $raw;
            }
            return 'return raw';
        }
    }

    /**
     * Ответ API контроллера в формате JSON
     * @param $data
     * @param bool $return
     * @return string
     */
    protected function SendJSONResponse($data, bool $return = false, bool $dump = false): string
    {
        $send = array
        (
            'access' => true,
            'data' => $data
        );
        return $this->SendResponse($send, true, $return, $dump);
    }

    /**
     * Ответ API контроллера об ошибке в формате JSON
     * @param $error
     * @param bool $return
     * @param bool $dump
     * @return string
     */
    protected function SendJSONError($error, bool $return = false, bool $dump = false): string
    {
        $error = array
        (
            'access' => false,
            'data' => array(
                'error' => $error
            )
        );
        return $this->SendResponse($error, true, $return, $dump);
    }
}