<?php

namespace Engine\API\Client;


use ErrorException;

class APIClient
{
    private static $user_agent = 'FelAPIRest';
    private static $curl_time_out = 60; // in seconds
    private static $curl_connect_time_out = 30; // in seconds

    /**
     * @param $url
     * @param string $method
     * @param array $params
     * @param bool $dump
     * @return bool|string
     * @throws ErrorException
     */
    public static function execs($url, $method = 'GET', $params = array(), bool $dump = false)
    {
        $httpBuildParams = http_build_query($params, '', '&');
        if ($dump) {
            dump($url);
        }
        $curl = curl_init($url . ($method == 'GET' && $params ? '?' . $httpBuildParams : ''));
        //dump($httpBuildParams);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, self::$user_agent);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $httpBuildParams);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-type: application/x-www-form-urlencoded'
            ));
            if ($dump) {
                dump($httpBuildParams);
            }
        } elseif ($method == 'JSON') {
            curl_setopt($curl, CURLOPT_POST, true);
            $params = json_encode($params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($params))
            );
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
            if ($dump) {
                dump($params);
            }
        } else {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/xrds+xml, */*'));
        }

        curl_setopt($curl, CURLOPT_TIMEOUT, self::$curl_time_out); // defaults to infinite
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::$curl_connect_time_out); // defaults to 300s

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            throw new ErrorException(curl_error($curl), curl_errno($curl));
        }

        return $response;
    }

}