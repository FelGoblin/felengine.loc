<?php

namespace Engine\API;

use Engine\Core\Objects\FillClass;
//use Modules\SiteObject\User\User;
use Engine\Core\Objects\IUser;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Src\Engine\Exceptions\ArgumentNullException;

/**
 * Класс - упаковка данных входящих параметров в GET строке запроса
 * Class APIData
 * @package Engine\API
 */
class APIData extends FillClass
{
    /**
     * @var bool
     */
    protected $mode = 'normal';

    /**
     * @var
     */
    protected $user;

    /**
     * Ериггер модуля
     * @var
     */
    protected $trigger;

    /**
     * Токен пользователя, запросившего API функцию
     *
     * @var string
     */
    protected $token = '';

    /**
     * Достпуп
     * @var string
     */
    protected $access = '';

    /**
     * Массив параметров API запроса
     *
     * @var array
     */
    protected $params = [];

    /**
     * Модуль апи к которому идёт обращение
     * @var string
     */
    protected $module = '';

    /**
     * ВОзвращаемый формат данных
     * @var string
     */
    protected $responseFormat = 'JSON';

    /**
     * Ответ должен быть выведен через echo в противном случа будет просто вернут массив через return
     * @var bool
     */
    protected $responseReturn = false;

    /**
     * Дамп ответ
     * @var bool
     */
    protected $doDump = false;

    /**
     * @return string: api.php?token=token_hash
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param array $access
     */
    public function setAccess(array $access): void
    {
        $this->access = $access;
    }

    /**
     * @return string: api.php?trigger=trigger_name
     */
    public function getTrigger(): string
    {
        return $this->trigger;
    }

    /**
     * @param string $trigger
     */
    public function setTrigger(string $trigger): void
    {
        $this->trigger = $trigger;
    }

    /**
     * В дате обязателен должен быть токен, пользователь и триггер. Остальное по факту
     * @return void
     * @throws APIException
     * @throws APIHeaderException
     */
    public function validationData(): void
    {
        //информация о требуемом заголовке ответа
        $header = $this->getParams('format') == 'header';
        if (empty($this->token)) {
            throw new APIException('Undefined token.', 400, $header);
        }
        if (empty($this->module)) {
            throw new APIException('Undefined module.', 400, $header);
        }
        if (empty($this->trigger)) {
            throw new APIException('Undefined trigger.', 400, $header);
        }
        //"http://stream-console.loc/api/cfac957f23ba82992652ebf8873738e0/service/qwerty";
    }

    /**
     * Получить значение параметра
     * @param string|null $key
     * @param bool $toLower
     * @return string|array
     */
    public function getParams(string $key = null, bool $toLower = false)
    {
        if ($key == null)
            return $this->params;

        $key = $this->params[$key] ?? null;

        if ($toLower)
            $key = strtolower($key);

        return $key;
    }

    /**
     * Приведённый к типу параметр
     * @param string $key
     * @param string $type
     * @param null $default
     * @return array|bool|float|int|string|null
     */
    public function getCastParam(string $key, string $type = 'string', $default = null)
    {
        return cast($this->getParams($key) ?? $default, $type);
    }

    /**
     * Добавить параметр
     * @param string $key
     * @param string $value
     */
    public function addParam(string $key, string $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * @return string: api.php?responseFormat=JSON/string
     */
    public function getResponseFormat(): ?string
    {
        return $this->responseFormat;
    }

    /**
     * @param bool $lower
     * @return string: api.php?module=module_name
     */
    public function getModule(bool $lower = true): string
    {
        if ($lower) {
            return strtolower($this->module);
        }
        return $this->module;
    }

    /**
     * @return bool
     */
    public function isResponseReturn(): bool
    {
        return $this->responseReturn;
    }

    public function isDump(): bool
    {
        return $this->doDump;
    }

    /**
     * @return null|IUser
     */
    public function getUser(): ?IUser
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $responseFormat
     */
    public function setResponseFormat(string $responseFormat): void
    {
        $this->responseFormat = $responseFormat;
    }
}