<?php

namespace Engine\API;


use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\Http\Redirect;
use Engine\Core\Objects\CoreServiceController;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Engine\Modules\ModData;
use Exception;

class API extends APIController
{
    /**
     * Зарегистрированные модули апи
     * @var array
     */
    private $registredAPIModules = [];

    /**
     * @param string|null $config
     * @param string|null $detour
     * @param array $detourData
     * @throws Exception
     */
    public static function init(string $config = null, string $detour = null, array &$detourData = []): void
    {
        /** @var API $self */
        $self = self::getInstance();
        $modsData = [];
        parent::init('api', DETOUR_API, $modsData);
        /** @var ModData $modData */
        foreach ($modsData as $modData) {
            //dump($modData->getApiAlias());
            //возможно создавать кучу методов разных алиасов.
            /*
            $alias = $modData->getApiAlias();
            if (is_array($alias)) { //чисто гипотетически можно задавать несколько псевдонимов
                foreach ($alias as $_alias) {
                    $self->registredAPIModules[$_alias][] = $modData;
                }
            } else {
            */
            $self->registredAPIModules[$modData->getApiAlias()][] = $modData;
            //}
        }
        //dump($self->registredAPIModules);
        $self->apiController();
    }

    /**
     * @return CoreServiceController
     */
    protected static function getInstance(): CoreServiceController
    {
        if (self::$instance == null) {
            self::$instance = new API();
        }
        return static::$instance;
    }

    /**
     * @param array $apiData
     * @return null|string
     */
    private function apiController(array $apiData = []): ?string
    {
        try {
            //dump($this->registredAPIModules);
            $apiData = self::getAPIData();
            //валидация параметров
            /**  @throws Exception */
            $apiData->validationData();
            //проверка владельца токена + проверка доступа владельца к такой функции
            /**  @throws Exception */
            APITokenControl::check($apiData);
            //нижный регистр
            $module = strtolower($apiData->getModule());
            //ищем в алиасах имя нужного неймспейса, он же имя файла

            if (!isset($this->registredAPIModules[$module])) {
                throw new APIException(sprintf('Not registred any API module by name or alias as: <b>%s</b>', $module));
            }
            $_call = false;
            $response = [];
            //TODO: отрефакторить этот момент.
            for ($i = 0; $i < count($this->registredAPIModules[$module]); $i++) {

                /** @var ModData $modData */
                $modData = $this->registredAPIModules[$module][$i] ?? null;
                if ($modData == null) {
                    throw new APIException(sprintf('Undefined API module <b>%s</b>', $module));
                }

                $apiClass = $modData->getClass();
                //вероятно всего это лишняя проверка
                if (!class_exists($apiClass) /*OR !class_exists($APIclass)*/) {
                    throw new APIException(sprintf('Unknown API module %s', $apiClass));
                }

                if (!($apiClass . '::class' instanceof ModuleAPI)) {
                    throw new APIException(sprintf('The module [%s] does not have the support extension method API', $apiClass));
                }

                /** @var ModuleAPI $apiClass */
                $apiClass = new $apiClass($apiData);
                self::loadAPIData($apiData, $apiClass->getDataTytpe());
                $trigger = $apiClass->APITriggerController($apiData);
                //dump($trigger);
                if ($trigger != null) { //получаем первый доступный метод.
                    $_call = true;
                    //инициализируем пост обработку модулей
                    API::initServices('modulesRun');
                    //результат
                    $response = $apiClass->getResponse();

                    // ### EVENT START ###
                    Events::invoke(Event::onAPImodDoTrigger,//отправляем на хук
                        [
                            'mod' => $module,
                            'trigger' => $trigger,
                            'apiClass' => $apiClass,
                            'apiData' => $apiData,
                            'response' => &$response //в процессе хукаем результат при необхожимости
                        ]);
                    // ### EVENT END ###
                    break;
                }
            }
            if ($_call == false) { //мы прошлись по всем методам и ничего не нашли.
                ModuleAPI::unknownModuleTrigger($apiData->getTrigger(), $module);
            }
            //возвращаем результат
            return $this->response($apiData, $response);
        } catch (APIHeaderException $ex) {
            Redirect::err($ex->getCode(), $ex->getMessage());
        } catch (APIException $ex) {
            //Todo: Log as API
            return $this->errorResponse($apiData, $ex->getMessage());
        } catch (Exception $ex) {
            //TODO: LOG AS ANOTHER IN API
            return $this->errorResponse($apiData, $ex->getMessage());
        }
        return null;
    }

    /**
     * Ответ API контроллера
     * @param APIData $apiData
     * @param $data
     * @return null|string
     */
    private function response(APIData $apiData, $data): ?string
    {
        if ($apiData->getResponseFormat() == 'JSON') {
            return $this->SendJSONResponse($data, $apiData->isResponseReturn(), $apiData->isDump());
        } else {
            return $this->SendResponse($data, false, $apiData->isResponseReturn(), $apiData->isDump());
        }
    }

    /*********************************************************************/

    /**
     * Ответ API контроллера - ошибка
     * @param APIData $apiData
     * @param string $message
     * @return null|string
     */
    private function errorResponse(APIData $apiData, string $message = ""): ?string
    {
        if ($apiData->getResponseFormat() == 'JSON')
            return $this->SendJSONError($message, $apiData->isResponseReturn(), $apiData->isDump());
        else
            return $this->SendResponse($message, false, $apiData->isResponseReturn(), $apiData->isDump());
    }
}
