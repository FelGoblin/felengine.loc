<?php

namespace Engine\API;


use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\Http\Input;
use Engine\Exceptions\APIException;
use Engine\Exceptions\APIHeaderException;
use Engine\Exceptions\ArgumentNullException;
use Exception;
use ReflectionClass;
use ReflectionMethod;

abstract class ModuleAPI implements IAPIModule
{
    /**
     * Тип данных передаваемых в модуль апи любой. Подойдут как и гет, так и пост
     */
    const BOTH = 0;
    /**
     * Тип данных принимается только POST и только JSON
     */
    const POST = 1;
    /**
     * Тип данных принимается только через GET api.php?param=value...
     */
    const GET = 2;
    /**
     * Работает только с методами внутри своего модуля.
     */
    const MODE_PROTECT = 'MODE_PROTECT';
    /**
     * Этот модуль можно хукать в любом месте апи инициализаии
     */
    const MODE_PUBLIC = 'MODE_PUBLIC';
    /**
     * Защищёный режим работы. Все отключено
     */
    const MODE_PRIVATE = 'MODE_PRIVATE';
    /**
     * Псевдоним модуля
     * @var null|string
     */
    public static $alias = null;
    /**
     * режим
     * @var string
     */
    public static $mode = self::MODE_PROTECT;
    /**
     * Метод получения данных
     * @var int
     */
    protected $dataTytpe = self::BOTH;
    /**
     * Название метода модуля апи
     * @var string
     */
    protected $trigger = '';
    /**
     * RAW Запрос
     * @var string
     */
    protected $response = '';
    /**
     * Собранные данные для АПИ
     * @var APIData
     */
    protected $APIData;
    /**
     * Результат обработки метода модуля АПИ
     * @var
     */
    protected $triggerResult;

    /**
     * ModuleAPI constructor.
     * @param APIData $apiData
     * @param bool $autoTrigger
     * @throws APIException
     * @throws APIHeaderException
     */
    public function __construct(APIData $apiData, bool $autoTrigger = false)
    {
        $this->APIData = $apiData;
        $this->trigger = $this->getAPIData()->getTrigger();

        if ($autoTrigger) {
            $this->APITriggerController();
        }
    }

    /**
     * @return APIData
     */
    protected function getAPIData(): APIData
    {
        return $this->APIData;
    }

    /**
     * Контроллер трриггера
     * @param APIData|null $apiData
     * @return mixed
     */
    public function APITriggerController(APIData $apiData = null): ?string
    {
        $trigger = str_replace(['_', '-', ' '], '', $this->trigger);
        if (method_exists($this, $trigger)) {
            if ($apiData !== null) {
                $this->APIData = $apiData;
            }
            $this->$trigger();
        } else {
            //self::unknownModuleTrigger();
            $trigger = null;
        }
        return $trigger;
    }

    /**
     * @param $module
     * @param $trigger
     * @throws APIException
     * @throws APIHeaderException
     */
    public static function unknownModuleTrigger($module, $trigger): void
    {
        throw new APIException(sprintf('The requested trigger [%s] is not implemented in the module [%s]', $trigger, $module));
    }

    /**
     * В данной процедуре моожео подписаться на событие или сделать еще что-либо
     */
    public static function modEventsInit(): void
    {
        //nothing here.
        return;
    }

    /**
     * Получить массив или строку данных из получееного сервером JSON файла
     * @param bool $asArray
     * @return string|array
     * @throws Exception
     */
    protected static function getJSONFromFile(bool $asArray = true)
    {
        return Input::getJSONFromFile($asArray);
    }

    /**
     * @param callable $cb
     * @param string|null $mod
     * @param string|null $trigger
     * @throws Exception
     */
    protected static function doModuleEvent(callable $cb, string $mod = null, string $trigger = null)
    {
        if ($mod == null AND $trigger != null) {
            $mod = static::getModName();
        }

        if (static::$mode != self::MODE_PRIVATE) {
            Events::on(Event::onAPImodDoTrigger, function ($args) use ($mod, $trigger, &$cb) {
                /** @var ModuleAPI $apiClass */
                $apiClass = $args['apiClass'];
                if (($apiClass::$mode == self::MODE_PUBLIC) OR ($apiClass::$mode == self::MODE_PROTECT AND ($args['mod'] == static::getModName() OR $apiClass instanceof static))) {
                    if (
                        ($mod == null AND $trigger == null) OR //любой мод и любой триг
                        ($mod == $args['mod'] AND $trigger == null) OR //любой триг указанного мода
                        ($mod == $args['mod'] AND $trigger == $args['trigger']) //указанный триг указанного мода
                    ) {
                        $cb($args);
                    }
                }
            });
        }
    }

    /**
     * Имя класса
     * @return string
     */
    public static function getModName(): string
    {
        return static::$alias ?? get_class(static::class);
    }

    /**
     * @param $mode
     * @param null $method
     * @return bool
     */
    public function checkMode($mode, $method = null): bool
    {
        if (is_array($this->mode)) {
            foreach ($this->mode as $k => $_mode) {
                if ((is_numeric($k) AND $mode === $this->mode)) {
                    return true;
                } else if ($method === $k && $mode === $_mode) {
                    return true;
                }
            }
            return false;
        }
        return $this->mode === $mode;
    }

    /**
     * @return array|string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return int
     */
    public function getDataTytpe(): int
    {
        return $this->dataTytpe;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getAPITriggers(): array
    {
        $class = new ReflectionClass($this);
        //Todo: добавить список-сключений.
        return array_column($class->getMethods(ReflectionMethod::IS_PUBLIC), 'name');
    }

    /**
     * Извлекаем поля запросов из массива
     * @param array $fields
     * @param array $addData
     * @return array
     */
    protected function extractFields(array $fields, array $addData = []): array
    {
        dump($this->APIData->getParams());
        //array_intersect_key — Вычислить пересечение массивов, сравнивая ключи
        $result = array_intersect_key($this->APIData->getParams(), array_flip($fields)); //возвращает массив, содержащий все элементы $input, имеющие ключи, содержащиеся в $fields (для этого свапаем местами ключи и значения).
        if (!empty($addData)) { //нужно добавить параметры еще к найденным
            $result = array_merge($result, $addData);
        }
        return $result;
    }

    /**
     * Получить параметры из полей.
     * @param array $fieldsName
     * @param bool $throwIfAnyUndefined - true = выбросить исклоючение / false = заполнить null если отсутствует
     * @return array
     * @throws ArgumentNullException
     */
    protected function getStrictFields(array $fieldsName, bool $throwIfAnyUndefined = true): array
    {
        $result = [];
        foreach ($fieldsName as $field) {
            $f = $this->getAPIData()->getParams($field) ?? null;
            if ($f == null) {
                if ($throwIfAnyUndefined) {
                    throw new ArgumentNullException("Undefined API field: $field");
                } else {
                    $result[$field] = null;
                }
            } else {
                $result[$field] = $f;
            }
        }
        return $result;
    }

    /**
     * Получить параметры из полей, однако они могут быть неопределены. Но если throw = true, то будет вызвано исключение, если все параметры неопределены.
     * @param array $fieldsName
     * @param bool $throwIfAllUndefined
     * @return array
     * @throws ArgumentNullException
     */
    protected function getFields(array $fieldsName, bool $throwIfAllUndefined = true): array
    {
        $result = [];
        $b = false;
        foreach ($fieldsName as $field) {
            $f = $this->getAPIData()->getParams($field) ?? null;
            if ($f == null) {
                $result[$field] = null;
            } else {
                $result[$field] = $f;
                $b |= true;
            }
        }
        if (!$b AND $throwIfAllUndefined) {
            $list = implode(" / ", $fieldsName);
            throw new ArgumentNullException("Undefined $list");
        }
        return $result;
    }

    /**
     * @param $data
     * @throws APIException
     * @throws APIHeaderException
     */
    protected function getAPIClientResponse($data): void
    {
        $response = json_decode($data, true);

        if (empty($response)) {
            throw new APIException('Empty response from service');
        }

        if ($response['access']) {
            $this->response = $response['data'];
        } else {
            throw new APIException($response['error'] ?? 'ERROR');
        }
    }
}