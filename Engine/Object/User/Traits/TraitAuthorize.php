<?php

namespace Engine\Object\User\Traits;

use Engine\Core\Core;
use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\Objects\IUser;
use Engine\Core\Objects\IUserData;
use Engine\Core\Session\Session;
use Engine\Core\View\Render;
use Engine\Object\User\_User;
use Exception;
use Modules\_Auth\BaseAuth;

/**
 * Авторизация пользователя
 * Trait TraitAuthorize
 * @package Engine\Object\User\Traits
 */
trait TraitAuthorize
{
    /**
     * Пытаемся восстановить пользователя
     * @inheritDoc
     * @throws Exception
     */
    public static function TryRestoreUser(): ?IUser
    {
        //Смотрим сессии
        if (!Session::has('userID') OR !Session::has('userHASH')) {
            return null;
        }
        //вытаскиваем ID пользователя из сессии
        $userName = Session::get('userID');

        //получаем из базы данные о пользователе с с сохранённым ID (ВНИМАНИЕ!!! ЭТО копия пользователя, а не текущий пользователь)
        //$userData = self::getUserByID((int)$userID);
        /** @var _User $user */
        $user = Core::getUser();
        $user = $user::getUser($userName, ['!UserAccess']);

        //dump($user);

        //Нет ничего
        if ($user == null) {
            Render::setError('Данные о пользователе, сохранённые на этом компьютере недействительные. Пользователь не существует или удалён.', true);
            BaseAuth::Destroy();
            return null;
        }

        //пользовательские данные
        //$userData = _UserData::initUserData($userData);

        //хеш, который хранится в сессии
        $hash = Session::get('userHASH');

        //проверяем хеш пользователя
        if (!self::checkHash($user->getUserData(), $hash)) {
            Render::setError('Не верные параметры входа. Необходимо войти еще раз.', true);
            //die;
            BaseAuth::Destroy();
            return null;
        }

        //проверяем блокировку пользователя
        if ($user->isBlocked()) {
            Render::setError('Пользователь заблокирован.', true);
            BaseAuth::Destroy();
            return null;
        }
        /** @var _User $user */
        $user->doRestore();
        return $user;
    }

    /**
     * Сверяем хеши авторизации
     * @param IUserData $user
     * @param string|null $hash
     * @return bool
     */
    public static function checkHash(IUserData $user, string $hash = null): bool
    {
        //dump(empty($hash), $user->getHash() == $hash, $user, $user->getHash(), $hash);
        return (!empty($user) AND !empty($hash) AND $user->getHash() == $hash);
    }

    /**
     * Производим восстановление пользователя
     * @throws Exception
     */
    private function doRestore()
    {
        //### EVENT START: устанавливаем пользователя ###
        Events::invoke(Event::onSetUser, ['user' => $this]);
        //### EVENT END ###

        //### EVENT START: пользователь авторизовался ###
        Events::invoke(Event::onUserAuthorize, ['user' => &$this]);
        //### EVENT END ###
    }
}
