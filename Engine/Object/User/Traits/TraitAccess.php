<?php

namespace Engine\Object\User\Traits;

use Engine\Core\Config\SiteConfig;
use Engine\Core\DataBase\DataBase;
use Engine\Object\User\_User;
use Exception;

/**
 * Система доступа пользователя к функциям сайта
 * Trait UserAccess
 * @package Objectss\User\APITraits
 */
trait TraitAccess
{
    /**
     * Инициирована ли система доступа
     * @var bool
     */
    private $userAccessInit = false;
    /**
     * Авторизация пользователя
     * @var bool
     */
    private $authorize = false;

    /**
     * Функции доступа
     * @var array
     */
    private $access = [];

    /**
     * Список всех доступных функций пользователя
     * @return array
     */
    public function getAccessList(): array
    {
        return $this->access;
    }

    /**
     * Устанавливаем функции по-уморлчанию
     * @param bool $reset
     * @throws Exception
     */
    public function userAccessAddDefaultFunctions(bool $reset = false): void
    {
        if ($reset) {
            $this->reset();
        }
        foreach ($this->userAccessGetDefault() as $function) {
            $this->userAccessAddPermission($function);
        }
    }

    /**
     * СБросить все функции доступа
     * @throws Exception
     */
    public function reset(): void
    {
        DataBase::query()
            ->delete('user-access')
            ->where('userName', $this->getUserName())
            ->run();
    }

    /**
     * Получаем функции по-умолчанию
     * @param string|null $type
     * @return array
     */
    public function userAccessGetDefault(string $type = null): array
    {
        if ($type == 'admin')
            return ['Root'];
        elseif ($type == 'moderator')
            return ['Moderator'];
        else {
            return SiteConfig::get('defaultAccess', 'access');
        }
    }

    /**
     * Добавить пользователю разрешение
     * @param string $permission
     * @throws Exception
     */
    public function userAccessAddPermission(string $permission): void
    {
        if ($this->userAccessCheck($permission)) {
            return;
        }
        DataBase::query()
            ->insert([
                'userName' => $this->getUserName(),
                'function' => $permission
            ])
            ->into('user-access')
            ->run();

        $this->access[] = strtolower($permission);
    }

    /**
     * Проверяем доступ к указанной функции
     * @param $permissions
     * @return bool
     */
    public function userAccessCheck($permissions): bool
    {
        $access = false;
        if (is_array($permissions)) {
            if (($access = $this->checkPermissions($permissions)) == false) {
                return false;
            }
        }

        //dump(['private' => true], $permissions, $this, $this->userAccessGetFunction('Root'));

        /**
         * старая проверка доступа TODO: будет удалена
         */
        if (!empty($permissions)) { //после новой проверки

            if (is_string($permissions)) {
                $permissions = [$permissions];
            } elseif (isset($permissions['access'])) {
                $permissions = $permissions['access'];
            }
            //1. Обязательно проверяем авторизацию.
            if (in_array('authorized', $permissions, true)) {
                if (!$this->isAuthorized()) { //не авторизован.
                    return false;
                }
                array_deleteValue('authorized', $permissions);
            }
            //root доступ, разрешаем всё
            if ($this->userAccessGetFunction('Root')) {
                return true;
            }

            //забаненный лишается доступа к функциям, которые требуют их
            if ($this->userAccessGetFunction('ban')) {
                return false;
            }

            //dump($permissions);

            foreach ($permissions as $permission) {
                if (!$this->userAccessGetFunction($permission)) {
                    return false;
                }
            }
            return true;
        }

        return $access;
    }

    /**
     * Новая проверка доступа
     * @param array $permissions
     * @return bool
     */
    private function checkPermissions(array &$permissions): bool
    {
        /*
        * Есть доступ на обязательные проверки доступа
        */
        if (isset($permissions['required'])) {
            $access = $this->checkRequired($permissions['required']);
            if (!$access) //не прошли обязательную проверку
            {
                return false;
            }
        }

        if (isset($permissions['access'])) {
            $access = $this->checkAccess($permissions['access']);
            unset($permissions['required'], $permissions['access']);
            return $access;
        }
        return true;
    }

    /**
     * Проверка обязательных прав доступа
     * @param array $permissions
     * @return bool
     */
    private function checkRequired($permissions): bool
    {
        $result = true;

        if (is_array($permissions)) {

            foreach ($permissions as $permission) {
                if ($permission == 'authorized') {
                    $result &= $this->isAuthorized();
                } else {
                    $result &= $this->userAccessGetFunction($permission);
                }
                if (!$result) {
                    return false;
                }
            }
        } else {
            if ($permissions == 'authorized') {
                $result &= $this->isAuthorized();
            } else {
                $result &= $this->userAccessGetFunction((string)$permissions);
            }
        }
        return $result; //$result тут будет всегда true...
    }

    /**
     * Пользователь авторизирован
     * @return bool
     */
    public function isAuthorized(): bool
    {
        return $this->authorize;
    }

    /**
     * Проверяем наличие функции у пользователя в доступе
     * @param string $function
     * @return bool
     */
    private function userAccessGetFunction(string $function): bool
    {
        // dump($function, $this->access, in_array(strtolower($function), $this->access, true));

        return in_array(strtolower($function), $this->access, true);
    }

    /**
     * Проверка доступа для групп.
     * @param array|null $permissions
     * @return bool
     */
    private function checkAccess(array $permissions = null): bool
    {
        //забаненный лишается доступа к функциям, которые требуют их
        if ($this->userAccessGetFunction('ban')) {
            return false;
        }

        if ($permissions == null OR empty($permissions)) {
            return true; //права не требует отдельных групп, а значит доступнодля всех.
        } //иначе есть группы к которым разрешён доступ
        //Но у пользователя Root, разрешаем всё
        if ($this->userAccessGetFunction('Root')) {
            return true;
        }
        //проверяем требования к группам
        $result = false;
        foreach ($permissions as $permission) {
            if ($permission == 'authorized') {
                $result |= $this->isAuthorized();
            } else {
                $result |= $this->userAccessGetFunction($permission);
            }
        }
        return $result;
    }

    /**
     * Пользователь забанен
     * @return bool
     */
    public function userIsBaned(): bool
    {
        return in_array('ban', $this->access, true);
    }

    /**
     * @inheritDoc
     */
    public function isBlocked(): bool
    {
        /** @var _User $this */
        return $this->getUserData()->isBlocked();
    }

    /**
     * Удалить разрешение
     * @param string $permission
     * @throws Exception
     */
    public function userAccessDelPermission(string $permission): void
    {/*
        if (!$this->userAccessCheck($permission)) {
            return;
        }
*/
        DataBase::query()
            ->delete('user-access')
            ->where('userName', $this->getUserName())
            ->where('function', $permission)
            ->run();

        array_deleteValue($permission, $this->access);
    }

    /**
     * Установить пароль пользователя
     */
    public function userAccessResetPassword(): void
    {
        /** @var _User $this */
        $this->getUserData()->setPassword('');
    }

    /**
     * Установить статус авторизации
     * @param bool $authorize
     */
    public function setAuthorize(bool $authorize): void
    {
        $this->authorize = $authorize;
    }
}