<?php


namespace Engine\Object\User\Traits;

use Engine\Core\DataBase\DataBase;
use Engine\Exceptions\AuthException;
use Exception;

/**
 * Получить пользователя
 * Trait TraitGetUser
 * @package Engine\Object\User\Traits
 * TODO: перевести все трейты в интерфейсы и реализовывать отдельно в _SiteModule
 */
trait TraitGetUser
{
    /**
     * Получить данные пользователя по ID
     * @param int $id
     * @param bool $full
     * @return array
     * @throws Exception
     */
    public static function getUserByID(int $id, bool $full = false): array
    {
        return self::getUserFromDB($id, false);
    }

    /**
     * @param string $userName
     * @return array
     * @throws Exception
     */
    public static function getUserByName(string $userName): array
    {
        return self::getUserFromDB($userName, true);
    }

    /**
     * Получить данные пользователя из базы данных
     * TODO: отвязать структуру пользователя в БД в движке
     * @param $uID
     * @param bool $isName
     * @return array
     * @throws Exception
     */
    public static function getUserFromDB($uID, bool $isName = true): array
    {
        if (DataBase::query() == null) {
            throw new AuthException('(ERROR) GET USER: DataBase not connected.');
        }
        $userDB = [];

        if (is_numeric($uID) && !$isName) {
            $userDB = DataBase::query()
                ->select()
                ->from('user')
                ->where('id', $uID, '=', 'int')
                ->one();
        } elseif (is_string($uID)) {
            $userDB = DataBase::query()
                ->select()
                ->from('user')
                ->where('userName', $uID)
                ->one();
        }
        return $userDB;
    }

}
