<?php


namespace Engine\Object\User;

use Engine\Core\DataBase\DataBase;
use Engine\Core\Objects\FillClass;
use Engine\Core\Objects\IUser;
use Engine\Core\Objects\IUserData;
use Engine\Core\Objects\IViewModel;
use Exception;

/**
 * Class _UserData
 * @package Engine\Object\User
 */
abstract class _UserData extends FillClass implements IUserData, IViewModel
{
    /**
     * ID пользователя
     * @var int
     */
    protected int $id = -1;
    /**
     * Имя пользователя
     * @var string
     */
    protected string $userName = '';
    /**
     * Пароль пользователя
     * @var string
     */
    protected string $password = '';

    /**
     * Токен АПИ
     * @var string
     */
    protected string $apiToken = '';

    /**
     * Соль пароля
     * @var string
     */
    protected string $salt = '';

    /**
     * Хеш сессии пользователя
     * @var string
     */

    protected string $hash = '';
    /**
     * Email пользователя
     * @var string
     */
    protected string $email = '';

    /**
     * Локализация
     * @var string|null
     */
    protected ?string $locale = null;
    /**
     * Метод входа пользователя
     * @var string|null
     */
    protected ?string $joinMethod = '';
    /**
     * Пользователь заблокирован
     * @var bool
     */
    protected bool $blocked = true;

    /**
     * @param $userData
     * @return static|null
     */
    static function initUserData($userData): IUserData
    {
        $class = get_called_class();
        return new $class($userData);
    }

    /**
     * @param IUser $host
     * @param null $data
     */
    public static function init(IUser $host, $data = null): void
    {
        if ($data != null) {
            $host->setModuleExtension('userData', $data);
            //Init API TOKEN
            if ($data instanceof IUserData) {
                $data->initAPIToken();
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function initAPIToken(): void
    {
        $this->apiToken = DataBase::query()
            ->select('token')
            ->from('api-token')
            ->where('apiUser', $this->userName)
            ->one('token');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getUriName(): string
    {
        return urldecode($this->userName);
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Хоть функция имеет публичный ключ, однако она ни на что не влияет. Она чисто для декора  и для генерации хеша. Вернее оно конечно сбросит, но только в текущем параметре, не более.
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * Хоть функция имеет публичный ключ, однако она ни на что не влияет. Она чисто для генерации нового хеша. Вернее оно конечно сбросит, но только в текущем параметре, не более.
     * @param string $salt
     */
    public function setSalt(string $salt): void
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @return null
     */
    public function getJoinMethod(): ?string
    {
        return $this->joinMethod;
    }

    /**
     * @param $joinMethod
     */
    public function setJoinMethod($joinMethod)
    {
        $this->joinMethod = $joinMethod;
    }

    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    /**
     * @param bool $blocked
     */
    public function setBlocked(bool $blocked): void
    {
        $this->blocked = $blocked;
    }

    /**
     * Сохраняем пользователя
     * @param array $data
     * @throws Exception
     */
    public function saveData(array $data = []): void
    {
        $data = array_merge($this->toArray(), $data);
        $id = $data['id'];
        unset($data['id']);
        //обновляем данные пользователя
        DataBase::query()
            ->update($data)
            ->into('user')
            ->where('id', $id)
            ->run();
        //dump($data);
        //die;
    }

    /**
     * @return array|null
     */
    protected function toArray(): ?array
    {
        return [
            'id' => $this->id,
            'userName' => $this->userName,
            'password' => $this->password,
            'hash' => $this->hash,
            'salt' => $this->salt,
            'blocked' => $this->blocked,
            'email' => $this->email,
            'locale' => $this->locale,
            'joinMethod' => $this->joinMethod
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
