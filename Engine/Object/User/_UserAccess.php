<?php


namespace Engine\Object\User;


use Engine\Core\Objects\IUser;
use Engine\Core\Objects\IUserModule;
use Engine\Core\Objects\IViewModel;

/**
 * Class UserAccess
 * @package Engine\Object\User
 */
abstract class _UserAccess implements IUserModule, IViewModel
{

    /**
     * @inheritDoc
     */
    public static function init(IUser $host, $data = null): void
    {
        $host->userAccessInit();
    }
}