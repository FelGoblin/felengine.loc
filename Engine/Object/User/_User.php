<?php


namespace Engine\Object\User;


use Engine\Core\Core;
use Engine\Core\DataBase\DataBase;
use Engine\Core\Objects\IUser;
use Engine\Core\Objects\IUserData;
use Engine\Core\Objects\IUserModule;
use Engine\Core\Objects\IViewModel;
use Exception;
use Object\User\User;
use ReflectionClass;
use ReflectionException;

abstract class _User implements IUser, IViewModel
{
    use Traits\TraitAuthorize;
    use Traits\TraitGetUser;
    use Traits\TraitAccess;

    /**
     * TODO: наверное стоит убрать
     * @var IUser|null
     */
    protected static ?IUser $instance = null;

    /**
     * Модули расширения
     * @var array
     */
    protected array $modules = [];
    /**
     * @var bool
     */
    protected bool $needSave = false;
    /**
     * @var IUserData
     */
    protected IUserData $userData;
    /**
     * @var array
     */
    protected array $userDataRaw = [];

    /**
     * User constructor.
     * @param $data
     * @param array $includedModules
     * @throws Exception
     */
    public function __construct($data = null, array $includedModules = [])
    {
        if (is_array($data)) {
            $this->userDataRaw = $data;
        }
        $this->load($includedModules);
    }

    /**
     * @param array $includedModules
     * @throws Exception
     */
    public function load(array $includedModules = []): void
    {
        //dump('load', $includedModules);
        //инициируем модули
        $this->initModules($includedModules);
        /*
        //вызываем событие для инициализации
        Events::invoke(Event::onUserInit, ['user' => &$this, 'modules' => $includedModules]);
        */
    }

    /**
     * Инициируем все модули для текущего потомка класса
     * Каждый модуль в потомке должен содержать подписку на событие инициализации
     * @param array $includedModules
     * @throws ReflectionException
     */
    public function initModules(array $includedModules = []): void
    {
        $required = [];
        if (!empty($this->userDataRaw)) {
            $required['UserData'] = $this->userDataRaw;
            $this->userDataRaw = [];
        }

        //нет информации о доступе
        if (!in_array('UserAccess', $includedModules) AND !in_array('!UserAccess', $includedModules)) {
            $required[] = 'UserAccess';
        }
        if (count($required) > 0) {
            $includedModules = array_merge($required, $includedModules);
        }

        //dump($this->isAuthorized());

        foreach ($includedModules as $module => $data) {
            if (is_numeric($module)) {
                $module = $data;
            }
            $moduleClass = 'Object\\User\\' . $module;
           //dump($moduleClass, file_exists(pathSeparator(ROOT_DIR . $moduleClass . '.php')), class_exists($moduleClass));
            if (class_exists($moduleClass)) {
                $reflectionClass = new ReflectionClass($moduleClass);
                if ($reflectionClass->implementsInterface('Engine\\Core\\Objects\\IUserModule')) {
                    /** @var IUserModule $moduleClass */
                    $moduleClass::init($this, $data);
                }
            }
        }
    }

    /**
     * TODO: переделать на динамику
     * @param array $data
     * @return array
     */
    public static function getAllowModules(array $data = []): array
    {
        return $data;
    }

    /**
     * Глобальный активный текущий пользователь на сайте
     * @return self
     * @throws Exception
     */
    public static function this(): IUser
    {
        if (static::$instance == null) {
            $class = get_called_class();
            static::$instance = new $class();
        }
        return static::$instance;
    }

    /**
     * Установить Глобально значение текущего пользователя (обычно используется, где отсутствует GUI и данные пользователя берутся из других источников, например API или Widgets)
     * @param IUser $user
     */
    public static function setUser(IUser $user): void
    {
        static::$instance = $user;
    }

    /**
     * Имя текущего пользователя
     * @return string
     */
    public static function name()
    {
        return (static::$instance)->getUserData()->getName();
    }

    /**
     * Получить пользователя
     * @inheritDoc
     */
    public static function getUser(string $userName, array $includeModules = []): ?IUser
    {
        /** @var _User $class */
        $class = get_called_class();
        $userData = $class::getUserByName($userName);

        if (empty($userData)) {
            return null;
        }

        /** @var User $userClass */
        $userClass = Core::getUser();
        return new $userClass($userData, $includeModules);
    }

    /**
     * Создаём нового пользователя
     * @param array $userData
     * @return bool
     * @throws Exception
     */
    public static function createNewUser(array $userData): bool
    {
        return DataBase::query()
            ->insert($userData)
            ->into('user')
            ->run();
    }

    protected static function call(): string
    {
        return get_called_class();
    }

    /** Get
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->modules[$name] ?? null;
    }

    /**
     * @param string $field
     * @param $data
     */
    public function setModuleExtension(string $field, $data): void
    {
        if (property_exists($this, $field)) {
            $this->$field = $data;
        } else {
            $this->modules[$field] = $data;
        }
    }

    /**
     * @inheritDoc
     */
    public function refresh(): void
    {
        dump('TODO: Implement refresh() method.');
    }

    /**
     * @inheritDoc
     */
    public function saveUser(bool $checkSave = false): void
    {
        if ($checkSave AND !$this->isNeedSave()) {
            return;
        }
        //сохранем данные пользователя
        //dump('save this user');
        $this->getUserData()->saveData();
        if ($checkSave) {
            $this->needSave = false;
        }

    }

    /**
     * getter: Пользователя надо сохранить
     * @return bool
     */
    public function isNeedSave(): bool
    {
        return $this->needSave;
    }

    /**
     * setter: Пользователя надо сохранить
     * @param bool $needSave
     */
    public function setNeedSave(bool $needSave): void
    {
        $this->needSave = $needSave;
    }

    /**
     * @return IUserData|null
     */
    public function getUserData(): ?IUserData
    {
        return $this->userData;
    }

    /**
     * @inheritDoc
     */
    public function setUserData(IUserData $userData)
    {
        $this->userData = $userData;
    }

    /**
     * @inheritDoc
     */
    public function getViewModel(array $data = []): array
    {
        return array_merge(
            $data,
            [
                'userName' => $this->getUserData()->getName(),
                'joinMethod' => $this->getUserData()->getJoinMethod()
            ]
        );
    }

    /**
     * Пользователь заблокирован
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->getUserData()->isBlocked();
    }

    /**
     * @return bool
     */
    public function isPassword(): bool
    {
        return !empty($this->getUserData()->getPassword());
    }

    /**
     * Инициируем доступ пользователя
     * @param array $access
     * @throws Exception
     */
    public function userAccessInit(array $access = []): void
    {
        if ($this->userAccessInit)
            return;

        //dump($this->isAuthorized());
        /*
        if (!$this->isAuthorized()) {
            $this->userAccessInit = true;
            return;
        }
        */
        $access = DataBase::query()
            ->select('function')
            ->from('user-access')
            ->where('userName', $this->getUserName())
            ->all();

        if (!empty($access)) {
            $this->access = array_map('strtolower', array_column($access, 'function'));
        }

        //dump($this->access);

        $this->userAccessInit = true;
    }

    /**
     * @param bool $uri
     * @return string
     */
    public function getUserName(bool $uri = false): string
    {
        if (!$this->isAuthorized()) {
            return lang('@user_not_authorized');
        }
        if ($uri) {
            return urlencode($this->getUserData()->getName());
        } else {
            return $this->getUserData()->getName();
        }
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        if ($this->getUserData() != null) {
            return $this->getUserData()->getName();
        }
        return 'guest';
    }

    /**
     * Авторизирован ли пользователь
     * @return bool
     */
    function isAuthorized(): bool
    {
        return $this->authorize;
    }
}
