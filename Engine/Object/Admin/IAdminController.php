<?php


namespace Engine\Object\Admin;

/**
 * Interface IAdminController
 * @package Engine\Object\Admin
 */
interface IAdminController
{
    /**
     * @param array $data
     * @return bool
     */
    function controller(array $data = []): bool;
}