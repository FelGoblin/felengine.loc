<?php


namespace Engine\Object\Admin;

/**
 * Interface IAdmin
 * @package Engine\Object\Admin
 */
interface IAdmin
{
   /**
     * @param null $data
     */
    static function controller($data = null): void;
}