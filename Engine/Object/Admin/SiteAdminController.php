<?php


namespace Engine\Object\Admin;

use Engine\Core\Http\Input;
use Engine\Core\Objects\IUser;
use Engine\Core\View\Notice\ErrorMessage;
use Exception;

abstract class SiteAdminController implements IAdminController
{
    protected static $triggerName = 'admControl';
    protected $trigger;
    protected $count;
    /**
     * @var IUser
     */
    protected $user;

    protected $controllers = [];

    /**
     * BaseAdminController constructor.
     * @param IUser $user
     * @param array $data
     */
    public function __construct(IUser $user, array $data = [])
    {
        $this->user = $user;
        $this->count = Input::getPost('count', CAST_INT);
        if (isset($data['trigger'])) {
            $this->trigger = $data['trigger'];
        }
    }

    /**
     * @param array $data
     * @return IAdminController|null
     */
    public static function getController(array $data = []): ?IAdminController
    {
        $dir = pathSeparator(ROOT_DIR . $data['admin'] . '/Controller');

        //dump($dir, is_dir($dir));

        if (!is_dir($dir)) {
            return null;
        }
        $find = array_diff(scandir($dir), Array(".", ".."));
        foreach ($find as $d) {
            $path_parts = pathinfo($d);
            $class = sprintf('%s\\Controller\\%s', $data['admin'], $path_parts['filename']);
            if (class_exists($class)) {
                /** @var IAdminController $controller */
                $controller = new $class($data['user']);
                //dump($controller);
                $data['count'] = $data['count'] ?? $controller->count;
                if ($controller->controller($data)) {
                    return $controller;
                }
            }
        }
        return null;
    }

    protected function doController(): bool
    {
        $target = $this->trigger;
        $fn = $this->triggersMap[$target] ?? null;
        if ($target != null AND is_callable($fn)) {
            $fn();
        }
    }

    /**
     * Отходим от switch
     * @param array $triggers
     * @return bool
     */
    protected function doTriggers(array $triggers): bool
    {
        foreach ($triggers as $trigger) {
            if (self::doTrigger($trigger)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function doTrigger(array &$data): bool
    {
        $triggerName = $data[0];
        $triggerAction = $data[1];
        $fn = $data[2] ?? null;

        if (is_string($triggerAction)) {
            if (Input::getPost($triggerName, CAST_NULLSTR) == $triggerAction AND method_exists($this, $fn)) {
                $this->_do($fn, $data);
                return true;
            }
        } elseif (is_null($triggerAction)) {
            if (($val = Input::getPost($triggerName, CAST_NULLSTR)) !== null AND method_exists($this, $fn)) {
                $data[3][$triggerName] = $val;
                //dump($data);
                $this->_do($fn, $data);
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $method
     * @param array $data
     */
    private function _do(string $method, array $data)
    {
        try {
            $this->$method($data[3] ?? []);
        } catch (Exception $ex) {
            new ErrorMessage($ex->getMessage());
        }
    }

    /**
     * @param string|null $triggerName
     * @return string|null
     */
    protected function getControllerTrigger(string $triggerName = null): ?string
    {
        if ($triggerName != null) {
            return Input::getPost($triggerName, CAST_NULLSTR);
        } elseif ($this->trigger != null) {
            return $this->trigger;
        } else {
            return Input::getPost(self::$triggerName, CAST_NULLSTR);
        }
    }
}