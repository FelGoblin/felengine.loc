<?php


namespace Engine\Object\Admin;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Core;
use Engine\Core\Development\DebugInfo;
use Engine\Core\Http\Input;
use Engine\Core\Objects\IUser;
use Engine\Core\View\Render;
use Engine\IController;
use Engine\Modules\ISiteModuleServiceInit;
use Exception;

abstract class SiteAdmin implements IAdmin, ISiteModuleServiceInit
{

    /**
     * @var bool
     */
    protected static bool $edit = false;
    /**
     * @var bool
     */
    protected static bool $new = false;
    /**
     * @var IUser|null
     */
    protected static ?IUser $target = null;
    /**
     * POST данные в переменной @admControl
     * @var string
     */
    protected static string $trigger = '';

    /**
     * @var Object
     */
    protected object $data;

    /**
     * @param null $data
     * @throws Exception
     */
    public static function controller($data = null): void
    {
        if (!self::isAdmin()) {
            return; //Не админ, контроллер не обрабатывается
        }
        //TODO: прочекать этот момент
        if (isset($data['extend']) and is_callable($data['extend'])) { //этот момент уже не актуальный скорей всего
            $data['extend']();
        } else {
            $user = Input::getPost('user');
            /** @var IUser $_user */
            if (($_user = Core::getUser()) != null) {
                if ($user == null) {
                    $user = $_user::this();
                } elseif ($user == $_user::name()) {
                    $user = $_user::this();
                } else {
                    $user = $_user::getUser($user, $_user::getAllowModules());
                    if ($user == null) {
                        Render::setWarning('Target user not found');
                        return;
                    }
                }
            }
            $data['user'] = self::$target = $user;
            $data['trigger'] = self::getTrigger();
            $data['isNew'] = self::isNew();
            $data['isEdit'] = self::isEdit();
            //dump($data);
            if (($controller = SiteAdminController::getController($data)) != null) {
                if (self::$target != null) {
                    //dump(self::$target);
                    self::$target->saveUser(true);
                }
            } else {
                DebugInfo::debugAdd('<warning>[admin] Unknown or empty ADMIN controller</warning>');
            }
        }
    }

    /**
     * @param string|null $access
     * @return bool
     * @throws Exception
     */
    public static function isAdmin(string $access = null): bool
    {
        /** @var IUser $user */
        $user = Core::getUser();
        if ($user !== null) {
            return $user::this()->userAccessCheck($access ?? 'Admin');
        } else {
            return false;
        }
    }

    /**
     * @return string|null
     */
    protected static function getTrigger(): ?string
    {
        self::$trigger = Input::getPost('admControl') ?? '';
        return self::$trigger;
    }

    /**
     * @return bool
     */
    public static function isNew(): bool
    {
        return self::$new;
    }

    /**
     * @return bool
     */
    public static function isEdit(): bool
    {
        return self::$edit;
    }

    /**
     * @param array $userAccess
     * @return string
     * @throws Exception
     */
    public static function getAccessList(array $userAccess): array
    {
        if (self::isAdmin()) {
            $list = SiteConfig::get('accessList', 'access') ?? [];
            $l_list = array_map('strtolower', $list);
            $res_list = [];
            foreach ($l_list as $k => $access) {
                if (!in_array(strtolower($access), $userAccess, false)) {
                    $res_list[] = $list[$k];
                }
            }
            return $res_list;
        }
        return [];
    }

    public static function doSiteModuleEventInit(array $data = []): void
    {
        // nothing here
    }

    /**
     * @param IController $controller
     * @param array $params
     * @return IController
     * @throws Exception
     */
    protected static function preController(IController $controller, array $params): IController
    {
        if (!self::isAdmin()) { //не админ
            Render::setWarning('Error access!');
            return $controller->main(null, $params);  //поэтому вернём main метод модуля по-умолчанию
        }
        //ТЕКУЩАЯ КОМАНДА ТРИГГЕРА.
        self::$trigger = $params['action'] ?? Input::getPost('admControl', CAST_NULLSTR) ?? ''; //из POST данных пытаемся получить значения @admControl иди принулительно переданные параметры
        //НОВОЕ. Для примера у админ контроллера есть команда для создания чего-либо.
        self::$new = (self::$trigger == 'addNew' or ($params['action'] ?? null) == 'addNew' or Input::getPost('isNew', CAST_BOOL)); //флаг того, что идёт процесс создания
        //dump(self::$trigger, $params, self::$new);
        //РЕДАКТИРОВАНИЕ. Так же просто маркер того, что контроллер в режиме редактирования.
        self::$edit = ((self::$trigger == 'edit') or self::$new);
        //специфический параметр. В основном необходим для всяких профилей, где есть списки.
        self::$target = Input::getPost('target', CAST_NULLSTR) ?? $params['target'] ?? null;

        return $controller;
    }
}
