<?php

namespace Engine\Core\Session;

use Engine\Core\Objects\CoreElement;

class Session extends CoreElement
{
    private static $session = null;

    public static function add(string $key, $data)
    {
        return self::getSession()->add($key, $data);
    }

    public static function getSession(): SessionWork
    {
        if (self::$session == null){
            self::initialize();
        }
        return self::$session;
    }

    public static function has($key): bool
    {
        return self::getSession()->has($key);
    }

    public static function delete($values)
    {
        return self::getSession()->del($values);
    }

    public static function initialize()
    {
        self::$session = new SessionWork();
    }

    public static function get(string $key)
    {
        return self::getSession()->get($key);
    }

    public static function put(string $key, $data)
    {
        return self::getSession()->put($key, $data);
    }

    public static function reset()
    {
        self::getSession()->flush();
    }

    public static function restore()
    {
        return self::getSession()->restore();
    }
}