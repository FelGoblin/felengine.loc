<?php

namespace Engine\Core\Session;


use Exception;
use Engine\Helper\Cookie;

class SessionWork
{
    protected $keep = [];
    private $key = 'felSite';

    public function __construct()
    {
        try {
            if (!headers_sent()) {
                /*
                if (defined('ENV') AND ENV == 'CRON') { //TODO: Привести к модулю.
                    session_id('cron-tasks');
                    session_start('cron-tasks');
                } else {
                    if (Cookie::get('PHPSESSID'))
                        session_start(Cookie::get('PHPSESSID'));
                    else
                        session_start();
                }
                */
                session_start();
            }
            // Initialize main session array if it hasn't been set.
            if (!isset($_SESSION[$this->key])) {
                $_SESSION[$this->key] = [];
            }

            // Do the session with the flash data key.
            if (!isset($_SESSION['flash'])) {
                $_SESSION['flash'] = [];
            }
            // Successfully initialzed.
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Destroys the session.
     *
     * @return void
     */
    public function __destruct()
    {
        static::finalize();
    }

    public function finalize()
    {
        // Remove flash data that is not being kept.
        foreach (array_keys($this->kept()) as $name) {
            if (!in_array($name, $this->keep, true)) {
                unset($_SESSION['flash'][$name]);
            }
        }

        // Successfully finalized.
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function kept()
    {
        return $this->keep;
    }

    public function add(string $name, $data)
    {
        $_SESSION[$this->key][$name][] = $data;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function put(string $name, $data)
    {
        // Insert the session data.
        $_SESSION[$this->key][$name] = $data;

        // Return class instance.
        return $this;
    }

    public function get(string $name)
    {
        return $_SESSION[$this->key][$name] ?? null;
    }

    /**
     * @param $values string/Array - списол занченгий для удаления из ссессии.
     * @return $this
     */
    public function del($values)
    {
        if (is_array($values)) {
            foreach ($values as $value) {
                $this->del($value);
            }
        } elseif (is_string($values)) {
            if ($this->has($values)) {
                unset($_SESSION[$this->key][$values]);
            }
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $name): bool
    {
        return isset($_SESSION[$this->key][$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function restore()
    {
        $id = Cookie::get('userID');
        $hash = Cookie::get('userHASH');
        $customTmp = Cookie::get('customTemplate');

        if (!empty($customTmp)) {
            self::put('customTemplate', $customTmp);
            Cookie::delete('customTemplate');
        }

        if (!empty($hash) AND !empty($id)) {
            self::put('userID', $id);
            self::put('userHASH', $hash);
        }
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        foreach ($_SESSION[$this->key] as &$item) {
            unset($item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function all(bool $onlyKey = true): array
    {
        return $onlyKey ? $_SESSION[$this->key] : $_SESSION;
    }
}