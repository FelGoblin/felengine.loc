<?php

namespace Engine\Core\EventsHandler;


class Event
{
    /**
     * Получаем текущего пользователя (User::this())
     */
    public const onGetThisUser = 'ON_GET_THIS_USER';
    /**
     * получить неймспейс для класса юзер
     */
    public const onGetUser = 'ON_GET_USER';
    /**
     * Установить текущего пользователя для все ситемы (по сути это или для виджетов или для апи)
     */
    public const onSetUser = 'ON_SET_USER';
    /**
     * Пользователь авторизовался.
     */
    public const onUserAuthorize = 'ON_USER_AUTHORIZE';

    public const onGetUserInfo = 'ON_GET_USER_INFO';

    const onUserInit = 10008;
    /**
     * Загружается конфиг
     */
    public const onSiteLoadConfig = 20000;
    /**
     *
     */
    public const onSiteModulesInit = 'ON_SITE_MODULES_INIT';
    const onGetSiteLabel = 'ON_GET_SITE_LABEL';
    const onGetSiteCard = 'ON_GET_SITE_CARD';
    const onGetDevModule = 'ON_GET_DEV_MODULE';

    const onSiteModuleInit = 'ON_SITE_MODULE_INIT';
    const onSiteModulePostInit = 'ON_SITE_MODULE_POST_INIT';
    const onSiteModuleDevelopmentInit = 20003;
    const onCompilationBegine = 'ON_COMPILATION_BEGINE';
    const onAPImodDoTrigger = 'ON_API_MOD_DO_TRIGGER';
}