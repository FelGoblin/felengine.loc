<?php

namespace Engine\Core\EventsHandler;

use Closure;
use Engine\Core\Development\DebugInfo;
use Exception;

/**
 * @method on(string $action, Closure $function)
 * @method invoke(string $action, array|null $arg)
 */
class Events
{
    private static ?Events $instance = null;
    /**
     * Список событий
     * @var array
     */
    private array $callbacks = [];
    /**
     * @var string
     */
    private string $text = '';

    /**
     * Magic method
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array([get_called_class(), 'static' . ucfirst($method)], $args);
    }

    /**
     * Events::on(...)
     * @param $action
     * @param Closure $function
     * @throws Exception
     */
    private static function staticOn($action, Closure $function)
    {
        $d = debug_backtrace()[0];
        DebugInfo::debugAdd(sprintf(lang('@events_register_with_count_count'), count(static::getInstance()->callbacks), $action, $d['function'] ?? '_unknown_function_', $d['file'] ?? '_unknown_file', $d['line'] ?? '_unknown_line'));
        static::getInstance()->callbacks[$action][] = $function;
    }

    /**
     * @return Events синглтон
     */
    private static function getInstance(): Events
    {
        if (static::$instance == null) {
            static::$instance = new Events();
        }
        return static::$instance;
    }

    /**
     * @param $name
     * @param array|null $arg
     * @throws Exception
     */
    private static function staticInvoke($name, array $arg = null)
    {
        static::doInvoke(static::getInstance()->callbacks, $name, $arg);
    }

    /**
     * @param array $callbacks
     * @param string $name
     * @param array|null $arg
     */
    private static function doInvoke(array $callbacks, string $name, array $arg = null)
    {
        $d = debug_backtrace()[0];
        if (empty($callbacks[$name])) {
            //dump_trace($name);
            DebugInfo::debugAdd(sprintf(lang('@event_has_no_implementation'), $name));
            DebugInfo::debugAddF(lang('@line_file_info'), $d['function'] ?? '_unknown_function_', $d['file'] ?? '_unknown_file', $d['line'] ?? '_unknown_line');
            return;
        }
        //В цикле вызов переданных функций обратного вызова
        foreach ($callbacks[$name] ?? [] as $callback) {
            if (is_callable($callback)) {
                DebugInfo::debugAddF(lang('@event_line_file_info'), $name, $d['function'] ?? '_unknown_function_', $d['file'] ?? '_unknown_file', $d['line'] ?? '_unknown_line');
                call_user_func($callback, $arg);
            }
        }
    }

    /**
     * Magic method
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([get_called_class(), 'obj' . ucfirst($method)], $args);
    }

    /**
     * $events->on(...)
     * @param $action
     * @param Closure $function
     */
    private function objOn($action, Closure $function)
    {
        $d = debug_backtrace()[0];
        DebugInfo::debugAdd(sprintf(lang('@events_register_with_count_count'), count(static::getInstance()->callbacks), $action, $d['function'] ?? '_unknown_function_', $d['file'] ?? '_unknown_file', $d['line'] ?? '_unknown_line'));
        $this->callbacks[$action][] = $function;
    }

    /**
     * @param $name
     * @param array|null $arg
     */
    private function objInvoke($name, array $arg = null)
    {
        static::doInvoke($this->callbacks, $name, $arg);
    }
}
