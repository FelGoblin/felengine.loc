<?php

namespace Engine\Core\DataBase;

use Engine\Core\Config\SiteConfig;
use Engine\Core\DataBase\SQL\SqlQuery;
use Engine\Core\Objects\CoreElement;
use Exception;
use PDO;
use PDOException;

final class DataBase extends CoreElement
{
    /**
     * @var ?PDO The database connection.
     */
    protected static ?PDO $connection;

    /**
     * Initializes the database connection.
     *
     * @return void
     * @throws Exception
     */
    public static function initialize()
    {
        // Connect to the database.
        self::$connection = self::connect();
        //var_dump( self::$connection);
    }

    /**
     * Connect to the database.
     * @return null|PDO
     * @throws Exception
     */
    private static function connect(): ?PDO
    {
        //var_dump(SiteConfig::getConfigDataStored());
        if (empty(SiteConfig::group('database'))) {
            //dump('config not found');
            return null;
        }
        // Setup connection settings.
        $driver = SiteConfig::get('driver', 'database');
        $host = SiteConfig::get('host', 'database');
        $username = SiteConfig::get('username', 'database');
        $password = SiteConfig::get('password', 'database');
        $name = SiteConfig::get('db_name', 'database');
        $dsn = sprintf('%s:host=%s;dbname=%s', $driver, $host, $name);
        $options = [
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        // Don't attempt a connection if we have no database username, name
        // or driver.
        if ($driver === '' || $username === '' || $name === '') {
            return null;
        }
        //printf("\n\n%s,%s,%s;\n",$dsn, $username, $password);
        //dump($dsn, $username, $password, $options);
        // Attempt to connect to the database.
        try {
            $connection = new PDO($dsn, $username, $password, $options);
        } catch (PDOException $error) {
            throw new Exception('MYSQL error: ' . $error->getMessage());
        }
        // Return connection if successful.
        return $connection ?? null;
    }

    /**
     * Finalize the database connection.
     *
     * @return void
     */
    public static function finalize()
    {
        // Close connection.
        static::$connection = null;
    }

    /**
     * Gets the last inserted record ID.
     *
     * @param string|null $name
     * @return int
     */
    public static function insertId(string $name = NULL): int
    {
        return (int)self::connection()->lastInsertId($name);
    }

    /**
     * Get the current connection.
     *
     * @return PDO
     */
    public static function connection(): ?PDO
    {
        return self::$connection;
    }

    /**
     * @param bool|null $exception
     * @return SqlQuery|null
     */
    public static function query(bool $exception = null): ?SqlQuery
    {
        if ($connection = self::connection()) {
            return new SqlQuery($connection, $exception);
        }
        return null;
    }
}
