<?php

namespace Engine\Core\DataBase\SQL;


class SqlBuilder extends SqlBinder
{
    public static $isWhere = null;

    /**
     * @param string $table
     * @param array $update
     * @return string
     */
    public static function insert(string $table, array $update): string
    {
        $table = self::shield($table);
        //dump(['style'=>'red'],$result);
        return "INSERT INTO $table SET " . self::_buildFromArray($update, self::AS_ENUM);
    }

    /**
     * @param string $table
     * @param array $update
     * @return string
     */
    public static function update(string $table, array $update): string
    {
        //dump($update);
        $table = self::shield($table);
        //dump($result);
        return "UPDATE $table SET " . self::_buildFromArray($update, self::AS_ENUM);
    }

    /**
     * @param string $table
     * @param $fields
     * @return string
     */
    public static function select(string $table, &$fields): string
    {
        $table = self::shield($table);

        if (empty($fields) or $fields == '*') {
            $result = 'SELECT * FROM ' . $table;
        } else {
            $result = 'SELECT ' . self::_buildFromArray($fields, self::AS_ENUM_NO_KEYS) . ' FROM ' . $table;
        }
        return $result;
    }

    /**
     * @param string|null $side
     * @param string $table
     * @param array $onConditions
     * @return string
     */
    public static function join(?string $side, string $table, array $onConditions): string
    {
        $side = $side ?? '';
        self::$logicFirst = false;
        $result = '';
        $table = self::shield($table);
        foreach ($onConditions as $key => $onCondition) {
            $result .= self::_buildLogicADD($onConditions, $onCondition, $key);
        }
        return ' ' . strtoupper($side) . ' JOIN ' . $table . ' ON ' . $result;
    }

    /**
     * @param array $wheres
     * @return string
     */
    public static function where(array &$wheres = []): string
    {
        self::$logicFirst = false;
        $result = '';

        foreach ($wheres as $key => $where) {
            if (is_object($where)) {
                $result .= self::_buildLogicADD($wheres, $where, $key);
                self::$isWhere = true;
            } elseif (is_string($where) and strtolower($where) == 'where') {
                $result = '';
                self::$isWhere = false;
            }
        }
        return sprintf(' WHERE %s', $result);
    }

    /**
     * @param array $ins
     * @param bool $setWhere
     * @return string
     */
    public static function in(array $ins, bool $setWhere = false): string
    {
        if ($setWhere) {
            self::$isWhere = null;
        }
        $result = '';
        foreach ($ins as $in) {
            $result .= self::_in($in[0], $in[1], $in[2]);
        }
        return $result;
    }

    /**
     * @param string $column
     * @param array $keys
     * @param bool|null $or
     * @return string
     */
    private static function _in(string $column, array $keys, bool $or = null): string
    {
        self::$countsPlaceHolders += count($keys);
        self::$placeholders = array_merge(self::$placeholders, $keys);

        if (is_null(self::$isWhere) || self::$isWhere === false) {
            self::$isWhere = true;
            $or = '';
        } elseif (self::$isWhere === true and $or == null) {
            $or = ' AND ';
        } else {
            $or = $or ? ' OR ' : ' AND ';
        }


        if (!self::isShieldingVar($column))
            $column = self::shield($column);
        //$clause = $or . $column . ' IN (';
        $in = str_repeat('?,', count($keys) - 1) . '?';

        // dump(sprintf(' %s %s IN (%s)', $or, $column, $in));

        return sprintf(' %s %s IN (%s)', $or, $column, $in);
        //return $clause . $in . ')';
    }

    /**
     * @param string $table
     * @return string
     */
    public static function delete(string $table): string
    {
        return ' DELETE FROM ' . self::shield($table);
    }

    /**
     * @param string $table
     * @return string
     */
    public static function describe(string $table): string
    {
        return 'DESCRIBE ' . self::shield($table);
    }

    /**
     * @param array $orderList
     * @return string
     */
    public static function order(array $orderList = [])
    {
        return ' ORDER BY ' . self::_buildFromArray($orderList, self::AS_ENUM_NO_KEYS);
    }

    /**
     * @param array $orderList
     * @return string
     */
    public static function group(array $orderList = [])
    {
        return ' GROUP BY ' . self::_buildFromArray($orderList, self::AS_ENUM_NO_KEYS);
    }

    /**
     * Вставить в запрос LIMIT и OFFSET
     *
     * @param int $limit
     * @param int $offset
     * @return string
     */
    public static function limit(int $limit, int $offset = 0)
    {
        $clause = " LIMIT $limit ";
        if ($offset > 0) {
            $clause .= " OFFSET $offset ";
        }
        return $clause;
    }
}
