<?php

namespace Engine\Core\DataBase\SQL;


use PDO;
use PDOStatement;

class SqlBinder
{
    /**
     * Перечисляемый тип, который будет обработан через запятую
     */
    public const AS_ENUM = 0;
    public const AS_ENUM_NO_KEYS = 1;
    /**
     * Логический тип, будет обработан через  OR
     */
    public const AS_LOGIC_OR = 2;
    public const AS_LOGIC_OR_NO_KEYS = 2;
    /**
     * Логический тип, будет обработан через  AND
     */
    public const AS_LOGIC_AND = 4;
    public const AS_LOGIC_AND_NO_KEYS = 4;
    public static int $countsPlaceHolders = 0;
    public static array $placeholders = [];
    protected static ?bool $logicFirst = false;

    /**
     * @param PDOStatement $stmnt
     * @return void
     */
    public static function bind(PDOStatement $stmnt): void
    {
        $i = 0;
        //dump(self::$placeholders);
        foreach (self::$placeholders as $placeholder) {
            $i++;
            //$placeholder = SqlCompareBlock::getSelfInstance($placeholder);
            //
            if ($placeholder instanceof SqlCompareBlock) {
                self::stmtBind($stmnt, $i, $placeholder->value, $placeholder->type);
            } else {
                self::stmtBind($stmnt, $i, $placeholder);
            }
        }
        //dump(count(self::$placeholders), $i);
    }

    /**
     * @param PDOStatement $stmnt
     * @param $parameter
     * @param $value
     * @param $type
     * @return void
     */
    protected static function stmtBind(PDOStatement $stmnt, $parameter, $value, $type = null)
    {
        if (is_null($type)) {
            $type = strtolower(gettype($value));
        }

        switch ($type) {
            case 'integer':
            case 'int':
            case 'numeric':
            case 'num':
                $type = PDO::PARAM_INT;
                break;
            case 'boolean':
            case 'bool':
                $type = PDO::PARAM_BOOL;
                break;
            case 'null':
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;

        }
        //dump(['style'=>'dark'], gettype($value), $type, $parameter, $value, $type);
        $stmnt->bindValue($parameter, $value, $type);
    }

    /**
     * @param string $sql
     * @param callable $cb
     */
    public static function doDebug(string $sql, callable $cb): void
    {
        $_sql = $sql;
        $count = 0;
        $i = 0;
        //dump(self::$placeholders);
        do {
            $placeholder = '_undefined_';
            if ($i < count(self::$placeholders)) {
                $placeholder = self::$placeholders[$i];
                $i++;
            }

            if ($placeholder instanceof SqlCompareBlock) {
                $placeholder = $placeholder->value;
            }
            $_sql = preg_replace('/\?/m', " '$placeholder'", $_sql, 1, $count);
            //dump($_sql, $count);
        } while ($count);
        $cb($_sql);
    }

    /**
     * @param array $array
     * @param int $type
     * @return string
     */
    public static function _buildFromArray(array &$array, int $type = self::AS_ENUM): string
    {
        self::$logicFirst = null;
        $enum = [];
        $result = '';
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                continue;
            }
            switch ($type) {
                case self::AS_ENUM:
                    $enum = self::_buildEnumADD($enum, $array, new SqlCompareBlock(true, $key, $value));
                    break;
                case self::AS_ENUM_NO_KEYS:
                    $enum = self::_buildEnumADD($enum, $array, new SqlCompareBlock(true, $value, null));
                    break;
                case self::AS_LOGIC_OR:
                    self::$logicFirst = false;
                    $result .= self::_buildLogicADD($array, new SqlCompareBlock(true, $key, $value));
                    break;
                case self::AS_LOGIC_OR_NO_KEYS:
                    self::$logicFirst = false;
                    $result .= self::_buildLogicADD($array, new SqlCompareBlock(true, $value, null));
                    break;
                case self::AS_LOGIC_AND:
                    self::$logicFirst = false;
                    $result .= self::_buildLogicADD($array, new SqlCompareBlock(false, $key, $value));
                    break;
                case self::AS_LOGIC_AND_NO_KEYS:
                    self::$logicFirst = false;
                    $result .= self::_buildLogicADD($array, new SqlCompareBlock(false, $value, null));
                    break;
            }
        }
        if ($type == self::AS_ENUM or $type == self::AS_ENUM_NO_KEYS) {
            return self::_buildEnum($enum);
        }
        return $result;
    }

    /**
     * @param array $enum
     * @param array $main
     * @param SqlCompareBlock $block
     * @return array
     */
    protected static function _buildEnumADD(array $enum, array &$main, SqlCompareBlock $block): array
    {
//        dump($block);
        if (!is_null($block->value)) {
            if (self::isShieldingVar($block->value)) {
                $enum[] = "`{$block->key}`{$block->operator}{$block->value}";
                unset($main[$block->key]);
            } else {
                $enum[] = "`{$block->key}`{$block->operator}?";
                self::$countsPlaceHolders++;
                self::$placeholders[] = $block;
            }
        } else {
            if (self::isShieldingVar($block->key)) {
                unset($main[$block->key]);
                $enum[] = $block->key;
            } elseif (substr($block->key, 0, 1) == '(') {
                $enum[] = $block->key;
            } elseif (preg_match('/count\((\w?.+)\)/', $block->key, $matches)) {
                $enum[] = $block->key;
            } else
                $enum[] = self::shield($block->key);
        }
        return $enum;
    }

    /**
     * Строки биндятся или остаются как есть
     *
     * @param $var
     * @return bool
     */
    protected static function isShieldingVar($var): bool
    {
        //dump_trace($var);
        /*
         * Нам нужна только первая часть, напрример ORDER t1.field asc
         */
        if (substr_count($var, ' ') > 0) {
            $var = explode(' ', $var)[0];
        }
        return (
            //!is_string($var) OR //не строка или это
            self::safe($var) or //не оборачиваемое значение
            (preg_match('/^t\d+\.(\w+|\*)$/', $var) === 1) or //таблицы t1. t21
            (preg_match('/^(\(?)\w+\(/', $var) === 1) //выражения у которых есть скобка
            //(substr_count($var,'.') == 1) AND strpos($var, '.') //параметры как alias.field
        );
    }

    /**
     * Значения, который не биндятся и не обрабатываются
     *
     * @param $var
     * @return bool
     */
    protected static function safe($var)
    {
        return in_array($var, [
            '*',
            'count(*)',
            'NOW()',
            'true',
            'CURRENT_DATE',
            'TRUE',
            'false',
            'FALSE',
            'NULL',
            'null'
        ], true);
    }

    /**
     * Оборачиваем имя табьлицы
     *
     * @param $var
     * @return string
     */
    protected static function shield($var)
    {
        $expl = explode(' ', trim($var));
        //обрабатываем такие запросы типа table as t1
        if (count($expl) >= 1) {
            $expl[0] = '`' . $expl[0] . '`';
            return implode(' ', $expl);
        }
        return '`' . $var . '`'; //оборачиваем
    }

    /**
     * @param array $main
     * @param SqlCompareBlock $block
     * @param null $deleteKey
     * @return string
     */
    protected static function _buildLogicADD(array &$main, SqlCompareBlock $block, $deleteKey = null): string
    {
        $logic = self::$logicFirst ? ($block->logic ? ' OR ' : ' AND ') : '';
        self::$logicFirst = true;
        $key = !self::isShieldingVar($block->key) ? '`' . $block->key . '`' : $block->key;
        //dump($key);
        if (self::isShieldingVar($block->value)) {
            if ($deleteKey == null)
                unset($main[$block->key]);
            else
                unset($main[$deleteKey]);
            return "$logic $key {$block->operator} {$block->value}";
        }
        //dump($block->value);
        self::$countsPlaceHolders++;
        self::$placeholders[] = $block;

        return "$logic $key {$block->operator}?";
    }

    /**
     * @param array $enum
     * @return string
     */
    protected static function _buildEnum(array $enum): string
    {
        return implode(',', $enum);
    }

    protected static function extractPlaceHolder(array $array): array
    {
        return array_values($array);
    }
}
