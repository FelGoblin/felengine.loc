<?php

namespace Engine\Core\DataBase\SQL;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Database\DataBase;
use Engine\Core\Development\DebugInfo;
use Exception;
use PDO;
use PDOException;
use PDOStatement;

class SqlQuery
{
    /**
     * @var string Текущий SQL запрос
     */
    public static string $sql = '';
    /**
     * @var array
     */
    public static array $placeholders = [];
    /**
     * @var mixed Результат запроса
     */
    public static $rowCount;
    /**
     * Счётчик обращений
     * @var int
     */
    private static int $counter = 0;
    /**
     * История запросов
     * @var array
     */
    private static array $queryHistory = [];
    /**
     * @var string Текущая таблица, к котрой совершается запрос.
     */
    private string $_table = '';
    /**
     * @var PDO|null Текущее соединение к БД
     */
    private ?PDO $db;
    /**
     * @var PDOStatement
     */
    private PDOStatement $stmt;
    /**
     * @var bool Запрос был собран
     */
    private bool $build = false;
    /**
     * @var bool
     */
    private bool $_or = false;
    /**
     * @var array
     */
    private array $_insert = [];
    /**
     * @var array
     */
    private array $_update = [];
    /**
     * @var array
     */
    private array $_select = [];
    /**
     * @var bool
     */
    private bool $_delete = false;
    /**
     * @var bool
     */
    private bool $_describe = false;
    /**
     * @var string
     */
    private string $_jointable = '';
    /**
     * @var array
     */
    private array $_join = [];
    /**
     * @var array
     */
    private array $_leftjoin = [];
    /**
     * @var array
     */
    private array $_rightjoin = [];
    /**
     * @var array
     */
    private array $_where = [];
    /**
     * @var array
     */
    private array $_in = [];
    /**
     * @var array
     */
    private array $_limit = [];
    /**
     * @var array
     */
    private array $_order = [];
    /**
     * @var array
     */
    private array $_group = [];
    /**
     * При ошибке выкидывать исключение.
     * @var bool
     */
    private bool $exception;
    /**
     * Дебаг-тег
     * @var string
     */
    private string $debugTag = 'sql';

    /**
     * Query constructor.
     * @param PDO|null $db
     * @param bool|null $exception
     */
    public function __construct(PDO $db = null, bool $exception = null)
    {
        if ($db == null) {
            $this->db = DataBase::connection();
        } else {
            $this->db = $db;
        }

        $this->exception = $exception ?? SiteConfig::getBool('exception', 'database');
    }

    /**
     * Счётчик количества запросов к БД
     *
     * @return int
     */
    public static function getCounter(): int
    {
        return self::$counter;
    }

    /**
     * @return array
     */
    public static function getQueryHistory(): array
    {
        return self::$queryHistory;
    }

    /**
     * @param string $sql
     * @return mixed
     */
    public function exec(string $sql)
    {
        self::$sql = $sql;
        self::$counter++;
        return $this->getDb()->exec($sql);
    }

    /**
     * @return PDO
     */
    private function getDb(): PDO
    {
        return $this->db;
    }

    /**
     * @param string $sql
     * @param string $return
     * @return mixed
     */
    public function query(string $sql, string $return = 'all')
    {
        self::$sql = $sql;
        $r = $this->getDb()->query($sql);
        self::$counter++;

        switch ($return) {
            case 'all':
                return $r->fetchAll(PDO::FETCH_ASSOC);
                break;
            case 'one':
                return $r->fetch(PDO::FETCH_ASSOC);
                break;
            default:
                return $r;
        }
    }

    /**
     * @return SqlQuery
     */
    public function and_(): SqlQuery
    {
        $this->_or = false;
        return $this;
    }

    /**
     * @return SqlQuery
     */
    public function or_(): SqlQuery
    {
        $this->_or = true;
        return $this;
    }

    /**
     * Alias table()
     *
     * @param string $table
     * @return SqlQuery
     */
    public function into(string $table)
    {
        return $this->table($table);
    }

    /**
     * Установить имя таблицы
     *
     * @param string $table
     * @return SqlQuery
     */
    public function table(string $table): SqlQuery
    {
        $this->_table = $table;

        return $this;
    }

    public function delete(string $table = null): SqlQuery
    {
        if ($table)
            $this->_table = $table;

        $this->_delete = true;

        return $this;
    }

    /**
     * Alias table()
     *
     * @param string $table
     * @return SqlQuery
     */
    public function from(string $table)
    {
        return $this->table($table);
    }

    /**
     * @param string $table
     * @return SqlQuery
     */
    public function joinTable(string $table): SqlQuery
    {
        $this->_jointable = $table;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @param string $operator
     * @param string $type
     * @return SqlQuery
     */
    public function join($key, $value, string $operator = '=', string $type = 'str'): SqlQuery
    {
        $this->_join[] = $this->_doJoin($this->_or, $key, $value, $operator, $type);
        return $this;
    }

    /**
     * @param bool $logic
     * @param $key
     * @param $value
     * @param string $operator
     * @param string $type
     * @return SqlCompareBlock
     */
    private function _doJoin(bool $logic = false, $key, $value, string $operator = '=', string $type = 'str'): SqlCompareBlock
    {
        return new SqlCompareBlock($logic, $key, $value, $operator, $type);
    }

    /**
     * @param $key
     * @param $value
     * @param string $operator
     * @param string $type
     * @return SqlQuery
     */
    public function leftJoin($key, $value, string $operator = '=', string $type = 'str'): SqlQuery
    {
        $this->_leftjoin[] = $this->_doJoin($this->_or, $key, $value, $operator, $type);
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @param string $operator
     * @param string $type
     * @return SqlQuery
     */
    public function rightJoin($key, $value, string $operator = '=', string $type = 'str'): SqlQuery
    {
        $this->_rightjoin[] = $this->_doJoin($this->_or, $key, $value, $operator, $type);
        return $this;
    }

    /**
     * Получить ид последнего запроса вставки INSERT
     *
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->getDb()->lastInsertId();
    }

    /**
     * Fetches the number of results.
     *
     * @return int
     */
    public function rowCount(): int
    {
        return $this->getStmt()->rowCount();
    }

    /**
     * @return PDOStatement
     */
    public function getStmt(): PDOStatement
    {
        return $this->stmt;
    }

    /**
     * Returns error information associated with the last query.
     *
     * @return array
     */
    public function errors(): array
    {
        return $this->getStmt()->errorInfo();
    }

    /**
     * @param string|null $column
     * @return array
     * @throws Exception
     */
    public function all(string $column = null): array
    {
        $this->run();

        $result = $this->getStmt()->fetchAll(PDO::FETCH_ASSOC);

        $this->flush();

        if ($column != null) {
            return array_column($result, $column);
        }

        return $result;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        try {
            //выстраиваем запрос
            $this->build();

            //dump(self::$sql);

            //подготавливаем.
            $this->build = $this->prepare(self::$sql);

            //dump(self::$sql);
            //биндим
            SqlBuilder::bind($this->getStmt());

            //dump($this->getStmt());
            //выполняем
            $return = $this->getStmt()->execute();
            //
            //dump(self::$sql);
            self::$queryHistory[] = sprintf('[%s]: %s', $this->debugTag, self::$sql);
            SqlBuilder::doDebug(self::$sql, function ($sql) {
                DebugInfo::debugAddF(sprintf('<sql>[%s]: %s</sql>', $this->debugTag, $sql));
            });

            self::$rowCount = $this->getStmt()->rowCount();

            //dump($this->result, $return, $this->lastInsertId(),self::$sql);

            $this->flush();

            self::$counter++;

            return $return;

        } catch (Exception $e) {
            //dump(self::$sql);
            if ($this->exception) {
                throw $e; //пербросим

            } else { //выведем сообщение
                echo sprintf("Ошибка MySQL запроса :%s<br/>SQL query:<b>%s</b>", $e->getMessage(), self::$sql);
                return false;
            }
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function build(): string
    {
        if ($this->_table == '') {
            throw new PDOException('Empty table');
        }
        //reset
        self::$sql = '';
        SqlBuilder::$countsPlaceHolders = 0;
        SqlBuilder::$placeholders = [];

        if (!empty($this->_insert)) {
            self::$sql .= SqlBuilder::insert($this->_table, $this->_insert);
        } elseif (!empty($this->_update)) {
            self::$sql .= SqlBuilder::update($this->_table, $this->_update);
        } elseif (!empty($this->_select)) {
            self::$sql .= SqlBuilder::select($this->_table, $this->_select);
        } elseif ($this->_delete) {
            self::$sql .= SqlBuilder::delete($this->_table);
        } elseif ($this->_describe) {
            self::$sql .= SqlBuilder::describe($this->_table);
        }

        try {
            //проверяем наличяие JOIN таблицы
            if ((!empty($this->_join) or !empty($this->_leftjoin) or !empty($this->_rightjoin)) and empty($this->_jointable)) {
                throw new Exception('empty <b>JOIN</b> table');
            }
            //Строим запрос
            if (!empty($this->_join)) {
                self::$sql .= SqlBuilder::join('', $this->_jointable, $this->_join);
            } elseif (!empty($this->_leftjoin)) {
                self::$sql .= SqlBuilder::join('left', $this->_jointable, $this->_leftjoin);
            } elseif (!empty($this->_rightjoin)) {
                self::$sql .= SqlBuilder::join('right', $this->_jointable, $this->_rightjoin);
            }

            //dump(self::$sql);

        } catch (Exception $ex) {
            throw $ex;
        }

        if (!empty($this->_where)) {
            self::$sql .= SqlBuilder::where($this->_where);
        }
        if (!empty($this->_in)) {

            $setWhere = false;
            if (empty($this->_where)) {
                self::$sql .= ' WHERE ';
                $setWhere = true;
            }
            self::$sql .= SqlBuilder::in($this->_in, $setWhere);
        }
        // GROUP
        if (!empty($this->_group)) {
            self::$sql .= SqlBuilder::group($this->_group);
        }
        //5 ORDER
        if (!empty($this->_order)) {
            self::$sql .= SqlBuilder::order($this->_order);
        }
        //6 LIMIT
        if (!empty($this->_limit)) {
            self::$sql .= SqlBuilder::limit($this->_limit['limit'], $this->_limit['offset']);
        }

        //dump(self::$sql);

        return self::$sql;
    }

    /**
     * @param string $sql
     * @return bool
     */
    private function prepare(string $sql): bool
    {
        $this->stmt = $this->getDb()->prepare($sql);

        return $this->stmt != null;
    }

    /**
     * Очищаем данные о запросе
     *
     * @return $this
     */
    public function flush(): SqlQuery
    {
        //self::$sql =
        $this->_table =
        $this->_jointable = '';

        $this->_insert =
        $this->_update =
        $this->_select =
        $this->_join =
        $this->_leftjoin =
        $this->_rightjoin =
        $this->_where =
        $this->_in =
        $this->_order =
        $this->_group =
        $this->_limit = [];


        $this->build =
        $this->_delete =
        $this->_describe = false;

        SqlBuilder::$isWhere = null;

        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function count(): int
    {
        $this->select('count(*) as count');
        return $this->one()['count'] ?? 0;
    }

    /**
     * @param null $fields
     * @return SqlQuery
     */
    public function select($fields = null): SqlQuery
    {
        if ($fields == null) {
            $fields = '*';
        }
        if (is_array($fields)) {
            $this->_select = array_merge($this->_select, $fields);
        } elseif (is_string($fields)) {
            $this->_select[] = $fields;
        }
        return $this;
    }

    /**
     * Один объект или одно плое
     * @param null $field
     * @return array|mixed|string
     * @throws Exception
     */
    public function one($field = null)
    {
        if (empty($this->_limit)) {
            $this->limit(1, 0);
        }

        $this->run();

        $result = $this->getStmt()->fetch(PDO::FETCH_ASSOC);

        if ($field != null) {
            $result = $result[$field] ?? null;
        }

        $this->flush();

        return $result;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return SqlQuery
     */
    public function limit(int $limit = 1, int $offset = 0): SqlQuery
    {
        $this->_limit = array('limit' => $limit, 'offset' => $offset);

        return $this;
    }

    /**
     * BIT тип 0/1 выбрать как true/false (строка)
     * @param string $field
     * @return SqlQuery
     */
    public function selectAsBool(string $field): SqlQuery
    {
        $this->_select[] = sprintf('(CASE WHEN `%s`=1 THEN \'true\' ELSE \'false\' END) as `%s`', $field, $field);
        return $this;
    }

    /**
     * @param array $insert
     * @return SqlQuery
     */
    public function insert(array $insert = []): SqlQuery
    {
        $this->_insert = array_merge($this->_insert, $insert);
        return $this;
    }

    /**
     * @param array $fields
     * @return SqlQuery
     */
    public function update(array $fields = []): SqlQuery
    {
        $this->_update = array_merge($this->_update, $fields);
        return $this;
    }

    /**
     * @return SqlQuery
     */
    public function where(/*$key, $value, string $operator = '=', string $type = 'str'*/): SqlQuery
    {
        $args = func_get_args();
        if (count($args) == 0) {
            $this->_where[] = 'WHERE';
        } elseif (is_array($args[0])) {
            foreach ($args[0] as $whereArg) {
                $this->where($whereArg[0], $whereArg[1], ($whereArg[2] ?? '='), ($whereArg[3] ?? 'str'));
            }
        } else {
            $key = $args[0]; //key
            $value = $args[1]; //value
            $operator = $args[2] ?? '='; //operator
            $type = $args[3] ?? 'str'; //value type

            if (empty($this->_where)) //первое where устанавливает AND
                $this->_or = false;

            $this->_where[] = new SqlCompareBlock($this->_or, $key, $value, $operator, $type);
        }
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @param string $operator
     * @param string $type
     * @return SqlQuery
     */
    public function orWhere($key, $value, string $operator = '=', string $type = 'str'): SqlQuery
    {
        $this->_where[] = new SqlCompareBlock(true, $key, $value, $operator, $type);
        return $this;
    }

    public function andWhere($key, $value, string $operator = '=', string $type = 'str'): SqlQuery
    {
        $this->_where[] = new SqlCompareBlock(false, $key, $value, $operator, $type);
        //dump($this->_where);
        return $this;
    }

    /**
     * @param $key
     * @param array $values
     * @return SqlQuery
     */
    public function andIn($key, array $values): SqlQuery
    {
        return $this->in($key, $values, false);
    }

    /**
     * @param $key
     * @param array $values
     * @param string|null $or
     * @return SqlQuery
     */
    public function in($key, array $values, string $or = null): SqlQuery
    {
        $this->_in[] = array($key, $values, $or);
        return $this;
    }

    /**
     * @param $key
     * @param array $values
     * @return SqlQuery
     */
    public function orIn($key, array $values): SqlQuery
    {
        return $this->in($key, $values, true);
    }

    /**
     * @param $order
     * @return SqlQuery
     */
    public function order($order, string $dest = null): SqlQuery
    {
        if (is_array($order))
            $this->_order = array_merge($this->_order, $order);
        elseif (is_string($order))
            $this->_order[] = sprintf('%s %s', $order, $dest);
        return $this;
    }

    /**
     * GROUP BY
     *
     * @param $group
     * @return SqlQuery
     */
    public function group($group): SqlQuery
    {
        if (is_array($group))
            $this->_group = array_merge($this->_group, $group);
        elseif (is_string($group))
            $this->_group[] = $group;
        return $this;
    }

    /**
     * В случае ошибки выбросить исключение.
     * @param bool $ex
     * @return void
     */
    public function throwException(bool $ex = true)
    {
        $this->exception = $ex;
    }

    /**
     * Метка для дебага
     * @param string $tag
     * @return SqlQuery
     */
    public function addDebugTag(string $tag): SqlQuery
    {
        $this->debugTag = $tag;
        return $this;
    }
}
