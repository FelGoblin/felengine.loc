<?php

namespace Engine\Core\DataBase\SQL;


class SqlCompareBlock
{
    public string $key;
    public ?string $value;
    public string $operator;
    public ?string $type;
    public bool $logic;
    public ?string $data;

    /**
     * SqlCompareBlock constructor.
     * @param bool $logic
     * @param $key
     * @param mixed $value
     * @param string $operator
     * @param string|null $type
     * @param null $data
     */
    public function __construct(bool $logic, $key, $value, string $operator = '=', string $type = null, $data = null)
    {
        $this->logic = $logic;
        $this->key = $key;
        $this->value = $value;
        $this->operator = $operator;
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * Преобразуем $var в SqlCompareBlock
     *
     * @param $var
     * @return SqlCompareBlock
     */
    public static function getSelfInstance($var): SqlCompareBlock
    {
        return $var;
    }
}
