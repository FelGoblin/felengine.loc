<?php

namespace Engine\Core\View;

use Engine\Core\DataBase\SQL\SqlQuery;
use Engine\Core\Http\Input;
use Engine\Core\Http\Uri;
use Engine\Core\Session\Session;

final class Paginator
{
    /**
     * максимальное количество записей на сран6ицу
     */
    public const MAX_LIMIT = 50;
    /**
     * Количество ссылок для переходов по страницам.
     */
    const PAGE_LINKS_COUNT = 10;
    /**
     * Как будут выглядеть пропущенные ссылки
     */
    const SKIP_PAGES = '...';
    /**
     * Общее число записей
     *
     * @var int
     */
    public $count = 0;
    /**
     * Записей на страницу
     *
     * @var int
     */
    public $limit = 25;
    /**
     * Общее число страниц
     *
     * @var int
     */
    private $pages = 0;
    /**
     * Текущая страница
     *
     * @var int|mixed
     */
    private $page = 0;
    /**
     * Текущее количество ссылок для перехода
     *
     * @var int
     */
    private $pagelinks = 0;
    /**
     * Фильтруем запрос
     *
     * @var array
     */
    private $request = '';
    /**
     * Результат
     *
     * @var array
     */

    private $result = [];

    /**
     * Paginator constructor.
     * @param int $count
     * @param bool $clear
     */
    public function __construct(int $count, bool $clear = false)
    {
        if (!$clear)
            $limit = Session::get('pageLimit');

        if (Input::get('pagelimit')) {
            $limit = max(1, min(self::MAX_LIMIT, (int)Input::get('pagelimit')));
            Session::put('pageLimit', $limit);
        }
        //устанавливаем общее число записей.
        $this->count = $count;
        //устанавливаем лимит вывода на страницу
        $this->limit = max(1, min(self::MAX_LIMIT, $limit ?? 25));
        //получаем общее количество страниц
        $this->pages = max(1, ceil($count / $this->limit));

        if (!$clear) {
            $page = Session::get('page');
        }

        if (Input::get('page')) {
            $page = max(1, min($this->pages, (int)Input::get('page')));
            Session::put('page', $page);
        }
        //получаем значение текущей страницы
        $this->page = max(1, min($this->pages, $page ?? 1));
        //получаем количество ссылок, для перехода на страницы
        $this->pagelinks = max(1, min(self::PAGE_LINKS_COUNT, $this->pages));
        //фильтруем входящие данные
        $this->request = Input::filter('page', 'do');

        $this->result = $this->build();
    }

    /**
     * Создаём пагинатор
     *
     * @return array
     */
    public function build(): array
    {
        //1. Ссылка на предыдущую страницу PREW
        $result['prew']['title'] = '«';
        if ($this->page != 1) {
            $result['prew']['href'] = $this->buildRequest($this->page - 1);
        }

        $startPage = 1; //Начало
        //текущшая страница должна находиться в середине списка ссылок.
        if ($this->page > $this->pagelinks / 2)
            $startPage = ceil($this->page - $this->pagelinks / 2);
        //4. Текущая страница превышает диапазон количества вывода
        if ($startPage > 1 AND $this->pagelinks != $this->pages) {
            //создаём ссылку на первую страницу
            $result[1] = [
                'title' => 1,
                'href' => $this->buildRequest()
            ];
            //скрываем дапазон
            $result[2]['title'] = self::SKIP_PAGES;
        }
        //5. в случае если текущая страница
        if ($this->page + $this->pagelinks / 2 >= $this->pages)
            $startPage = $this->pages - $this->pagelinks + 1;
        //6. создаём ммылки на страницы.
        for ($i = 1, $p = $startPage; $i <= $this->pagelinks; $i++, $p++) {
            $result[$p]['title'] = $p;
            //текушая страница
            if ($this->page == $p)
                $result[$p]['active'] = true;
            $result[$p]['href'] = $this->buildRequest($p);
        }

        // END
        if ($this->page + $this->pagelinks / 2 < $this->pages AND $this->pagelinks != $this->pages) {
            $result[]['title'] = self::SKIP_PAGES;
            $result[] = [
                'title' => $this->pages,
                'href' => $this->buildRequest($this->pages)
            ];
        }
        //2. ссылка на следующую страницу NEXT
        $result['next']['title'] = '»';
        if ($this->page != $this->pages) {
            $result['next']['href'] = min($this->buildRequest($this->page + 1), $this->pages);
        }
        return $result;
    }

    /**
     * @param int $page
     * @param $request
     * @return string
     */
    private function buildRequest(int $page = 1): string
    {
        $maped = array_map(function ($v, $k) {
            if (!is_string($k))
                return null;
            return "$v=$k";
        }, array_keys($this->request), $this->request);

        $maped[] = "page=$page";
        return Uri::getFullPathUrl() . '?' . implode('&', $maped);
    }

    public function getLimit(SqlQuery $query): void
    {
        //dump('limit', $this->limit, 'offset', (($this->page * $this->limit) - $this->limit));
        $query->limit($this->limit, (($this->page * $this->limit) - $this->limit));
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }
}