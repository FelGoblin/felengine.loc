<?php

namespace Engine\Core\View;


use Engine\Core\Objects\FillClass;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Modules\BaseAbstractModule;

final class BuildComponent extends FillClass
{
    /**
     * @var SiteComponent
     */
    private $component;

    /**
     * @var BaseAbstractModule
     */
    private $module;

    /**
     * @var string
     */
    private $target = '';

    /**
     * @return SiteComponent
     */
    public function getComponent(): SiteComponent
    {
        return $this->component;
    }

    /**
     * @return BaseAbstractModule
     */
    public function getModule(): BaseAbstractModule
    {
        return $this->module;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

}