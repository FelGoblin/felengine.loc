<?php

namespace Engine\Core\View;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Objects\ICoreElement;
use Engine\Core\Session\Session;

class Template implements ICoreElement
{
    /**
     * @var string
     */
    public static $theme = 'Default';
    /**
     * @var array
     */
    private static $laoutConfig = [];

    /**
     * @var string
     */
    private static $nmspc = 'Default';

    /**
     * @param string $theme
     */
    public static function setTheme(string $theme): void
    {
        self::$theme = $theme;
    }

    /**
     * @param array $route
     */
    public static function initialize(?array $route = []): void
    {

        //dump($route);

        if (defined('API')/* || $route === null*/) //при апи запросе, эта херня нафиг не нужна
            return;

        self::$theme = SiteConfig::get('theme') ?? 'Default';

        self::$nmspc = $route['nameSpace'] ?? 'Site';

        //если есть папка с темой поимени шаблона
        //dump(self::$nmspc, is_dir(TPL_DIR . self::$nmspc));
        if (self::$nmspc != null && is_dir(TPL_DIR . self::$nmspc)) {
            self::$theme = self::$nmspc;
        }
        //dump( self::$theme ,self::$nmspc);
        //пользовательская тема
        if (class_exists('Session') AND Session::get('customTemplate')) { //TODO: переместить в UserConfig::item
            self::$theme = Session::get('customTemplate');
        }
        //Дефайним
        define('TPL', '/Templates/' . self::$theme . '/'); //и дефайн
        //dump(1, TPL);

        define('NMSPC', self::getNmspc());
    }

    /**
     * @return string
     */
    public static function getNmspc(): string
    {
        return self::$nmspc;
    }

    /**
     * @param string $path
     * @return string
     */
    public static function getTemplateComponents(string $path): string
    {
        return pathSeparator(self::get(true) . 'config/' . $path . '.components.php');
    }

    /**
     * @param bool $full
     * @param string|null $forceTemplate
     * @return string
     */
    public static function get(bool $full = false, string $forceTemplate = null): string
    {
        $theme = $forceTemplate ?? static::$theme;

        if ($full) {
            $path = pathSeparator(sprintf('%s%s/', TPL_DIR, $theme));
        } else {
            $path = sprintf('%s/%s/', 'Templates', $theme);
        }

        return $path;
    }

    public static function getPageTitle(): string
    {
        return 'Page Title';
    }

    public static function getPageFavicon(): string
    {
        return TPL . 'resources/favicon.png';
    }

    /**
     * @param string $component
     * @return void
     */
    public static function BuildLayout(string $component): void
    {
        if (isset(self::$laoutConfig[$component])) { //модуль построен
            //dump(self::$laoutConfig[$component]);
            return;
        }

        $defaultPath = pathSeparatorF('%s%s/View/layout.php', MOD_DIR, $component);
        if (file_exists($defaultPath)) {
            $defaultLayout = include $defaultPath;
            self::$laoutConfig[$component]['module'] = $defaultLayout;
        }

        $modTplPath = pathSeparatorF('%sinterface/%s/', self::get(true), strtolower($component));
        $modTplLayoutFile = sprintf('%s%s', $modTplPath, 'layout.php');
        //dump($modTplLayoutFile, file_exists($modTplLayoutFile));
        if (file_exists($modTplLayoutFile)) {
            $modTplLayout = include $modTplLayoutFile;
            self::$laoutConfig[$component]['template'] = $modTplLayout;
        }
    }

    /**
     * Полечить
     * @param string $component
     * @param string $element
     * @return array
     *      'tplType => link to tpl
     */
    public static function getLayout(string $component, string $element): array
    {
        if (!isset(self::$laoutConfig[$component])) {
            return [];
        }

        $def = self::$laoutConfig[$component]['module'][$element] ?? null;
        $tpl = self::$laoutConfig[$component]['template'][$element] ?? null;
        return $tpl != null ? ['template' => $tpl] : ['module' => $def];
    }
}