<?php
/*
 * Компонет - она же View - вьюшка - отображение шаблона
 */

namespace Engine\Core\View;

use DumpConfig;
use Engine\Core\Config\SiteConfig;
use Engine\Core\Development\DebugInfo;
use Engine\Core\Objects\ICoreElement;
use Engine\Core\Objects\INamespace;
use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Core\View\SiteComponent\SystemComponent;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseAbstractModule;
use ReflectionException;

final class ComponentManager implements ICoreElement
{
    public const EXCLUDE_ALL = 0x00000f;

    public const EXCLUDE_ALL_MAIN = 0x00001f;
    /**
     * @var ComponentManager
     */
    private static $instance;
    /**
     * Отложенная инъекция
     * @var array
     */
    private static $injectQueueu = [];
    /**
     * Автозагружаемые компоненты
     * @var array
     */
    private $componentMap = [];
    /**
     * @var array
     */
    private $components = [];
    /**
     * Компоненты готовые к компиляции
     * @var array
     */
    private $buildComponents = [];
    /**
     * @var array
     */
    private $injectData = [];
    /**
     * Отклоючённые в процессе работы системные компоненты
     * @var array
     */
    private $excluded = [];
    /**
     * Включённые в процессе работы компоненты
     * @var array
     */
    private $inluded = [];
    /**
     * @var array
     */
    private $trash = [];
    /**
     * @var
     */
    private $linkedNamespaces = [];

    /**
     * ComponentManager constructor.
     */
    public function __construct()
    {
        //dump(['style' => 'blue'], 'Comp manager begin');
        self::$instance = $this;

        if (!empty(self::$injectQueueu)) {
            $this->injectData = self::$injectQueueu;
        }

        $this->nmspcControl();
        //и в подключенных неймспейсах
        if (!empty($this->linkedNamespaces)) {
            foreach ($this->linkedNamespaces as $namespace) {
                $this->layoutMapFile(pathSeparator(Template::get(true, $namespace) . 'layout.php'));
            }
        }
        //проинициируем описание шаблона, так как это самое приоритетное
        $this->layoutMapFile(pathSeparator(Template::get(true) . 'layout.php'));
        //dump($this->componentMap);
    }

    /**
     *
     */
    public function nmspcControl(): void
    {
        $nmspcClass = sprintf('Namespaces\\%s\\NmspcConfig', Template::getNmspc());
        //dump(class_exists($nmspcClass), $nmspcClass, $nmspcClass::getMode());
        /** @var INamespace $nmspcClass */
        if (class_exists($nmspcClass)) {
            $this->linkedNamespaces = $nmspcClass::getLinkedNamespaces();
            //dump($this->linkedNamespaces);
            if ($nmspcClass::getMode() == INamespace::MODE_EMPTY) {
                ComponentManager::ExcludeComponents(ComponentManager::EXCLUDE_ALL);
            } elseif ($nmspcClass::getMode() == INamespace::MODE_INCLUDE) {
                ComponentManager::IncludeComponents($nmspcClass::getIncludedSystemComponents());
            } elseif ($nmspcClass::getMode() == INamespace::MODE_EXCLUDE) {
                ComponentManager::ExcludeComponents($nmspcClass::getExcludedSystemComponents());
            }
        }
    }

    /**
     *
     */
    public static function ExcludeComponents(): void
    {
        static::push(static::$instance->excluded, func_get_args());
    }

    /**
     * @param array $array
     * @param $value
     */
    private static function push(array &$array, $value): void
    {
        if (is_array($value[0] ?? null)) {
            $array = array_merge($array, $value[0]);
        } elseif (is_array($value)) {
            $array = array_merge($array, $value);
        } else {
            $array[] = $value;
        }
    }

    /**
     *
     */
    public static function IncludeComponents(): void
    {
        static::push(static::$instance->inluded, func_get_args());
    }

    /**
     * @param string $mapFile
     */
    private function layoutMapFile(string $mapFile): void
    {
        //dump(['style' => 'red', 'footer' => 'try load layout'], $mapFile);
        if (file_exists($mapFile)) {
            $map = include $mapFile;
            $this->componentMap = array_replace_recursive($this->componentMap ?? [], $map);
        }
    }

    /**
     * Ищем файлы TODO: возможно стоит добавить все найденные варианты
     * @param $file
     * @return string|null
     */
    public static function findFile($file): ?string
    {
        $self = self::$instance;
        $_file = pathSeparator(Template::get(true) . $file);
        // dump(['style' => 'red', 'footer' => 'Try find'], $_file, $self->linkedNamespaces);
        if (file_exists($_file)) {
            return $_file;
        }
        //поищем в неймспейсах
        if (!empty($self->linkedNamespaces)) {
            foreach ($self->linkedNamespaces as $namespace) {
                $_file = pathSeparator(Template::get(true, $namespace) . $file);
                if (file_exists($_file)) {
                    return $_file;
                }
            }
        }
        //поищем в интерфейсе
        return null;
    }

    /**
     * @param $component IComponent|SiteComponent
     * @param string $moduleName
     * @param string $target
     * @param string|null $asLabel
     * @param int $mode
     * @throws ReflectionException
     */
    public static function add($component, string $moduleName = null, string $target = NULL, string $asLabel = null, int $mode = IComponent::INJECT_AS_DATA)
    {
        self::$instance->_add($component, $moduleName, $target, $asLabel, $mode);
    }

    /**
     * @param $component SiteComponent
     * @param string $moduleName
     * @param string $target
     * @param string|null $asLabel
     * @param int $mode
     * @throws ReflectionException
     */
    private function _add($component, string $moduleName = null, string $target = NULL, string $asLabel = null, $mode = IComponent::INJECT_AS_DATA): void
    {
        if (!empty($moduleName)) {
            $component->setName($moduleName . '.' . $component->getName());
        }

        if ($target == null) {
            $this->components['components'][] = $component;
        } else {
            /** @var SiteComponent $component */
            $_component = $this->findComponent($target);

            //dump($_component, $asLabel);

            if ($_component !== null) {
                $_component->addInnerComponents($component, $asLabel);
            } else {
                $component->setInject(InjectComponentConfig::make($target, $asLabel ?? $component->getName(), $mode));
                $this->buildComponents[] = $component;
            }
        }
    }

    /**
     * @param string|int $search
     * @param array $list
     * @param bool $delete
     * @return BuildComponent|null
     * @throws ReflectionException
     */
    private function findComponent($search, array &$list = [], bool $delete = false): ?AbstractComponent
    {
        $list = $list ?? self::$instance->components['components'];
        $res = null;

        if ($search instanceof IComponent) {
            $search = $search->getId();
        }
        /** @var $component AbstractComponent */
        foreach ($list as $i => $component) {

            if ((is_numeric($search) AND $component->getId() == $search) //ищем по ID
                OR
                (strtolower($component->getName()) == strtolower($search))) //ищем по имени
            {

                if ($delete) {
                    //добавляем индекс в мусорку
                    $this->trash[] = $i;
                }
                return $component;
            } elseif ($component->countInner() > 0) {
                if ($component->findInner($search, $res)) {
                    return $res;
                }
            }
        }

        return null;
    }

    /**
     * Добавляем системный компонент. Не может быть
     * @param string $get_called_class
     * @param string $component
     */
    public static function addSystemComponent(string $get_called_class, string $component): void
    {
        self::$instance->components[] = $component;
    }

    /**
     *
     */
    public static function initialize()
    {
        //not use here
    }

    /**
     * @param string $componentName
     * @param array $data
     */
    public static function injectData(string $componentName, array $data): void
    {
        if (self::$instance != null) {
            self::$instance->injectData[$componentName] = array_merge_recursive(self::$instance->injectData[$componentName] ?? [], $data);
            //dump(['style' => 'green', 'footer' => $componentName], self::$instance->injectData);
        } else {
            self::$injectQueueu[$componentName] = array_merge_recursive(self::$injectQueueu[$componentName] ?? [], $data);
        }
    }

    /**
     * Получить данные
     * @param string $componentName
     * @param string $key
     * @param bool $extract
     * @return mixed|null
     */
    public static function getData(string $componentName, string $key, bool $extract = false)
    {
        $result = null;
        if (isset(self::$instance->injectData[$componentName])) {
            $result = self::$instance->injectData[$componentName][$key] ?? null;
        }
        if ($result AND $extract) {
            unset(self::$instance->injectData[$componentName][$key]);
        }
        return $result;
    }

    /**
     * Переназначение пути шаблона для компонента
     * @param string $componentName
     * @param string $path
     * @param int $injectConfig
     */
    public static function injectComponentPath(string $componentName, string $path, int $injectConfig = 0): void
    {
        self::$instance->injectData['path'][$componentName] = $path;
        //dump(self::$instance->injectData);
    }

    /**
     * Строим
     * @param array $runModules
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public static function build(array $runModules): void
    {
        self::$instance->_build($runModules);
    }

    /**
     * @param array $runModules
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    private function _build(array $runModules): void
    {
        Render::$runModules = $runModules;

        DumpConfig::$globalExpand = false;
        //dump('notify', SiteConfig::get('notice-type', 'main', 'render') == 'render', !$this->checkExcluded('notify-panel'));
        if (
            SiteConfig::get('notice-type', 'main', 'render') == 'render'
            AND !$this->checkExcluded('notify-panel')) { //все нотисы - идут как компоненты
            //создадим для них отдельный компонент
            self::createSiteComponent('notify-panel',
                [
                    'notify' => Render::getNotice('notify'),
                    'error' => Render::getNotice('errors'),
                    'warning' => Render::getNotice('warning')
                ]
                , InjectComponentConfig::make('main', 'notifyPanel', IComponent::INJECT_AS_CONTENT));
        }

        //снова проинициируем основное описание шаблона. В случае если в процессе был добавлены компоненты (хотя это врядли)
        //$this->layoutMapFile(pathSeparator(Template::get(true) . 'layout.php'));

        //dump($this->buildComponents);

        //dump($runModules);
        //добавляем компоненты на рендеринг из модулей + загружаем конфиг шаблонов для подключения компонентов
        /** @var BaseAbstractModule $module */
        foreach ($runModules as $module) {
            //ищем описание шаблона в папке с модулем
            $this->layoutMapFile($module::getModulePath() . DIRECTORY_SEPARATOR . 'layout.php');

            //ищем в папке с макетами для текущего модуля
            //dump($module->getModuleName(true) . '.php');
            $this->layoutMapFile(pathSeparator(Template::get(true) . 'layouts/' . $module->getModuleName(true) . '.php'));

            //и так же в прилинкованных
            if (!empty($this->linkedNamespaces)) {
                foreach ($this->linkedNamespaces as $namespace) {
                    $this->layoutMapFile(pathSeparator(Template::get(true, $namespace) . 'layouts/' . $module->getModuleName(true) . '.php'));
                }
            }
            //
            $this->buildComponents = array_merge($this->buildComponents, $module->getBuildComponents());
        }
        //dump($this->buildComponents, $this->components);
        /** @var SystemComponent $component */
        foreach ($this->components as $component) {
            if (is_array($component)) {
                foreach ($component as $_c) {
                    if ($_c instanceof IComponent) {
                        $this->buildComponents[] = $_c;
                    }
                }
            } else {
                //  dump($component::$componentRegisteredName, $this->checkExcluded($component::$componentRegisteredName), $component::$enable);
                if ($component::$componentRegisteredName == 'main' OR (!$this->checkExcluded($component::$componentRegisteredName) AND $component::$enable)) {
                    //dump($component::$componentRegisteredName);
                    if (($comp = $component::Init()) != null) { //самоинициация
                        //$this->layoutMapFile(pathSeparator(Template::get(true) . 'layouts/' .$component::$componentRegisteredName . '.php'));
                        //dump($comp);
                        $this->buildComponents[] = $comp;
                    }
                }
            }
        }

        //dump($this->buildComponents);

        //делаем ремап структуры компонентов, согласно конфигу шаблона
        //dump($this->componentMap);
        if (!empty($this->componentMap)) {
            $this->reMap($this->componentMap['frame'] ?? []);
        }

        if (!empty($this->injectData['path'])) {
            $this->componentMap['templates'] = array_merge($this->componentMap['templates'], $this->injectData['path']);
        }

        //dump($this->componentMap, $this->buildComponents);
        //А теперь перебираем компоненты и вставляем согласно InjectComponentConfig
        //dump($this->buildComponents);
        foreach ($this->buildComponents as &$component) {
            /** @var AbstractComponent $component */
            $this->injectComponent($component);
        }
        $this->clear();
        //dump($this->buildComponents);
    }

    /**
     * Компонент отключён
     * @param $componentName
     * @return bool
     */
    private function checkExcluded($componentName): bool
    {
        if (!empty($this->inluded)) {
            //dump($this->inluded, $componentName, (in_array($componentName, $this->inluded)));
            return !(in_array($componentName, $this->inluded));
        }
        $allExcluded = ($this->excluded[0] ?? null) == self::EXCLUDE_ALL;
        //dump($this->excluded, $componentName, (in_array($componentName, $this->excluded)));
        return $allExcluded OR in_array($componentName, $this->excluded);
    }

    /**
     * @param string $name
     * @param array $data
     * @param InjectComponentConfig|null $inject
     * @return SiteComponent
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public static function createSiteComponent(string $name, array $data = [], InjectComponentConfig $inject = null): SiteComponent
    {
        //dump($data);
        $component = new SiteComponent($name, null, $data, $inject);
        // ВНИМАНИЕ. Так как на момент создания какого--либо компонента цели может и не быть

        if ($inject != null) {
            $targetCompponent = ComponentManager::$instance->findComponent($inject->target);
            if ($targetCompponent != null) {
                $targetCompponent->addInnerComponents($component);
                return $component;
            }
        }

        //сработает если не нашли цель или нет параметров иньекции
        self::$instance->buildComponents[] = $component;

        return $component;
    }

    /**
     * @param array $branch
     * @param string|null $parent
     * @throws ReflectionException
     */
    private function reMap(array $branch = [], string $parent = null): void
    {
        //dump_trace($branch, $parent);

        $tmp = $branch;
        foreach ($tmp as $component => $data) {
            //dump(['style' => 'red'], $component, $data);
            $label = null;
            //есть родительский компонент
            if ($parent != null) {
                //dump($component, $data, $parent);
                if (is_numeric($component)) {
                    $component = $data;
                } else if (!is_array($data)) {
                    $label = $data;
                }

                //strtolower($parent), strtolower($component)
                //$this->injectComponent($component, InjectComponentConfig::make($parent, $component->getName()));
                //dump($parent, $component);
                $this->_injectByRemap($parent, $component, $label);
            }
            //есть потомки
            if (is_array($data)) {
                $this->reMap($data, $component);
            }
        }
    }

    /**
     * @param $parent
     * @param $source
     * @param null $label
     * @throws ReflectionException
     */
    private function _injectByRemap($parent, $source, $label = null): void
    {
        $target = $this->findComponent(strtolower($parent), $this->buildComponents);
        $inject = $this->findComponent(strtolower($source), $this->buildComponents);
        //dump($target, $source, $inject);
        //
        if ($target != null AND $inject != null) {
            //dump($inject);
            $inject->setInject(InjectComponentConfig::make($parent, $label ?? $source, $inject->getInjectMode()));
        }
    }

    /**
     * @param AbstractComponent $component
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function injectComponent(AbstractComponent $component): void
    {
        //добавляем данные в компоненты, которые были назначены где-то снаружи. Например в модулях.
        if (isset($this->injectData[$component->getName()])) {
            $component->setData($this->injectData[$component->getName()]);
        }

        $injectConfig = $component->getInject();
        if ($injectConfig == null) {
            return;
        }
        $target = $this->findComponent($injectConfig->target, $this->buildComponents);
        $inject = $this->findComponent($component->getId(), $this->buildComponents, true);
        if ($target != null) {
            $target->addInnerComponents($inject, $injectConfig->asLabel);
        } else {
            DebugInfo::debugAddF(lang('@error_injection_root'), $inject->getName());
        }
    }

    /**
     * Подчищаем
     */
    private function clear(): void
    {
        foreach ($this->trash as $id) {
            unset($this->buildComponents[$id]);
        }
    }

    /**
     * @param string $searching
     * @param bool $searchLink
     * @return string|null
     */
    public static function findPathFor(string $searching, bool $searchLink = false): ?string
    {
        return self::$instance->_findPathFor($searching, [], $searchLink);
    }

    /**
     * Найти путь до шаблона
     * @param string $searching
     * @param array $branch
     * @param bool $searchLink
     * @return string
     */
    private function _findPathFor(string $searching, array $branch = [], bool $searchLink = false): ?string
    {

        if (empty($branch)) {
            $branch = $this->componentMap['templates'] ?? [];
        }

        //dump(['style' => 'red', 'footer' => 'searching for' . $searching, 'expand' => false], $branch);

        if (empty($branch)) {
            return null;
        }

        $inject = $this->_injectComponentPath($searching);

        if ($inject != null) {
            return $inject;
        }

        foreach ($branch as $comp => $data) {
            /*
            if (!$searchLink) {
                //dump(strtolower($comp), strtolower($searching), $data, (strtolower($comp) == strtolower($searching)));
            }
            */

            if (strtolower($comp) == strtolower($searching)) {
                return $data;
            }
            if (is_array($data)) {
                $find = $this->_findPathFor($searching, $data);
                if ($find !== null) {
                    return $find;
                }
            }
        }

        //если добьрались до сюда, значит ничего не нашли. Однако смотрим линкованные конфиги
        //dump(['style'=>'dark'],$this->componentMap);
        if (!empty($this->componentMap['link']) AND $searchLink) {
            $module = explode('.', $searching)[0] ?? $searching;
            if (isset($this->componentMap['link'][$searching]) OR isset($this->componentMap['link'][$module])) {
                //$module = explode('.', $searching)[0] ?? $searching;
                $file = pathSeparator(Template::get(true) . $this->componentMap['link'][$module] . '.php');

                //dump(['footer'=>'search: '.$module],$file, file_exists($file));

                if (file_exists($file)) {
                    $map = include $file;
                    //dump(['style' => 'red'], $file, $searching, $map);
                    if ($find = $this->_findPathFor($searching, $map['templates'], false)) {
                        return $find;
                    }
                }

                //поищем еще в прилинкованных неймспейсах
                if (!empty($this->linkedNamespaces)) {
                    foreach ($this->linkedNamespaces as $namespace) {
                        $file = pathSeparator(Template::get(true, $namespace) . $this->componentMap['link'][$module] . '.php');
                        if (file_exists($file)) {
                            $map = include $file;
                            //dump($file, $namespace, $map['templates']);
                            if ($find = $this->_findPathFor($searching, $map['templates'], false)) {
                                return $find;
                            }
                        }
                    }
                }
                //ну и самый последний вариант - посмотрим просто в папках. Ну мало ли забыли прописать (или поленились)
            }
        }

        return null;
    }

    /**
     * @param string $componentName
     * @return string|null
     */
    private function _injectComponentPath(string $componentName): ?string
    {
        return $this->injectData['path'][$componentName] ?? null;
    }

    /**
     * @param string $name
     * @param string $path
     * @param float $rev
     * @return string|null
     */
    public static function loadSystemComponentStyle(string $name, string $path, float $rev = 1.0): void
    {
        $file = pathSeparatorF('%sresources/css/system-components/%s.css', Template::get(true), $name);
        //dump($file);
        if (file_exists($file)) {
            //dump(['style' => 'orange', 'footer' => $name], $file, $path);
            Render::addCSS(['/system-components/' . $name], $path, $rev);
            return;
        }
        //смотрим в подключённых
        foreach (self::$instance->linkedNamespaces as $namespace) {
            $file = pathSeparatorF('%sresources/css/system-components/%s.css', Template::get(true, $namespace), $name);
            //dump($file);
            if (file_exists($file)) {
                //dump(['style' => 'orange', 'footer' => $name], $file, $path);
                Render::addCSS([$namespace => '/system-components/' . $name], $path, $rev);
                break;
            }
        }
    }

    /**
     * @return array
     */
    public static function getBuildComponents(): array
    {
        return self::$instance->buildComponents;
    }

    /**
     * @param array $autoLoadComponents
     */
    public function setComponents(array $autoLoadComponents): void
    {
        $this->autoLoadComponents = $autoLoadComponents;
    }
}