<?php

namespace Engine\Core\View;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Development\DebugInfo;
use Engine\Core\Session\Session;
use Engine\Core\View\SiteComponent\AbstractComponent;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Exceptions\SiteComponentException;
use Engine\Modules\BaseAbstractModule;

final class Render
{
    public static $frame = 0;
    /**
     * Текущие активные модули
     * @var
     */
    public static $runModules;
    /**
     * Стили, которые добаляются из шаблонов компонента
     * @var array
     */
    private static $cssStyles = [];
    /**
     * Информация о подключенных стилей (не обрабатываются дубликаты)
     * @var array
     */
    private static $cssIncludeInfo = [];
    /**
     * переменные для JS
     * @var array
     */
    private static $jsVariables = array();

    //Ошибки
    private static $outData;
    private static $error = array();
    /**
     * Уведомления
     * @var array
     */
    private static $notify = array();
    /**
     * Предупреждения
     * @var array
     */
    private static $warning = [];
    /**
     * Диологовое окно с пользователем
     * @var array
     */
    private static $dialog = [];

    /**
     * Иньекции
     * @var array
     *
     * private static $injectData = [];
     */

    /**
     * Установить контент для вывода (используется в основном в контроллере)
     * @param string $outData
     *
     * public static function setOutData(string $outData): void
     * {
     * self::$outData = $outData;
     * }
     */

    /**
     * @param array $componentsList
     * @throws \ReflectionException
     * @throws SiteComponentException
     */
    public static function render(array $componentsList)
    {
        //dump($componentsList);
        //сперваизвлекём дебаг инфо, так как его компилировать надо в последню очередь, ведь в него поступают данные во время компиляции других компонентов..
        $debug = self::extractComponent($componentsList, 'debuginfo'); //TODO: единственная проблема, что то что компилировалось внутри дебаг компонента в дебаг уже  соответственно не попадает.
        if ($debug != null) {
            $debugDisplay = $debug->display();
        }
        //dump($componentsList);
        /** @var AbstractComponent $component */
        foreach ($componentsList as $component) {
            //dump($component);
            //main компонент
            if ($component->getName() == 'main') {
                //собираем все компоненты, которые находятся внутри
                $component->buildInnerComponents();
                //Добавляем CSS стили
                if (!empty(self::$cssStyles)) {
                    $component->setData(self::includeCss());
                }
                //Собираем уведомления
                self::includeNotice();
                //Добавляем JavaScript данные в шаблон
                if (!empty(self::$jsVariables)) {
                    $component->setData(self::includeJS());
                }
                DebugInfo::debugAddF(lang('@render_compiled_count_components'), static::$frame + 1);
                //если в системе был компонент дебагинфо, то установим его данные в данные main шаблона
                if ($debug != null) {
                    $component->setData('debuginfo', $debugDisplay);
                }
            }
            echo $component->display();
        }
    }

    /**
     * @param array $list
     * @param $comp
     * @return IComponent|null
     */
    private static function extractComponent(array &$list, $comp): ?IComponent
    {
        /** @var IComponent $_comp */
        foreach ($list as $i => $_comp) {
            if (is_numeric($comp) AND $_comp->getId() == $comp) {
                unset($list[$i]);
                return $_comp;
            } elseif ($_comp->getName() == $comp) {
                unset($list[$i]);
                return $_comp;
            }
        }
        return null;
    }

    /**
     * Подключаем CSS стили
     * @return array
     */
    private static function includeCss(): array
    {
        $result = '';
        //dump(self::$cssStyles);

        foreach (self::$cssStyles as $theme => $css) {
            if (empty($css))
                continue;
            if (is_array($css)) {
                $result .= self::css($theme, $css);
            } else {
                $result .= self::css(0, [$css]);
            }
        }
        return ['includeCSS' => $result];
    }

    /**
     * @param string $theme
     * @param array $styles
     * @return string
     */
    private static function css(string $theme, array $styles): string
    {
        $result = '';
        $compiled = [];
        foreach ($styles as $style) {

            if (empty($style) OR in_array($style, $compiled)) {
                continue;
            }

            $compiled[] = $style;
            $resLink = 'resources/css/';
            if ($style[0] === '@') {
                $style = substr($style, 1);
                $tpl = '';
                $resLink = '';
            } elseif (!is_numeric($theme) AND is_dir(TPL_DIR . $theme . DIRECTORY_SEPARATOR)) {
                $tpl = sprintf('/Templates/%s/', $theme);
            } else {
                $tpl = TPL;
            }
            $cssParts = explode(':', $style);

            if (count($cssParts) < 2) {
                $cssParts[1] = 1; //default revision
            }
            list($css, $rev) = $cssParts;

            $result .= sprintf("\n\t\t<link rel=\"stylesheet\" href=\"%s%s%s.css?rev=%s\"/>\n", $tpl, $resLink, $css, $rev);
        }
        return $result;
    }

    /**
     * Добавляем уведомления, ошибки и диалоги (лобавляем в JS переменные данные о уведомлениях)
     * Но только если в конфиге включено, что обработка сообщений будет через JS
     */
    private static function includeNotice()
    {
        if (SiteConfig::get('notice-type', 'main', 'render') == 'js') {
            Render::AddJS([
                'notice' => [
                    'noticeInterval' => 5000,
                    'noticePosition' => array( //TODO: Пользовательский конфиг
                        'Error' => 'tc',
                        'Warn' => 'tc',
                        'Success' => 'tc'
                    ),
                    'notify' => self::getNotice('notify'),
                    'errors' => self::getNotice('errors'),
                    'warning' => self::getNotice('warning')
                ],
                'dialogs' => self::getNotice('dialog')
            ]);
        }
    }

    /**
     * Подключить в main компонент глобальную js переменную core в которую бедем передавать параметры.
     * @param array $jsArray
     */
    public static function addJS(array $jsArray)
    {
        self::$jsVariables = array_merge_recursive(self::$jsVariables, $jsArray);
    }

    /**
     * Получаем все уведомления
     *
     * @param $type
     * @return array
     */
    static public function getNotice($type): array
    {
        $notes = [];
        switch ($type) {
            case 'errors':
                $notes = self::$error;
                break;

            case 'notify':
                $notes = self::$notify;
                break;
            case 'warning':
                $notes = self::$warning;
                break;

            case 'dialog':
                $notes = self::$dialog;
                break;
        }
        //Достаём из сессий.
        if ($type == 'errors' && Session::get('error') != null) {
            $notes = array_merge_recursive($notes, Session::get('error'));
            Session::delete('error');
        } elseif ($type == 'notify' && Session::get('notify') != null) {
            $notes = array_merge_recursive($notes, Session::get('notify'));
            Session::delete('notify');
        } elseif ($type == 'warning' && Session::get('warning') != null) {
            $notes = array_merge_recursive($notes, Session::get('warning'));
            Session::delete('warning');
        } elseif ($type == 'dialog' && Session::get('dialog')) {
            $notes = array_merge_recursive(Session::get('dialog'));
            Session::delete('dialog');
        }
        return $notes;
    }

    /**
     * Добавляем псевдо компонент из JS данных
     * @return array
     */
    private static function includeJS(): array
    {
        $json = json_encode(self::$jsVariables);
        return ['includeJS' => "<script>window['globals']=$json</script>\n"];
    }

    /**
     * Подключение JS файла
     * @param string $jsFile
     */
    public static function includeJSFIle($jsFile)
    {
        if (is_array($jsFile)) {
            self::addJS(['extrernalFiles' => $jsFile]);
        } elseif (is_string($jsFile)) {
            self::addJS(['extrernalFiles' => [$jsFile]]);
        }
    }

    /**
     * @param $css
     * @param float $rev
     */
    public static function addPrivateCSS($css, float $rev = 1.0)
    {
        self::$cssStyles[] = "@$css:$rev";
    }

    /**
     * Перечень имём css файлов, которые будтут подключены в main компонент (если конечно это нужно будет)
     * @param array $include
     * @param string|null $tmpPath
     * @param float $rev
     */
    public static function addCSS(array $include, string $tmpPath = null, float $rev = 1.0)
    {
        foreach ($include as $theme => $style) {
            //dump($theme , $style, $tmpPath);
            //добавляем информацию о подключеном css, чтобы в дебаге можно было вывести где и какой стиль подключается
            $includeCSSFile = [
                'theme' => is_numeric($theme) ? Template::$theme : $theme,
                'css' => $style,
                'rev' => ':' . $rev,
                'tplPath' => $tmpPath
            ];
            //добавляем в дебаг инфу
            DebugInfo::debugAddF(
                '[css include] тема: <b>%s</b>; Подключение: <b>%s.css</b>; В шаблоне: <b>%s.php</b>',
                $includeCSSFile['theme'],
                is_array($includeCSSFile['css']) ? implode('>', $includeCSSFile['css']) : $includeCSSFile['css'],
                $includeCSSFile['tplPath']
            );
            //добавляем данные в переменную. Это нужно чтобы.. эм.. ну например отдампить гден-ить.. хз правда где. Единственное место, где данные будут корректные данные - в момент рендера main template
            self::$cssIncludeInfo[] = $includeCSSFile;

            //dump($style, self::$cssStyles);

            if (is_numeric($theme)) {
                if (!in_array($style . $includeCSSFile['rev'], self::$cssStyles)) {
                    self::$cssStyles[] = $style . $includeCSSFile['rev'];
                }
            } else {
                self::addThemeCSS($theme, $style);
            }
        }
    }

    /**
     * Добавить стили из дргуого неймспейса (TODO: можно проерить сперва, что такие неймспейсы подключены Render::addStyleNamespacePath(_namespace_))
     * @param string $theme
     * @param $style
     */
    private static function addThemeCSS(string $theme, $style): void
    {
        if (!isset(self::$cssStyles[$theme])) {
            self::$cssStyles[$theme] = [];
        }

        if (is_array($style)) {
            self::$cssStyles[$theme] = array_merge(self::$cssStyles[$theme], $style);
        } else {
            if (!in_array($style, self::$cssStyles[$theme])) {
                self::$cssStyles[$theme][] = $style;
            }
        }
    }

    /**
     * @param string $error
     * @param bool $session
     */
    public static function setError(string $error, bool $session = false): void
    {
        if ($session) {
            Session::add('error', $error);
        } else {
            self::$error[] = $error;
        }
    }

    /**
     * @param string $notice
     * @param bool $session
     */
    public static function setNotice(string $notice, bool $session = false): void
    {
        if ($session) {
            Session::add('notify', $notice);
        } else {
            self::$notify[] = $notice;
        }
    }

    /**
     * @param string $warning
     * @param bool $session
     */
    public static function setWarning(string $warning, bool $session = false): void
    {
        if ($session) {
            Session::add('warning', $warning);
        } else {
            self::$warning[] = $warning;
        }
    }

    /**
     * @param string $msg Сообщение (текст, html)
     * @param string $type
     *        html - данные - это хтмл код
     *        text - данные - текст
     * @param bool $session - данные сохраняются в сессию
     */
    static public function dialog(string $msg, $type = 'text', bool $session = false)
    {
        if ($session) {
            Session::add('dialog', [$type => $msg]);
        } else {
            if (empty(self::$dialog[$type]))
                self::$dialog[$type] = $msg;
            else
                self::$dialog[$type] .= $msg;
        }
    }

    /**
     * @param string $moduleName
     * @return mixed
     * @throws \ReflectionException
     */
    public static function isRunModule(string $moduleName): bool
    {
        ///dump($moduleName, self::$runModules);
        /** @var BaseAbstractModule $module */
        foreach (self::$runModules as $module) {
            //dump(strtolower($moduleName), $module->getModuleName(true, true));
            if ($module->getModuleName(true, true) == strtolower($moduleName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public static function getStyleNamespacePath(): array
    {
        return array_keys(self::$cssStyles);
    }
}