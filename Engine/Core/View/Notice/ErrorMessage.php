<?php


namespace Engine\Core\View\Notice;


use Engine\Core\View\Notifications;

final class ErrorMessage extends Notifications
{
    protected $type = 'error';
}