<?php


namespace Engine\Core\View\Notice;


use Engine\Core\View\Notifications;

final class DialogMessage extends Notifications
{
    protected $type = 'dialog';
}