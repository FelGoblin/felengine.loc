<?php


namespace Engine\Core\View\Notice;


use Engine\Core\View\Notifications;

final class NoticeMessage extends Notifications
{
    protected $type = 'notice';
}