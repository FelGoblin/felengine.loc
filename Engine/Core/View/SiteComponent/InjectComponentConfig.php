<?php

namespace Engine\Core\View\SiteComponent;


final class InjectComponentConfig
{
    /**
     * Цель иньекции
     * @var string|int
     */
    public $target;

    /**
     * Режим иньекции
     * @var int
     */
    public $injectMode = IComponent::INJECT_AS_DATA;

    /**
     * Метка внедрённого компонента
     * @var string
     */
    public $asLabel = '';

    /**
     * @var int
     */
    public $pos = -1;

    /**
     * InjectComponentConfig constructor.
     * @param $target
     * @param string $label
     * @param int $mode
     * @param int $pos
     */
    public function __construct($target, string $label, int $mode = IComponent::INJECT_AS_DATA, int $pos = -1)
    {
        $this->target = $target;
        $this->injectMode = $mode;
        $this->asLabel = $label;
        $this->pos = $pos;
    }

    /**
     * @param $target
     * @param string $asLabel
     * @param int $mode
     * @param int $pos
     * @return InjectComponentConfig
     */
    public static function make($target, string $asLabel, int $mode = IComponent::INJECT_AS_DATA, int $pos = -1): InjectComponentConfig
    {
        return new InjectComponentConfig($target, $asLabel, $mode, $pos);
    }
}