<?php

namespace Engine\Core\View\SiteComponent;


use Engine\Core\Development\DebugInfo;
use Engine\Core\View\ComponentManager;
use Engine\Core\View\Render;
use Engine\Core\View\Template;
use Engine\Exceptions\SiteComponentException;
use Exception;
use ReflectionClass;
use ReflectionException;

abstract class AbstractComponent implements IComponent
{

    /**
     * Зарегистрированное имя компоента
     * @var string
     */
    public static string $componentRegisteredName = 'example';
    /**
     * Счётчик
     * @var int
     */
    private static int $UID = 0;
    /**
     * Выкинуть исключение, если нет определения шаблона
     * @var bool
     */
    public bool $templateException = false;
    /**
     * Сортировать внутри лежащие компоненты согласно позициями или нет
     * @var bool
     */
    public bool $sortInner = false;
    /**
     * ИД компонента
     * @var int
     */
    protected $id = 0;
    /**
     * Переменные, которые будут использованы в шаблоне компонента.
     * @var array
     */
    protected array $data = [];
    /**
     * Собранный контент комопонента
     * @var string
     */
    protected string $content = '';
    /**
     * Путь до шаблона компонента
     * @var string
     */
    protected string $path = '';
    /**
     * Имя компонента
     * @var string|null
     */
    protected ?string $name;
    /**
     * Стили, которые использует компонент, лежат в Templates/%Theme%/resources/css/system-components
     * @var array
     */
    protected array $componentCSS = [];
    /**
     * Компоненты, которые выводят данные в текущий компонент.
     * @var array
     */
    protected array $innerComponents = [];
    /**
     * @var InjectComponentConfig
     */
    protected ?InjectComponentConfig $inject = null;
    /**
     * Иньекция по-умолчанию
     * @var int
     */
    protected int $injectMode = self::INJECT_AS_DATA;
    /**
     * Компилировали ли мы внутренние элементы
     * @var bool
     */
    private bool $innerBuilds = false;
    /**
     * Добавочный контент
     * @var string
     */
    private string $tmpContent = '';

    /**
     * AbstractComponent constructor.
     * @param string|null $name
     * @param string|null $path
     * @param array $data
     * @param InjectComponentConfig|null $inject
     * @throws SiteComponentException
     */
    public function __construct(string $name = null, string $path = null, array $data = [], InjectComponentConfig $inject = null)
    {
        //имя шаблона
        if ($name != null) {
            $this->name = $name;
        }
        //есть путь
        if ($path != null) {
            $this->path = pathSeparator($path);
        }
        //dump(['style' => 'dark'], $this, $path, $this->path);
        //есть еще и данные (врядли они будут использоваться в такой кончтрукции, но шоп было)
        if (!empty($data)) {
            $this->setData($data);
        }

        if ($inject != null) {
            $this->inject = $inject;
        }

        $this->id = self::$UID++;
    }

    /**
     * @param string $path
     */
    public function addPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * Вывод шаблона данного компонента
     * @param string|null $path
     * @param array $data
     * @return string
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public function display(string $path = null, array $data = []): string
    {
        //обработаем путь до пути
        if ($path != null) {
            $this->path = pathSeparator(Template::get(true) . $path . '.php');
        }
        //добавляем данные
        if (!empty($data)) {
            $this->setData($data);
        }
        //контента нет, собираем компонент
        if (empty($this->content)) {
            $this->build();
        }
        return $this->content;
    }

    /**
     * Собрать компонент
     * @return mixed
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function build(): void
    {
        //dump(['private' => true, 'style' => 'yellow', 'expand' => false, 'footer' => $this->getName()], $this->path);
        $path = null;
        if ($this->path == null) {
            $path = ComponentManager::findPathFor($this->getName(), true) ?? null;

            if ($path != null) {
                $this->path = $path;
            }
        }
        /*
        if ($this->name == 'main') {
            dump_trace(['private' => true, 'style' => 'orange', 'expand' => false, 'footer' => $this->name], $path, $this->path);
        }
        */
        if (empty($this->path) AND $path == null) {
            //путя нет.. но ... попробуем поискать файл
            if (!empty($this->name)) {
                //dump(1, $this->name);
                $path = 'interface' . DIRECTORY_SEPARATOR . str_replace('.', DIRECTORY_SEPARATOR, $this->name);
                $file = ComponentManager::findFile($path . '.php');
                if ($file !== null) {
                    $this->path = $path;
                }
            }

            //и опять проверяем, а вдруг нашли всё же
            if (empty($this->path)) {
                //пробуем найти переопределение для папки шаблона у текущего модуля
                $names = explode('.', $this->name);
                $module = array_shift($names);
                //dump(2, $module);
                if (($module ?? null) != null) {
                    $fPath = ComponentManager::findPathFor($module, true) ?? null;
                    if ($fPath != null) {
                        $path = $fPath . implode(DIRECTORY_SEPARATOR, $names);
                        //dump($path);
                        $file = ComponentManager::findFile($path . '.php');//pathSeparator(Template::get(true) . $path . '.php');
                        //TODO: есть баг, плавающий. Есть описание, но имена совпадают с именем модуля, то при проверке на файл, которого нет, то оп просто выбрасывает, что путь неопределён, хотя он есть в layout`е.
                        //$this->path = $path;
                        if (file_exists($file)) {
                            $this->path = $path;
                        }
                    }
                    //вот прям крайний случай. Ищем внаглую и напрямую.
                    if (empty($this->path)) {
                        $file = pathSeparator(Template::get(true) . 'interface/' . $module . '/' . ($names[0] ?? '_error_') . '.php');
                        //dump($file);
                        //dump($file);
                        if (file_exists($file)) {
                            $this->path = 'interface/' . $module . '/' . $names[0];
                        }
                    }
                }
            }


            if (empty($this->path)) {
                $message = sprintf(lang('@error_template_undefined'), $this->name);
                if ($this->templateException) {
                    //нет не нашли
                    throw new SiteComponentException($this, $message);
                } else {
                    $this->content = $message;
                    return;
                }
            }
        }

        //dump($this->getPath() . '.php');

        $path = ComponentManager::findFile($this->getPath() . '.php');

        if (!is_file($path)) { //проверка на существование файла
            $this->content = sprintf(lang('@error_template_file_not_exists'), $this->path, $path);
            //dump_trace($path);
            return;
        }

        //собираем
        try {
            ob_start();

            //собираем компоненты, которые находятся внутри текущего компонента
            if (!$this->innerBuilds) {
                $this->buildInnerComponents();
            }

            // Load component into a variable.
            extract($this->getData());

            //dump($this->getData());

            //подключаем файл шаблона
            include $path;

            //CSS Styles
            $_css = $addCSS ?? $addCss ?? $css ?? $CSS ?? null;
            //dump($path, $_css);

            if ($_css != null) {
                $this->prepareCSSStyle($_css);
            }
            //JS links files
            $_js = $uncludeJS ?? $js ?? null;
            //dump($_js, $uncludeJS, $js);
            if ($_js != null) {
                Render::includeJSFIle($_js);
            }
            //это собирается системный компонент
            if ($this instanceof SystemComponent) {
                ComponentManager::loadSystemComponentStyle($this->name, $this->path, $rev ?? 1.0);
            }
            $this->content = ob_get_clean() . $this->tmpContent;
            Render::$frame++;
        } catch (Exception $ex) {
            $this->content = sprintf(
                lang('@error_template_exception'),
                (new ReflectionClass($ex))->getShortName(),
                $this->path,
                $ex->getMessage(),
                $ex->getLine()
            );
            ob_end_flush();
        }
    }

    /**
     * @return string|null
     * @throws ReflectionException
     */
    public function getName(): string
    {
        return $this->name ?? strtolower((new ReflectionClass($this))->getShortName());
    }

    /**
     * @param string $string
     */
    public function setName(string $string): void
    {
        $this->name = $string;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws ReflectionException
     * @throws SiteComponentException
     */
    public function buildInnerComponents(): void
    {
        if (!empty($this->innerComponents)) {

            if ($this->sortInner) {
                usort($this->innerComponents, function (IComponent $a, IComponent $b) {
                    if (($a->getInject() != null AND $b->getInject() != null) AND ($a->getInject()->pos >= 0 AND $b->getInject()->pos >= 0)) {
                        return ($a->getInject()->pos < $b->getInject()->pos) ? -1 : 1;
                    } else {
                        return 0;
                    }
                });
            }

            //dump('build: ' . $this->countInner() . ' in ' . $this->name, $this->innerComponents);
            /** @var SiteComponent $comp */
            foreach ($this->innerComponents as $compName => $component) {
                /** @var AbstractComponent $component */
                $componentContent = $component->display();
                //смотрим режим иньекции
                if ($component->inject != null) {
                    if ($component->inject->injectMode == self::INJECT_AS_DATA) {
                        //dump('graphData > ' . $compName . ' ? ' . Render::$frame++);
                        $this->data[$component->inject->asLabel] = $componentContent;
                    } elseif ($component->inject->injectMode == self::INJECT_AS_ARRAYDATA) {
                        $this->data[$component->inject->asLabel][] = $componentContent;
                        //dump($this->data);
                    } elseif ($component->inject->injectMode == self::INJECT_AS_CONTENT) {
                        $this->tmpContent .= $componentContent;
                    }
                } else {
                    $this->data[$compName] = $componentContent;
                }
            }
        }
        /*
        if ($this->getName() == 'leftMenu') {
            dump_trace($this->data);
        }
        */
        $this->innerBuilds = true;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function getData(array $data = []): array
    {
        $data = array_merge_recursive($data, $this->data);
        foreach ($data as $k => $d) {
            if (is_numeric($k)) {
                unset($data[$k]);
            }
        }
        $this->data = $data;
        return $data;
    }

    /**
     * Установить данные в шаблон компонента
     * @param $data array|string
     * @param $value null|string
     * @throws SiteComponentException
     */
    public function setData($data, $value = null): void
    {
        //проверяем на наличие контента
        if (!empty($this->content)) { //TODO: придумать как это отработать, хотя такой кардинальный вариант пойдёт.
            //dump_trace($this->content);
            throw new SiteComponentException($this, sprintf(lang('@error_template_data_inject'), $this->name));
        }
        //добавляем данные
        if (is_array($data) AND !empty($data)) { //массив и не пустой
            $this->data = array_merge_recursive($this->data, $data);
        } elseif (is_string($data) AND $value !== null) { //строки
            $this->data[$data] = $value;
        } else {
            DebugInfo::debugAddF(lang('@error_template_data_empty'), $this->name);
        }
    }

    /**
     * Приписываем стилям в компонентах путь компонентов
     * @param $css
     */
    private function prepareCSSStyle($css): void
    {
        foreach ($css as $theme => $style) {
            if (is_array($style)) {
                $_css = array_map(function ($style) {
                    if (!empty($style)) {
                        return 'components/' . $style;
                    }
                    return $style;
                }, $style);
                Render::addCSS([$theme => $_css], $this->path);
            } else {
                Render::addCSS(['components/' . $style], $this->path);
            }
        }
    }

    /**
     * Алиас
     * @return array
     */
    public function getViewModel(): array
    {
        return $this->getTemplateVariables();
    }

    /**
     * ВОзвращает все именнные переменные
     * @return array
     */
    public function getTemplateVariables(): array
    {
        return array_values(array_diff(array_keys($this->data), ['injected', 'extraData']));
    }

    /**
     * @param string | int $search
     * @param null $res
     * @param bool $deepSearch
     * @return bool
     * @throws ReflectionException
     */
    public function findInner($search, &$res = null, bool $deepSearch = false): bool
    {
        if (empty($this->innerComponents)) {
            return false;
        }
        //dump('try search inner ' . $search, $this);

        /** @var AbstractComponent $innerComponent */
        foreach ($this->innerComponents as $innerComponent) {
            if ((is_numeric($search) AND $innerComponent->getId() == $search)//ищем по ИД
                OR
                (strtolower($search) == strtolower($innerComponent->getName()))) //ищм по имени
            {
                $res = $innerComponent;
                //dump(['style' => 'yellow'], $res);

                return true;
            } elseif ($innerComponent->countInner() > 0 AND $deepSearch) {
                //dump("ищем $name внутри компонента {$innerComponent->getName()}", $innerComponent->countInner());
                $find = $innerComponent->findInner($search, $res);
                if ($find) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function countInner(): int
    {
        return count($this->innerComponents);
    }

    /**
     * @param IComponent $component
     * @param string|null $name
     * @throws ReflectionException
     */
    public function addInnerComponents(IComponent $component, string $name = null): void
    {
        /** @var AbstractComponent $component */
        if ($component->getInject() != null) {
            if ($component->getInject()->injectMode == self::INJECT_AS_ARRAYDATA) {
                $this->innerComponents[$component->getId()] = $component;
                return;
            }
        }
        $this->innerComponents[$name ?? $component->getName()] = $component;
    }

    /**
     * @return InjectComponentConfig|null
     */
    public function getInject(): ?InjectComponentConfig
    {
        return $this->inject;
    }

    /**
     * @param InjectComponentConfig $injectComponentConfig
     */
    public function setInject(InjectComponentConfig $injectComponentConfig): void
    {
        $this->inject = $injectComponentConfig;
    }

    /**
     * @return int
     */
    public function getInjectMode(): int
    {
        return $this->injectMode;
    }

    /**
     * @param int $injectMode
     */
    public function setInjectMode(int $injectMode): void
    {
        $this->injectMode = $injectMode;
    }
}
