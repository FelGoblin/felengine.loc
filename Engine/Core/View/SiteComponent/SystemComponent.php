<?php

namespace Engine\Core\View\SiteComponent;


use Engine\Core\Objects\FillClass;
use ReflectionException;

abstract class SystemComponent extends AbstractComponent
{
    /**
     * Регистрация имени системного компонента
     * @var string
     */
    public static string $componentRegisteredName = 'input';

    /**
     * Компонент включен
     * @var bool
     */
    public static bool $enable = true;

    /**
     * Внедрённые данные
     * @var array
     */
    protected array $injectedData = [];

    /**
     * Хуки контроллера
     * @var array
     */
    protected array $injectedControllers = [];

    /**
     * Обязательно у системных компонентов должна быть инициализация
     * @param string|null $path
     * @return AbstractComponent|null
     */
    public abstract static function init(string $path = null): ?AbstractComponent;

    /**
     * Внедряем данные в компонент
     * @param array $inject - Внедряемые данные в дату компонента
     */
    public function injectData(array $inject): void
    {
        $this->injectedData = $inject;
    }

    /**
     * Внедрить контроллер
     * @param callable|array $controller
     */
    public function injectController($controller): void
    {
        if (is_array($controller)) {
            $this->injectedControllers = array_merge_recursive($this->injectedControllers, $controller);
        } elseif (is_callable($controller)) {
            $this->injectedControllers[] = $controller;
        }
        //dump($this->injectedControllers);
    }

    /**
     * Обрабатываем иньекции.
     * @param array $data
     * @return array
     */
    public function getData(array $data = []): array
    {
        if (!empty($this->injectedData)) {
            foreach ($this->injectedData as $inject) {
                if ($inject['merge']) {
                    $data = array_merge_recursive($data, $inject['data']);
                } else {
                    $data = ['injectData' => $inject['data']];
                }
            }
        }
        return parent::getData($data);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Контроллер /калбек системного компонента
     */
    public function controller(): void
    {
        foreach ($this->injectedControllers as $controller) {
            if (is_callable($controller)) {
                $controller();
            }
        }
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    protected function viewModel(): array
    {
        return FillClass::asArray($this);
    }
}
