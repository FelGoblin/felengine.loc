<?php

namespace Engine\Core\View\SiteComponent;

class_alias( 'Engine\\Core\\View\\SiteComponent\\IComponent','IComponent');
/**
 * Interface IComponent
 * @package Engine\Core\Template\SiteComponent
 */
interface IComponent
{
    /**
     * Компонент внедрён как данные
     */
    public const INJECT_AS_DATA = 0;
    /**
     * Компонент внедрён как дополнительный контент
     */
    public const INJECT_AS_CONTENT = 1;

    /**
     * Компонент внедрён как данные, но при этом если есть совпадение по ключу, то ключ превращается в массив
     */
    public const INJECT_AS_ARRAYDATA = 2;

    /**
     * Имя компонента
     * @return string
     */
    public function getName(): string ;

    /**
     * ID компонента
     * @return int
     */
    public function getId():int;

    /**
     * Устанавлиаем данные для шаблона
     * @param $data
     * @param null $value
     */
    public function setData($data, $value = null): void;

    /**
     * @param IComponent $component
     * @param string|null $name
     */
    public function addInnerComponents(IComponent $component, string $name = null): void;

    /**
     * Компиляция компонента
     *
     * @return string
     */
    public function display(): string;

    /**
     * Получить конфиг иньекций
     * @return InjectComponentConfig|null
     */
    public function getInject(): ?InjectComponentConfig;
}