<?php

namespace Engine\Core\View\SiteComponent;


use Engine\Core\View\ComponentManager;
use Engine\Core\View\Template;
use Engine\Exceptions\LibraryBlockException;
use Engine\Exceptions\SiteComponentException;
use Exception;
use ReflectionException;

/**
 * Class SiteComponent
 * @package Engine\Core\Template\SiteComponent
 */
class SiteComponent extends AbstractComponent
{
    /**
     * Safe Quick Template
     * @param string $tpl
     * @param array $setData
     * @return string
     */
    public static function quick(string $tpl, array $setData = []): string
    {
        try {
            $result =(new SiteComponent(null, self::findPath($tpl), $setData))->display();
            //$result = SiteComponent::quick(self::findPath($tpl), $setData);
        } catch (Exception $ex) {
            $result = "Ошибка шаблона: [<b>{$ex->getMessage()}</b>]";
        }
        return $result;
    }

    /**
     * Упрощение доступа к компоненту
     * @param string $tpl
     * @param array $setData
     * @return string
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public static function rawQuick(string $tpl, array $setData = []): string
    {
        return (new SiteComponent(null, $tpl, $setData))->display();
    }

    /**
     * @param $label
     * @return string|null
     * @throws LibraryBlockException
     */
    public static function findPath($label)
    {
        $path = ComponentManager::findPathFor($label, true);
        if ($path === null) {
            throw new LibraryBlockException("Описание шаблона для <b>$label</b> не найден");
        }
        return $path;
    }

    /**
     * Просто получить содержимое файла
     *
     * @param string $link
     * @param string $ext
     * @return string
     * @throws \Exception
     */
    public static function getContent(string $link, string $ext = 'php'): string
    {
        $link = Template::get(true) . $link . '.' . $ext;

        if (file_exists($link)) {
            return file_get_contents($link);
        } else {
            return sprintf(lang('@error_component_file_not_found'), $link);
        }
    }
}