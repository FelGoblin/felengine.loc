<?php


namespace Engine\Core\View;


abstract class Notifications
{
    protected $type;

    /**
     * Notices constructor.
     * @param string $message
     * @param bool $keep
     */
    public function __construct(string $message = '', bool $keep = false)
    {
        if (substr($message, 0, 1) == '@') {
            $message = lang($message);
        }

        switch ($this->type) {
            case 'error':
                Render::setError($message, $keep);
                break;

            case 'warning':
                Render::setWarning($message, $keep);
                break;

            case 'notice':
                Render::setNotice($message, $keep);
                break;

            case 'dialog':
                Render::dialog($message, 'html', $keep); //TODO: переработать!
                break;
        }
    }
}