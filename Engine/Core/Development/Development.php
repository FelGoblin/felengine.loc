<?php

namespace Engine\Core\Development;

use Exception;

/**
 * Class Development
 * Класс-пустышка. Только вызывает иньекции. Инициируется системным компонентом.
 * @package Engine\Core\Development
 */
final class Development
{
    private array $injectControllers = [];

    /**
     * Development constructor.
     * @param $injectControllers
     * @throws Exception
     */
    public function __construct($injectControllers)
    {
        $this->inject($injectControllers);
    }

    /**
     * Добавим иньекцию
     * @param $inject
     * @throws Exception
     */
    public function inject($inject): void
    {
        if (is_array($inject)) {
            $this->injectControllers = array_merge($this->injectControllers, $inject);
        } elseif (is_callable($inject)) {
            $this->injectControllers[] = $inject;
        } else {
            DebugInfo::debugAdd(lang('@unknown_inject_type_controller'));
        }
    }

    /**
     * Контроллер разработчика. Ничего не умеет кроме как вызывать иньекции.
     */
    public function controller(): void
    {
        foreach ($this->injectControllers as $controller) {
            if (is_callable($controller)) { //контроллер - это функция
                $controller();//вызываем контроллер
            }
        }
    }
}
