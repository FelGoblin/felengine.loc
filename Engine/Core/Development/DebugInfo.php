<?php

namespace Engine\Core\Development;

/**
 * Class DebugInfo
 * @package Engine\Core\Development
 */
class DebugInfo
{
    /*
     * DEBUG
     */
    private static $debugs = [];

    /**
     * Добавление информации в отладку
     * @param $debug
     */
    public static function debugAdd($debug): void
    {
        if (is_array($debug)) {
            self::$debugs = array_merge(self::$debugs, $debug);
        } else {
            self::$debugs[] = $debug;
        }
    }

    /**
     * Добавить в дубаф с форматированием входящей строки:
     *  debugAddF('string %s','param1',...,'paramN')
     */
    public static function debugAddF(): void
    {
        $debug = func_get_args();
        $string = array_shift($debug);
        //dump($string);
        $str = @vsprintf($string, $debug);
        self::$debugs[] = $str;
    }

    /**
     * Получить отладочную информацию.
     * @return array
     */
    public static function debugInfo(): array
    {
        return self::$debugs;
    }
}