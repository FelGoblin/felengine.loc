<?php

namespace Engine\Core;

use Engine\Core\Config\SiteConfig;
use Engine\Core\DataBase\DataBase;
use Engine\Core\Development\DebugInfo;
use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\Objects\INamespace;
use Engine\Core\Objects\IUser;
use Engine\Core\Routing\Router;
use Engine\Core\View\ComponentManager;
use Engine\Core\View\Render;
use Engine\Core\View\Template;
use Engine\Modules\BaseAbstractModule;
use Engine\Modules\IBaseModule;
use Engine\Modules\IService;
use Engine\Modules\ISiteModuleServiceInit;
use Engine\Modules\ModData;
use Engine\Modules\ModulesDetour;
use Exception;
use ReflectionClass;
use ReflectionException;


/**
 * Class Core
 * @package Engine
 */
final class Core
{
    /**
     * Имя активного модуля
     * @var null
     */
    public static $activeModule = null;
    /**
     * @var Core
     */
    private static $instance;
    /**
     * @var array
     */
    private static $injectedControllers = [];
    /**
     * Все модули в системе
     * @var array
     */
    private $modules = [];
    /**
     * Все работающие модули
     * @var array
     */
    private $activeModules = [];
    /**
     * Список сервисов
     * @var array
     */
    private $services = [];
    /**
     * Список системных компонентов, который автоподключаются в системе
     * @var array
     */
    private $systemComponets = array();
    /**
     * Соисок добавляемых во время работы системы компонентов.
     * @var array
     */
    private $components = array();

    /**
     * Core constructor.
     */
    public function __construct()
    {
        self::$instance = $this;
    }

    /**
     * Добавить системный компонент. Он будет автозагружаем.
     * @param string $component
     * @param string|null $pathFile
     */
    public static function addSystemComponent(string $component, string $pathFile = null): void
    {
        self::$instance->systemComponets[$component] = pathSeparator($pathFile) ?? $component;
    }

    /**
     * @param BaseAbstractModule $module
     */
    public static function addModule(BaseAbstractModule $module): void
    {
        self::$instance->activeModules[] = $module;
    }

    /**
     * Добавить просто компонент в систему. Загружается вручную.
     * @param string $component
     * @param string|null $pathFile
     */
    public static function addComponent(string $component, string $pathFile = null): void
    {
        self::$instance->components[$component] = pathSeparator($pathFile);
    }

    /**
     * Инъекцция контроллеров в модули
     * @param string $moduleName
     * @param callable $controller
     */
    public static function injectControllers(string $moduleName, callable $controller): void
    {
        self::$injectedControllers[$moduleName][] = $controller;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public static function getSiteLabel(): ?string
    {
        return self::getSiteElement(Event::onGetSiteLabel, 'label');
    }

    /**
     * @param int|string $event
     * @param string $result
     * @return string|null
     * @throws Exception
     */
    private static function getSiteElement($event, string $result): ?string
    {
        $res = null;
        Events::invoke($event, [$result => &$res]);
        return $res;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public static function getSiteCard(): ?string
    {
        return self::getSiteElement(Event::onGetSiteCard, 'card');
    }

    /**
     * Получить ссылку на класс пользователя (например Object\User\User)
     * @return string|null
     * @throws Exception
     */
    public static function getUser(): ?string
    {
        return self::getSiteElement(Event::onGetUser, 'user');
    }

    /**
     * @param IUser $user
     * @throws Exception
     */
    public static function setUser(IUser $user): void
    {
        ### EVENT: ###
        Events::invoke(Event::onGetUser, ['user' => &$user]);
        //### /EVENT ###
    }

    /**
     * Синглтон
     * @return Core
     */
    public static function getInstance(): ?Core
    {
        return self::$instance;
    }

    /**
     * @return array
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * Компонент с указанным именем зарегистрирован.
     * @param string $component
     * @return bool
     */
    public function isComponent(string $component): bool
    {
        return (isset($this->systemComponets[$component]) OR isset($this->components[$component]));
    }

    /**
     * Run site
     * @throws Exception
     */
    public function run()
    {
        try {

            $self = $this;
            //### EVENTS: ON SITE MODULES START INIT AS SERVICE ####
            Events::on(Event::onSiteModulesInit, function ($args) use ($self) {
                $self->initServices($args['config'], $args['mode']);
            });
            //### /END ###

            //обходим модули
            self::detourModules(DETOUR_COMP, $this->modules);

            //dump($this->modules);

            //Выстраиваем список сервисов (внутренний + из модулей)
            //$this->buildServiceList();
            $this->services = self::buildServicesList($this->modules);
            //Запускаем сервисы. Основные (системные) функции сайта
            //dump('system services init');
            $this->InitServices(null, SYSTEM_LOAD);
            //проверяем состояние сайта
            //dump('site status check');
            $user = self::getThisUser();
            if (SiteConfig::get('close')) {
                //Определяем текущего пользователя
                $access = [
                    'required' => 'authorized',
                    'access' => ['Development', 'debug']
                ];
                //TODO: переработать структуру закрытия сайта с возможностью зайти на него разработчикам или администраторам.
                if (!defined('API') AND ($user == null OR !($user instanceof IUser) OR !$user->userAccessCheck($access))) {
                    die(SiteConfig::get('closemessage', 'main', 'site close'));
                }
            }

            //Постинициализация сервисов сайта
            $this->InitServices(null, POST_LOAD);

            //Обрабатываем руты
            $route = Router::route();

            //инициализируем шаблоны, так как только после того как определив неймпсейс в роутах, мы сможем понять какой шаблон выдавать
            Template::initialize(Router::getRoute());
            //
            new ComponentManager();

            //команды разработчика, инициируются до основного контроллера
            $dev = self::getDevModule();
            if ($dev) {
                $dev::init(null, self::$injectedControllers['development'] ?? []);
            }

            //dump(['style' => 'red'], $route);

            if ($route != null) {
                //Обрабатываем руты
                Router::run($route);
            }

            //dump('modules post Service');
            $this->initServices('postInit');

            //после обработок рутов сохраняем пользователя.
            if ($user instanceof IUser) {
                //dump($user);
                $user->saveUser(true);
            }

            if (class_exists('DataBase') && DataBase::query() !== null) {
                DebugInfo::debugAdd(sprintf('%s запросов к БД:', DataBase::query()->getCounter()));
                DebugInfo::debugAdd(DataBase::query()->getQueryHistory());
            }

            // dump($this->activeModules);
            Events::invoke(Event::onCompilationBegine, []);

            //собираем
            ComponentManager::build($this->activeModules);

            //запускаем рендер компонентов
            Render::render(ComponentManager::getBuildComponents());

            //dump(RoutingRepository::stored(), DebugInfo::debugInfo());

            //echo '<br/>core end work...';
        } catch (Exception $exception) {
            if (SiteConfig::get('throwing_error', 'main', false)) {
                dump($exception);
            } else {
                echo $exception->getMessage();
            }
        }
    }

    /**
     * Запускам сервисы
     * @param string|null $config
     * @param int $mode
     */
    public function initServices(string $config = null, int $mode = 0)
    {
        if (empty($this->services)) {
            return;
        }
        //текущий конфиг
        if ($config == null) {
            $config = 'core';
        }
        //dump($config, $this->services['site'][$config]);
        //проходим по спсику сорвисов и инициируем их
        $i = 0;
        foreach ($this->services['site'][$config] ?? [] as $_service) {
            $this->initByConfig($_service, $config, $mode);

            $i++;
        }
    }

    /**
     * Собираем согласно конфигу
     * @param string $_service
     * @param string $config
     */
    private function initByConfig(string $_service, string $config, $mode): void
    {
        switch ($config) {
            case 'siteModuleInit': //инициализация сайт-модуля
                $this->siteModuleInit($_service);
                break;
            case 'siteModulePostInit'://пост инициализация сайт-модуля
                $this->siteModulePostInit($_service);
                break;
            case 'postInit'://пост инициализация модуля (после обработки рутов)
                $this->servicePostInit($_service);
                break;
            case 'service':
            case 'core':
                $this->serviceCoreInit($_service, $config, $mode);
                break;
        }
    }

    /**
     * @param string $_service
     */
    private function siteModuleInit(string $_service): void
    {
        if (class_exists($_service)) {
            /** @var ISiteModuleServiceInit $_service */
            $_service::doSiteModuleEventInit();
        }
    }

    /**
     * @param string $_service
     */
    private function siteModulePostInit(string $_service): void
    {
        if (class_exists($_service)) {
            /** @var ISiteModuleServiceInit $_service */
            $_service::doSiteModuleEventPostInit();
        }
    }

    /**
     * @param string $_service
     */
    private function servicePostInit(string $_service): void
    {
        /** @var INamespace $class */
        $class = sprintf('Namespaces\\%s\\NmspcConfig', Template::getNmspc());
        $mode = INamespace::MODE_NORMAL;
        if (class_exists($class)) {
            $mode = $class::getMode();
        }
        if (class_exists($_service)) {
            /** @var IBaseModule $_service */
            $_service::postControllerRun($mode);
        }
    }

    /**
     * @param string $_service
     * @param $config
     */
    private function serviceCoreInit(string $_service, $config, $mode): void
    {
        /** @var IService $_service */
        if ($_service::$mode == $mode OR $_service::$mode == ALWAYS_LOAD) {
            //dump($_service);
            $service = getProvider($_service);
            //Создаём алиасы (если необходимы)
            $service->classAlias();
            //Инициируем сервис
            //dump($_service);
            $service->init($config, $mode);
        }
    }

    /**
     * Обходчик модулей
     * @param string $target
     * @param array $out
     */
    public static function detourModules(string $target, array &$out): void
    {
        $fn = function ($modData) use (&$out) {
            $_modData = new ModData($modData);
            $apiClass = $_modData->getClass();
            if (class_exists($apiClass)) {
                $reflectedClass = new ReflectionClass($apiClass);
                //есть инициализация событий (вернее факт наличия. а что там с методе - хз)
                if ($reflectedClass->implementsInterface('Engine\API\IAPIModule') AND $reflectedClass->hasMethod('modEventsInit')) {
                    $apiClass::modEventsInit();
                }
            }
            $out[] = $_modData;
        };
        //обходчик модулей
        try {
            ModulesDetour::detour($target, $fn);
        } catch (ReflectionException $rex) {
            $out = [];
            DebugInfo::debugAddF('<error>Detour <%s> ERROR: []<b>%s</b></error>', $target, $rex->getMessage());
        }
    }

    /**
     * @param array $modList
     * @return array
     */
    public static function buildServicesList(array $modList): array
    {
        $result = [];
        //обходим все модули
        foreach ($modList as $modData) {
            //ищем файл с конфигом сервисов
            $file = realpath($modData->getModDir() . '/services.php');
            if ($file) {
                //dump(['style' => 'yellow'], "require $file;");
                $serv = require $file;
                $result = array_merge_recursive($result, $serv);
            }
        }
        //смотрим в папке с конфигом
        $servicePath = CONFIG_DIR . 'Service' . DIRECTORY_SEPARATOR . 'services.php';
        if (file_exists($servicePath)) {
            $result = array_merge_recursive($result, require($servicePath));
        }
        //еще глянем у виджетов
        $servicePath = ROOT_DIR . 'Widgets' . DIRECTORY_SEPARATOR . 'services.php';
        if (file_exists($servicePath)) {
            $result = array_merge_recursive($result, require($servicePath));
         //   dump($result);
        }

        return $result;
    }

    /**
     * Получить пользователя
     * @return IUser|null
     * @throws Exception
     */
    public static function getThisUser(): ?IUser
    {
        $user = null;
        /**
         * ### EVENT: получаем текущего пользователя системы ###
         * Это по сути система хуков модулей.
         */
        Events::invoke(Event::onGetThisUser, ['user' => &$user]);
        //### /EVENT ###
        return $user;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    private static function getDevModule(): ?string
    {
        return self::getSiteElement(Event::onGetDevModule, 'dev');
    }
}
