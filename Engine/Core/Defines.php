<?php

//bool
const CAST_B = 'bool';
const CAST_BOOL = 'bool';
//integer
const CAST_I = 'integer';
const CAST_INT = 'integer';
const CAST_INTEGER = 'integer';
const CAST_NUMERIC = 'integer';
//float
const CAST_FLOAT = 'float';
const CAST_F = 'float';
const CAST_D = 'float';
//string
const CAST_STR = 'string';
const CAST_STRING = 'string';
const CAST_NULLSTR = '?string';
//array
const CAST_A = 'array';
const CAST_ARRAY = 'array';

const DETOUR_API = 'detour_api';
const DETOUR_CRON = 'detour_cron';
const DETOUR_COMP = 'detour_components';

//Folders
define('ROOT_DIR', $dirname); //корень сайта
define('TPL_DIR', ROOT_DIR . 'Templates' . DIRECTORY_SEPARATOR);//путь к Шаблонам
define('SRC_DIR', ROOT_DIR . 'Src' . DIRECTORY_SEPARATOR);//путь к классам
define('MOD_DIR', ROOT_DIR . 'Modules' . DIRECTORY_SEPARATOR);//путь к классам
//TODO: убрать на сервере
if (isset($dev) AND $dev) {
    define('ENGINE_DIR', 'F:\WebServer\OSPanel\domains\felengine.loc\public_html\Engine');
} else {
    define('ENGINE_DIR', ROOT_DIR . 'Engine' . DIRECTORY_SEPARATOR);//
}
define('CONFIG_DIR', ROOT_DIR . 'Config' . DIRECTORY_SEPARATOR); //Путь к папке с конфигом
define('COMP_DIR', CONFIG_DIR . 'StreamerConsole' . DIRECTORY_SEPARATOR); //Путь к папке с конфигом
define('NMSPS_DIR', ROOT_DIR . 'Namespaces' . DIRECTORY_SEPARATOR);//путь к неймспейсам
define('RESOURCES_DIR', ROOT_DIR . 'Resources' . DIRECTORY_SEPARATOR);//путь к Ресурсам
define('UPLOAD_DIR', RESOURCES_DIR . 'upload' . DIRECTORY_SEPARATOR);//путь к Upload
define('SHARE_DIR', '/Resources/');//путь к Upload

/**
 * Системная загрузка
 */
const SYSTEM_LOAD = 0;
/**
 * Пост загрузка.
 */
const POST_LOAD = 1;
/**
 * Загрузка при любом обращении
 */
const ALWAYS_LOAD = 2;