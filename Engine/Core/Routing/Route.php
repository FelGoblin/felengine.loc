<?php

namespace Engine\Core\Routing;

use Closure;

final class Route
{

    /**
     * @var string The nameSpace we're setting routes for.
     */
    public static $controllerNameSpace;
    public static $redirect;
    /**
     * @var string Route prefix.
     */
    private static $prefix = '';
    private static $nameSpace;


    /**
     * Регистрируем рабочее место и его роуты
     *
     * @param string $nameSpace
     * @param string $dir
     * @param string $source
     * @param string $routeSearch
     */
    public static function registerWorkSpace(string $nameSpace, string $dir, string $source, string $routeSearch = 'routes')
    {
        $path = pathSeparator($dir . $nameSpace . '/Controller/' . $routeSearch . '.php');
        if (is_file($path)) {
            //dump($path);
            self::$controllerNameSpace = $source . '\\' . $nameSpace . '\\Controller';
            if ($source == 'Namespaces') {
                self::$nameSpace = $nameSpace;
            } else {
                self::$nameSpace = null;
            }
            require_once $path;
        }
    }

    /**
     * @param string $uri
     * @param array $options
     * @param bool $mainIndex
     * @return bool
     */
    public static function both(string $uri, array $options, bool $mainIndex = false): bool
    {
        //dump($uri, $options, $mainIndex);
        return static::add('post', $uri, $options, $mainIndex) AND static::add('get', $uri, $options, $mainIndex);
    }

    /**
     * Sets a route.
     *
     * @param string $method The route method,
     * @param string $uri The URI to route.
     * @param array $options The route options.
     * @param bool $mainIndex
     * @return bool
     */
    public static function add(string $method, string $uri, array $options, bool $mainIndex = false): bool
    {
        if (static::validateOptions($options)) {
            // Set nameSpace.
            if (!isset($options['nameSpace'])) {
                $options['nameSpace'] = static::$nameSpace;
            }

            $options['controller'] = sprintf('%s\\%s', static::$controllerNameSpace, $options['controller']);

            if ($mainIndex AND empty(RoutingRepository::$mainIndex)) {
                RoutingRepository::$mainIndex = $options;
            }

            // Set route.
            //dump($method, static::prefixed($uri), $options);
            RoutingRepository::store($method, static::prefixed($uri), $options);

            return true;
        }
        return false;
    }

    /**
     * Validates the route options.
     *
     * @param array $options Route options to validate.
     * @return bool
     */
    private static function validateOptions(array $options): bool
    {
        return array_key_exists('controller', $options) AND array_key_exists('action', $options);
    }

    /**
     * Preprends the prefix to the URI.
     *
     * @param string $uri The URI to prefix.
     * @return string
     */
    private static function prefixed(string $uri): string
    {
        // Normalize the URI.
        $uri = trim($uri, '/');

        // Prepend prefix if its set.
        if (static::$prefix !== '') {

            $uri = trim(static::$prefix, '/') . '/' . $uri;
        }

        return $uri;
    }

    /**
     * Sets a POST route.
     *
     * @param string $uri The URI to route.
     * @param array $options The route options.
     * @param bool $mainIndex
     * @return bool
     */
    public static function post(string $uri, array $options, bool $mainIndex = false): bool
    {
        return static::add('post', strtolower($uri), $options, $mainIndex);
    }

    /**
     * Sets a GET route.
     *
     * @param string $uri The URI to route.
     * @param array $options The route options.
     * @param bool $mainIndex
     * @return bool
     */
    public static function get(?string $uri, array $options = [], bool $mainIndex = false): bool
    {
        //dump('get', $uri, $options, $mainIndex);
        return static::add('get', strtolower($uri), $options, $mainIndex);
    }

    /**
     * Creates a group of routes with a particular prefix.
     *
     * @param string $prefix The route prefix.
     * @param Closure $routes The routes callback.
     * @return void
     */
    public static function group(string $prefix, Closure $routes)
    {
        // Define the prefix.
        static::$prefix = $prefix;

        // Set the routes.
        $routes();

        // Empty the prefix.
        static::$prefix = '';
    }
}
