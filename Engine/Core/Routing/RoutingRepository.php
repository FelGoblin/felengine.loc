<?php

namespace Engine\Core\Routing;

use Engine\Core\Development\DebugInfo;

class RoutingRepository
{

    /**
     * @var array Майн рут, на который будем перекидывать в непоянтных случаях, например ошибка или проблема с доступом
     */
    public static $mainIndex = null;
    /**
     * @var array Stored routes.
     */
    private static $stored = [
        'get' => [],
        'post' => []
    ];

    /**
     * Retrieve the stored routes.
     *
     * @return array
     */
    public static function stored(): array
    {
        return static::$stored;
    }

    public static function count(): int
    {
        return count(static::$stored['get']) + count(static::$stored['post']);
    }

    /**
     * Stores a new route.
     *
     * @param  string $method The route request method.
     * @param  string $uri The route URI.
     * @param  array $options The route options.
     * @return void
     */
    public static function store(string $method, string $uri, array $options, bool $mainIndex = false)
    {
        if (!in_array($uri, static::$stored[$method])) {
            static::$stored[$method][$uri] = $options;
            DebugInfo::debugAddF(lang('@route_added'), $method, $uri, $options['controller'], $options['action']);
        } else {
            DebugInfo::debugAddF(lang('@route_duplicate_warning'), $uri ?? '/', $options['controller']);
        }
    }

    /**
     * Retrieve a stored route.
     *
     * @param  string $method The route method.
     * @param  string $uri The route URI.
     * @return array
     */
    public static function retrieve(string $method, string $uri): array
    {
        $uri = trim($uri);
        if (strpos($uri, '?')) {
            $uri = str_replace('/?' . $_SERVER['QUERY_STRING'], '', $uri);
        }

        //dump($uri, static::$stored[$method][$uri], static::$stored[$method]);

        return static::$stored[$method][$uri] ?? [];
    }

    /**
     * Поиск 404
     * @param string $method
     * @param string $uri
     * @return array
     */
    public static function find404(string $method, string $uri): array
    {
        $stored = static::$stored[$method] ?? [];
        //dump($stored);
        foreach ($stored as $nmspc => $routes) {
            if (empty($routes['404'])) {
                continue;
            }
            $nmspc = strtolower($nmspc);
            $segments = explode('/', $uri);
            //dump($segments);
            if (in_array($nmspc, $segments) AND $nmspc == strtolower($routes['404']['nameSpace'])) {
                //dump($routes, $routes['404']);
                $_404 = $routes['404'];
                unset($routes['404']);
                $a = array_merge($routes, [
                    //'controller' => $routes['controller'] . ($routes['controller']) . (substr("testers", -1) !== '\\' ? '\\' : '') . $_404['controller'],
                    'controller' => sprintf('Namespaces\\%s\\Controller\\%s', $routes['nameSpace'], $_404['controller']),
                    'action' => ($_404['action'] ?? $routes['action']),
                    'nameSpace' => ($_404['nameSpace'] ?? $routes['nameSpace'])
                ]);
                //dump($a);
                return $a;
            }
            //dump($method, $uri, $u, $routes);
        }
        return [];
    }

    /**
     * Remove a stored route.
     *
     * @param  string $method The route method.
     * @param  string $uri The route URI.
     * @return bool
     */
    public static function remove(string $method, string $uri): bool
    {
        if (isset(static::$stored[$method][$uri])) {
            unset(static::$stored[$method][$uri]);
            return true;
        }

        return false;
    }
}
