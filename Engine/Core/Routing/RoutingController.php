<?php

namespace Engine\Core\Routing;


use Engine\Controller;
use Engine\Core\Objects\FillClass;
use Engine\Core\View\Render;
use Exception;

class RoutingController extends FillClass
{
    /**
     * @var string Параметры, которые ушли в триггеры
     */
    public $parameters = array();
    /**
     * @var Controller Объекта класса Controller, который выполняется.
     */
    protected $instance;
    /**
     * @var mixed То что вернул нам Триггер (return)
     */
    protected $response;
    /**
     * @var string Активная рабочая область.
     */
    protected $controller = '';
    /**
     * Область действия этого контроллера (неймспейс сайта в папке Namespace)
     * @var string
     */
    protected $scope = '';
    /**
     * @var string Имя вызываемого триггераr.
     */
    protected $trigger = '';
    /**
     * @var string Метод вызываемого триггера.
     */
    protected $action = '';

    public static function Init($route): RoutingController
    {
        return new RoutingController($route);
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        $_class = ''; //в эту переменную мы получим имя класса из контроллера
        $class = $this->findController();
        //dump($this->controller);
        //не нашли класс для контроллера
        if ($class === null) {
            throw new Exception(sprintf(
                'Controller <b>%s</b> does not exist.'
                , $this->controller));
        }
        // Instantiate class.
        $this->instance = $class;
        //Получаем ответ от контроллера, но только если есть необходимые методы
        if (method_exists($this->instance, $this->action)) {
            $this->response = call_user_func_array([$this->instance, $this->action], (array)$this->parameters);
        } else {
            Render::setWarning(sprintf('Не найден метод <b>%s</b> в <b>%s</b>', $this->action, $_class));
        }
    }

    /**
     * Возвращаем триггер, если такой имеется.
     *
     * @param string $class
     * @return null|Controller
     */
    private function findController(): ?Controller
    {
        return class_exists($this->controller) ? new $this->controller : null;
    }

    //############################################
    //################## GETTERS #################
    //############################################
    /**
     * @return mixed
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }
}