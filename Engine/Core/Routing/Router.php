<?php

namespace Engine\Core\Routing;

use Engine\Core\Core;
use Engine\Core\Development\DebugInfo;
use Engine\Core\Http\Request;
use Engine\Core\Http\Uri;
use Engine\Core\Objects\CoreElement;
use Engine\Core\View\Notice\ErrorMessage;
use Engine\Core\View\Render;

final class Router extends CoreElement
{
    /**
     * @var Router
     */
    static $instance;
    static protected $controller;
    /**
     * @var array
     */
    private $route = [];

    /**
     * Router constructor. INIT nameSpaces
     */
    public function __construct()
    {
        Route::both('/index.php',
            [
                'controller' => 'HomeController',
                'action' => 'main',
                'nameSpace' => 'Site'
            ]);

        $this->workSpace(NMSPS_DIR, 'Namespaces');

        if (file_exists(MOD_DIR)) {
            $this->workSpace(MOD_DIR, 'Modules');
        }

        DebugInfo::debugAddF(lang('@route_add_count_routes'), RoutingRepository::count());
        //dump(RoutingRepository::stored());
    }

    /**
     * @param string $dir
     * @param string $source
     */
    private function workSpace(string $dir, string $source): void
    {
        if (!is_dir($dir)) {
            return;
        }
        $find = array_diff(scandir($dir), Array(".", ".."));
        foreach ($find as $wSpace) {
            //Регистрируем рабочую область
            //dump(['footer' => 'Регистрируем рабочую область'], $wSpace, $dir);
            Route::registerWorkSpace($wSpace, $dir, $source, '_routes');
            Route::registerWorkSpace($wSpace, $dir, $source);
        }
    }

    public static function getInstance(): Router
    {
        if (self::$instance == null)
            self::initialize();

        return self::$instance;
    }

    /**
     * Синглтон
     */
    public static function initialize()
    {
        self::$instance = new Router();
    }

    /**
     * @throws \Exception
     */
    public static function route()
    {
        self::rewrite();
        //vars
        $method = Request::method();
        $segments = strtolower(Uri::segmentString());
        //dump($method, $segments);
        $controller = RoutingRepository::retrieve($method, $segments);
        //dump(['style' => 'red'], $controller);
        if (empty($controller)) { //не нашли путь, поищем переопределённый 404
            $controller = RoutingRepository::find404($method, $segments);
            //dump($controller);
            if (($controller['silence'] ?? false) === false) {
                new ErrorMessage(sprintf('Ошибка 404.<br />Страница <b>%s</b> не найдена', Uri::getFullPathUrl()));
            }
        }
        //custom 404
        if (empty($controller)) { //нет ничего не нашли, ни рут ни 404

            if (!is_dir(MOD_DIR) && !is_dir(NMSPS_DIR)) {
                return self::$instance->route = null;
            }
            /*
            DebugInfo::debugAddF('<error>Контроллер</error>');
            return self::$instance->route = null;
            */
            //создадим свой рут на 404
            $controller = [
                'controller' => 'Namespaces\SIte\Controller\ErrorController', //TODO: возможно стоит сделать что-то
                'action' => 'page404',
                'silence' => false,
                'nameSpace' => 'Site',
                'permission' => ['authorized'] //даже у ошибки 404 должен быть доступ для авторизованного, иначе на окно авторизации
            ];
        }

        if (isset($controller['permission'])) {
            $user = Core::getThisUser();
            if ($user != null AND !$user->userAccessCheck($controller['permission'])) {
                if (!($controller['silence'] ?? false)) {
                    Render::setError(sprintf('Доступ к <b>%s</b> запрещён.', Uri::segmentString()));
                }
                if (!empty($controller['deny']) AND $user != null AND $user->isAuthorized()) {
                    $controller = array_merge($controller, [
                        'action' => $controller['deny']
                    ]);
                } else {
                    $controller = RoutingRepository::$mainIndex ?? null;
                }
            }
        }
        self::$instance->route = $controller;

        //dump('Route init, Fined controller: ', $controller);

        return self::$instance->route;
    }

    /**
     * парсим и перезаписываем URI в случае удачи
     */
    private static function rewrite(): void
    {
        $rewriteCount = 0;
        foreach (RoutingRepository::stored() as $method => $routes) {
            //проходим по роутам
            foreach ($routes as $uri => $options) {
                $segments = explode('/', $uri);
                $rewrite = false;
                //идём по сегментам uri
                $trueWay = false;
                foreach ($segments as $key => $segment) {
                    if ($segment != Uri::segment($key + 1, true) AND !$trueWay) {
                        continue;
                    }
                    $trueWay = true;
                    $matches = [];

                    // Get route URI segments we need to rewrite.
                    preg_match('/\(([0-9a-z]+)\:([a-z]+)\)/i', $segment, $matches);

                    // Do we have matches?
                    if (!empty($matches)) {

                        // Get the real value for this segment and validate it
                        // against the rule.
                        $value = Uri::segment($key + 1);
                        $rule = $matches[2];
                        $valid = false;

                        //dump($key, $segment,$matches,$value, $rule);

                        // Validate the rule.
                        if ($rule === 'numeric' AND is_numeric($value)) {
                            $value = (int)$value;
                            //dump($value);
                            $valid = true;
                        } else if ($rule === 'any') {
                            $valid = true;
                        }

                        // If the segment is valid, assign the value.
                        if ($valid === true) {

                            //dump($segments[$key],$value);

                            $segments[$key] = $value;

                            // Add the parameters.
                            if (!isset($options['parameters'])) {
                                $options['parameters'] = [$value];
                            } else {
                                array_push($options['parameters'], $value);
                            }
                            //dump($options['parameters']);
                        }

                        // We will need to rewrite this URL.
                        $rewrite = true;
                    }
                }
                // Do we need to rewrite the URI value.
                if ($rewrite) {
                    $rewriteCount++;
                    // Remove the old URI.
                    RoutingRepository::remove($method, $uri);

                    // Add the new one.
                    RoutingRepository::store($method, strtolower(implode('/', $segments)), $options);
                }
            }
        }
        DebugInfo::debugAddF('<info>Rewrite %d rules</info>', $rewriteCount);
    }

    /**
     * Запускаем контроллеры роутера
     * @param $route
     * @throws \Exception
     */
    public static function run($route): void
    {
        RoutingController::Init($route)->run();
    }

    /**
     * @return array
     */
    public static function getRoute(): ?array
    {
        return self::$instance->route;
    }
}