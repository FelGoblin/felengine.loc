<?php
namespace Engine\Core\Http;

use Engine\Core\Core;
use Exception;
use Object\User\User;

/**
 * Class Redirect
 * @package Flexi\Http
 */
class Redirect
{
    /**
     * @param string $url
     * @param bool $permanent
     * @throws Exception
     */
    public static function go(string $url, $permanent = false)
    {
        $user = Core::getThisUser();

        if ($user != null){
            $user->saveUser(true);
        }

        if ($permanent) {
            header('HTTP/1.1 301 Moved Permanently');
        }

        dump($url);

        header('Location: ' . $url);
        exit();
    }

    private static function errorHTML(int $errorCode, string $error): string
    {
        return <<<HTML
            <html>
                <Head>
                    <title>$errorCode</title>
                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
                </Head>
                <Body>
                    <h1>$error</h1>
                </Body>
            </html>
HTML;

    }

    /**
     * @param int $error_code
     * @param string $error
     * @param bool $asHTML
     */
    public static function  err(int $error_code, string $error, bool $asHTML = false)
    {
        http_response_code($error_code);
        $error = $asHTML ? self::errorHTML($error_code, $error) : $error;
        echo($error);
        die();
    }
}
