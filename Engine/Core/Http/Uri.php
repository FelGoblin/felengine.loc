<?php

namespace Engine\Core\Http;

use Engine\Core\Objects\CoreElement;
use Engine\Helper\UriHelper;

/**
 * Class UriService
 * @package Flexi\Http
 */
class Uri extends CoreElement
{
    /**
     * @var string The getBase URL.
     */
    protected static string $base = '';

    /**
     * @var string The active URI.
     */
    protected static string $uri = '';

    /**
     * @var array URI segments.
     */
    protected static array $segments = [];

    protected static string $pathUrl = '';

    protected static string $fullPathUrl = '';

    /**
     * Initialize the URI class.
     *
     * @return void
     */
    public static function initialize()
    {
        // We need to get the different sections from the URI to process the
        // correct route.

        // Standard request in the browser?
        if (isset($_SERVER['REQUEST_URI'])) {
            // Get the active URI.
            $request = $_SERVER['REQUEST_URI'];
            $host = $_SERVER['HTTP_HOST'];
            $protocol = UriHelper::getProtocol();//'http' . (Request::https() ? 's' : '');
            $base = $protocol . '://' . $host;
            $uri = $base . $request;

            // Build the URI segments.
            //$length     = strlen($getBase);
            $str = (string)UriHelper::getPathUrl($request);//substr($uri, $length);
            $arr = (array)explode('/', ltrim($str, '/'));
            $segments = [];

            foreach ($arr as $segment) {
                if ($segment !== '') {
                    array_push($segments, $segment);
                }
            }

            // Assign properties.
            self::$pathUrl = $str;
            self::$fullPathUrl = $base . $str;
            static::$base = $base;
            static::$uri = $uri;
            static::$segments = $segments;

        } else if (isset($_SERVER['argv'])) {
            $segments = [];
            foreach ($_SERVER['argv'] as $arg) {
                if ($arg !== $_SERVER['SCRIPT_NAME']) {
                    array_push($segments, $arg);
                }
            }

            static::$segments = $segments;
        }
    }

    /**
     * @return string
     */
    public static function getPathUrl(): string
    {
        return self::$pathUrl;
    }

    /**
     * @return string
     */
    public static function getFullPathUrl(): string
    {
        return self::$fullPathUrl;
    }

    /**
     * Get the current URI.
     *
     * @return string
     */
    public static function uri(): string
    {
        return static::$uri;
    }

    /**
     * Get the URI segments.
     *
     * @return array
     */
    public static function segments(): array
    {
        return static::$segments;
    }

    /**
     * Gets a segment from the URI.
     *
     * @param  int $num The segment number.
     * @param bool $asLower
     * @return string
     */
    public static function segment(int $num, bool $asLower = false): string
    {
        // Normalize the number.
        $num = $num - 1;

        // Attempt to find the segment.
        if (isset(static::$segments[$num])) {
            if ($asLower) {
                return strtolower(static::$segments[$num]);
            }
            return static::$segments[$num];
        } else {
            return '';
        }
    }

    /**
     * Get the URI segments as a string.
     *
     * @return string
     */
    public static function segmentString(): string
    {
        return (string)implode('/', static::$segments);
    }

    /**
     * Returns a built site URL.
     *
     * @param  string $uri The URI to append onto the getBase.
     * @return string
     */
    public function url(string $uri = ''): string
    {
        return static::getBase() . ltrim($uri, '/');
    }

    /**
     * Get the getBase URI.
     *
     * @return string
     */
    public static function getBase(): string
    {
        return static::$base;
    }
}
