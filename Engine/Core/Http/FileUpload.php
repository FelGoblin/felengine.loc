<?php

namespace Engine\Core\Http;

use Exception;
use Engine\Core\View\Render;
use Engine\Exceptions\SaveException;

class FileUpload
{
    /**
     * @var array
     */
    private array $file;

    private string $error = '';

    private $uploadedFile;

    private $localPath;

    private bool $isUploaded = false;

    public function getId(): string
    {
        return $this->file['fileID'];
    }

    /**
     * @return array
     */
    public function getFile(): array
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }

    /**
     * @return mixed
     */
    public function getLocalPath()
    {
        return $this->localPath;
    }

    /**
     * @return bool
     */
    public function isUploaded(): bool
    {
        return $this->isUploaded;
    }

    public function __construct(array $file)
    {
        $this->file = $file;

        try {
            $this->isUploaded = $this->isFileUploaded();
            //проверяем на ошибки во время загрузки
            $this->IsError();
            $this->IsAllowFile();
            $this->moveUploadFile();

        } catch (Exception $e) {
            $this->isUploaded = false;
            $this->error = $e->getMessage();
            try { //удалим загруженный файл.
                if (file_exists($this->file['tmp_name'])) {
                    unlink($this->file['tmp_name']);
                }
            } catch (Exception $ex) {
                //TODO: добавим в лог
            }
        }
    }

    private function isFileUploaded()
    {
        return !(
            count($this->file) == 0
            or empty($this->file['name'])
            or empty($this->file['type'])
            or empty($this->file['tmp_name'])
            or empty($this->file['size'])
            or ($this->file['error'] > 0)
        );
    }

    /**
     * @return null|string
     * @throws Exception
     */
    private function IsError()
    {
        switch ($this->file['error']) {
            case 1:
                throw new Exception('Размер принятого файла превысил максимально допустимый размер, который задан директивой upload_max_filesize конфигурационного файла php.ini.');
                break;

            case 2:
                throw new Exception('Размер загружаемого файла превысил значение MAX_FILE_SIZE, указанное в HTML-форме');
                break;

            case 3:
                throw new Exception('Загружаемый файл был получен только частично.');
                break;

            case 4:
                throw new Exception('Файл не был загружен');
                break;

            case 6:
                throw new Exception('Отсутствует временная папка.');
                break;

            case 7:
                throw new Exception('Не удалось записать файл на диск.');
                break;

            case 8:
                throw new Exception('Загрузка была прервана.');
                break;

            default:
                return null;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function IsAllowFile(): bool
    {
        $type = $this->file['type'];
        //dump($type);
        if (
            $type != 'application/x-zip-compressed' and
            $type != 'application/zip' and
            $type != 'application/json' and
            $type != 'image/jpeg' and
            $type != 'image/gif' and
            $type != 'image/png' and
            $type != 'image/x-icon' and
            $type != 'image/bmp' and //Да кто уже пользуется BMP????
            $type != 'application/octet-stream'
        ) {
            Render::setError("Загружаемый файл ($type) не является допустимым типом");
            throw new Exception("Загружаемый файл ($type) не является допустимым типом");
        }
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function moveUploadFile(): bool
    {
        try {
            $upload = 'upload/';
            if (!empty($this->file['uploadDir'])) {
                $upload = $this->file['uploadDir'] . '/';
            }
            $target = RESOURCES_DIR . $upload;

            if (!is_dir($target) AND !mkdir($target, 0777, true)) {
                throw new Exception('Не могу создать директорию.');
            }

            if (!is_writable($target)) {
                throw new Exception('Папка сохранения не доступна для записи.');
            }

            $this->uploadedFile = FileControl::GenerateFName($this->file['name'], $this->file['randomName']);

            $this->localPath = SHARE_DIR . $upload . $this->uploadedFile;

            $this->uploadedFile = pathSeparator($target . $this->uploadedFile);

            //dump($this->file['tmp_name'], $this->uploadedFile);

            if (!move_uploaded_file($this->file['tmp_name'], $this->uploadedFile)) {
                throw new Exception('Ошибка записи файла на диск.');
            }
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param $newDir
     * @throws SaveException
     */
    public function remove($newDir): void
    {
        $new = FileControl::moveFile($this->getUploadedFile(), RESOURCES_DIR . $newDir, false);
        if ($new == null)
            throw new SaveException('Error remove file');
        $this->uploadedFile = $new;
        $path_parts = pathinfo($new);
        $this->localPath = SHARE_DIR . $newDir . $path_parts['basename'];
    }
}
