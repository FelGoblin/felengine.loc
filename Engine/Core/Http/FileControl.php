<?php

namespace Engine\Core\Http;

//TODO: Добавить мультизагрузку файлов
use Engine\Core\Config\SiteConfig;
use Engine\Exceptions\SaveException;

class FileControl
{
    private $uploadFiles = [];

    public function __construct(array $files = null)
    {
        $uploaded = [];
        foreach ($files ?? [] as $id => $file) {
            if (is_array($file['name'])) { //есть перечень файлов
                $uploaded = array_merge($uploaded, array_normalize($file)); //приводим к нормальному описанию загруженного файла
            } else { //создаём такое описание
                $uploaded[] = $file;
            }
        }

        $fileID = $this->execute('fileID');
        $uploadDir = $this->execute('uploadDir');
        $randomName = $this->execute('randomName');

        foreach ($uploaded as $i => $upload) {
            $upload['fileID'] = $fileID[$i] ?? '';
            $upload['uploadDir'] = $uploadDir[$i] ?? '';
            $upload['randomName'] = isset($randomName[$i]) ? toBool($randomName[$i]) : SiteConfig::get('upload_random_name', 'streamer-console');
            $this->uploadFiles[] = new FileUpload($upload);
        }
        //dump(['private' => true], $this->uploadFiles);
    }

    /**
     * Прлучить загруженный файл по ID или по номеру в массиве
     * @param $id
     * @return array|null
     */
    public function getUploadFile($id = 0): ?FileUpload
    {
        if ($this->uploadFiles == null OR count($this->uploadFiles) == 0) {
            return null;
        }
        if (is_numeric($id)) {
            return $this->uploadFiles[$id] ?? null;
        } elseif (is_string($id)) {
            foreach ($this->uploadFiles as $uploadFile) {
                if ($uploadFile->getId() == $id) {
                    return $uploadFile;
                }
            }
        }
        return null;
    }

    /**
     * @param $field
     * @return array
     */
    private function execute($field): array
    {
        $param = Input::getPost($field) ?? [];
        if (!is_array($param)) {
            $param = [$param];
        }
        return $param;
    }

    /****************************************************************************************************************/
    /**
     * @param $file
     * @param $newDir
     * @param null $rand
     * @return null|string
     * @throws SaveException
     */
    public static function moveFile($file, $newDir, $rand = null): ?string
    {
        $newName = self::GenerateFName($file, $rand ?? SiteConfig::get('upload_random_name', 'streamer-console') ?? false);
        $newFile = $newDir . $newName;

        if (!is_dir($newDir) AND !mkdir($newDir)) {
            throw new SaveException('Can`t create new folder');
        }

       // var_dump($file, $newFile);

        if (file_exists($file) AND rename($file, $newFile))
            return $newName;
        else
            return null;
    }

    /**
     * @param bool $rand
     * @return string
     */
    public static function GenerateFName(string $file, $rand = false)
    {
        $path_parts = pathinfo($file);

        if ($rand)
            $name = self::GenerateRandFName() . '.' . ($path_parts['extension'] ?? 'tmp');
        else
            $name = $path_parts['basename'];

        return $name;
    }

    /**
     * @return string
     */
    private static function GenerateRandFName()
    {
        return md5(uniqid(rand()));
    }
}