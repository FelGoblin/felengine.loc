<?php

namespace Engine\Core\Http;

use Engine\Core\Objects\CoreElement;
use Exception;

/**
 * Class Input
 * @package Flexi\Http
 */
class Input extends CoreElement
{
    private static array $post = [];
    private static array $unsafePost = [];
    private static array $get = [];
    private static array $request = [];
    private static array $json = [];
    private static array $files = [];
    private static ?FileControl $fileControl = null;
    private static array $all = [];

    /**
     * Initialize the Input class.
     *
     * @return void
     */
    public static function initialize()
    {

        self::$json = [];
        self::$post = self::EscapeArray($_POST);
        self::$get = self::EscapeArray($_GET);
        self::$request = self::EscapeArray($_REQUEST);
        self::$unsafePost = $_POST;
        self::$files = $_FILES;
        $json = file_get_contents('php://input');

        if (!empty(self::$json))
            self::$json = json_decode($json, true) ?? [];
        else
            self::$json = [];

        self::$fileControl = new FileControl(self::$files);

        //dump(self::$get, self::$post, self::$request, self::$json, self::$files);

        self::$all = array_merge(self::$get, self::$post, self::$request, self::$json, self::$files);

        unset($_GET, $_POST, $_REQUEST, $_FILES); //немного паранои.
    }

    /**
     * @param $val
     * @return array|string
     */
    private static function EscapeArray($val)
    {
        if (empty($val) or $val == null)
            return [];
        if (is_string($val)) {
            return htmlspecialchars($val);
        }

        if (is_array($val)) {
            return array_map(static::class . '::EscapeArray', $val);
        }
        return $val;
    }

    /**
     * Удалить параметры
     * @param string | array $key
     * @param string $type
     */
    public static function clear($key, $type = 'all'): void
    {
        if (is_array($key)) {
            foreach ($key as $keyID) {
                self::clear($keyID, $type);
            }
        } elseif (is_string($key)) {
            if ($type == 'all') {
                unset(self::${$type}[$key]);
                self::clear($key, 'get');
                self::clear($key, 'post');
                self::clear($key, 'request');
            } else {
                unset(self::${$type}[$key]);
            }
        }
    }

    public static function getByList(array $list, string $format = 'default')
    {
        $result = [];
        foreach ($list as $get) {
            //dump($get, self::get($get, $format));
            $result[$get] = self::get($get, $format);
        }
        //dump($result);
        return $result;
    }

    /**
     * @param null $key
     * @param string $format
     * @return mixed|null
     */
    public static function get($key = null, string $format = 'default')
    {
        $param = self::getParam('all', $key);
        return cast($param, $format);
    }

    /**
     * @param string $type
     * @param null $key
     * @param null $default
     * @return mixed|null
     */
    public static function getParam($type = 'all', $key = null, $default = null)
    {
        if ($key == null)
            return self::${$type};
        $result = self::$$type[$key] ?? null;
        if (is_null($result) and !is_null($default)) {
            return $default;
        }
        return $result;
    }

    /**
     * @param null $key
     * @param string|null $cast
     * @param null $default
     * @return array|string
     */
    public static function getPost($key = null, string $cast = null, $default = null)
    {
        $param = self::getParam('post', $key, $default);
        if ($cast != null) {
            return cast($param, $cast);
        }
        return $param;
    }

    /**
     * @param null $key
     * @param null $default
     * @return array array | string
     */
    public static function getRaw($key = null, $default = null)
    {
        $param = self::getParam('unsafePost', $key, $default);
        return $param;
    }

    /**
     * @param null $key
     * @param string|null $cast
     * @param null $default
     * @return array | string
     */
    public static function getGet($key = null, string $cast = null, $default = null)
    {
        $param = self::getParam('get', $key, $default);
        if ($cast != null) {
            return cast($param, $cast);
        }
        return $param;
    }

    /**
     * @param null $key
     * @return array
     */
    public static function getRequest($key = null)
    {
        return self::getParam('request', $key);
    }

    /**
     * @param null $key
     * @return array
     */
    public static function getJson($key = null)
    {
        return self::getParam('json', $key);
    }

    /**
     * @return array
     */
    public static function getFiles(): array
    {
        return self::$files;
    }

    /**
     * @param null $key
     * @return array
     */
    public static function getAll($key = null)
    {
        return self::getParam('all', $key);
    }

    /**
     * Отфильтрованный массив входящих данных
     * @return array Массив из параметров, котрые пропустим в случае обнаружения.
     */
    public static function filter(/*arguments*/): array
    {
        $args = func_get_args();

        $result = array_filter(self::$all, function ($v, $k) use ($args) {

            if (empty($v))
                return false;

            return !in_array($k, $args, true);
        }, ARRAY_FILTER_USE_BOTH);
        return $result;
    }

    /**
     * Вытащим из запроса только указанные поля.
     * @return array
     */
    public static function extract(): array
    {
        $args = func_get_args();

        if (is_array($args[0])) {
            $args = $args[0];
        }

        $result = array_filter(self::get(), function ($v, $k) use ($args) {
            if (empty($v)) {
                return false;
            }
            return in_array($k, $args, true);
        }, ARRAY_FILTER_USE_BOTH);

        return $result;
    }


    /**
     * @param array $filterRequest
     * @param array $data
     * @return string
     */
    public static function createFromRequest(array $filterRequest, array $data = []): string
    {
        $filterRequest = array_merge($filterRequest, $data);

        $result = array_map(function ($k, $v) {
            if (!is_string($v))
                return null;
            return "$k=$v";
        }, array_keys($filterRequest), $filterRequest);

        if (count($result) == 0) {
            return Uri::getFullPathUrl();
        }

        return Uri::getFullPathUrl() . '?' . implode('&', $result);
    }

    /**
     * @param bool $asArray
     * @param bool $throw
     * @return mixed|string
     * @throws Exception
     */
    public static function getJSONFromFile(bool $asArray = true, bool $throw = true)
    {
        try {
            $data = self::getFileData(true);

            if ($asArray) { //нам надо вернуть массив
                $data = json_decode($data, true);

                if (count($data) == 0) { //данные не получилось декодировать
                    throw new Exception(sprintf('Не могу дешифровать данные файла %s', self::getFileControl()->getUploadFile()->getUploadedFile()));
                }
            }
        } catch (Exception $ex) {
            if ($throw) {
                throw $ex;
            } else {//просто возвращаем пустые значения
                if ($asArray)  //если массив
                    $data = [];
                else //если строка
                    $data = "";
            }
        }

        return $data;
    }

    /**
     * Получить содержимое принятого файла.
     * @param bool $clearBOM
     * @return string
     * @throws Exception
     */
    public static function getFileData(bool $clearBOM): string
    {
        //Получаем загруженный файл
        $file = self::getFileControl(); //полученный файл

        //dump($file);

        if ($file === null or $file->getUploadFile() == null or $file->getUploadFile()->getUploadedFile() == null) {//файла нет
            throw new Exception('Файл не был передан.');
        }

        if (!empty($file->error)) { //были какие-то ошибки при загрузке файла
            throw new Exception($file->error);
        }

        //получаем содержимое файла
        $data = file_get_contents($file->getUploadFile()->getUploadedFile());

        //dump($data);

        if (!$data) { //данных нет
            throw new Exception(sprintf('Не могу прочитать содержимое файла %s.', $file->uploadedFile));
        }

        //Удаляем BOM (Byte Order Mark)
        if ($clearBOM and 0 === strpos(bin2hex($data), 'efbbbf')) {
            $data = substr($data, 3);
        }

        return $data;
    }

    /**
     * @return null
     */
    public static function getFileControl(): ?FileControl
    {
        return self::$fileControl;
    }
}
