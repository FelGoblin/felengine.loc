<?php

namespace Engine\Core\Config;

use Engine\Core\Development\DebugInfo;
use Engine\Core\Objects\CoreElement;
use Engine\Exceptions\ConfigException;
use Engine\Modules\LabelsStore;

final class SiteConfig extends CoreElement
{
    /**
     * Singleton
     * @var SiteConfig
     */
    private static SiteConfig $instance;

    /**
     * @var array
     */
    private array $configDataStored = [];
    private array $filesNames = [];

    /**
     * SiteConfig constructor.
     * @throws ConfigException
     */
    public function __construct()
    {
        $dir = pathSeparator(CONFIG_DIR);

        if (!is_dir($dir)) {
            return;
        }
        $list = array_diff(scandir($dir), array(".", ".."));
        if (empty($list)) {
            //dump('empty config list');
            return;
        }
        foreach ($list as $content) {
            if (is_file(CONFIG_DIR . $content)) {
                //dump(['style' => 'red'], 'load config flle: ' . $content);
                $this->loadFile($content);
            }
        }
    }

    /**
     * Загружаем файл
     * @param $path
     * @param string|null $group
     * @param bool $customConfig
     * @throws ConfigException
     */
    private function loadFile($path, string $group = null, bool $customConfig = false): void
    {
        //dump($path);
        if (!$customConfig) {
            $path = CONFIG_DIR . $path;
        }

        if ($group == null) {
            $group = strtolower(pathinfo($path)['filename']);
        }

        if (!file_exists($path)) {
            throw new ConfigException(
                sprintf(
                    'Cannot load config from file, file <strong>%s</strong> does not exist.',
                    $path
                )
            );
        }
        $items = include $path;
        //dump($items);
        if (!is_array($items)) {
            DebugInfo::debugAdd(
                sprintf(
                    'Config file <strong>%s</strong> is not a valid array.',
                    $path
                )
            );
            return;
        }

        $this->filesNames[$group] = $path;

        if (isset($groupAlias)) {
            //dump($groupAlias);
            $group = $groupAlias;
        }

        if ($group[0] == '_') {
            $group = substr($group, 1);
        }


        foreach ($items as $key => $value) {
            $this->store($group, $key, $value);
            LabelsStore::set(sprintf('config.%s.%s', $group, $key), $value);
            //dump($group, $key, $value);
            /*
            //### EVENT ###
            //Label::set(sprintf('config.%s.%s', $group, $key), $value);
            Events::invoke(Event::onSiteLoadConfig, ['group' => $group, 'key' => $key, 'value' => $value]);
            //### /EVENT ###
            //dump(sprintf('config.%s.%s',$group,$key),$value);
            */
        }
    }

    /**
     * Stores a config item.
     *
     * @param string $group The item group.
     * @param string $key The item key.
     * @param mixed $data The item data.
     * @param bool $merge
     * @return void
     */
    public function store(string $group, string $key, $data, bool $merge = false)
    {
        // Ensure the group is a valid array.
        if (!isset($this->configDataStored[$group]) || !is_array($this->configDataStored[$group])) {
            $this->configDataStored[$group] = [];
        }

        // Store the data.
        if ($merge && is_array($data)) {
            $this->configDataStored[$group][$key] = array_merge_recursive($this->configDataStored[$group][$key] = $data, $data);
        } else {
            $this->configDataStored[$group][$key] = $data;
        }
    }

    public static function initialize()
    {
        self::$instance = new SiteConfig();
    }

    /**
     * @param string $path
     * @param string $group
     * @param bool $customConfigFile
     * @throws ConfigException
     */
    public static function addConfigFile(string $path, string $group, bool $customConfigFile = false): void
    {
        static::$instance->loadFile($path, $group, $customConfigFile);
    }

    /**
     * Модуль включен
     * @param string $moduleName
     * @return bool
     */
    public static function isModule(string $moduleName): bool
    {
        return self::get($moduleName, 'modules', false);
    }

    /**
     * Retrieves a config item.
     *
     * @param string $key
     * @param string $group
     * @param string|null $default
     * @return int|string|bool|mixed
     */
    public static function get(string $key, $group = 'main', /*T*/ $default = null)
    {
        $result = static::$instance->retrieve($group, $key);

        if (is_null($result) and !is_null($default)) {
            return $default;
        }
        return $result;
    }

    /**
     * Алиас: Для соблюдения типов
     * @param string $key
     * @param string $group
     * @param null $default
     * @return bool
     */
    public static function getBool(string $key, $group = 'main', $default = null): bool
    {
        return static::get($key, $group, $default) ?? false;
    }

    /**
     * Алиас: Для соблюдения типов
     * @param string $key
     * @param string $group
     * @param null $default
     * @return int
     */
    public static function getInt(string $key, $group = 'main', $default = null): int
    {
        return static::get($key, $group, $default);
    }

    /**
     * Алиас: Для соблюдения типов
     * @param string $key
     * @param string $group
     * @param null $default
     * @return string|null
     */
    public static function getStr(string $key, $group = 'main', $default = null): ?string
    {
        return static::get($key, $group, $default);
    }

    /**
     * Возвращает
     * @param string $group The item group.
     * @param string $key The item key.
     * @return mixed
     */
    private function retrieve(string $group, string $key)
    {
        return $this->configDataStored[$group][$key] ?? null;
    }

    /**
     * @return array
     */
    public static function getDataStored(): array
    {
        return static::$instance->configDataStored;
    }

    /**
     * Save Config
     * @param string $group
     * @param array $array
     * @param bool $asJson
     * @return bool
     */
    public static function save(string $group, array $array = [], bool $asJson = false): bool
    {
        $array = array_merge(static::group($group), $array);
        if ($asJson) {
            $content = json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } else {
            $content = "<?php\n\treturn " . pretty_var_export($array) . ";\n?>";
        }
        $fileName = static::$instance->filesNames[$group] ?? null;
        if (is_null($fileName)) {
            $fileName = pathSeparatorF('%s%s%s', CONFIG_DIR, $group, ($asJson ? '.json' : '.php'));
        }
        //dump($fileName, $content);
        //сохраняем
        return file_put_contents($fileName, $content);
    }

    /**
     * Retrieves a config item
     * @param $group
     * @return array
     */
    public static function group($group): array
    {
        return static::$instance->configDataStored[$group] ?? [];
    }
}
