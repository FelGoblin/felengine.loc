<?php


namespace Engine\Core\Objects;

/**
 * Подключать модули нужно в событии onUserAuthorize в сервисном методе Модуль сайта
 * Interface IUserModule
 * @package Engine\Core\Objects
 */
interface IUserModule
{
    /**
     * Инициализация
     * @param $host
     * @param null $data
     * @return void
     */
    public static function init(IUser $host, $data = null): void;
}