<?php

namespace Engine\Core\Objects;

use Engine\Core\DataBase\DataBase;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use stdClass;

/**
 * Автозаполняющийся из массива класс
 * Class FillClass
 * @package Engine\Core\Object
 */
class FillClass implements IFillClass
{
    public function __construct($data, bool $dump = false)
    {
        if (!is_array($data)) {
            return;
        }
        //$this->inData = $data;
        if ($dump) {
            dump($data);
        }
        $this->fill($data);
    }

    /**
     * Заполняем данными масива текущий класс
     * @param $data
     */
    public function fill($data): void
    {
        foreach ($data as $key => $value) {
            if (property_exists(get_called_class(), $key)) {
                switch (gettype($this->$key)) {
                    case 'boolean':
                        $this->$key = cast($value, 'bool');
                        break;
                    case 'integer':
                        $this->$key = (int)$value;
                        break;
                    case 'double':
                        $this->$key = floatval($value);
                        break;
                    default:
                        $this->$key = $value;
                }
            }
        }
    }

    /**
     * @param $caller
     * @param $data
     * @param bool $errorReport
     * @return stdClass|null
     * @throws ReflectionException
     */
    public static function doFill(b&$caller, $data, bool $errorReport = false): ?stdClass
    {
        if ($caller == null) {
            return null;
        }
        $reflectionClass = new ReflectionClass(get_class($caller));

        foreach ($data ?? [] as $key => $value) {
            //if (property_exists($caller, $key)) {
            try {
                $reflectionProperty = $reflectionClass->getProperty($key);
                if (!$reflectionProperty->isPublic()) {
                    $reflectionProperty->setAccessible(true);
                }
                $val = $reflectionProperty->getValue($caller);
            } catch (Exception $ex) {
                if ($errorReport) {
                    dump(['style' => 'red'], "property $key does not exist in " . get_class($caller));
                }
                continue;
            }
            switch (gettype($val)) {
                case 'boolean':
                    $reflectionProperty->setValue($caller, cast($value, 'bool'));
                    break;
                case 'integer':
                    $reflectionProperty->setValue($caller, (int)$value);
                    break;
                case 'double':
                    $reflectionProperty->setValue($caller, floatval($value));
                    break;
                default:
                    $reflectionProperty->setValue($caller, $value);
            }
        }
        return $caller;
    }

    /**
     * Представить класс в виде массива
     *
     * @param $caller
     * @param array $requiredFields
     * @param bool $exclude
     * @return array|null
     * @throws ReflectionException
     */
    public static function asArray($caller, array $requiredFields = [], bool $exclude = true): ?array
    {
        $ref = new ReflectionClass($caller);
        $props = $ref->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

        if (!empty($requiredFields)) {
            $props = array_filter($props, function ($key) use ($requiredFields, $exclude) {
                return $exclude ? !in_array($key, $requiredFields) : in_array($key, $requiredFields);
            }, ARRAY_FILTER_USE_KEY);
        }

        $props_arr = [];

        foreach ($props as $prop) {

            $reflectionProperty = $ref->getProperty($prop->getName());
            if (!$reflectionProperty->isPublic()) {
                $reflectionProperty->setAccessible(true);
            }
            $val = $reflectionProperty->getValue($caller);

            $f = $prop->getName();
            $props_arr[$f] = $val;
        }

        if ($parentClass = $ref->getParentClass()) {
            $parent_props_arr = self::asArray($parentClass->getName());//RECURSION
            if (count($parent_props_arr) > 0)
                $props_arr = array_merge($parent_props_arr, $props_arr);
        }
        return $props_arr;
    }

    /**
     * @param string|null $tableName
     * @param array $where
     * @param bool $result
     * @return FillClass
     * @throws Exception
     */
    public static function loadSelf(?string $tableName, array $where, bool &$result): IFillClass
    {
        $data = DataBase::query()
                ->select()
                ->from($tableName)
                ->where($where)
                ->one() ?? [];

        $result = !empty($data);

        $class = get_called_class();
        return new $class($data);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (property_exists(get_called_class(), $name)) {
            return $this->$name;
        } else {
            trigger_error(sprintf('Method <b>%s</b> not undefined in <b>%s</b>', $name, get_called_class()));
            //dump_trace($name, get_called_class());
            return null;
        }
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (!property_exists(get_called_class(), $name)) {
            trigger_error(sprintf('Method <b>%s</b> not undefined in <b>%s</b>', $name, get_called_class()));
        } elseif (gettype($this->$name) != gettype($value)) {
            trigger_error(sprintf('The type of the variable <b>%s</b> was expected, <b>%s</b> was received in <b>%s</b>', gettype($this->{$name}), gettype($value), get_called_class()), E_USER_ERROR);
        } else {
            $this->$name = $value;
        }
    }

    /**
     * получить параметр поле класса
     * @param string $field
     * @return mixed|null
     */
    public function getParam(string $field)
    {
        return $this->$field ?? null;
    }


    /**
     * Добавить данные
     * @param $key
     * @param $value
     */
    public function add($key, $value): void
    {
        if (property_exists(get_called_class(), $key)) {
            if (gettype($this->$key) != gettype($value)) {
                trigger_error(sprintf('The type of the variable <b>%s</b> was expected, <b>%s</b> was received in <b>%s</b>', gettype($this->{$key}), gettype($value), get_called_class()), E_USER_ERROR);
            }
            switch (gettype($this->$key)) {
                case'integer':
                case'double':
                    $this->$key += $value;
                    break;
                case 'string':
                    $this->$key .= $value;
                    break;
                case 'boolean':
                    $this->$key |= $value;
                    break;
                case 'array':
                    $this->$key = array_merge_recursive($this->$key, $value);
                    break;
                default:
                    $this->$key = $value;
            }
        } else {
            trigger_error(sprintf('Method <b>%s</b> not undefined in <b>%s</b>', $key, get_called_class()));
        }
    }

    /**
     * Поднять значение
     * @param $key
     * @param int $value
     * @return float | null
     */
    public function inc($key, $value = 1)
    {
        if (property_exists(get_called_class(), $key)) {
            if (gettype($this->$key) == 'integer' or gettype($this->$key) == 'double') {
                $this->$key += $value;
            } else {
                trigger_error(sprintf('The type of the variable <b>%s</b> must be a <u>number</u>, <b>%s</b> was received in <b>%s</b>', gettype($this->{$key}), gettype($value), get_called_class()), E_USER_ERROR);
            }
        } else {
            trigger_error(sprintf('Method <b>%s</b> not undefined in <b>%s</b>', $key, get_called_class()));
        }
        return $this->$key;
    }

    /**
     * Представить класс в виде массива
     *
     * @return array|null
     */
    protected function toArray(): ?array
    {
        return call_user_func('get_object_vars', $this);
    }
}
