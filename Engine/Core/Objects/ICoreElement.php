<?php

namespace Engine\Core\Objects;


interface ICoreElement
{
    public static function initialize();
}