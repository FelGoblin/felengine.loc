<?php

namespace Engine\Core\Objects;

use Exception;

interface IUser
{
    /**
     * Пытаемся восстановить пользователя
     * @return mixed
     */
    public static function TryRestoreUser();

    /**
     * @param string $userName
     * @param array $includeModules
     * @return IUser|null
     * @throws \Exception
     */
    public static function getUser(string $userName, array $includeModules = []): ?IUser;

    /**
     * Создаём нового пользователя
     * @param array $data
     * @return bool
     */
    public static function createNewUser(array $data): bool;

    /**
     * Установить параметр
     * @param string $field
     * @param $data
     */
    public function setModuleExtension(string $field, $data): void;

    /**
     * Получить данные пользователя
     * @return IUserData
     */
    public function getUserData(): ?IUserData;

    /**
     * Установить данные пользователя
     * @param IUserData $userData
     * @return mixed
     */
    public function setUserData(IUserData $userData);

    /**
     * Загружаем пользовательские данные
     * @param array $includedModules
     */
    public function load(array $includedModules = []): void;

    /**
     * Обновить данные пользователя
     * @throws Exception
     */
    public function refresh(): void;

    /**
     * Сохраняем пользователя
     * @param bool $checkSave
     * @throws \Exception
     */
    public function saveUser(bool $checkSave = false): void;

    /**
     * Пользователь авторизирован
     * @return bool
     */
    public function isAuthorized(): bool;

    /**
     * Проверяем доступ пользователя
     * @param $permissions
     * @return bool
     */
    public function userAccessCheck($permissions): bool;

    /**
     * Инициализация прав доступа
     * @param array $access
     */
    public function userAccessInit(array $access = []): void;

    /**
     * Пользователь заблокирован
     * @return bool
     */
    public function isBlocked(): bool;

    /**
     * Пролучить имя  пользовтеля (алиас)
     * @return mixed
     */
    public function getName();

    /**
     * Авторизуем пользователя
     * @param bool $true
     * @return mixed
     */
    public function setAuthorize(bool $true);
}