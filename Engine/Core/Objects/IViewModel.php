<?php


namespace Engine\Core\Objects;

/**
 * Вью модель
 * Interface IViewModel
 * @package Engine\Core\Objects
 */
interface IViewModel
{
    /**
     * @param array $data
     * @return array
     */
    public function getViewModel(array $data = []): array;
}