<?php


namespace Engine\Core\Objects;


interface IUserData extends IUserModule
{
    /**
     * Сохранить данные пользователя
     * @param array $data
     */
    public function saveData(array $data = []): void;

    /**
     * Получить имя текущего пользователя
     * @return string
     */
    public function getName(): string;

    /**
     * Получить имя текущего пользователя в URL кодировке (чтобы учитывать символьные пробелы)
     * @return string
     */
    public function getUriName(): string;

    /**
     * Хеш пароля
     * @return string|null
     */
    public function getPassword(): ?string;

    /**
     * Соль пароля
     * @return string
     */
    public function getSalt(): ?string;

    /**
     * Хеш текущей авторизации
     * @return string|null
     */
    public function getHash(): ?string;

    /**
     * Установленная пользователем локализация сайта
     * @return string|null
     */
    public function getLocale(): ?string;

    /**
     * Метод, которым пользователь вошёл на сайт
     * @return string|null
     */
    public function getJoinMethod(): ?string;

    /**
     * Пользователь заблокирован (забанен)
     * @return bool
     */
    public function isBlocked(): bool;
}