<?php

namespace Engine\Core\Objects;


abstract class CoreElement implements ICoreElement
{
    /**
     * @var int
     */
    protected static $loadMode = SYSTEM_LOAD;
}