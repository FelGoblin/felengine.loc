<?php


namespace Engine\Core\Objects;

/**
 * Interface ICompile
 * @package Engine\Core\Objects
 */
interface  ICompile extends IViewModel
{
    /**
     * Эта хернЯ может собираться в компонент менеджере.
     * @param array|null $data
     * @return mixed
     */
    public function compile(array $data = null): void;
}