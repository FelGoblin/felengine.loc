<?php

namespace Engine\Core\Objects;


use Engine\Core\View\Render;

/**
 * Элемент боквого меню
 * Class MenuElement
 * @package Engine\Core\Object
 */
final class MenuElement extends FillClass
{
    /**
     * Тип элемента
     *      menu
     *      hr
     * @var string
     */
    protected $type = 'menu';
    /**
     * Иконка (ссылка класса на fel-font или в зависимости от реализации шаблона)
     * @var string
     */
    protected $icon = '';
    /**
     * Заголовок меню
     * @var string
     */
    protected $title = '';
    /**
     * @var string
     */
    protected $url;
    /**
     * Модуль к которому относитася это меню. Меню так же отключается, если модуль в конфиге modules откдючён
     * @var string
     */
    protected $module = '';
    /**
     * Для текущей реализации по-умолчанию (см. шаблон Templates/Default/system-components/leftMenu.php)
     * Ярлычок - метка. В него будут добавлдены данные из шаблона из перменной имени этого ярлыка
     *  Пример: В шаблон приходить переменная $count = 10
     *      bage = count;
     *    в шаблон вставляем как ${$menu->getBage()} (это $bage)
     * @var string
     */
    protected $bage = '';
    /**
     * Всплывающая подсказка
     * @var
     */
    protected $hint;
    /**
     * Доступ к ссылке. Можно перечеслять через запятую.
     * @var string
     */
    protected $permission = '';

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Получитья ярлык - метку
     * @return mixed
     */
    public function getBage()
    {
        return $this->bage;
    }

    /**
     * Установить ярлык - метку
     * @param $bage
     */
    public function setBage($bage): void
    {
        $this->bage = $bage;
    }

    /**
     * @return mixed
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * @return mixed
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param bool $tolower
     * @return mixed
     */
    public function getModule(bool $tolower = false): ?string
    {
        if ($tolower) {
            return strtolower($this->module);
        }
        return $this->module;
    }

    public function viewModel(): array
    {
        return $this->toArray();
    }

    /**
     * @return array|null
     * @throws \ReflectionException
     */
    protected function toArray(): ?array
    {
        $result = parent::toArray();
        $result['active'] = $this->isActive();
        if (!empty($this->title) AND $this->title[0] == '@') {
            $result['title'] = lang($this->title);
        }
        return $result;
    }

    /**
     * Текущий модуль к которому привязано меню активный (например для выделения пункта меню)
     * @return bool
     * @throws \ReflectionException
     */
    public function isActive(): bool
    {
        if (!empty($this->module)) {
            return Render::isRunModule($this->module);
        }
        return false;
    }
}