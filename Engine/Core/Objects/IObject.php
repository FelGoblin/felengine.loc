<?php


namespace Engine\Core\Objects;


interface IObject
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getDescription(): string;
}