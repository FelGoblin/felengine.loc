<?php

namespace Engine\Core\Objects;


interface INamespace
{
    /**
     * Режим без подключенных системных компонентов.
     */
    const MODE_EMPTY = -1;
    /**
     * Режим в котором подключаются все компоненты, зарегистрированные в движке и модуле
     */
    const MODE_NORMAL = 0;
    /**
     * Тольео по указанному списку в методе getIncludedSystemComponents(). Остальные отключаются
     */
    const MODE_INCLUDE = 1;
    /**
     * Компоненты в указанном списке в методе getExcludedSystemComponents() отключаются.
     */
    const MODE_EXCLUDE = 2;

    /**
     * Получить список исключённых системных модулей
     * @return array
     */
    static function getExcludedSystemComponents(): array;

    /**
     * Список только включённых компонентов
     * @return array
     */
    static function getIncludedSystemComponents(): array;

    /**
     * Получить список подключённых неймспейсов, их layouts и из ссылок на шаблоны
     * @return array
     */
    static function getLinkedNamespaces(): array;

    /**
     * Текущий режим
     * @return int
     */
    static function getMode(): int;
}