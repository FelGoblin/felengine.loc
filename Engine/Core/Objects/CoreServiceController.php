<?php

namespace Engine\Core\Objects;


use Engine\Core\Core;
use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\View\Template;
use Exception;

/**
 * Class CoreServiceController
 * @package Engine\Core\Object
 */
abstract class CoreServiceController
{

    protected static ?CoreServiceController $instance;

    protected static array $services = [];

    /**
     * @param string|null $config
     * @param string|null $detour
     * @param array $detourData
     * @throws Exception
     */
    protected static function init(string $config = null, string $detour = null, array &$detourData = []): void
    {
        //new Core();
        $modules = [];
        //обходим модули
        Core::detourModules(DETOUR_COMP, $modules);

        //Выстраиваем список сервисов (внутренний + из модулей)
        static::$services = Core::buildServicesList($modules)[$config] ?? [];

        if ($detour != null) {
            Core::detourModules($detour, $detourData);
        }

        $self = static::getInstance();

        //
        Events::on(Event::onSiteModulesInit, function ($args) use ($self) {
            $self->initServices('service', $args['mode']);
        });

        $self->initServices('core');
        $self->initServices('core', POST_LOAD);
    }

    /**
     * @return CoreServiceController
     */
    protected abstract static function getInstance(): CoreServiceController;

    /**
     * @param string|null $config
     * @param int $mode
     */
    protected function initServices(string $config = null, int $mode = SYSTEM_LOAD)
    {
        $services = self::$services; //require CONFIG_DIR . '/Service/Services.php';

        if (empty($services ?? [])) {
            //dump('empty');
            return;
        }

        //текущий конфиг
        if ($config == null) {
            $config = 'core';
        }
        foreach ($services[$config] ?? [] as $_service) {
            if ($config == 'postInit') {
                $nmspcClass = sprintf('Namespaces\\%s\\NmspcConfig', Template::getNmspc());
                $mode = INamespace::MODE_EMPTY;
                if (class_exists($nmspcClass)) {
                    $mode = $nmspcClass::getMode();
                }
                $_service::postControllerRun($mode);
            } else {
                if ($_service::$mode == $mode or $_service::$mode == ALWAYS_LOAD) {
                    //echo("$config => $_service [$mode]\n");
                    $service = getProvider($_service);
                    //Создаём алиасы (если необходимы)
                    $service->classAlias();
                    //Инициируем сервис
                    $service->init($config, $mode);
                }
            }
        }
    }
}
