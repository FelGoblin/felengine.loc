<?php

namespace Engine\Core\Objects;


abstract class BaseNameSpace implements INamespace
{
    /**
     * Фиксированная тема для текущего неймпсейса
     * @var string
     */
    protected string $fixTheme = 'default';
}
