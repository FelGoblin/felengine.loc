<?php

namespace Engine\Core\Objects;


interface IFillClass
{
    public static function loadSelf(?string $tableName, array $where, bool &$result): self;
}