<?php


namespace Engine\AdmControl\Ext;


use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SaveException;
use Engine\TemplatesLibrary\ExtLibraryElements\IInspectedNode;

final class FileConfig implements IInspectedNode
{
    // --- VARS ---
    /**
     * @var string
     */
    public $id = 'undefined';
    /**
     * @var string
     */
    public $node = '';

    /**
     * @var
     */
    private $fileName;

    /**
     * @var string
     */
    private $ext = '';

    /**
     * @var array
     */
    private $config = [];

    /**
     * FileConfig constructor.
     * @param $filename
     * @throws ConfigException
     * @throws \Exception
     */
    public function __construct($filename)
    {
        if (!file_exists($filename)) {
            throw new ConfigException(langF('@config_not_found', $filename));
        }

        $this->fileName = $filename;
        ['basename' => $this->id, 'dirname' => $this->node, 'extension' => $this->ext] = pathinfo($filename);
        $this->node = str_replace(ROOT_DIR, '', $this->node); //удалим ROOT_DIR из пути
        //
        if ($this->ext == 'json') {
            $data = file_get_contents($filename);
            $this->config = json_decode($data, true);
        } elseif ($this->ext == 'php') {
            $this->config = include $filename;
        } else {
            throw new ConfigException(langF('@config_unknown_file_config_extensions', $this->ext, $this->id, $this->node));
        }
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getNodeName(): string
    {
        return $this->node;
    }

    /**
     * @inheritDoc
     */
    public function getFile(): string
    {
        return $this->fileName;
    }

    /**
     * @inheritDoc
     */
    public function getExt(): string
    {
        return $this->ext;
    }

    /**
     * @inheritDoc
     */
    public function getParam(): array
    {
        return $this->getConfig();
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return count($this->config);
    }

    /**
     * СОхранить конфиг
     * @param int $method
     * @return bool
     * @throws SaveException
     * @throws \Exception
     */
    public function save(int $method = self::AS_AUTO): bool
    {
        if ($method == self::AS_AUTO) {
            if ($this->ext == 'php') {
                $method = self::AS_ARRAY;
            } elseif ($this->ext == 'json') {
                $method = self::AS_JSON;
            }
        }

        switch ($method) {
            case self::AS_ARRAY:
                return file_put_contents($this->fileName, "<?php\n\$return " . var_export($this->config) . ";\n?>");
            case self::AS_SERIALIZE:
                return file_put_contents($this->fileName, serialize($this->config));
            case self::AS_JSON:
                return file_put_contents($this->fileName, json_encode($this->config));
            default:
                throw new SaveException(lang('@unknown_save_method'));
        }
    }
}
