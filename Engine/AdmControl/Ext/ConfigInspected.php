<?php


namespace Engine\AdmControl\Ext;


use Engine\Core\Development\DebugInfo;
use Engine\Exceptions\ConfigException;
use Engine\Exceptions\SaveException;
use Engine\TemplatesLibrary\ExtLibraryElements\IInspectedClass;
use Engine\TemplatesLibrary\ExtLibraryElements\InspectedTemplate;
use Exception;

class ConfigInspected implements IConfigController, IInspectedClass
{

    /**
     * @var string
     */
    public $title = '@config_inspected_title';
    /**
     * @var string
     */
    public $description = '@config_inspected_description';
    /**
     * Возможно ли добавить еще конфиг в текущий узел конфигураций (в папку)
     * @var bool
     */
    public $canNodeInsert = true;
    /**
     * Возможно ли добавлять новые параметр в текущий узел
     * @var bool
     */
    public $canNewParamValue = true;
    /**
     * @var string
     */
    public $id = '';
    /**
     * @var array of IInspectedNewParam
     */
    private $configTemplates = [];
    /**
     * @var array of IInspectedNode
     */
    private $nodes = [];

    /**
     * ConfigInspected constructor.
     * @param string $id
     * @param string $title
     * @param string $description
     */
    public function __construct(string $id, string $title, string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->loadConfigTemplates($this->id);
    }

    /**
     * Загрузка шаблонов конфига
     * @param string $name
     * @return void
     */
    public function loadConfigTemplates(string $name): void
    {
        $file = pathSeparatorF('%sdata/editable_conf/%s_config.php', RESOURCES_DIR, $name);
        if (file_exists($file)) {
            $res = include $file;
            foreach ($res ?? [] as $re) {
                //dump($res, $re, self::$configTemplates);
                $this->configTemplates[] = new InspectedTemplate($re);
            }
        }
    }

    function getId(): string
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @@inheritDoc
     */
    public function isCanNodeInsert(): bool
    {
        return $this->canNodeInsert;
    }


    /**
     * @inheritDoc
     */
    public function isCanNewParamValue(): bool
    {
        return $this->canNewParamValue;
    }

    /**
     * @inheritDoc
     */
    public function getInspected(): array
    {
        return $this->getNodes();
    }

    /**
     * @return array
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }

    /**
     * @param $needle
     * @return FileConfig|null
     */
    public function find($needle): ?FileConfig
    {
        foreach ($this->nodes as $config) {
            if ($config->id = $needle) {
                return $config;
            }
        }
        return null;
    }

    /**
     * @param $file
     */
    public function load($file)
    {
        try {
            $this->nodes[] = new FileConfig($file);
        } catch (ConfigException $ex) {
            DebugInfo::debugAdd(sprintf('<warning>%s</warning>', $ex->getMessage()));
        } catch (Exception $ex) {
            DebugInfo::debugAdd(sprintf('<error>%s</error>', $ex->getMessage()));
        }
    }

    /**
     * Сохранить конфиг
     * @param FileConfig|null $config
     * @return bool
     * @throws SaveException
     */
    function save($config = null): bool
    {
        return $config->save(FileConfig::AS_AUTO);
    }

    /**
     * @return int
     */
    function getCountAllLoadedConfigs(): int
    {
        $i = 0;
        /** @var FileConfig $c */
        foreach ($this->nodes as $c) {
            $i += $c->getCount();
        }
        return $i;
    }

    /**
     * @return array
     * @throws Exception
     */
    function toArray(): array
    {
        return [
            'id' => $this->id,
            'title' => lang($this->title),
            'description' => lang($this->description)
        ];
    }
}
