<?php


namespace Engine\AdmControl\Ext;


interface IConfigController
{

    /**
     * Сохранить конфиг
     * @param FileConfig|null $config
     * @return bool
     */
    function save($config = null): bool;

    function getCountAllLoadedConfigs(): int;

    function getId(): string;
    
    function toArray() : array;
}