<?php

namespace Engine\AdmControl;

use Engine\AdmControl\Ext\IConfigController;
use Engine\Exceptions\CoreException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

abstract class EngineController
{
    /**
     * @param array $configControllers
     * @param bool $loadConfig
     * @return IConfigController
     */
    abstract static function init(&$configControllers = [], bool $loadConfig = true): IConfigController;

    /**
     *
     */
    abstract static function build(): void;

    /**
     * @param string $dir
     * @return bool
     * @throws CoreException
     * @throws \Exception
     */
    protected function loadDir(string $dir): bool
    {
        if (!is_dir(realpath($dir))) {
            throw new CoreException(langF('@directory_does_not_exusts', $dir));
        }

        $directory = new RecursiveDirectoryIterator(pathSeparator($dir));
        $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
        self::detour($dir, $iterator, $cb);


        return true;
    }

    /**
     * @param string $dir
     * @param RecursiveIteratorIterator $iterator
     * @param callable|null $cb
     */
    public static function detour(string $dir, RecursiveIteratorIterator $iterator, callable $cb = null): void
    {
        /** @var SplFileInfo $splFileInfo */
        foreach ($iterator as $splFileInfo) {
            if ($splFileInfo->getFilename() == '.' OR $splFileInfo->getFilename() == '..') //скипаем папки
                continue;
            if (!$splFileInfo->isDir()) { //это не папка
                $parent = str_replace($dir . DIRECTORY_SEPARATOR, '', $splFileInfo->getPath()); //заменяем корневой путь на слеш
                if ($cb != null) {
                    //dump($parent, $splFileInfo->getPath());
                    $cb([
                        'parent' => $parent,
                        'ext' => $splFileInfo->getExtension(),
                        'file' => $splFileInfo->getRealPath()
                    ]);
                }
            }
        }
    }
}