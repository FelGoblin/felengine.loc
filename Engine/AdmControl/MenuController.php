<?php


namespace Engine\AdmControl;

use Engine\AdmControl\Ext\ConfigInspected;
use Engine\AdmControl\Ext\IConfigController;
use Engine\Core\View\Template;

final class MenuController extends EngineController
{
    /**
     * @param array $configControllers
     * @param bool $loadConfig
     * @return IConfigController
     */
    static function init(&$configControllers = [], bool $loadConfig = true): IConfigController
    {
        $nmspc = strtolower(Template::getNmspc());
        $c = new ConfigInspected('menu', '@menu_controller_title', '@menu_controller_description');
        $c->canNodeInsert = false;
        $configControllers[] = $c;

        //Загружаем шаблоны для конфига
        //self::loadConfigTemplates('menu');
        //dump(self::$configTemplates);

        if (!$loadConfig) { //не надо загружать конфиги
            return $c;
        }

        //смотрим общий файд меню в папке ./Resources
        $configFile = pathSeparatorF('%sdata/left-menu.json', RESOURCES_DIR, $nmspc);
        if (file_exists($configFile)) {//
            $c->load($configFile);
        }
        //ищем в неймспейсах
        $find = array_diff(scandir(TPL_DIR), Array(".", ".."));
        foreach ($find as $dir) {
            if (is_dir(TPL_DIR . $dir)) {
                $configFile = pathSeparatorF('%s/resources/data/left-menu.json', TPL_DIR . $dir);
                if (file_exists($configFile)) {
                    $c->load($configFile);
                }
            }
        }
        return $c;
    }

    static function build(): void
    {
        // TODO: Implement build() method.
    }
}