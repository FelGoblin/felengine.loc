<?php

namespace Engine\AdmControl;

use Engine\AdmControl\Ext\ConfigInspected;
use Engine\AdmControl\Ext\IConfigController;
use Engine\Localization\Localization;

class LangController extends EngineController
{
    /**
     * @param array $configControllers
     * @param bool $loadConfig
     * @return IConfigController
     */
    static function init(&$configControllers = [], bool $loadConfig = true): IConfigController
    {
        $c = new ConfigInspected('langconfig', '@lang_configs_controller_title', '@lang_configs_controller_description');
        $configControllers[] = $c;
        $c->loadConfigTemplates('lang');

        if (!$loadConfig) { //не надо загружать конфиги
            return $c;
        }
        $fn = function ($finfo) use ($c) {
            $c->load($finfo['file']);
        };
        Localization::loadLanguage($fn);
        return $c;
    }

    static function build(): void
    {
        // TODO: Implement build() method.
    }
}