<?php

namespace Engine\AdmControl;

use Engine\AdmControl\Ext\ConfigInspected;
use Engine\AdmControl\Ext\IConfigController;
use Engine\Modules\ModulesDetour;
use ReflectionException;

/**
 * Собираются все конфиги сайта из Config и модулей
 * Class SiteConfigsController
 * @package Engine\AdmControl
 */
final class SiteConfigsController extends EngineController
{
    /**
     * @param array $configControllers
     * @param bool $loadConfig
     * @return IConfigController
     * @throws ReflectionException
     */
    static function init(&$configControllers = [], bool $loadConfig = true): IConfigController
    {
        $dir = CONFIG_DIR;
        $find = array_diff(scandir($dir), Array(".", ".."));
        $c = new ConfigInspected('site', '@site_configs_controller_title', '@site_configs_controller_description');
        $configControllers[] = $c;
        //$c->loadConfigTemplates('site');

        if (!$loadConfig) { //не надо загружать конфиги
            return $c;
        }

        foreach ($find as $file) {
            if (is_file(CONFIG_DIR . $file)) {
                $c->load(CONFIG_DIR . $file);
            }
        }

        $fn = function ($cbData) use ($c) {
            if (method_exists($cbData['class'], 'autoloadConfig')) {
                $cbData['class']::autoloadConfig(function ($file) use ($c) {
                    $c->load($file);
                });
            }
        };
        ModulesDetour::detour('components', $fn);
        return $c;
    }

    static function build(): void
    {
        // TODO: Implement build() method.
    }
}
