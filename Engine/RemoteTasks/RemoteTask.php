<?php

namespace Engine\RemoteTasks;

use Engine\Core\Objects\CoreServiceController;
use Exception;

/**
 * Class RemoteTask (Cron)
 * @package Engine\RemoteTasks
 */
class RemoteTask extends CoreServiceController
{
    /**
     * @var RemoteTaskParams
     */
    private $params;

    /**
     * @return RemoteTaskParams
     */
    public function getParams(): RemoteTaskParams
    {
        return $this->params;
    }

    //#######################################################################

    /**
     * @return RemoteTask
     */
    protected static function getInstance(): CoreServiceController
    {
        if (self::$instance == null) {
            self::$instance = new RemoteTask();
        }
        return static::$instance;
    }

    /**
     * @param string|null $config
     */
    public static function init(string $config = null, string $detour = null, array &$detourData = []): void
    {
        $self = self::getInstance();
        parent::init('cron', 'cron');
        //run
        //ИНициализируем параметры Cron
        try {
            $self->params = new RemoteTaskParams();
            if ($self->params->verification()) { //всё ок
                $self->params->getModule()->task();
                $self->params->getModule()->logOut();
                $self->initServices('postInit');
            } else {
                throw new Exception($self->params->getError());
            }
        } catch (Exception $ex) {
            exit($ex->getMessage());
        }
    }
}