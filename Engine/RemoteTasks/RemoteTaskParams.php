<?php

namespace Engine\RemoteTasks;


use Engine\Core\Config\SiteConfig;
use Exception;
use Engine\Modules\CronModule;

class RemoteTaskParams
{
    private $token;

    private $_module;

    private $module;

    private $error = '';

    /**
     * argv: token module
     * RemoteTaskParams constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (($_SERVER['argc'] ?? 0) > 1) {
            list(, $this->token, $this->_module) = $_SERVER['argv'];
            unset($_SERVER['argv']);//параноим
        } else {
            list(, $this->token, $this->_module) = [null, 'BCF785EBD71A2FAD13095802C161E', 'SiteLottery'];
            //throw new Exception('Forbidden script initialization method');
        }
    }

    /**
     * @return bool
     */
    public function verification(): bool
    {
        try {
            /*
            echo "\n1.\n";
            print_r($this->token);
            echo "\n2.\n";
            print_r(SiteConfig::get('cron_token'));
            echo "\n";
            */
            if (empty($this->token) OR ($this->token != SiteConfig::get('cron_token'))) {
                throw new Exception('Unavailable cron token');
            }
            $mod = '\Modules\\' . $this->_module . '\Cron';
            if (empty($this->_module) or !class_exists($mod, true)) {
                throw new Exception(sprintf('Unknown site module (%s)', $mod));
            } else {
                $this->module = new $mod();
            }
            $result = true;
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getModule(): ?CronModule
    {
        return $this->module;
    }

    /**
     * @return mixed
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }
}