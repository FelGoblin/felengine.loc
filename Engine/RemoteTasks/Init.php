<?php
/*
 * Инициализация cron.php
 */

require dirname(__DIR__) . '/Dump.php';
require($dirname . 'Engine/Core/Defines.php');
require dirname(__DIR__) . '/Autoloader.php';

use Engine\Autoloader;
use Engine\RemoteTasks\RemoteTask;

try {
    Autoloader::Init();
    //инициируем сервисы
    RemoteTask::init();

} catch (Exception $ex) {
    echo $ex->getMessage(); //TODO: Error Page!
}
