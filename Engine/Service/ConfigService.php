<?php

namespace Engine\Service;

use Engine\Core\Config\SiteConfig;
use Engine\Exceptions\ConfigException;
use Engine\Modules\AbstractService;

/**
 * Сервис инициализации конфигурации сайта
 * Class ConfigService
 * @package Engine\Service
 */
final class ConfigService extends AbstractService
{

    /**
     * @param string|null $config
     * @param int $mode
     * @throws ConfigException
     */
    public function init(?string $config, int $mode): void
    {
       //dump('init configs');
        //ИНициализируем конфиги
        SiteConfig::initialize();;
    }

    /**
     *
     */
    function classAlias(): void
    {
        class_alias('\\Engine\\Core\\Config\\SiteConfig', 'SiteConfig');
        class_alias('Engine\\Core\\View\\Render', 'Render');
    }
}
