<?php

namespace Engine\Service;

use Engine\Core\Core;
use Engine\Core\Session\Session;
use Engine\Modules\AbstractService;

class SessionService extends AbstractService
{
    /**
     * @var string
     */
    public $serviceName = 'session';

    /**
     *
     */
     public function init(?string $config, int $mode): void
    {
        Session::initialize();

        Session::restore(); //достаём из кукисов

    }

    function classAlias(): void
    {
        class_alias('\\Engine\\Core\\Session\\Session', 'Session');
    }
}