<?php

namespace Engine\Service;

use Engine\Core\Config\SiteConfig;
use Engine\Core\Core;
use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Core\Objects\IUser;
use Engine\Modules\AbstractService;
use Engine\Object\User\_User;

class UserService extends AbstractService
{
    /**
     * Пытаемся восстановить пользователя из кукисов и сессии
     *
     * @param string|null $config
     * @param int $mode
     * @throws \Exception
     */
    public function init(?string $config, int $mode): void
    {
        $user = Core::getUser();
        if ($user != null) {
            Events::on(Event::onUserAuthorize, function ($args) {
                /** @var IUser $user */
                $user = $args['user'];

                $modules = [];
                //опыт
                if (SiteConfig::isModule('expModule')) {
                    $modules[] = 'UserExp';
                }

                //инвентарь
                if (SiteConfig::isModule('inventoryModule')) {
                    $modules[] = 'UserInventory';
                }

                //
                if (SiteConfig::isModule('bufsModule')) {
                    $modules[] = 'UserBufs';
                }
                //
                if (SiteConfig::isModule('carmaModule')) {
                    $modules[] = 'UserCarma';
                }

                $modules[] = 'UserEventsActions';

                $user->load($modules);

            });
            /** @var _User $user */
            $user::TryRestoreUser();
        }
    }
}