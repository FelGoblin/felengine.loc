<?php

namespace Engine\Service;

use Engine\Core\Core;
use Engine\Core\Routing\Router;
use Engine\Modules\AbstractService;

class RouterService extends AbstractService
{
    /**
     * СОздадим класс роутера для последующего его вызова в ядре.
     */
     public function init(?string $config, int $mode): void
    {
        Router::initialize();
    }

    function classAlias(): void
    {
        class_alias('\\Engine\\Core\\Routing\\Route', 'Route');
        class_alias('\\Engine\\Core\\View\\Template', 'Template');
        class_alias('Engine\\Core\\View\\SiteComponent\\SiteComponent', 'SiteComponent');
    }
}