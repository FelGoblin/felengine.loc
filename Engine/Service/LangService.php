<?php

namespace Engine\Service;


use Engine\Localization\Localization;
use Engine\Modules\AbstractService;

final class LangService extends AbstractService
{

    /**
     * {@inheritdoc}
     * @var int
     */
    public static $mode = POST_LOAD;

    /**
     * Инициализация
     * @param string|null $config
     * @param int $mode
     * @throws \Exception
     */
    public function init(?string $config, int $mode): void
    {
        Localization::initialize();
    }

    /**
     * Создаём алиас
     */
    public function classAlias(): void
    {
        class_alias('Engine\Localization\Localization', 'Localization');
    }
}