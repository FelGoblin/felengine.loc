<?php

namespace Engine\Service;

use Engine\Core\Development\DebugInfo;
use Engine\Core\EventsHandler\Event;
use Engine\Core\EventsHandler\Events;
use Engine\Modules\AbstractService;
use Exception;

class ModulesService extends AbstractService
{
    /**
     * {@inheritdoc}
     * @var int
     */
    public static $mode = ALWAYS_LOAD;

    /**
     * Инициализация сервиса, который загружает модули сайта
     * @param string|null $config
     * @param int $mode
     * @throws Exception
     */
    public function init(?string $config, int $mode): void
    {
        //запускаем инициализацию модулей, прописанных в конфиге TODO: сделать автоподключение
        //dump_trace($config, $mode);
        DebugInfo::debugAdd('Invoke onSiteModulesInit');
        Events::invoke(Event::onSiteModulesInit, ['config' => 'service', 'mode' => $mode]);
    }
}
