<?php

namespace Engine\Service;

use Engine\Core\DataBase\DataBase;
use Engine\Modules\AbstractService;

class DatabaseService extends AbstractService
{

    /**
     * @param string|null $config
     * @param int $mode
     * @throws \Exception
     */
    public function init(?string $config, int $mode): void
    {
        //dump('init database');
        DataBase::initialize();
    }
}
