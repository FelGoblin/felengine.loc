<?php

namespace Engine\Service;


use Engine\Core\Core;
use Engine\Core\Template\Label;
use Engine\Core\Http\Uri;
use Engine\Modules\AbstractService;

class UriService extends AbstractService
{
    /**
     * Обрабатываем URI
     */
    public function init(?string $config, int $mode): void
    {
        Uri::initialize();
        define('SERVER_URI', Uri::getBase());
    }
}