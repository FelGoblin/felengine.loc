<?php

namespace Engine\Service;

use Engine\Modules\AbstractService;
use Engine\TemplatesLibrary\ITplLibrary;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

final class LibraryService extends AbstractService
{

    private $libraryElements = [];

    /**
     * Инициализация
     * @param string|null $config
     * @param int $mode
     */
    public function init(?string $config, int $mode): void
    {
        //библиотеки движка
        $libDir = realpath(ROOT_DIR . 'Engine/TemplatesLibrary/SiteLibraryElements/');
        $directory = new RecursiveDirectoryIterator(pathSeparator($libDir));
        $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
        $this->detour($iterator);

        //пользовательские библиотеки
        $path = pathSeparator(ROOT_DIR . 'Object/TemplatesLibrary/');
        //dump($path, is_dir($path));

        if (is_dir($path)) {
            $directory = new RecursiveDirectoryIterator($path);
            $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
            $this->detour($iterator);

        }
        //$this->classAlias(true);

    }

    /**
     * Обходчик
     * @param RecursiveIteratorIterator $iterator
     */
    private function detour(RecursiveIteratorIterator $iterator): void
    {
        foreach ($iterator as $splFileInfo) {
            if ($splFileInfo->getFilename() == '.' or $splFileInfo->getFilename() == '..') //скипаем папки
                continue;
            if (!$splFileInfo->isDir()) { //это не папка
                $path = str_replace(ROOT_DIR, DIRECTORY_SEPARATOR, $splFileInfo->getPath()); //заменяем корневой путь на слеш
                //меняем слеши на слеши для неймспейсов
                $class = $splFileInfo->getBasename('.php');
                $path = str_replace(DIRECTORY_SEPARATOR, '\\', $path . '\\' . $class); //неймспейс класа = имя файла класса
                //dump($path, $splFileInfo->getBasename('.php'));
                /** @var ITplLibrary $path */
                $path::alias();
                //$this->libraryElements[$class] = $path;
            }
        }
    }

    /**
     */
    public function classAlias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\TplLibrary', 'Html');
    }
}
