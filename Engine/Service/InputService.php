<?php

namespace Engine\Service;

use Engine\Core\Core;
use Engine\Core\Http\Input;
use Engine\Modules\AbstractService;

class InputService extends AbstractService
{
    /**
     * Иницилизируем входящие данные _GET, _POST, _JSON, _FILE
     * @param Core $core
     */
     public function init(?string $config, int $mode): void
    {
        Input::initialize();
    }

    function classAlias(): void
    {
        class_alias('\\Engine\\Core\\Http\\Input', 'Input');
    }
}