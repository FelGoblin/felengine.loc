<?php
// src/Autoloader/NamespaceAutoloader.php
namespace Engine;

final class Autoloader
{

    /**
     * @var Autoloader|null
     */
    private static ?Autoloader $instance = null;
    protected array $namespacesMap = array();
    private bool $loaded = false;

    /**
     * Autoloader constructor.
     */
    public function __construct()
    {
        $list = require CONFIG_DIR . 'Service/Autoload.php';

        if (!empty($list['namespaces'])) {

            foreach ($list['namespaces'] as $nameSpace) {
                //dump('added namespace' . $nameSpace);
                $this->addNamespace($nameSpace);
            }
        }
        $this->register();

        if (!empty($list['files'])) {
            foreach ($list['files'] as $file) {
                if (file_exists(ROOT_DIR . $file)) {
                    //dump('included: ' . $file);
                    include ROOT_DIR . $file;
                }
            }
        }
        $this->loaded = true;
    }

    /**
     * @param $namespace
     * @return bool
     */
    public function addNamespace($namespace)
    {
        $dir = strtr($namespace, '\\', DIRECTORY_SEPARATOR);
        if (is_dir(ROOT_DIR . $dir)) {
            $this->namespacesMap[$namespace] = $dir;
            return true;
        }

        return false;
    }

    /**
     * @param bool $prepend
     */
    public function register($prepend = false)
    {
        $reg = array($this, 'autoload');
        spl_autoload_register($reg, true, $prepend);
    }

    /**
     *
     */
    public static function Init()
    {

        self::$instance = new Autoloader();
    }

    /**
     * @return bool
     */
    public static function isLoaded(): bool
    {
        return self::$instance->loaded;
    }

    /**
     * @param $class
     */
    private function autoload($class)
    {
        $filePath = strtr($class, '\\', DIRECTORY_SEPARATOR) . '.php';

        //dump($class, $this->namespacesMap, $filePath);

        $subPath = $class;
        while (false !== $lastPos = strrpos($subPath, '\\')) {
            $subPath = substr($subPath, 0, $lastPos);
            $search = $subPath . '\\';
            if (isset($this->namespacesMap[$search])) {
                $file = ROOT_DIR . $filePath;
                if (file_exists($file)) {
                    include $file;
                    return;
                }
            }
        }
    }
}
