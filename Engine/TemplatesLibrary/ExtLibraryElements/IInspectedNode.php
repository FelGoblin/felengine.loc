<?php

namespace Engine\TemplatesLibrary\ExtLibraryElements;

/**
 * Исследуемый узел (файл конфигурации)
 * Class IInspectedNode
 * @package Engine\TemplatesLibrary\SiteComponents
 */
interface IInspectedNode
{
    // --- CONST ---
    /**
     * As ARRAY
     *  <?php
     *      return [
     *  ...
     *  ]
     * ?>
     */
    const AS_ARRAY = 0;
    /**
     * AS JSON Format
     *  {
     *      ...
     *  }
     */
    const AS_JSON = 1;
    /**
     * Serialize ARRAY
     */
    const AS_SERIALIZE = 2;

    /**
     *
     */
    const AS_AUTO = -1;
    /**
     * Текущий ID узла (файла) - уникальное имя
     * @return string
     */
    public function getId(): string;

    /**
     * Имя узла в текущем классе инспекции.
     * @return string
     */
    public function getNodeName(): string;

    /**
     * Текущий файл конфигурации.
     * @return string
     */
    public function getFile(): string;

    /**
     * Расширение файла (он же тип сохраняемой конфигурации)
     * @return string
     */
    public function getExt(): string;

    /**
     * Массив иследуемых параметров.
     * @return array
     */
    public function getParam(): array;
}
