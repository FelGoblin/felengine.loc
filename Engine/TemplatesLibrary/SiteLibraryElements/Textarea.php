<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Textarea extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Textarea', 'Textarea');
    }
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeTextarea($data['text'] ?? '', $data);
    }
}