<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Tabs extends TplLibrary
{
    protected static $required = [
        'Tabs' => [
            'tabs'
        ],
        'Tabs.Tab' => [
            'name',
            'id',
            'label',
            'value',
            'checked',
            'content',
            'contentId'
        ]
    ];

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Tabs', 'Tabs');
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function build(array $data = []): string
    {
        $data = self::extract(self::$required['Tabs'], $data);
        //dump($data);
        $_data = [];
        foreach ($data['tabs'] ?? [] as $tab) {
            $tab = self::extract(self::$required['Tabs.Tab'], $tab);
            $_data['tabs'][] = $tab;
        }
        $_data['tabId'] = self::$id++;
        //dump_trace($_data);
        return parent::customTemplate('library.tabs', $_data);
    }
}