<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Wrapper extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Wrapper', 'Wrapper');
    }
    protected static $required =  [
        'type',
        'class',
        'id'
    ];
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeWrapper($data['content'] ?? '', $data);
    }
}