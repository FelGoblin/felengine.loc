<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Link extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Link', 'Link');
    }
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeLink($data['link'], $data['text'], $data);
    }
}