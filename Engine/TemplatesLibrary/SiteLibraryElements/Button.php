<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Button extends TplLibrary
{

    protected static $required = [
        'name',
        'debug',
        'id',
        'tag',//a, button, input, span, div
        'type',
        'link',
        'hint',
        'extHint',
        'hintClass',
        'class',
        'label',
        'text',
        /*
        'textWrapper',
        'textWrapperClass',
        */
        'disable',
        'value',
        'icon',
        'image',
        'imageClass',
        'checked',
        'content',
        'onClick'
    ];

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Button', 'Button');
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @param bool $dump
     * @return string
     */
    public static function build(array $data = []): string
    {
        $buttonData = self::extract(self::$required, $data);
        $data = ['data' => $buttonData];
        return parent::customTemplate('library.button', $data);
    }
}