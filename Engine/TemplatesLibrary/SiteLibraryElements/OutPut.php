<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

/**
 * Class OutPut
 * @package Engine\TemplatesLibrary\SiteLibraryElements
 */
final class OutPut extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\OutPut', 'OutPut');
    }
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeOutPut(($data['content'] ?? ''), $data);
    }
}