<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

/**
 * Class CList
 * @package Engine\TemplatesLibrary\SiteLibraryElements
 */
final class CList extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\CList', 'CList');
    }
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeList($data);
    }
}