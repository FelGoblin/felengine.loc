<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;

use Engine\TemplatesLibrary\TplLibrary;

/**
 * Class HintDialog
 * @package Engine\TemplatesLibrary\SiteLibraryElements
 */
class HintDialog extends TplLibrary
{
    protected static $required = [
        'class', //класс диалога
        'classOverlay', //класс оверлдея диалога
        'classDialog', //кдасс активируемого диалога
        'classDialogHeader', //класс хаголовка диалога
        'classBtnClose', //класс кнопки закрытия
        'closeButton', //код (html) кнопки закрытия
        'controls',//дополнительные кнопки
        //'closeButtonAsOverlay',
        'id', //id
        'replace', //кнопка закрытия должна быть помещена в метку в шаблоне -> {js:closeDialog}
        'content', //контент, который будет всё это активировать
        'hint', //подсказка, которая будет появляться пари наведении на контент
        'dialogHeader', //заголовок диалога при клике/тапе на контенте
        'dialog' //диалог при клике/тапе на контенте
    ];
    /**
     * Кнопка закрытия
     *  [представляет из себя Button::build([...])];
     *  Ну или просто голый HTML
     * @var null|string
     */
    private static $_closeButton = null;

    /**
     * {@inheritDoc}
     */
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\HintDialog', 'HintDialog');
    }

    /**
     * {@inheritDoc}
     * @param array $data
     * @param string|null $tpl
     * @return string
     */
    public static function build(array $data = [], string $tpl = null): string
    {
        $data = self::extract(self::$required, $data);
        //dump($data);
        $id = _assert([$data, 'id'], '$1', 'h-dialog-' . static::$id++);
        $data['id'] = $id;
        if (isset($data['dialog'])) {
            //$data['controls'] = $data['controls'] ?? [];
            //передаём кнопку закрытия окна. Она может быть как Button::build([...]) так и обычным HTML <button>...</button>(в этом случе весь контроль на пользователе)
            if (isset($data['closeButton'])) {
                self::closeButton($data['closeButton']);
            }

            if (is_null(self::$_closeButton)) { //нет кнопки, не передавали.
                $button = Button::build([
                    'class' => 'hint--top ' . ($data['classBtnClose'] ?? ''),
                    'hint' => 'Закрыть диалог.',
                    'text' => ' Закрыть',
                    'icon' => 'close',
                    'onClick' => self::onClick($id)
                ]);
            } else if (is_array(self::$_closeButton)) { //так же можно передать массив с параметрами для Button::build()
                /** @var array $button */
                $button = self::$_closeButton;
                if (!isset($button['onClick'])) {//нет обработчика для клика по кнопке
                    $button['onClick'] = self::onClick($id);
                }
                $button = Button::build($button);
            } else {
                $button = self::$_closeButton;
            }
            //Так же можно отправить метку в шаблон и при сборке она будет перезаписана скомпилированным кодом кнопки.
            if (_assert([$data, 'replace'])) {
                $data['dialog'] = str_replace('{js:closeDialog}', self::onClick($id), $data['dialog']);
            } else {
                $data['controls'][] = $button;
            }
        }
        //dump($data);
        return parent::customTemplate(($tpl ?? 'library.hint-dialog'), $data);
    }

    /**
     * Сохранение информации о кнопке закрытия
     * @param $button
     */
    public static function closeButton($button): void
    {
        self::$_closeButton = $button;
    }

    /**
     * Стандартный обработчик клика по кнопке закрытия (просто снимаем фокус с элемента)
     * @param $id
     * @return string
     */
    private static function onClick($id): string
    {
        return "return (function(){let e=document.getElementById('$id');e.focus();e.blur();return false;})()";
    }
}
