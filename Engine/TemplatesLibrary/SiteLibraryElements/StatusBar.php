<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class StatusBar extends TplLibrary
{
    /**
     * @var StatusBar
     */
    protected static $instance;
    protected static $required = [
        'name',
        'id',
        'type',//a, button, submit, sbutton, span, div
        'class',
        'label',
        'value',
        'icon',
        'checked',
        'content'
    ];
    /**
     * @var array
     */
    private $elements = [];
    private $data = [];

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\StatusBar', 'StatusBar');
    }

    /**
     * @param array $data
     * @param array $elements
     */
    public static function quick(array $data, array $elements = []): void
    {
        self::create($data);
        foreach ($elements as $element) {
            self::addStatusPanel($element);
        }
        self::display();
    }

    /**
     * @param array $data
     */
    public static function create(array $data = [])
    {
        self::$instance = new StatusBar();
        self::$instance->data = $data;
    }

    /**
     * @param string $elementData
     */
    public static function addStatusPanel(string $elementData): void
    {
        //dump($elementData, self::$instance->elements);
        self::$instance->elements[] = $elementData;
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function build(array $data = []): string
    {
        $data = self::extract(self::$required, static::$instance->data);
        $data['elements'] = self::$instance->elements;
        return parent::customTemplate('library.statusbar', $data);
    }
}