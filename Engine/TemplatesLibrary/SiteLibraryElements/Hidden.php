<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Hidden extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Hidden', 'Hidden');
    }
    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeHidden($data);
    }
}