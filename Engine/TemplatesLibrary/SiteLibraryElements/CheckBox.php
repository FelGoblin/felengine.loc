<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class CheckBox extends TplLibrary
{

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\CheckBox', 'CheckBox');
    }

    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeCheckBox($data);
    }
}