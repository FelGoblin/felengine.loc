<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;

use Engine\TemplatesLibrary\TplLibrary;

final class Spoiler extends TplLibrary
{
    protected static $required = [
        'class',
        'header',
        'headerClass',
        'spoilerBtn',
        'onclick',
        'content',
        'contentClass',
        'close'
    ];

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Spoiler', 'Spoiler');
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function build(array $data = []): string
    {
        $data = self::extract(self::$required, $data);
        return parent::customTemplate('library.spoiler', $data);
    }
}