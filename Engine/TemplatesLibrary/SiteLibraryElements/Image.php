<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;

use Engine\TemplatesLibrary\TplLibrary;

final class Image extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Image', 'Image');
    }

    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        return self::makeImage($data);
    }
}
