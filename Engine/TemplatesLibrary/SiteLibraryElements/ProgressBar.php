<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

/**
 * Class ProgressBar
 * @package Engine\TemplatesLibrary\SiteLibraryElements
 */
final class ProgressBar extends TplLibrary
{
    protected static $required = [
        'name',
        'id',
        'progress',
        'type', //normal [default], steps
        'steps', //array of steps
        'step',
        'class',
        'textPosition',//none [default], insideMain, bottomMain, topMain, insideProgress
        'text',
        'textType'
    ];

    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {
        $progBarData = self::extract(self::$required, $data);
        $data = ['data' => $progBarData];
        return parent::customTemplate('library.progressbar', $data);
    }

    /**
     * @inheritDoc
     */
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\ProgressBar', 'ProgressBar');
    }
}