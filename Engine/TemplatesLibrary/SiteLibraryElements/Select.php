<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\TemplatesLibrary\TplLibrary;

final class Select extends TplLibrary
{
    private $type = 'normal';

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Select', 'Select');
    }

    /**
     * @inheritDoc
     */
    public static function build(array $data = []): string
    {

        if (self::assert([$data, 'type', 'HTML'])) {
            $select = self::extract(self::$required['Select'], $data);
            $selectList = self::extract(self::$required['Select.List'], $data);
            return parent::customTemplate('library.select', ['select' => $select]);
        } else {
            return self::makeSelect($data);
        }
    }
}