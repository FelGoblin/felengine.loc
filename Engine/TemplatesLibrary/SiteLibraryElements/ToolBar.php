<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\Exceptions\LibraryBlockException;
use Engine\Exceptions\SiteComponentException;
use Engine\TemplatesLibrary\TplLibrary;
use Exception;
use ReflectionException;

final class ToolBar extends TplLibrary
{
    /**
     * @var ToolBar
     */
    protected static $instance;
    protected static $required = [
        'name',
        'id',
        'type',//a, button, submit, sbutton, span, div
        'class',
        'label',
        'value',
        'icon',
        'checked',
        'content',
        'onClick'
    ];
    /**
     * @var array
     */
    private $elements = [];
    private $data = [];

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\ToolBar', 'ToolBar');
    }

    /**
     * @param array $data
     * @param array $elements
     * @param bool $display
     * @return string
     */
    public static function quick(array $data, array $elements = [], bool $display = true): ?string
    {
        try {
            self::create($data);
            foreach ($elements as $element) {
                self::addElement($element);
            }
            if ($display) {
                self::display();
            } else {
                return self::build();
            }
        } catch (Exception $ex) {
            if ($display) {
                echo $ex->getMessage();
            }
            return '';
        }
        return null;
    }

    /**
     * @param array $data
     */
    public static function create(array $data = [])
    {
        self::$instance = new ToolBar();
        self::$instance->data = $data;
    }

    /**
     * @param string $elementData
     */
    public static function addElement(string $elementData): void
    {
        //dump($elementData, self::$instance->elements);
        self::$instance->elements[] = $elementData;
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function build(array $data = []): string
    {
        $data = self::extract(self::$required, static::$instance->data);
        $data['elements'] = self::$instance->elements;
        return parent::customTemplate('library.toolbar', $data);
    }
}
