<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;

use Engine\TemplatesLibrary\TplLibrary;

final class Breadcrumb extends TplLibrary
{

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Breadcrumb', 'Breadcrumb');
    }

    protected static $required = [
        'Breadcrumb' => [
            'links'
        ],
        'Breadcrumb.Link' => [
            'title',
            'path',
            'active'
        ]
    ];

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     */
    public static function build(?array $data = []): string
    {
        $data = self::extract(self::$required['Breadcrumb'], $data);
        return parent::customTemplate('library.breadcrumb', $data);
    }
}