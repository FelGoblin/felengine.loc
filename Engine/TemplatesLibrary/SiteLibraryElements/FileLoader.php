<?php


namespace Engine\TemplatesLibrary\SiteLibraryElements;


use Engine\Exceptions\LibraryBlockException;
use Engine\Exceptions\SiteComponentException;
use Engine\TemplatesLibrary\TplLibrary;
use ReflectionException;

final class FileLoader extends TplLibrary
{
    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\FileLoader', 'FileLoader');
    }
    protected static $required = [
        'icon',
        'value',
        'label',
        'uploadDir',
        'randomName',
        'onchange',
        'currentFile'
    ];

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     * @throws LibraryBlockException
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public static function build(array $data = []): string
    {
        $data = self::extract(self::$required, $data);
        $data['id'] = static::$id++;
        return parent::customTemplate('library.fileloader', $data);
    }
}