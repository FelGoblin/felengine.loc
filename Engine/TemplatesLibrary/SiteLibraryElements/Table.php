<?php

namespace Engine\TemplatesLibrary\SiteLibraryElements;

use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\LibraryBlockException;
use Engine\Exceptions\SiteComponentException;
use Engine\TemplatesLibrary\TplLibrary;
use ReflectionException;

final class Table extends TplLibrary
{
    /**
     * @var
     */
    protected static $instance;
    /**
     * @var int
     */
    private static $cuid = -1;
    /**
     * @var array
     */
    private $table = [
        'headers' => [],
        'rows' => []
    ];
    private $config = [];
    private $pos = [
        'row' => -1,
        'col' => -1
    ];
    private $footer = null;
    /**
     * @var string
     */
    private $rowClass = [];
    private $fixRowStyle = false;

    /**
     * Table constructor.
     * @param array $data
     * @param $config
     */
    public function __construct(array $data, $config)
    {
        self::$cuid++;
        $this->table['headers'] = $data;
        //$this->config['class'] = self::assert([$config, 'class'], '$1');
        $this->config = $config;
        $this->config['cols'] = count($data);
        parent::__construct(false);
        self::$instance[self::$cuid] = $this;
    }

    public static function alias(): void
    {
        class_alias('Engine\\TemplatesLibrary\\SiteLibraryElements\\Table', 'Table');
    }

    /**
     * Установить стиль для строки в таблице.
     * @param string $class
     * @param bool $fix - Зафиксировать. Стиль будет добавляться до тех пор,
     *  пока не будет дана команда Table::setRowClass();
     */
    public static function setRowClass(string $class = null, bool $fix = false): void
    {
        /** @var Table $table */
        $table = self::$instance[self::$cuid];
        $table->rowClass[$table->pos['row']] = $class ?? '';
        $table->fixRowStyle = $fix;
    }

    /**
     * @param array $table_headers
     * @param null $config
     */
    public static function create(array $table_headers, $config = null): void
    {
        new Table($table_headers, $config);
    }

    /**
     * @param string $text
     * @param $note
     * @param array $data
     * @return array
     */
    public static function addTextField(string $text, $note = null, array $data = []): array
    {
        $field = self::makeText($text, $note, $data);
        return self::addField($field);
    }

    /**
     * Добавить поле. Поля добавляются по порядку, начиная с первой колонки.
     * @param string $fieldData
     * @param bool $autoWalk
     * @return array
     */
    public static function addField(string $fieldData, bool $autoWalk = true): array
    {
        /** @var Table $table */
        $table = self::$instance[self::$cuid];

        if ($table->pos['row'] == -1) {
            $table->pos['row'] = count($table->table['rows']);
        }
        if ($table->pos['col'] == -1) {
            $table->pos['col'] = 0;
        }
        if (!isset($table->table['rows'][$table->pos['row']])) {
            $table->table['rows'][$table->pos['row']] = [];
        }
        //dump($table->pos, $table->table['rows']);
        $table->table['rows'][$table->pos['row']][$table->pos['col']] = trim($fieldData);
        $res = $table->pos;
        if ($autoWalk) {
            if ($table->pos['col']++ >= count($table->table['headers']) - 1) {
                //$table->pos['row']++;
                $table->newRow();
                $table->pos['col'] = 0;
            }
        }
        return $res;
    }

    /**
     *
     */
    private function newRow(): void
    {
        $this->pos['col'] = 0;
        $this->pos['row']++;
        if ($this->fixRowStyle) {
            $class = $this->rowClass[$this->pos['row'] - 1] ?? '';
            $this->rowClass[$this->pos['row']] = $class;
        }
    }

    /**
     * @param string $innerText
     * @param array $options
     * @return array
     */
    public static function addWrapperField(string $innerText, array $options = []): array
    {
        $field = self::makeWrapper($innerText, $options);
        return self::addField($field);
    }

    // ----------------------FIELDS ----------------------

    /**
     * @param array $data
     * @param bool $closeGroup
     * @return array
     */
    public static function addRadioField(array $data, bool $closeGroup): array
    {
        $table = self::$instance[self::$cuid];
        $field = self::makeRadio($data);
        $_field = self::getField($table->pos);
        $_field .= self::assert(!empty($_field), '<br />');
        $field = $_field . $field;
        return self::addField($field, $closeGroup);
    }

    /**
     * Получить содержимое ячейки
     * @param int $row
     * @param int $col
     * @return string|null
     */
    public static function getField(): ?string
    {
        $table = self::$instance[self::$cuid];
        if (is_array(func_get_arg(0))) {
            $pos = func_get_arg(0);
            $col = $pos['col'];
            $row = $pos['row'];
        } else {
            list($row, $col) = func_get_args();
        }
        return $table->table['rows'][$row][$col] ?? null;
    }

    /**
     * @param string $link
     * @param string $text
     * @param array $data
     * @return array
     */
    public static function addLinkField(string $link, string $text, array $data = []): array
    {
        $field = self::makeLink($link, $text, $data);
        return self::addField($field);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function addSelectField(array $data): array
    {
        $field = self::makeSelect($data);
        return self::addField($field);
    }

    /**
     * @param string $text
     * @param array $data
     * @return array
     */
    public static function addButtonField(string $text, array $data): array
    {
        $field = self::makeButton($text, $data);
        return self::addField($field);
    }

    /**
     * @param string $text
     * @param array $data
     * @return array
     */
    public static function addTextareaField(?string $text, array $data): array
    {
        $field = self::makeTextarea($text ?? '', $data);
        return self::addField($field);
    }

    /**
     * @param array $data
     * @param bool $asString
     * @return array | string
     * @throws LibraryBlockException
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public static function addFileLoaderField(array $data, bool $asString = false)
    {
        $field = FileLoader::build($data);
        if ($asString) {
            return $field;
        }
        return self::addField($field);
    }

    /**
     *
     */
    public static function addFooter($field)
    {
        /** @var Table $table */
        $table = self::$instance[self::$cuid];
        $table->footer = $field;
    }

    /**
     * Добавить строки
     * @param array $row
     * @throws LibraryBlockException
     */
    public static function addRow(array $row): void
    {
        /** @var Table $table */
        $table = self::$instance[self::$cuid];
        if (count($table->table['headers']) != count($row)) {
            throw new LibraryBlockException('Количество стобцов должно быть равно количеству заголовков');
        }
        $table->table['rows'][] = $row;
        $table->newRow();
        $table->fieldCounter = 0; //Ресетим счётчик. TODO: возможно сделать автозаполнение недостающих полей null`ом
    }

    /**
     * Вывести содержимое.
     * @param array $data
     * @return string
     * @throws LibraryBlockException
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public static function build(array $data = []): string
    {
        /** @var Table $table */
        $table = self::$instance[self::$cuid];
        self::$cuid--;
        return parent::_build(SiteComponent::rawQuick(SiteComponent::findPath($data['templateLabel'] ?? 'library.table'), $table->getData()));
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return array_merge($this->table, $this->config, ['rowClass' => $this->rowClass, 'footer' => $this->footer]);
    }

    // ----------------------END FIELDS ----------------------

    public static function addAutoField($data, array $params = []): void
    {
        if (is_numeric($data)) {
            self::addNumberField($data, $params);
        } elseif (is_bool($data)) {
            self::addCheckBoxField(array_merge($params, ['checked' => $data, 'label' => 'BOOLEAN']));
        } else {
            self::addInputTextField(array_merge($params, ['value' => $data]));
        }
    }

    /**
     * @param int|null $value
     * @param array $data
     * @return array
     */
    public static function addNumberField(?int $value = null, array $data = []): array
    {
        $field = self::makeNumber($value, $data);
        return self::addField($field);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function addCheckBoxField(array $data): array
    {
        $field = self::makeCheckBox($data);
        return self::addField($field);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function addInputTextField(array $data): array
    {
        $field = self::makeInputText($data);
        return self::addField($field);
    }
}
