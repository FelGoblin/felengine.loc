<?php

namespace Engine\TemplatesLibrary;


use Engine\Core\View\SiteComponent\SiteComponent;
use Engine\Exceptions\LibraryBlockException;
use Engine\Exceptions\SiteComponentException;
use Exception;
use ReflectionException;

/**
 * Class TplLibrary
 * @package Engine\TemplatesLibrary
 */
abstract class TplLibrary implements ITplLibrary
{
    /**
     * @var TplLibrary
     */
    protected static $instance;
    protected static $required = [
        'Text' => [
            'class',
            'icon',
            'id',
            'hint',
            'required'
        ],
        'CheckBox' => [
            'class',
            'id',
            'label',
            'name',
            'value',
            'labelPos',
            'labelClass',
            'checked',
            'onchange',
            'icon',
            'iconHint',
            'note',
            'required',
            'hint'
        ],
        'Radio' => [
            'class',
            'id',
            'label',
            'name',
            'value',
            'labelPos',
            'labelClass',
            'checked',
            'group',
            'onchange',
            'icon',
            'iconHint',
            'note',
            'hint'
        ],
        'Link' => [
            'class',
            'onclick',
            'hint',
            'note',
            'icon',
            'iconHint'
        ],
        'InputText' => [
            'class',
            'id',
            'onchange',
            'onkeydown',
            'onkeyup',
            'type',
            'value',
            'type',
            'name',
            'note',
            'hint',
            'placeholder'
        ],
        'Select' => [
            'class',
            'id',
            'attr',
            'onchange',
            'name',
            'list',
            'note',
            'type',//Normal, HTML
            'hint'
        ],
        'Select.List' => [
            'value',
            'class',
            'style',
            'attr',
            'selected',
            'disable',
            'label'
        ],
        'Button' => [
            'class',
            'id',
            'onclick',
            'value',
            'name',
            'icon',
            'type',
            'note',
            'hint'
        ],
        'Textarea' => [
            'class',
            'textareaClass',
            'id',
            'onchange',
            'onkeydown',
            'onkeyup',
            'placeholder',
            'name',
            'icon',
            'header',
            'note'
        ],
        'Number' => [
            'class',
            'id',
            'onchange',
            'placeholder',
            'name',
            'icon',
            'label',
            'labelClass',
            'labelPos',
            'note',
            'step',
            'min',
            'max'
        ],
        'Image' => [
            'id',
            'class',
            'src',
            'alt',
            'onLoad',
            'onClick',
            'title'
        ],
        'List' => [
            'class',
            'id',
            'type',
            'list',
            'note'
        ],
        'List.List' => [
            'type',
            'class',
            'id',
            'value'
        ],
        'Wrapper' => [
            'type',
            'class',
            'id'
        ],
        'OutPut' => [
            'class',
            'id',
            'icon',
            'header',
            'content'
        ],
        'ParamValue' => [
            'inspect',
            'class',
            'id'
        ]
    ];
    protected static $id = 0;

    /**
     * TemplatesLibrary constructor.
     * @param bool $createInstance
     */
    public function __construct(bool $createInstance = true)
    {
        if ($createInstance) {
            self::$instance = $this;
        }
    }

    /**
     * Получить список всех элементов данной библиотеки
     * @return array
     */
    public static function getElmenets(): array
    {
        $keys = array_keys(self::$required);
        $res = [];
        foreach ($keys as $key) {
            if (($i = strpos($key, '.')) === false) {
                $res[] = [
                    'method' => 'Html::make' . $key,
                    'allow-prms' => 'Html::getRequire(\'' . $key . '\')'
                ];
            } else {
                $res[] = [
                    'for_method' => 'Html::make' . substr($key, 0, $i),
                    'allow-prms' => 'Html::getRequire(\'' . $key . '\')'
                ];
            }
        }
        return $res;
    }

    /**
     * Получить все требующие для данного блока зависимости
     * @return array
     */
    public static function getRequire(): array
    {
        $elements = func_get_args();
        if (!empty($elements)) {
            $element = $elements[0];
        } else {
            $element = 0;
        }
        if (get_called_class() !== 'Html' && $element === 0) {
            return static::$required;
        }
        return self::$required[$element] ?? ['not found'];
    }

    /**
     * @param string $text
     * @param null $note
     * @param array $data
     * @return string|null
     */
    public static function makeText(string $text, $note = null, array $data = []): ?string
    {
        $data = self::extract(self::$required['Text'], $data);
        $note = self::getNote($note);
        $icon = self::getIcon($data);
        $required = self::getRequired($data);
        $field = sprintf(
            '<span %s %s %s>%s</span> %s',
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'id'], ' id="checkbox_$1"'),
            self::assert([$data, 'hint'], ' title="$1"'),
            $text,
            $note
        );
        return $icon . $field . $required;
    }

    /**
     * @param $marker
     * @param $source
     * @return array
     */
    protected static function extract($marker, $source): array
    {
        $r = [];
        if (empty($source)) {
            return $r;
        }
        //dump($marker);
        foreach ($marker as $v) {
            $r[$v] =
                $source[$v] ?? null;
        }
        return $r;
    }

    /**
     * @param null $note
     * @return string|null
     */
    private static function getNote($note = null): ?string
    {
        return self::assert(!empty($note), function () use ($note) {
            $result = '';//'<ul class="note">';
            if (is_array($note)) {
                foreach ($note as $n) {
                    $result .= "<div class=\"note\">$n</div>";
                }
            } else {
                $result .= "<div class=\"note\">$note</div>";
            }
            return $result;// . '</ul>';
        });
    }


    /**
     * Кастомный assert
     * @param $condition
     * @param $trueResult
     * @param string $falseResult
     * @param bool $dump
     * @return string|null
     */
    public static function assert($condition, $trueResult = true, $falseResult = false, bool $dump = false): ?string
    {
        //dump($condition, $trueResult, $falseResult);
        return _assert($condition, $trueResult, $falseResult, $dump);
    }

    /**
     * @param array $data
     * @return string
     */
    private static function getIcon(array $data): string
    {
        //$data['iconHint'] = $data['iconHint'] ?? null;
        $hint = self::assert(
            [$data, 'iconHint'],
            ' title="$1"'
        );
        return !empty($data['icon']) ? "<icon class=\"{$data['icon']} $hint\"></icon> " : '';
    }

    private static function getRequired(array $data): string
    {
        if ($data['required'] ?? null) {
            return '<span class="required">*</span>';
        }
        return '';
    }

    // ------------

    /**
     * @param array $data
     */
    public static function display(array $data = []): void
    {
        try {
            echo static::build($data);
        } catch (Exception $ex) {
            echo "Build error: [<b>{$ex->getMessage()}</b>]";
        }
    }

    /**
     * Ячейка таблицы - чекбокс
     * @param array $data
     *      [
     *          'class'=>Имя класса
     *          'id'=> Ид чекбокса
     *          'label'=> Дейб чекбокса
     *          'labelPos'=> after / before
     *          'checked'=> true/ (false or null) - выделено
     *          'onchange'=> JS function
     *      ]
     * @return array
     */
    public static function makeCheckBox(array $data): string
    {
        $data = self::extract(self::$required['CheckBox'], $data);
        $note = self::getNote($data['note']);
        //Хитрый костыль. Создаём возле чекбокса hidden элемент с именем чекбокса. Если чекбока не выделен, его нет в POST,  а значит прилетит из hidden
        //если выделим, то значение чекбокса перекроет значение hidden
        $field = sprintf(
            "<input type=\"hidden\" %s%s/>\n",
            self::assert([$data, 'name'], ' name="$1"'),
            ' value="off"'
        );
        $field .= sprintf(
            '<input type="checkbox" %s%s%s%s%s%s%s />',
            self::assert([$data, 'class'], ' class="$1"'),
            self::assert([$data, 'id'], ' id="$1"'),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'value'], ' value="$1"'),
            self::assert([$data, 'onchange'], ' onChange="$1"'),
            self::assert([$data, 'checked'], ' checked="checked"'),
            self::assert([$data, 'hint'], ' title="$1"')
        );
        $field = self::assLabelFor($data, $field);//+2 параметра туда уходят
        $field .= $note;
        return $field;
    }

    /**
     * @param array $data
     * @param $field
     * @return string
     */
    private static function assLabelFor(array $data, $field): string
    {
        if (empty($data['label'])) {
            return $field;
        }
        $icon = self::getIcon($data);
        $labelClass = _assert([$data, 'labelClass'], ' class="$1"', '');
        if (!empty($data['id'])) {
            $label = "<label for=\"{$data['id']}\">{$icon}{$data['id']}</label>";
            if (($data['labelPos'] ?? 'after') == 'before') {
                $field = $label . ' ' . $field;
            } else {
                $field = $field . ' ' . $label;
            }
        } else {
            if (($data['labelPos'] ?? 'after') == 'before') {
                $field = "<label$labelClass>{$icon}{$data['label']} $field</label>";
            } else {
                $field = "<label$labelClass>$field {$icon}{$data['label']}</label>";
            }
        }
        return $field;
    }

    // -----------  HTML -------------

    /**
     * Обёртка
     * @param string $content
     * @param array $data
     * @return array
     */
    public static function makeWrapper(string $content, array $data): string
    {
        $data = self::extract(self::$required['Wrapper'], $data);
        $type = self::assert([$data, 'type'], '$1', 'div');
        $field = sprintf(
            "<$type %s%s>%s</$type>",
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'id'], "id=\"$1\""),
            $content);
        return $field;
    }

    /**
     * Hidden
     * @param array $data
     *  [
     *      'name' => Имя элемента
     *      'value' => Значение элемента
     *  ]
     * @return string
     */
    public static function makeHidden(array $data): string
    {
        $field = sprintf(
            '<input type="hidden" %s %s />',
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'value'], ' value="$1"')
        );
        return $field;
    }

    /**
     * Ячейка таблицы - чекбокс
     * @param array $data
     *      [
     *          'class'=>Имя класса
     *          'id'=> Ид чекбокса
     *          'label'=> Дейб чекбокса
     *          'labelPos'=> after / before
     *          'checked'=> true/ (false or null) - выделено
     *          'onchange'=> JS function
     *      ]
     * @return array
     */
    public static function makeRadio(array $data): string
    {
        $data = self::extract(self::$required['Radio'], $data);
        $note = self::getNote($data['note']);
        $field = sprintf(
            '<input type="radio" %s%s%s%s%s%s%s/>',
            self::assert([$data, 'class'], ' class="$1"'),
            self::assert([$data, 'id'], "id=\"$1\""),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onchange'], ' onChange="$1"'),
            self::assert([$data, 'checked'], 'checked="checked"'),
            self::assert([$data, 'value'], ' value="$1"'),
            self::assert([$data, 'hint'], ' title="$1"')
        );
        $field = self::assLabelFor($data, $field);//+2 параметра туда уходят
        $field .= $note;
        return $field;
    }

    /**
     * Картинка
     * @param array $data
     * @return string
     */
    public static function makeImage(array $data): string
    {
        $data = self::extract(self::$required['Image'], $data);
        $field = sprintf(
            '<img src="%s" %s%s%s%s%s%s />',
            self::assert([$data, 'src'], '$1'),
            self::assert([$data, 'id'], ' id="$1"'),
            self::assert([$data, 'class'], ' class="$1"'),
            self::assert([$data, 'onClick'], ' onClick="$1"'),
            self::assert([$data, 'onLoad'], ' onLoad="$1"'),
            self::assert([$data, 'alt'], ' alt="$1"', 'alt="img"'),
            self::assert([$data, 'title'], ' title="$1"'),
        );
        return $field;
    }

    /**
     * Ячейка таблицы - ссылка
     * @param string $link - ссылка
     * @param string $text - текст ссылки
     *      [
     *          'class'=>Класс ссылки
     *          'onclick'=> JS function
     *           "hint'=>Подсказка ссылки
     *          'icon' => имя класса иконски (<i class=""></i>)
     *          'iconHint' => подсказка иконки
     *      ]
     * @param array $data
     * @return array
     */
    public static function makeLink(string $link, string $text, array $data = []): string
    {
        $data = self::extract(self::$required['Link'], $data);
        $note = self::getNote($data['note'] ?? '');
        $field = sprintf(
            '<a href="%s" %s %s %s>%s%s</a>',
            $link,
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'onclick'], "onClick=\"$1\""),
            self::assert([$data, 'hint'], ' title="$1"'),
            self::getIcon($data),
            $text
        );
        $field .= $note;
        return $field;
    }

    /**
     * Вставить поле с текстом ввода
     * @param array $data
     *  [
     *       'class' => класс поля ввода
     *        'id' => ID элемента
     *       'onchange' => событие onChange JS,
     *       'value' => значение по-умолчанию,
     *       'name' => имя поля ввода,
     *       'hint'=> подсказка поля ввода
     *       'placeholder' => плейсхолдер
     *   ]
     * @return array
     */
    public static function makeInputText(array $data): string
    {
        $data = self::extract(self::$required['InputText'], $data);
        $note = self::getNote($data['note'] ?? '');
        $field = sprintf(
            '<input type="%s" %s%s%s%s%s%s%s%s%s />',
            self::assert([$data, 'type'], '$1', "text"),
            self::assert([$data, 'class'], ' class="$1"'),
            self::assert([$data, 'id'], ' id="$1"'),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onchange'], ' onChange="$1"'),
            self::assert([$data, 'onkeydown'], ' onKeydown="$1'),
            self::assert([$data, 'onkeydown'], ' onKeydown="$1"'),
            self::assert([$data, 'value'], ' value="$1"'),
            self::assert([$data, 'placeholder'], ' placeholder="$1"'),
            self::assert([$data, 'hint'], ' title="$1"'),
        );
        $field = self::assLabelFor($data, $field);//+2 параметра туда уходят
        $field .= $note;
        return $field;
    }

    /**
     * Добавить поле с выпадающим меню
     * @param array $data
     * @return string
     */
    public static function makeSelect(array $data): string
    {
        $data = self::extract(self::$required['Select'], $data);
        $note = self::getNote($data['note']);
        $field = sprintf(
            '<select %s%s%s%s%s%s>',
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'id'], ' id="$1"'),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onchange'], ' onChange="$1"'),
            self::assert([$data, 'attr'], $data['attr']),
            self::assert([$data, 'hint'], ' title="$1"')
        );
        $i = 0;
        foreach ($data['list'] ?? [] as $select) {
            if (is_array($select)) {
                $select = self::extract(self::$required['Select.List'], $select);
                $field .= sprintf(
                    '<option %s %s %s %s %s %s>%s</option>',
                    self::assert([$select, 'value'], "value=\"{$select['value']}\""),
                    self::assert([$select, 'class'], "class=\"{$select['class']}\""),
                    self::assert([$select, 'style'], "class=\"{$select['style']}\""),
                    self::assert([$select, 'selected'], "selected"),
                    self::assert([$select, 'disable'], "disabled"),
                    self::assert([$select, 'attr'], $select['attr']),
                    self::assert([$select, 'label'], $select['label'], $select['value'])
                );
            } elseif (is_string($select)) {
                $field .= sprintf(
                    '<option value="%s">%s</option>',
                    $select,
                    $select);
            } else {
                $field .= sprintf(
                    '<option value="%s">%s</option>',
                    $i,
                    gettype($select));
            }
            $i++;
        }
        $field .= '</select>';
        $field .= $note;
        return $field;
    }

    /**
     * @param string $text
     * @param array $data
     * @return string
     */
    public static function makeButton(string $text, array $data): string
    {
        $data = self::extract(self::$required['Button'], $data);
        $icon = self::getIcon($data);
        $note = self::getNote($data['note']);
        $field = sprintf(
            '<button %s %s %s %s %s %s %s>%s%s</button>',
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'id'], "id=\"text_$1\""),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onclick'], "onClick=\"$1\""),
            self::assert([$data, 'value'], ' value="$1"'),
            self::assert([$data, 'type'], "type=\"$1\""),
            self::assert([$data, 'hint'], ' title="$1"'),
            $icon,
            $text
        );
        $field .= $note; //Для отладки dump($field), но можно сразу возвращать return $field . $note;
        return $field;
    }

    /**
     * @param string $text
     * @param array $data
     * @return string
     */
    public static function makeTextarea(string $text, array $data): string
    {
        $data = self::extract(self::$required['Textarea'], $data);
        $head = self::assert([$data, 'header'], "<span class=\"textarea-header\">$1</span>");
        $note = self::assert([$data, 'note'], "<span class=\"textarea-note\">$1</span>");
        $icon = self::assert([$data, 'icon'], "<icon class=\"$1\"></icon>");

        $labelClass = self::assert([$data, 'class'], "$1");
        if (!empty($icon)) {
            $labelClass .= ' prepend-icon';
        }
        return sprintf(
            '<label %s>%s<textarea %s%s%s%s%s%s%s>%s</textarea>%s%s</label>',
            self::assert($labelClass, 'class="$1"'),
            $head,
            self::assert([$data, 'textareaClass'], 'class="$1"'),
            self::assert([$data, 'id'], ' id="$1"'),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onchange'], ' onChange="$1"'),
            self::assert([$data, 'onkeyup'], ' onKeyup="$1"'),
            self::assert([$data, 'onkeydown'], ' onKeydown="$1"'),
            self::assert([$data, 'placeholder'], ' placeholder="$1"'),
            $text,
            $icon,
            $note
        );
    }

    /**
     * @param string $content
     * @param array $data
     * @return string
     */
    public static function makeOutPut(string $content, array $data): string
    {
        $data = self::extract(self::$required['OutPut'], $data);
        $header = self::assert([$data, 'header'], "$1");
        if ($icon = self::assert([$data, 'icon'], "<icon class=\"$1\"></icon>")) {
            $header = $icon . $header;
        }
        return sprintf(
            '<div class="output%s">%s<div class="outData"%s>%s</div></div>',
            self::assert([$data, 'class'], ' $1'),
            self::assert($header, "<div class=\"header\">$1</div>"),
            self::assert([$data, 'id'], ' id="$1"'),
            $content
        );
    }

    /**
     * @param int|null $value
     * @param array $data
     * @return string
     */
    public static function makeNumber(?int $value = null, array $data = []): string
    {
        $data = self::extract(self::$required['Number'], $data);
        $note = self::getNote($data['note'] ?? null);
        $field = sprintf(
            '<input type="number" %s %s %s %s %s %s %s %s %s />',
            self::assert([$data, 'class'], "class=\"$1\""),
            self::assert([$data, 'id'], "id=\"text_$1\""),
            self::assert([$data, 'name'], ' name="$1"'),
            self::assert([$data, 'onchange'], "onClick=\"$1\""),
            self::assert([$data, 'placeholder'], "placeholder=\"$1\""),
            self::assert([$data, 'step'], "step=\"$1\""),
            self::assert([$data, 'max'], "max=\"$1\""),
            self::assert([$data, 'min'], "min=\"$1\""),
            self::assert($value, "value=\"$value\"")
        );
        $field .= $note; //Для отладки dump($field), но можно сразу возвращать return $field . $note;
        $field = self::assLabelFor($data, $field);
        return $field;
    }

    /**
     * @param array $data
     * @return string
     * @throws LibraryBlockException
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public static function makeList(array $data): string
    {
        $data = self::extract(self::$required['List'], $data);
        $note = self::getNote($data['note']);
        $field = self::customTemplate('library.list', $data);
        $field .= $note;
        return $field;
    }

    /**
     * @param $templateLabel
     * @param array $data
     * @return string
     */
    public static function customTemplate($templateLabel, array $data): string
    {
        return SiteComponent::quick($templateLabel, $data);
    }

    /**
     * @param string $data
     * @param bool $return
     * @return string
     */
    protected static function _build(string $data): string
    {
        self::$instance = null;
        return $data;
    }
}