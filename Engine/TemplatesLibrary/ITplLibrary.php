<?php

namespace Engine\TemplatesLibrary;


interface ITplLibrary
{
    /**
     * полчить содержимое.
     * @param array $data
     * @return string
     */
    public static function build(array $data = []): string;

    /**
     * Вывести содержимое
     * @param array $data
     * @return void
     */
    public static function display(array $data = []): void;

    /**
     * Алиас
     */
    public static function alias(): void;
}
