<?php

namespace Engine\Localization;


use Engine\AdmControl\EngineController;
use Engine\Core\Core;
use Engine\Core\Development\DebugInfo;
use Engine\Core\Objects\CoreElement;
use Engine\Helper\Cookie;
use Exception;
use Object\User\User;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class Localization
 * @package Engine\Localization
 */
final class Localization extends CoreElement
{
    /**
     * @var Localization
     */
    private static $instance;
    private static $loaded = false;
    /**
     * текущая лолкализация
     * @var mixed
     */
    private $lang;
    /**
     * Словарь
     * @var array
     */
    private $dictionary = [];

    /**
     * Localization constructor.
     * @param bool $loadDictionaries
     */
    public function __construct(bool $loadDictionaries = true)
    {
        self::$instance = $this;

        $lang = null;
        try {
            /** @var User $user */
            if ($user = Core::getThisUser() AND $user->getUserData()) {
                //dump($user,$user->getUserData());
                $lang = $user->getUserData()->getLocale();
            }
        } catch (Exception$ex) {
            DebugInfo::debugAddF('<error>%s</error>', $ex->getMessage());
        }
        if ($lang === null && ($lang = Cookie::get('localization') === null)) {
            $lang = (new Lang_detect())->getBestMatch('en', [
                'ru' => ['ru', 'be', 'uk', 'ky', 'ab', 'mo', 'et', 'lv'],
                'de' => 'de'
            ]);
        }
        $this->lang = $lang;

        self::$loaded |= $loadDictionaries;

        $fn = function ($finfo) {
            if ($finfo['ext'] === 'php') {
                $lang = include($finfo['file']);
            } elseif ($finfo['ext'] === 'json') {
                $data = file_get_contents($finfo['file']);
                $lang = json_decode($data, true);
            } else {
                return;
            }
            $locale = $finfo['parent'];
            if (!array_key_exists($locale, $this->dictionary)) {
                $this->dictionary[$locale] = [];
            }
            $this->dictionary[$locale] = array_merge($this->dictionary[$locale], $lang);
        };

        if (self::$loaded) {
            self::loadLanguage($fn);
        }
    }

    /**
     * @param callable|null $cb
     */
    public static function loadLanguage(callable $cb = null): void
    {
        //ресурсы
        self::loadResource(RESOURCES_DIR . 'locale', $cb);
        //из модов
        $files = array_diff(scandir(MOD_DIR), Array(".", ".."));
        foreach ($files as $d) {
            if (is_dir(MOD_DIR . $d) AND is_dir($_d = MOD_DIR . $d . DIRECTORY_SEPARATOR . 'Locale')) {
                self::loadResource($_d, $cb);
            }
        }
    }

    /**
     * @param string|null $dir
     * @param callable|null $cb
     */
    private static function loadResource(string $dir = null, callable $cb = null): void
    {
        if (is_dir(realpath($dir))) {
            $directory = new RecursiveDirectoryIterator(pathSeparator($dir));
            $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
            EngineController::detour($dir, $iterator, $cb);
        }
    }

    /**
     * @return Localization
     */
    public static function getInstance(): Localization
    {
        if (self::$instance == null) {
            self::initialize();
        }
        return self::$instance;
    }

    /**
     * Инициализация
     */
    public static function initialize(bool $loadDictionaries = true): void
    {
        self::$instance = new Localization($loadDictionaries);
    }

    /**
     * @param string $key
     * @param bool $upperFirst
     * @return string
     */
    public function translate(string $key, bool $upperFirst = false): string
    {
        $result = $this->dictionary[$this->lang][$key] ?? null;
        //нет словаря, смотрим возможность взять в английской локализации
        if ($result == null && $this->lang !== 'en') {
            $result = $this->dictionary['en'][$key] ?? null;
        }
        //нет ничего, вернём значение ключа
        if ($result == null) {
            return $key;
        }

        return ($upperFirst ? mb_ucfirst($result) : $result);
    }
}