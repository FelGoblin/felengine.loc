<?php

namespace Engine;

use Engine\Modules\BaseSiteModule;

interface IController
{
    public function main(BaseSiteModule $mod = null, $data = null): ?IController;
}