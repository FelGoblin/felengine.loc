<?php

namespace Engine\Widget;

use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;

interface IWidget
{
    public function build(string $label, array $data = []);

    /**
     * @param string $label
     * @param array $data
     * @param InjectComponentConfig|null $ingect
     * @return IComponent
     */
    public function setWidgetComponent(string $label, array $data = [], InjectComponentConfig $ingect = null): IComponent;
}