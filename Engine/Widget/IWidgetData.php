<?php


namespace Engine\Widget;


interface IWidgetData
{
    /**
     * IWidgetData constructor.
     * @param array $data
     */
    public function __construct(array $data);

    /**
     * Собрать данные виджета
     * @return array
     */
    public function build(): array;
    /**
     * Загружаем данные из сохранённых конфигов в класс
     */
    public function load(): void;

}