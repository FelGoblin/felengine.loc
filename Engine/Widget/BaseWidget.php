<?php

namespace Engine\Widget;


use Engine\Core\Core;
use Engine\Core\DataBase\DataBase;
use Engine\Core\Http\Uri;
use Engine\Core\Objects\IUser;
use Engine\Core\View\ComponentManager;
use Engine\Core\View\Render;
use Engine\Core\View\SiteComponent\IComponent;
use Engine\Core\View\SiteComponent\InjectComponentConfig;
use Engine\Exceptions\AccessException;
use Engine\Exceptions\SiteComponentException;
use Engine\Exceptions\WidgetException;
use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionException;

abstract class BaseWidget implements IWidget
{
    /**
     * @var string
     */
    protected static $widgetService = 'undefined';
    /**
     * @var string
     */
    protected static $widgetType = 'undefined';
    /**
     * @var bool
     */
    protected $doJScript = true;
    /**
     * @var array
     */
    protected $widgetData = ['widgetData' => ''];
    /**
     * @var
     */
    protected string $activeTemplateLabel;

    /**
     * @param null $options
     * @param array $widgetData
     * @param bool $simple
     * @throws AccessException
     * @throws WidgetException
     * @throws Exception
     */
    public function __construct($options = null, array $widgetData = [], bool $simple = false)
    {
        $this->widgetData = array_merge($this->widgetData, $widgetData);

        if ($simple) {
            return;
        }

        $user = null;

        $access = $this->getUserData($options, $user);
        /** @var IUser $u */
        $u = Core::getUser();
        if ($u == null) { //Очень врядли
            throw new AccessException('Системная ошибка определения пользователя.');
        }

        $_user = $u::getUser($user, ['access', 'mods']);
        if ($_user->isBlocked()) {
            throw new AccessException('Пользователь заблокирован..');
        }

        if (!$this->checkAccess($access, $options['widget'])) {
            throw new AccessException('Access deny.');
        }

        Core::setUser($_user);

        self::autoIncludedCss($_user->getName(), true);

        self::autoIncludedCss($_user->getName(), false, static::$widgetType);

        $this->widgetData = [
            'streamer' => $_user->getName(),
            'token' => $options['token']
        ];
    }

    /**
     * @param array $data
     * @param $user
     * @return array
     * @throws WidgetException
     * @throws Exception
     */
    private function getUserData(array $data, &$user): array
    {
        //информация о требуемом заголовке ответа
        if (($token = $data['token']) == null) {
            throw new WidgetException('Undefined token.', 400);
        }
        if (($widget = $data['widget']) == null) {
            throw new WidgetException('Undefined widget.', 400);
        }

        $access = DataBase::query()
            ->select('t1.*')
            ->from('api-token-access as t1')
            ->joinTable('api-token as t2')
            ->join('t1.apiUser', 't2.apiUser')
            ->where('t2.token', $token)
            ->all();

        if (count($access) == 0) {
            throw new WidgetException('Invalid token.');
        }

        $user = $access[0]['apiUser'];

        if (empty($user)) {
            throw new WidgetException('User does not exist');
        }

        return array_column($access, 'access');
    }

    /**
     * Проверяем лоступа пользователя к текущей команде.
     * @param array $accessList
     * @return bool
     */
    public static function checkAccess(array $accessList, string $widget): bool
    {
        $access = array_map('trim', $accessList);

        return (
            in_array('widget', $access, true) //смотрим общую группу
            OR in_array('widget.' . $widget, $access, true) //смотрим конкретный триггер
        );
    }

    /**
     * Подгружаем в рендер виджета автоматически стили
     * @param string $userName
     * @param bool $mode
     * @param string $style
     * @return array|null
     */
    public static function autoIncludedCss(string $userName = null, $mode = true, $style = ''): ?array
    {
        $result = [];

        if ($mode) {//режим автоподключения
            $dir = pathSeparatorF('%swidgets/%s/styles/autoload', RESOURCES_DIR, $userName);

            if (!is_dir($dir)) {
                return null;
            }

            if ($userName == null) {
                $userName = 'default';
            }

            //нужно найти базовый конфиг
            $directory = new RecursiveDirectoryIterator($dir);
            $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);

            foreach ($iterator as $widgetConfig) {
                if ($widgetConfig->getFilename() == '.' OR $widgetConfig->getFilename() == '..') //скипаем папки
                    continue;
                if (!$widgetConfig->isDir() AND (empty($style) OR $widgetConfig->getBasename() == $style)) {
                    //dump($widgetConfig->getPath(), $widgetConfig->getBasename('.css'));
                    Render::addPrivateCSS(sprintf("/Resources/widgets/%s/styles/autoload/%s", $userName, $widgetConfig->getBasename('.css')));
                }
            }

        } else { //режим подключения стиля виджета
            $file = pathSeparatorF('%swidgets/%s/styles/%s.css', RESOURCES_DIR, $userName, $style);
            if (file_exists($file)) {
                Render::addPrivateCSS(sprintf("/Resources/widgets/%s/styles/%s", $userName, $style));
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    public static function getWidgetService(): string
    {
        return static::$widgetService;
    }

    /**
     * @return string
     */
    public static function getWidgetType(): string
    {
        return static::$widgetType;
    }

    /**
     * @param string $label
     * @param array $data
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public function build(string $label, array $data = [])
    {
        $this->activeTemplateLabel = $label;

        $c = ComponentManager::createSiteComponent(
            'widget',
            $this->getWidgetData(),
            InjectComponentConfig::make('main', 'widget')
        );

        ComponentManager::createSiteComponent(
            'widget.' . $this->activeTemplateLabel,
            $this->getWidgetData(),
            InjectComponentConfig::make($c->getId(), 'widgetData')
        );

        if ($this->doJScript) {
            ComponentManager::createSiteComponent(
                'widget.jsBuilder',
                $this->getWidgetData(true),
                InjectComponentConfig::make($c->getId(), 'widgetJS')
            );
        }
    }

    /**
     * @param bool $js
     * @return array
     */
    protected function getWidgetData(bool $js = false): array
    {
        if ($js) {
            return array_merge($this->widgetData, [
                'widgetService' => static::$widgetService,
                'widgetType' => static::$widgetType,
                'uri' => Uri::getFullPathUrl(),
                'onShow' => 'widgetOnShow',
                'onData' => 'widgetOnData',
                'onConnect' => 'widgetOnConnect',
                'onConnectClose' => 'widgetOnConnectClose',
                'onEnd' => 'widgetOnEnd'
            ]);
        }
        return $this->widgetData;
    }

    /**
     *
     * @param string $label
     * @param array $data
     * @param InjectComponentConfig|null $ingect
     * @return IComponent
     * @throws SiteComponentException
     * @throws ReflectionException
     */
    public function setWidgetComponent(string $label, array $data = [], InjectComponentConfig $ingect = null): IComponent
    {
        return ComponentManager::createSiteComponent($label, $data, $ingect);
    }

    public function getTemplateLabel(): string
    {
        return $this->activeTemplateLabel;
    }
}
