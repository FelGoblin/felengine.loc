<?php

if (empty($dirname)) {
    die('warning! the system will self-destruct in 3..2..1..');
}

$start = microtime(true);

require($dirname . '/Engine/Core/Defines.php');
require ROOT_DIR . '/Ext/Dump.php';
require ENGINE_DIR . 'Autoloader.php';

use Engine\App;
use Engine\Autoloader;
use Engine\Core\Core;

register_shutdown_function("fatal_handler");

/**
 * @throws Exception
 */
function fatal_handler()
{
    $error = error_get_last();
    $user = Core::getThisUser();
    if ($error !== null && $user != null && $user->userAccessCheck('development')) {
        dump($error);
    }
}

try {
    //инициализариуем автозагрузчик классов
    Autoloader::Init();

    //Запускаем приложение
    (new App())->run();

    /*
    if (Core::getThisUser()->userAccessCheck('debug')) {
        dump('Время выполнения скрипта: ' . round(microtime(true) - $start, 4) . ' сек.');
    }
    */
    //ob_flush();
} catch (Exception $ex) {
    echo $ex->getMessage(); //TODO: Error Page!
}
