<?php

namespace Engine;

use Exception;
use Engine\Core\Core;

final class App
{
    /**
     * @var App|null
     */
    private static ?App $instance = null;
    /**
     * @var Core
     */
    private Core $core;
    /**
     * Ошибки в работе приложения
     * @var array
     */
    private array $error = [];

    /**
     * App constructor.
     */
    public function __construct()
    {
        /**
         * All Errors
         */
        function shutDownFunction()
        {
            $error = error_get_last();

            if ($error !== NULL) {
                /*
                $date = new DateTime("now");
                $str = $date->format("Y-m-d_H:i:s");
                $file = pathSeparator(ROOT_DIR . 'logs/shutDownFunction_exception_' . $str . '.log');
                $s = sprintf('%s;</br/>Файл: <b>%s</b>, линия: <b>%d</b>', $error['message'], $error['file'], $error['line']);
                file_put_contents($file, $s);

                if (!headers_sent()) {
                    header('HTTP/1.1 500 Internal Server Error');
                }

                ob_clean();

                */
                $user = Core::getThisUser();
                if ($user != null AND $user->userAccessCheck('debug')) {
                    dump($error);
                }
            }
        }

        //register
        register_shutdown_function('Engine\\shutDownFunction');

        class_alias('\\Engine\\Core\\Core', 'Core');

        $this->core = new Core();

        self::$instance = $this;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function run(): void
    {
        //запускаем в работу ядро
        $this->core->run();
    }

    /**
     * Добавляем ошибку
     * @param $error
     * @return void
     */
    public function setError($error): void
    {
        $this->error[] = $error;
    }
}