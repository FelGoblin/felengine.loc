<?php

use Engine\TemplatesLibrary\SiteLibraryElements\ParamValue;

$css = [
    'Default' => ['SI/table']
];
?>
<div class="content dark">
    <?php
    //dump(['private' => true], $this->configControllers);
    if ($paramValue ?? null !== null):
        /*
        Table::create(['test', 'test'], 'simple header-nowrap checkBox center');
        Table::addTextField('1');
        Table::addTextField('2');
        echo Table::display();
        */
        ParamValue::create();
        foreach ($paramValue as $value) {
            ParamValue::loadObject($value);
        }
        ParamValue::display();

        ?>
    <?php
    else:
        ?>
        <ul>
            <?php
            foreach ($links ?? [] as $_link):
                ?>
                <li><a href="<?= ($link ?? 'service-config') . $_link['id'] ?>"><?= $_link['title'] ?></a></li>
            <?php
            endforeach;
            ?>
        </ul>
    <?php
    endif;
    ?>
</div>