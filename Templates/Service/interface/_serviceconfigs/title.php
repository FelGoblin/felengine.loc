<?php

use Engine\TemplatesLibrary\SiteLibraryElements\Breadcrumb;

?>
<?= Breadcrumb::build(['links' => $paths ?? []]) ?>