<?php

use Engine\TemplatesLibrary\SiteLibraryElements\Button;
use Engine\TemplatesLibrary\SiteLibraryElements\OutPut;
use Engine\TemplatesLibrary\SiteLibraryElements\Table;
use Engine\TemplatesLibrary\SiteLibraryElements\Textarea;

$css = [
    'Default' => [
        'SI/table'
    ]
];

$js = Template::get() . 'resources/js/apisandbox.js';

?>
<div class="content dark padding">
    <?php
    Table::Create(['Параметр', 'Значение'], ['class' => 'nowrap inspected']);
    // -- row --
    Table::addTextField('URI:');
    Table::addTextField(SERVER_URI . 'Api');
    // -- row --
    Table::addTextField('Auth Hash:');
    Table::addInputTextField([
        'value' => $authHash,
        'class' => 'w-max',
        'id' => 'authHash'
    ]);
    // -- row --
    Table::addTextField('API Token:');
    Table::addInputTextField([
        'value' => $apiToken,
        'class' => 'w-max',
        'id' => 'apiToken'
    ]);
    // -- row --
    Table::addTextField('Module:');
    Table::addInputTextField([
        'value' => 'events',
        'class' => 'w-max',
        'id' => 'apiModule',
        'name' => 'events'
    ]);
    // -- row --
    Table::addTextField('Trigger:');
    Table::addInputTextField([
        'value' => 'auth',
        'class' => 'w-max',
        'id' => 'apiTrigger',
        'name' => 'trigger'
    ]);
    // -- row --
    Table::addTextField('Дополнительные параметры:');
    $field = Table::makeCheckBox([
        'label' => 'JSON Ответ API',
        'id' => 'apiJSON',
        'onchange' => 'console.log(\'Change\', this);'
    ]);
    $field .= '<br />';
    $field .= Table::makeCheckBox([
        'label' => 'Dump вывод',
        'id' => 'apiDoDump',
        'checked' => true,
        'onchange' => 'console.log(\'Change\', this);'
    ]);
    Table::addField($field);
    // -- row --
    Table::addTextField('Доп. JSON данные:');
    Table::addField(Textarea::build([
        'header' => '<icon class="object-group"></icon>JSON',
        'id' => 'JSON',
        'name' => 'json',
    ]));
    // -- row --
    $SERVER_URI = SERVER_URI;
    Table::addFooter(Button::build([
        'tag' => 'button',
        'class' => 'w-long access left',
        'onClick' => "sendJSON('$SERVER_URI',this)",
        'text' => 'Выполнить',
        'icon' => 'external-link'
    ]));

    Table::display();
    ?><br/><?php
    OutPut::display([
        'class' => 'w-max',
        'id' => 'outData',
        'header' => 'Вывод',
        'icon' => 'terminal'
    ]);
    ?>
</div>