<?php
$css = [
    'Default' => [
        'SI/paramvalue',
        'SI/table'
    ],
    'SI/api_sand_box'
];

?>
<div class="content dark padding">
    <?php

    use Engine\TemplatesLibrary\SiteLibraryElements\Table;

    Table::create([
        'Параметр',
        'Значение'
    ], 'inspected simple header-nowrap');

    Table::addTextField('URI:');
    Table::addTextField(SERVER_URI . 'Api');

    Table::addTextField('Auth Hash:');
    Table::addTextField($authHash ?? '_error_auth_hash_');

    Table::addTextField('API Token:');
    Table::addTextField($apiToken ?? '_error_api_token_');

    Table::addTextField('Module:');
    Table::addAutoField('Module');

    Table::addTextField('Trigger:');
    Table::addAutoField('');

    Table::addTextField('Дополнительные параметры:');
    $field = Table::makeCheckBox([
        'label' => 'JSON Ответ API',
        'onchange' => 'console.log(\'Change\', this);'
    ]);
    $field .= '<br />';
    $field .= Table::makeCheckBox([
        'label' => 'Dump вывод',
        'checked' => true,
        'onchange' => 'console.log(\'Change\', this);'
    ]);
    Table::addField($field);

    Table::addTextField('JSON запрос:');
    $SERVER_URI = SERVER_URI;
    $field = <<<HTML
        <div class="sendMessage">
            <div class="sendHeader"><i class="fel-message"></i> JSON</div>
            <div class="sendBody" style="padding:initial;">
                <textarea id="JSON"></textarea>
            </div>
        </div>
HTML;

    Table::addField($field);
    $field = <<<HTML
        <button type="button" onclick="sendJSON('$SERVER_URI',this)"><i class="fel-external-link"></i>
            Отправить
        </button>
HTML;

    Table::addFooter($field);

    Table::display();
    ?>
    <div class="sendMessage">
        <div class="sendHeader"><i class="fel-terminal"></i> Вывод</div>
        <div class="outData" id="outData"></div>
    </div>
</div>