function outData(data, el) {
    El('#outData').html(data);
    el.attrDel('disabled');
}

window.addEventListener('beforeunload', e => e.returnValue = 'Вы действительно хотите закрыть эту вкладку?')

function sendJSON(uri, el) {
    el = El(el);
    try {
        el.attr({disabled: 'disable'});
        El('#outData').html();
        let
            token = El('#apiToken').value(),
            module = El('#apiModule').value(),
            trigger = El('#apiTrigger').value(),
            data = {
                responseFormat: El('#apiJSON').checked() ? 'JSON' : 's',
                doDump: El('#apiDoDump').checked()
            },
            json = El('#JSON').value();
        if (json !== '') {
            var
                sData = JSON.parse(json),
                tosend = Object.assign(data, sData);
        } else {
            tosend = data;
        }


        //console.log(tosend, json);

        let
            _uri = uri + '/api/' + token + '/' + module + '/' + trigger,
            query = _uri + ':' + JSON.stringify(tosend);
        El('#apiQuery').html(query);

        Ajax({
            url: _uri,
            data: tosend,
            format: 'POST',
            sendJSON: true,
            getFormat: 'string',
            onReady: function (data) {
                /*Notify.Success('API запрос  обработан.');*/
                //console.log(data);

                outData(data, el);
            },
            onError: function (error) {
                /*Notify.Error(error);*/
                outData(error, el);
            }
        });

    } catch (objError) {
        if (objError instanceof SyntaxError) {
            console.error(objError.name);
            //Notify.Error('JSON ERROR: ' + objError.toString());
            outData(objError.toString(), el);
            el.attrDel('disabled');
        } else {
            console.error(objError.message);
            //Notify.Error(objError.message);
            outData(objError.message, el);
            el.attrDel('disabled');
        }
    }
}