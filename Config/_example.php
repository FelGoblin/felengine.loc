<?php
/**
 * Пример подгрузки конфига в другое окружение.
 * По идее система загрузит это файл в окружение конфига _example
 * Но переменной $groupAlias мы переопределяем окружение. И теперь этот параметр вызывается как:
 *      SiteConfig::get('exampleParam') --Указание без группы main по-умолчанию
 *      SiteConfig::get('exampleParam', 'main')  --Указание с группой
 *      SiteConfig::get('exampleParam', 'main', @defaultValue) -- Указание с значением по-умолчанию, если пароаметр не определён (группа обязательна)
 */
$groupAlias = 'main';

return [
    'exampleParam' => 'this_example_config_param'
];