<?php
/**
 * Это по факту конфиг main по-умолчанию. Все данные будут перезаписаны в дальнейшем<b>  из main.php
 */
return [
    'close' => true,
    'closemessage' => 'Engine has no implementation',
    //дебаг вывод включен
    'debuginfo' => true
];