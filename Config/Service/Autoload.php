<?php
/**
 * Автолоадер
 */
return [
    //Неймспейсы, они же папки в которых будет производиться поиск
    'namespaces' => [
        'Modules\\',
        'Engine\\',
        'Namespaces\\',
        'Object\\',
        'Widgets\\',
    ],
    //Автоподключаемые файлы.
    'files' => [
        'Engine/Helper/UriHelper.php',
        'Engine/Helper/Functions.php',
        'Engine/Helper/Cookie.php'
    ]
];