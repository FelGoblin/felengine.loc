<?php
/**
 * Системные классы.
 */
return [
    'site' => [ //работа внутри сайта
        'core' => [ //сервисы ядра
            //сессии
            Engine\Service\SessionService::class,

            //конфиги
            Engine\Service\ConfigService::class,

            //Локализация
            Engine\Service\LangService::class,

            //коннект к БД
            Engine\Service\DatabaseService::class,

            //Подготавливаем и инициилизируем входящие данные
            Engine\Service\InputService::class,

            //обрабатываем URI
            Engine\Service\UriService::class,

            //Загружаем все модули, требующие загрузки в память;
            Engine\Service\ModulesService::class,

            //Пытаемся восстановить сессию пользователя, иначе пользователь остаётся в гостевом режиме.
            Engine\Service\UserService::class,

            //Роутер
            Engine\Service\RouterService::class,

            //библиотека компонентов
            Engine\Service\LibraryService::class,

        ],
        'service' => [ //модули, которые отрабатывают как сервисные
            //системный модуль
            //Engine\Core\SystemModule\Service::class,
        ],
        'postInit' => [ //по сути пост обработка модулей (после работы контроллера)
            //системный модуль
            //Engine\Core\SystemModule\SystemModule::class,
        ]
    ],
    'api' => [ //запускаемые сервисы для апи
        'core' => [//сервисы ядра
            //конфиги
            Engine\Service\ConfigService::class,

            //коннект к БД
            Engine\Service\DatabaseService::class,

            //Подготавливаем и инициилизируем входящие данные
            Engine\Service\InputService::class,

            //обрабатываем URI
            Engine\Service\UriService::class,

            //Загружаем все вещи/бафы/скилы в память;
            Engine\Service\ModulesService::class,

            //Роутер
            //Engine\Service\RouterService::class,
        ],
        'service' => [//инициализируем модули, которы необходимо инициализировать до работы контроллеров (например базы вещей)
            //Example
            //Modules\Example\Service::class
        ],
        'postInit' => [ //по сути пост обработка модулей (после работы контроллера)
            //Example
            //Modules\Example\Example::class
        ]
    ],
    'cron' => [

        'core' => [//сервисы ядра
            //конфиги
            Engine\Service\ConfigService::class,

            //коннект к БД
            Engine\Service\DatabaseService::class,

            //Загружаем все вещи/бафы/скилы в память;
            Engine\Service\ModulesService::class
        ],
        'service' => [//инициализируем модули, которы необходимо инициализировать до работы контроллеров (например базы вещей)
            //Example
            //Modules\Example\Service::class
        ],
        'postInit' => [ //по сути пост обработка модулей (после работы контроллера)
            //Example
            //Modules\Example\Example::class
        ]
    ]
];